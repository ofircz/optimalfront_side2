# encoding=utf8

# libs
import os
import sys
import webapp2
import json
import datetime
from google.appengine.ext import ndb

from models import User, EnteredData, PersistentData
import views_classes.Users as Users
import views_classes.General as General
# import views_classes.QPX as QPX
import views_classes.TBS as TBS
import views_classes.Client as Client
#
#
from api.FlightsArea import Itinerary_Keys, Itinerary_Filters
from airports_main import AP_DBs_main

import logging


class Saved_Results():
    """ An object to save all results from queries. This object will be available all over the code,
        so different functions could use it to store and restore results per user. Later on, we could
        extend this class to save results in database in the background, in case we want to deal with
        them at later time and to save memory. For now, everything is in one big dict in the memory. """
    def __init__(self):
        self.data = {}

    def get_filtered_sorted_itineraries(self, username):
        itineraries = self.get_itineraries(username)
        logging.info(" Len of itineraries: %s" %len(itineraries))
        # if username in self.data.keys():
        #     if 'itineraries' in self.data[username].keys():
        #         itineraries = self.data[username]['itineraries']
        # if not itineraries:
        #     logging.warning("No results in memeory, retrieving from DB")
        #     from google.appengine.ext import ndb
        #     from models import Itineraries
        #     import json
        #     # get from database if exist
        #     ancestor_key = ndb.Key("TBS_Results", username)
        #     data = Itineraries.query_data(ancestor_key)
        #     for i, element in enumerate(data, 1):
        #         itineraries.append(json.loads(element.itinerary))
        #     self.data[username] = {'itineraries': itineraries}
        #itineraries_after_cut = self.cut_itineraries(itineraries, username)
        sorting_data = self.get_sorting_data(username)
        logging.info('Sorting data: %s' % sorting_data)
        if not sorting_data:
            sorting_data = {'name':'price', 'reverse': False}
            self.put_sorting_data(username,sorting_data)
            logging.info('No sorting data. Using: %s' % sorting_data)
        key = eval('Itinerary_Keys.%s' % sorting_data['name'])
        price_key = eval('Itinerary_Keys.%s' % 'price')
        # first sort by price, then by other key, so every sort will have price as secondary key.
        sorted_list = sorted(sorted(itineraries, key=price_key),key=key, reverse=sorting_data['reverse'])
        len_list = len(sorted_list)
        logging.info(" Len of sorted_list: %s" %  len_list)
        filter_data = self.get_filters_data(username)
        logging.info('Filters Data: %s' % filter_data)
        filtered_list = filter(lambda x: Itinerary_Filters.combined_filters(filter_data, x),sorted_list)
        len_filtered_list = len(filtered_list)
        logging.info('Len of filtered_list: %s' % len_filtered_list)
        limit = self.get_display_limit(username)
        if not limit:
            limit = 200
            self.put_display_limit(username,limit)
        logging.info("Display limit: %s" % limit)
        return {'len': len_list, 'len_filtered': len_filtered_list, 'itineraries': filtered_list[0:limit]}


    def get_itineraries(self, username):
        itineraries = []
        if username in self.data.keys():
            if 'itineraries' in self.data[username].keys():
                itineraries = self.data[username]['itineraries']
        if not itineraries:
            itineraries = []
            logging.info("No results in memeory, retrieving from DB")
            # from google.appengine.ext import ndb
            # from models import Itineraries
            # import json
            # # get from database if exist
            # ancestor_key = ndb.Key("PersistentData", username)
            # for element in PersistentData.query_data(ancestor_key):
            #     persistent_data = json.loads(element.data)
            #     logging.info('Restored from DB: %s' % persistent_data)
            #     self.data[username] = persistent_data
            # ancestor_key = ndb.Key("TBS_Results", username)
            # data = Itineraries.query_data(ancestor_key)
            # print data
            # for i, element in enumerate(data, 1):
            #     itineraries.append(json.loads(element.itinerary))
            # self.data[username]['itineraries'] = itineraries
        return itineraries

    def get_passengers(self, username):
        if username in self.data.keys():
            if 'passengers' in self.data[username].keys():
                return self.data[username]['passengers']

    def get_hotels(self, username):
        hotels = []
        if username in self.data.keys():
            if 'hotels' in self.data[username].keys():
                hotels = self.data[username]['hotels']
            if not hotels:
                hotels = []
                logging.info("No results in memeory")

        logging.info("----------hotels in get_hotels -----------")
        logging.info(hotels)
        return hotels


    def get_all_users(self):
        return self.data.keys()

    def get_user_input(self, username):
        if username in self.data.keys():
            if 'user_input' in self.data[username].keys():
                return self.data[username]['user_input']

    def put_itineraries(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['itineraries'] =  data
        self.data[username]['last_updated'] = datetime.datetime.now()

    def get_last_update(self, username):
        if username in self.data.keys():
            if 'last_updated' in self.data[username].keys():
                return self.data[username]['last_updated']

    def put_hotels(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}



        self.data[username]['hotels'] = data


    def clear_progress(self, username):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['progress'] = {'data':[], 'stop': False}

    def add_progress(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['progress']['data'].append(data)

    def stop_progress(self, username):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['progress']['stop'] = True


    def get_progress(self, username):
        if username in self.data.keys():
            if 'progress' in self.data[username].keys():
                data = self.data[username]['progress']['data']
                return {'data': data, 'stop':self.data[username]['progress']['stop']}

    def put_passengers(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['passengers'] = data

    def get_filters_data(self, username):
        if username in self.data.keys():
            if 'filters_data' in self.data[username].keys():
                return self.data[username]['filters_data']

    def get_hotel_filters_data(self, username):
        if username in self.data.keys():
            if 'hotel_filters_data' in self.data[username].keys():
                return self.data[username]['hotel_filters_data']

    def put_sorting_data(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['sorting_data'] = data

    def get_sorting_data(self, username):
        if username in self.data.keys():
            if 'sorting_data' in self.data[username].keys():
                return self.data[username]['sorting_data']

    def put_display_limit(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['display_limit'] = data

    def get_display_limit(self, username):
        if username in self.data.keys():
            if 'display_limit' in self.data[username].keys():
                return self.data[username]['display_limit']

    def put_server_filters_limits(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['server_filters_limits'] = data

    def get_server_filter_limit(self, username):
        if username in self.data.keys():
            if 'server_filters_limits' in self.data[username].keys():
                return self.data[username]['server_filters_limits']

    def put_filters_data(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['filters_data'] = data

    def put_hotel_filters_data(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['hotel_filters_data'] = data


    def put_user_input(self, username, data):
        if not username in self.data.keys():
            self.data[username] = {}
        self.data[username]['user_input'] = data
        EnteredData(parent=ndb.Key("Entered Data", username), enteredData=json.dumps(data)).put() # save in DB


    def get_user_input_history(self, username):
        key = ndb.Key("Entered Data", username)
        history_list = []
        history = EnteredData.query_data(key)
        for search in history:
            logging.info(search)
            history_list.append(json.loads(search.enteredData))
        return history_list


    def cut_itineraries(self, itineraries, username):
        from api.FlightsArea import Itinerary_Filters
        new_itineraries = []
        max_stops = self.data[username]['user_input']['list_legs'][0]['max_stop']
        logging.info("Received max_stop from client: %s" % max_stops)
        for itinerary in itineraries:
            logging.info("Itinerary has %s legs" % len(itinerary['legs']))
            if Itinerary_Filters.max_stops(itinerary, max_stops):
                new_itineraries.append(json.loads(itinerary))
        return new_itineraries


api_results = Saved_Results()

config = {
    'webapp2_extras.auth': {
        'user_model': User,
        # the attributes below will be available during the session without query the database:
        'user_attributes': ['name', 'company', 'admin', 'auth_ids',]
    },
    # I'm not sure what is this key, will investigate later (Uri).
    'webapp2_extras.sessions': {
        'secret_key': 'YOUR_SECRET_KEY'
    },
    'api_results':api_results
}


app = webapp2.WSGIApplication([
    # Users
    webapp2.Route('/', General.MainHandler, name='home'),
    webapp2.Route('/signup', Users.SignupHandler),
    webapp2.Route('/signup_angular', Users.SignupHandlerAngular),
    webapp2.Route('/adminsignup_angular', Users.AdminSignupHandlerAngular),
    webapp2.Route('/adminsignup', Users.AdminSignupHandler),
    webapp2.Route(r'/<verification_type:signup|signup_angular|reset>/<user_id:\d+>/<token:.+>',
                  Users.VerificationHandler, name='verification'),
    webapp2.Route('/reset', Users.ResetPasswordHandler),
    webapp2.Route('/login_angular', Users.LoginHandlerAngular, name='login'),
    webapp2.Route('/login', Users.LoginHandler, name='login'),
    webapp2.Route('/logout', Users.LogoutHandler),
    webapp2.Route('/forgot', Users.ForgotPasswordHandler),
    webapp2.Route('/authenticated', Users.AuthenticatedHandler),
    webapp2.Route('/ListCompanyUsers', Users.ListCompanyUsers),
    webapp2.Route(r'/GetUserByID/<id:.+>', Users.GetUserByID),
    webapp2.Route('/UpdateUser', Users.UpdateUser),
    # General, not realted to specific API
    webapp2.Route('/DisplayUserInput', General.DisplayUserInput),
    webapp2.Route('/DisplayDataForAPIGenerator', General.DisplayDataForAPIGenerator),
    webapp2.Route('/AddCalculationsOnResults',General.AddCalculationsOnResults),
    # views related to the TBS API
    webapp2.Route('/SaveResultsToDisk', TBS.SaveResultsToDisk),
    webapp2.Route('/DisplayTBSQueries', TBS.DisplayTBSQueries),
    webapp2.Route('/DisplayTBSResults',TBS.QueryTBS),
    webapp2.Route('/progress', TBS.ShowProgress),
    webapp2.Route('/stop_progress', TBS.StopProgress),

    # webapp2.Route('/RestoreTBSResults',TBS.RestoreTBSResults),
    # webapp2.Route('/DebugRestoreTBSResults', TBS.DebugRestoreTBSResults),
    webapp2.Route('/ShowTBSResults',TBS.ShowTBSResults),
    webapp2.Route(r'/DisplayBookings/<id:.+>', TBS.DisplayBookings),
    webapp2.Route(r'/CancleBookings/<id:.+>', TBS.CancelBookings),
    webapp2.Route('/BookItineraries',TBS.BookItineraries),
    webapp2.Route('/ShowBookingXML',TBS.ShowBookingXML),
    webapp2.Route('/ShowCompanyBookings',General.ShowCompanyBookings),
    webapp2.Route('/ShowCompanyActiveBookings',General.ShowCompanyActiveBookings),
    webapp2.Route('/ShowAllActiveBookings',General.ShowAllActiveBookings),
    webapp2.Route('/HotelSearch', TBS.HotelSearch),
    webapp2.Route('/clear_memory', General.ClearMemory),
    webapp2.Route('/ShowSearchHistory', General.ShowSearchHistory),

    # # views related to the QPX API
    # webapp2.Route('/DisplayQPXQueries', QPX.DisplayQPXQueries),
    # webapp2.Route('/DisplayQPXResults',QPX.DisplayQPXResults),
    # webapp2.Route('/RestoreQPXResults',QPX.RestoreQPXResults),
    # webapp2.Route('/ConvertQPXResults', QPX.ConvertQPXResults),
    # views to serve POST calls from the client
    webapp2.Route('/flightFormAtt', Client.FlightFormAttHandler),
    webapp2.Route('/finalOrder', Client.FinalOrder),
    webapp2.Route('/roundTripFlightForm', Client.roundTripFlightForm),
    webapp2.Route('/IsLoggedIn', Client.IsLoggedIn),
    webapp2.Route('/UpdateFiltersData', Client.UpdateFilteredData),
    # webapp2.Route('/APFirstDB',AP_DBs_main.FirstAirportDBByAmadeus),
    webapp2.Route('/APCodeAC', Client.APcodeAutocomplete),
    webapp2.Route('/APCodeACdet', Client.APcodeAutocompleteDet),
    webapp2.Route('/ApNearbyOpt', Client.ApNearbyOptions),
    webapp2.Route('/NearbyByAddress', Client.NearbyByAddress),
    # views for test
    webapp2.Route('/testAC', AP_DBs_main.testAPautocomplete),
    webapp2.Route('/testACdet', AP_DBs_main.testAPautocomplete_details),
    webapp2.Route('/createAPlocationsDB', AP_DBs_main.AirportDBByAmadeusOnlyWithLOcations),
    webapp2.Route('/checkDoubles', AP_DBs_main.check_doubles_ap_db_locations),
    webapp2.Route('/rmDoubles', AP_DBs_main.remove_doubles_ap_db_locations),
    webapp2.Route('/createComb400', AP_DBs_main.CreateApDbDistanceCombTill400km),
    webapp2.Route('/testAPnearby', AP_DBs_main.testAPnearby),
    webapp2.Route('/TestNearbyByAddress', AP_DBs_main.TestNearbyByAddress),
    webapp2.Route('/addTimezoneToAllAirports', AP_DBs_main.add_timezone_to_all_airports),

], debug=True, config=config)

# from google.appengine.api.runtime import memory_usage
# logging.warn("Memory usage:  %d MB" % (memory_usage().current()))
