# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Summary of set up
##### File Structure
1. API - API calls, file for each site
2. csv - CSV formatted static Db files
3. lib - external needed python libraries
4. models - the Db, each class represent a table
5. parsers - contains needed parsers
6. site_parsers - specific site's parsing methods
7. templates - frontend html templates
8. web - @nadavmor123 's Web folder
9. xls - excel static Db files

DAL.py - dal layer

app.yaml - routing and server managing file

cron.yaml - creating timed automatic jobs

views.py  - REST API calls handler (the actual server)

 
#### Configuration

* when creating a rest call handler make sure the desired handler (class) has a callback inside 
```
#!python
app = webapp2.WSGIApplication([
    ('/', Main),
    ('/summary', TripSummary),
    ('/createdb', CreateBb),
    ('/csv', csvParser),
    ('/apitest', apiTester),
    ('/upload', FileUploadFormHandler),
    ('/upload_file', FileUploadHandler),
    ('/view_file/([^/]+)?', ViewFileHandler),
    ('/updateflight', UpdateFlight),
    ('/tripAdvisor', TripAdvisor)

], debug=True)

```
* make sure each folder has __init__.py in it to make importing it optional
* make sure you know what are you doing before touching the app.yaml file - any mistake can cause the frontend crash, and you dont want @nadavmor123 yelling at you :)

* Dependencies

GAE doesnt support most of the Python libs, thus you need to use them internally.
To do this, download the desired lib, and put it inside lib folder.
To import the lib in the code you can write:

```
#!python

from lib import <lib_name>
```

#### Database configuration

* make all dB tables in models folder
* each calls represent a table
* each inner parameter represent row name
* all actions on the Db need to be done in DAL.py (keep good coding convention)
* UserFiles - Blob dB - keeps FILES (uploaded in views.py)
#### Deployment instructions
read GAE guide
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact