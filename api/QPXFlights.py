__author__ = 'Ofir & Elad'

from lib import requests
import urllib2
import json
from api.Amadeus import Amadeus
from api.google_maps import GoogleMaps
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(120)

amadeus = Amadeus
google_maps = GoogleMaps

#google apis, type:server - https://console.cloud.google.com/apis/library?project=ultra-mediator-129910
# enable apis on 4/5/16: Google Maps Directions API , Google Maps Distance Matrix API , Google Maps Elevation API  , Google Maps Geocoding API , Google Maps Geolocation API , Google Maps JavaScript API , Google Places API Web Service , QPX Express Airfare API
api_key1 = 'AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04'
api_key4 = 'AIzaSyCS8PTaJ7f2VEJWzk-qvLhaYPH3KHeQVh0'
api_key3 = 'AIzaSyCC1Ne42exQs66bT4euID-anESedD2vhts'  #user:ofircz
api_key2 = 'AIzaSyAXaQgGhIhIc3vahqaSbt8mBSJp_JNQIu8'  #user:ofircz
api_qpx=api_key2


class QpxExpress:


    # send 1 query to QPX API
    #QPX reference: https://developers.google.com/qpx-express/v1/trips/search#request-body
    @staticmethod
    def qpx_send_query(list_legs, dict_details, round_trip=False, return_date=None):

        print_debug = 1
        if (print_debug):
            print '--------def qpx_send_query(list_legs, dict_details, round_trip=False, return_date=None)------'
            print 'def qpx_flights- list_legs'
            print list_legs
            print 'def qpx_flights- dict_details'
            print dict_details

        # arrange the data in the QPX format
        Query_params_data_QPX = qpx_create_one_query_input(list_legs, dict_details)
        url = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=' + api_qpx
        headers = {'Content-Type': 'application/json'}
        if (print_debug):
            print "Query_params_data_QPX"
            print Query_params_data_QPX
        # urllib2
        req = urllib2.Request(url, Query_params_data_QPX, headers)
        u = urllib2.urlopen(req)
        read_url = u.read()
        QPX_query_result = json.loads(read_url)

        if (print_debug):
            print 'def qpx_send_query - QPX_query_result'
            print QPX_query_result

        return QPX_query_result


    # get the final input for 1 query and arrange it for the query
    @staticmethod
    def qpx_create_one_query_input(list_legs, dict_details, round_trip=False, return_date=None):

        print_debug = 1
        if return_date is None:
            return_date = ''
        legs = []

        for leg in list_legs:
            legs.append({
                "kind": "qpxexpress#sliceInput",
                "origin": leg['origin'],
                "destination": leg['destination'],
                "date": leg['date'],
                "maxStops": leg['max_stop'],
                "maxConnectionDuration": leg['max_connection_duration'],
                "preferredCabin": leg['cabin'],
                "permittedDepartureTime": {
                    "kind": "qpxexpress#timeOfDayRange",
                    "earliestTime": leg['earliest_time'],
                    "latestTime": leg['latest_time']
                }
            })

        if round_trip:
            legs.append({
                "kind": "qpxexpress#sliceInput",
                "origin": list_legs[-1]['destination'],  # Last element
                "destination": list_legs[0]['origin'],
                "date": return_date,
                "maxStops": list_legs[0]['max_stop'],
                "maxConnectionDuration": list_legs[0]['max_connection_duration'],
                "preferredCabin": list_legs[0]['cabin'],
                "permittedDepartureTime": {
                    "kind": "qpxexpress#timeOfDayRange",
                    "earliestTime": list_legs[0]['earliest_time'],
                    "latestTime": list_legs[0]['latest_time']
                }
            })

        params = {"request": {
            "passengers": {
                "kind": "qpxexpress#passengerCounts",
                "adultCount": dict_details['adult'],
                "childCount": dict_details['child'],
                "infantInLapCount": dict_details['infant_in_lap'],
                "infantInSeatCount": dict_details['infant_in_seat'],
                "seniorCount": dict_details['senior']
            },
            "slice": legs,
            "maxPrice": dict_details['max_price'],
            "refundable": dict_details['refundable'],
            "solutions": dict_details['solutions']
        }
        }

        data_for_QPX_query = json.dumps(params)

        if (print_debug):
            print 'def qpx_create_one_query_input: data_for_QPX_query'
            print data_for_QPX_query

        return data_for_QPX_query





















    @staticmethod
    def qpx_fix_json(flights, vector_distance):
        print '****************class QpxExpress**************'
        print '--------qpx_fix_json(flights, vector_distance)------'
        print 'qpx_fix_json- flights'
        print flights
        print 'qpx_fix_json- vector_distance'
        print vector_distance
        if 'tripOption' not in flights['trips']:
            print "qpx_fix_json - Error Flights -tripOption not in flights[trips]"
            return []
        airport_data = []
        carrier_data = []
        print "flights[trips][data][airport]"
        print flights['trips']['data']['airport']
        for airports in flights['trips']['data']['airport']:
            airport_data.append({
                'code': airports['code'],
                'name': airports['name']
            })

        for carriers in flights['trips']['data']['carrier']:
            carrier_data.append({
                'code': carriers['code'],
                'name': carriers['name']
            })

        new_flights = flights
        del new_flights['kind']
        del new_flights['trips']['requestId']
        del new_flights['trips']['data']
        del new_flights['trips']['kind']
        for option in new_flights['trips']['tripOption']:
            del option['id']
            del option['kind']
            option['saleTotalQpx'] = option.pop('saleTotal')
            option.update({
                'saleTotalAmadeus': ''
            })
            option['legs'] = option.pop('slice')
            count_legs = 0
            for legs in option['legs']:
                del legs['kind']
                legs['part'] = legs.pop('segment')
                count_parts = 0
                legs.update({  # Adding new Data to json
                    'originAddress': '',
                    'distanceToAirport': '',
                    'timeToAirport': '',
                    'destinationAddress': '',
                    'distanceFromAirport': '',
                    'timeFromAirport': ''
                })
                for parts in legs['part']:
                    del parts['marriedSegmentGroup']
                    del parts['id']
                    del parts['kind']
                    del parts['bookingCodeCount']
                    parts['flight'].update({
                        'name': ''
                    })
                    parts['leg_details'] = parts.pop('leg')
                    for details in parts['leg_details']:
                        del details['id']
                        del details['kind']
                        del details['duration']
                        details.update({
                            'destination_airport': '',
                            'origin_airport': ''
                        })
                    count_parts += 1
                    print 'qpx_fix_json- for vector in vector_distance:'
                    print 'qpx_fix_json - vector_distance'
                    print vector_distance
                    for vector in vector_distance:  # Adding distance vector to json
                        for distance in vector:
                            print 'for distance in vector'
                            print 'distance'
                            print distance
                            print 'vector'
                            print vector
                            if parts['leg_details'][0]['origin'] == distance['airport']:
                                legs['originAddress'] = distance['origin_address']
                                legs['timeToAirport'] = distance['duration']['text']
                                legs['distanceToAirport'] = distance['distance']['text']
                            if parts['leg_details'][0]['destination'] == distance['airport']:
                                legs['destinationAddress'] = distance['origin_address']
                                legs['timeFromAirport'] = distance['duration']['text']
                                legs['distanceFromAirport'] = distance['distance']['text']
                legs.update({
                    'stops': (count_parts-1),
                    'durationWithDrive': legs['duration']+int(legs['timeFromAirport'].split(' ', 1)[0])+int(legs['timeToAirport'].split(' ', 1)[0])
                })

                count_legs += 1
            option.update({
                'number_of_legs': count_legs,
                'passengersDetails': option['pricing'][0]['passengers']
            })
            del option['passengersDetails']['kind']
        print "qpx_fix_json - before QpxExpress.fix_airport_data(new_flights, airport_data, carrier_data)"
        print "new_flights"
        print new_flights
        print "airport_data"
        print airport_data
        print "carrier_data"
        print carrier_data
        QpxExpress.fix_airport_data(new_flights, airport_data, carrier_data)
        print "qpx_fix_json - after QpxExpress.fix_airport_data(new_flights, airport_data, carrier_data)"
        print "new_flights"
        print new_flights
        return new_flights

    @staticmethod
    def fix_airport_data(new_flights, airport_data, carrier_data):
        print '****************class QpxExpress**************'
        print 'def fix_airport_data(new_flights, airport_data, carrier_data)'
        print "new_flights"
        print new_flights
        print 'airport_data'
        print airport_data
        print 'carrier_data'
        print carrier_data
        for flights in new_flights['trips']['tripOption']:
            for legs in flights['legs']:
                for parts in legs['part']:
                    for a_data in airport_data:
                        if parts['leg_details'][0]['destination'] == a_data['code']:
                            parts['leg_details'][0]['destination_airport'] = a_data['name']
                        elif parts['leg_details'][0]['origin'] == a_data['code']:
                            parts['leg_details'][0]['origin_airport'] = a_data['name']

                    for c_data in carrier_data:
                        if parts['flight']['carrier'] == c_data['code']:
                            parts['flight']['name'] = c_data['name']

    '''
    1-getting list of address legs
    2-find for each address nearest relevant airports
    3-measure time and distance from each airport to origin address and add to json
    4-matrix query from all relevant airports
    '''
    #  TODO complete fill data to json
    @staticmethod
    def qpx_wrapping(list_legs, dict_details, round_trip=False, return_date=''):
        print "--------def qpx_wrapping(list_legs, dict_details, round_trip=False, return_date='')---------"
        vector_distance_origin = []
        vector_distance_destination = []
        print "def qpx_wrapping - list_legs"
        print list_legs
        #Ofir - new
        origin_relevant_airports = QpxExpress.get_origin_relevant_airports(list_legs)  # Relevant airports
        destination_relevant_airports = QpxExpress.get_destination_relevant_airports(list_legs)  # Relevant airports
        # relevant_airports = [[{u'city_name': u'London', u'airport_name': u'London Heathrow Airport', u'airport': u'LHR', u'aircraft_movements': 648214, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 23.335168202039316, u'location': {u'latitude': 51.4775, u'longitude': -0.461389}}, {u'city_name': u'London', u'airport_name': u'London Gatwick Airport', u'airport': u'LGW', u'aircraft_movements': 234279, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 39.21520554224877, u'location': {u'latitude': 51.15609, u'longitude': -0.17818}}, {u'city_name': u'London', u'airport_name': u'London City Airport', u'airport': u'LCY', u'aircraft_movements': 83703, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 12.670330640206116, u'location': {u'latitude': 51.50528, u'longitude': 0.05528}}], [{u'city_name': u'Berlin', u'airport_name': u'Berlin Tegel Airport', u'airport': u'TXL', u'aircraft_movements': 196581, u'city': u'BER', u'timezone': u'Europe/Berlin', u'distance': 9.074304149094248, u'location': {u'latitude': 52.55969, u'longitude': 13.28771}}], [{u'city_name': u'Amsterdam', u'airport_name': u'Amsterdam Airport Schiphol', u'airport': u'AMS', u'aircraft_movements': 570544, u'city': u'AMS', u'timezone': u'Europe/Amsterdam', u'distance': 11.444460314515048, u'location': {u'latitude': 52.3103, u'longitude': 4.76028}}], [{u'city_name': u'London', u'airport_name': u'London Heathrow Airport', u'airport': u'LHR', u'aircraft_movements': 648214, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 23.335168202039316, u'location': {u'latitude': 51.4775, u'longitude': -0.461389}}, {u'city_name': u'London', u'airport_name': u'London Gatwick Airport', u'airport': u'LGW', u'aircraft_movements': 234279, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 39.21520554224877, u'location': {u'latitude': 51.15609, u'longitude': -0.17818}}, {u'city_name': u'London', u'airport_name': u'London City Airport', u'airport': u'LCY', u'aircraft_movements': 83703, u'city': u'LON', u'timezone': u'Europe/London', u'distance': 12.670330640206116, u'location': {u'latitude': 51.50528, u'longitude': 0.05528}}], [{u'city_name': u'Milano', u'airport_name': u'Milano Linate Airport', u'airport': u'LIN', u'aircraft_movements': 102907, u'city': u'MIL', u'timezone': u'Europe/Rome', u'distance': 7.43509432076132, u'location': {u'latitude': 45.445103, u'longitude': 9.276739}}], [{u'city_name': u'New York City', u'airport_name': u'Newark Liberty International Airport', u'airport': u'EWR', u'aircraft_movements': 385054, u'city': u'NYC', u'timezone': u'America/New_York', u'distance': 10.021479827071987, u'location': {u'latitude': 40.69125, u'longitude': -74.17883}}]]
        print "def qpx_wrapping - relevant_airports"
        print origin_relevant_airports
        print destination_relevant_airports
        for i in range(len(list_legs)):
            print "!!!!!!!!!!!!! check error !!!!!! list_legs[i][origin]"
            print list_legs[i]['origin']
            vector_distance_origin.append(QpxExpress.get_distance_airport(list_legs[i]['origin'], origin_relevant_airports[i]))

        for i in range(len(list_legs)):
            print "!!!!!!!!!!!!! check error !!!!!! list_legs[i][origin]"
            print list_legs[i]['destination']
            vector_distance_destination.append(QpxExpress.get_distance_airport(list_legs[i]['destination'], destination_relevant_airports[i]))
            # Distance Vector
        print "!!!!!!!!!!!!! check error !!!!!! list_legs[-1][destination]"
        print "vector_distance_origin + vector_distance_destination"
        print vector_distance_origin
        print vector_distance_destination

        #OFIR PUT next line in COMMENT!!
        #vector_distance.append(QpxExpress.get_distance_airport(list_legs[-1]['destination'], relevant_airports[-1]))
        # vector_distance= [[{'airport': u'LHR', u'distance': {u'text': u'26.6 km', u'value': 26639}, u'dest_address': u'450 Northern Perimeter Rd W, Longford, Hounslow, Greater London TW6 2RR, UK', u'origin_address': u'London, UK', u'duration': {u'text': u'35 mins', u'value': 2104}}, {'airport': u'LGW', u'distance': {u'text': u'46.0 km', u'value': 45987}, u'dest_address': u'Gatwick Airport (LGW), North Terminal, Horley, Gatwick, West Sussex RH6 0NP, UK', u'origin_address': u'London, UK', u'duration': {u'text': u'1 hour 13 mins', u'value': 4366}}, {'airport': u'LCY', u'distance': {u'text': u'14.7 km', u'value': 14670}, u'dest_address': u'92 Newland St, London E16 2HN, UK', u'origin_address': u'London, UK', u'duration': {u'text': u'32 mins', u'value': 1900}}], [{'airport': u'TXL', u'distance': {u'text': u'651 km', u'value': 651439}, u'dest_address': u'Zufahrt zum Flughafen Tegel, 13405 Berlin, Germany', u'origin_address': u'Dam, 1012 Amsterdam, Netherlands', u'duration': {u'text': u'6 hours 6 mins', u'value': 21952}}], [{'airport': u'AMS', u'distance': {u'text': u'1,080 km', u'value': 1080146}, u'dest_address': u'Transportstraat, 1118 Schiphol, Netherlands', u'origin_address': u'Milan, Italy', u'duration': {u'text': u'10 hours 46 mins', u'value': 38771}}], [{'airport': u'EWR', u'distance': {u'text': u'16.2 km', u'value': 16234}, u'dest_address': u'Express Rd, Newark, NJ 07114, USA', u'origin_address': u'88 Highland Ave, Jersey City, NJ 07306, USA', u'duration': {u'text': u'18 mins', u'value': 1083}}]]
        # Distance Vector
        clear_relevant_airport_origin = QpxExpress.clear_relevant_airport_data(origin_relevant_airports)
        clear_relevant_airport_destination = QpxExpress.clear_relevant_airport_data(destination_relevant_airports)

        permute_origin = QpxExpress.permute_airports(clear_relevant_airport_origin)
        permute_destination = QpxExpress.permute_airports(clear_relevant_airport_destination)
        new_permute_origin = QpxExpress.string_to_list(permute_origin)  # All possible airports trips
        new_permute_destination = QpxExpress.string_to_list(permute_destination)  # All possible airports trips
        print "permute_origin"
        print permute_origin
        print "permute_destination"
        print permute_destination
        print "new_permute_origin"
        print new_permute_origin
        print "new_permute_destination"
        print new_permute_destination


        all_fixed_queries = QpxExpress.fix_leg_address(list_legs, new_permute_origin, new_permute_destination, dict_details, vector_distance_origin, vector_distance_destination,
                                                       round_trip, return_date)
        #all_fixed_queries_destination = QpxExpress.fix_leg_address(list_legs, new_permute_destination, dict_details, vector_distance,
        #                                               round_trip, return_date)
        return all_fixed_queries  # TODO fill missing data(distances)

    '''Private Method - getting the right airport for a specific address'''
    #ofir - new (was get_relevant_airports)
    @staticmethod
    def get_origin_relevant_airports(list_legs):
        # list_legs = [{'destination': 'berlin germany', 'date': '2016-08-20', 'cabin': '', 'latest_time': '22:00', 'max_connection_duration': 2000, 'origin': 'london, england', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': 'london, england', 'date': '2016-09-15', 'cabin': '', 'latest_time': '23:00', 'max_connection_duration': 2000, 'origin': 'dam square, amsterdam', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': '86-88 Highland Ave Jersey City, NJ 07306, USA', 'date': '2016-10-20', 'cabin': '', 'latest_time': '19:00', 'max_connection_duration': 2000, 'origin': 'milan italy', 'max_stop': 2, 'earliest_time': '05:00'}]
        print "def get_origin_relevant_airports(list_legs):"
        print "list_legs"
        print list_legs
        relevant_airports = []
        for legs in list_legs:
            relevant_airports.append(amadeus.nearest_relevant_airport(legs['origin']))

        return relevant_airports

    #ofir - new
    @staticmethod
    def get_destination_relevant_airports(list_legs):
        # list_legs = [{'destination': 'berlin germany', 'date': '2016-08-20', 'cabin': '', 'latest_time': '22:00', 'max_connection_duration': 2000, 'origin': 'london, england', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': 'london, england', 'date': '2016-09-15', 'cabin': '', 'latest_time': '23:00', 'max_connection_duration': 2000, 'origin': 'dam square, amsterdam', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': '86-88 Highland Ave Jersey City, NJ 07306, USA', 'date': '2016-10-20', 'cabin': '', 'latest_time': '19:00', 'max_connection_duration': 2000, 'origin': 'milan italy', 'max_stop': 2, 'earliest_time': '05:00'}]
        print "def get_destination_relevant_airports(list_legs):"
        print "list_legs"
        print list_legs
        relevant_airports = []
        for legs in list_legs:
            relevant_airports.append(amadeus.nearest_relevant_airport(legs['destination']))

        return relevant_airports

    '''gets distance matrix for specific address and many airports'''
    @staticmethod
    def get_distance_airport(address, relevant_airports):
        # airports = [airport['airport'] for airport in relevant_airports]
        print "def get_distance_airport(address, relevant_airports)"
        print "address"
        print address
        print "relevant_airports"
        print relevant_airports
        airports = []
        for airport in relevant_airports:
                airports.append(airport['location'])

        vector_distance = google_maps.get_distance_one_to_many(address, airports)

        for i in range(len(relevant_airports)):
                vector_distance[i]['airport'] = relevant_airports[i]['airport']

        return vector_distance

    '''Recursion method , permute all optional trips from all optional airports '''
    @staticmethod
    def permute_airports(vector_distance):
        print "def permute_airports(vector_distance):"
        print "vector_distance: ", vector_distance
        if len(vector_distance) == 1:
            return vector_distance[0]
        else:
            l_new = []
            for a in vector_distance[0]:
                for b in QpxExpress.permute_airports(vector_distance[1:]):
                    l_new.append(a + "," + b)
            return l_new

    '''Clear data from relevant airports and keeps the airports code'''
    @staticmethod
    def clear_relevant_airport_data(relevant_airports):
        print "def clear_relevant_airport_data"
        print "relevant_airports: ", relevant_airports
        new_l = []
        if relevant_airports is None:
            return new_l

        new_l = relevant_airports
        for legs in new_l:
            for airport in legs:
                del airport['city']
                del airport['distance']
                del airport['city_name']
                del airport['airport_name']
                del airport['aircraft_movements']
                del airport['location']
                del airport['timezone']

        no_dict = []
        trip = []

        for legs in new_l:
            for airport in legs:
                trip.append(airport['airport'])
            no_dict.append(trip)
            trip = []

        return no_dict

    '''Private method splits string and inserts to array'''
    @staticmethod
    def string_to_list(permute):
        new_l = []
        if permute is None:
            print "ERROR: def string_to_list(permute) - permute is None "
            return new_l

        for airport in permute:
            airports = airport.split(",")
            new_l.append(airports)

        return new_l

    '''Private Method - Fix all addresses(origin and destination) in flights leg, for making correct queries'''
    @staticmethod
    def fix_leg_address(list_legs, new_permute_origin, new_permute_destination, dict_details, vector_distance_origin,vector_distance_destination,  round_trip=False, return_date=''):
        print "def fix_leg_address(list_legs, new_permute, dict_details, vector_distance, round_trip=False, return_date='')"
        print "list_legs: ", list_legs
        print "new_permute_origin: ", new_permute_origin
        print "new_permute_destination: ", new_permute_destination
        print "dict_details: ", dict_details
        print "vector_distance: ", vector_distance_origin
        print "vector_distance: ", vector_distance_destination

        new_l = []
        if list_legs is None or new_permute_origin is None or new_permute_destination is None:
            print "def fix_leg_address Arguments Error: list_legs is None or new_permute_origin is None or new_permute_destination is None"
            return new_l
        # if len(new_permute_origin) != len(new_permute_destination):
        #     print "len(new_permute_origin) != len(new_permute_destination)"
        #     return new_l
        for i in range(len(new_permute_origin)):
            new_l.append(list(list_legs))


        new_flights = []
        print "len(new_permute_origin)"
        print len(new_permute_origin)
        for i in range(len(new_permute_origin)):
            for j in range(len(new_permute_destination[i])-1):
                one = new_permute_origin[i][j]
                two = new_permute_destination[i][j+1]
                print "def fix_leg_address - one: ", one
                print "def fix_leg_address - one: ", two

                new_l[i][j]['origin'] = one
                new_l[i][j]['destination'] = two
            print "def fix_leg_address - query = QpxExpress.qpx_flights(new_l[i], dict_details, round_trip, return_date)"
            print "new_l[i]: ", new_l[i]
            print "dict_details: ", dict_details
            query = QpxExpress.qpx_flights(new_l[i], dict_details, round_trip, return_date)
            print "query: =", query
            print "query: =", vector_distance
            fix = QpxExpress.qpx_fix_json(query, vector_distance)
            print "fix: =", fix
            new_flights.append(fix)

        print "new_flights: =", new_flights
        return new_flights
