__author__ = 'Uri, Ofir & Elad'

# TBD - check why there is urllib2 and urllib (urllib2 from QPX and urllib from google_maps oringinal files
from lib import requests
import sys
import urllib2
import urllib
import json
from api.Amadeus import Amadeus
from api.google_maps import GoogleMaps
import xml.etree.cElementTree as ET
from google.appengine.api import urlfetch
import xmltodict

urlfetch.set_default_fetch_deadline(60)
import itertools
# from suds_memcache import Client
from models import Itineraries, User, Booked_Itineraries, PersistentData
from google.appengine.ext import ndb
import time
import logging
import datetime
import copy
from airports_main import AP_DBs_main
from traffic_calculation import drive_calc
from traffic_calculation import transit_calc
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

amadeus = Amadeus
google_maps = GoogleMaps
from ap_db_loc_wo_doubles import AirportDbLocationsWoDoubles



class Itinerary_Keys:
    @staticmethod
    def price(itinerary):
        return int(itinerary['price'])

    @staticmethod
    def FlightDuration(itinerary):
        #return sum(leg['duration'] for leg in itinerary['legs'])
        return itinerary['calculations']['total_flights_time']

    @staticmethod
    def FlightDriveDuration(itinerary):
        #return sum(leg['duration'] for leg in itinerary['legs'])
        return itinerary['calculations']['total_drives_and_flights_time']

    @staticmethod
    def FlightDriveTrafDuration(itinerary):
        #return sum(leg['duration'] for leg in itinerary['legs'])
        return itinerary['calculations']['total_drives_and_flights_time_with_traffic']

    @staticmethod
    def StopsNum(itinerary):
        #return (sum(len(leg['segments']) for leg in itinerary['legs']) - 1)
        return itinerary['calculations']['total_stops']

    @staticmethod
    def StopsDuration(itinerary):
        # return (sum(len(leg['segments']) for leg in itinerary['legs']) - 1)
        return itinerary['calculations']['total_stops_time']


    @staticmethod
    def DriveDuration(itinerary):
        # return (sum(len(leg['segments']) for leg in itinerary['legs']) - 1)
        if itinerary['calculations']['total_drives_time'] > 0 :
            return itinerary['calculations']['total_drives_time']
        else: #price + very big number (in case there are other
            return  999999 + int(itinerary['price'])



class Leg_Keys:
    @staticmethod
    def itinerary_stops_num(leg):
        return (sum(len(leg['segments']) for leg in itinerary['legs']) - 1)

    @staticmethod
    def leg_stops_durations(leg):
        return sum(leg['duration'] for leg in itinerary['legs'])

class Hotel_Filters:

    @staticmethod
    def max_price(hotel, data):
        #if 1: return True
        return int(round(hotel['price'])) <= data['value'] # todo: tbd round up / down by math.ceil math.floor

    @staticmethod
    def combined_filters(filter_data, hotel):
        """ filter_data has this structure:
        filter_data = {
            'min_price': {'value': 4000},
            'max_price': {'value': 20000},
            'stops_range': {'values': [1,2]},
                       }
        where the keys of filter_data are the name of the functions in Itinerary_Filters calss,
        the the value is a dict with one or more of the keys: value, values (a list), leg_index, segment_index.
        """
        if not filter_data:
            # logging.info('not filter_data - filter: %s' %filter_data )
            return True
        for key in filter_data.keys():
            result = eval("Hotel_Filters.%s" % key)(hotel, filter_data[key])
            if not result:
                # logging.info('filter: %s, result: %s' % (key, result))  # print only false filters
                # logging.info("filter_data[key]: %s" % filter_data[key])
                # logging.info("itinerary: %s" %itinerary)
                return False
        return True


class Itinerary_Filters:

    @staticmethod
    def max_distance(itinerary, data):
        return True


    @staticmethod
    def max_sum_time_all_stops(itinerary, data):
        return True

    @staticmethod
    def min_price(itinerary, data):
        #if 1: return True
        return int(round(itinerary['price'])) >= data['value'] # todo: tbd round up / down by math.ceil math.floor

    @staticmethod
    def max_price(itinerary, data):
        #if 1: return True
        return int(round(itinerary['price'])) <= data['value'] # todo: tbd round up / down by math.ceil math.floor

    @staticmethod
    def min_time_arrival_leg(leg, value):
            current_value = (datetime.datetime.strptime(leg['segments'][-1]['destination']['time'], '%H:%M:%S')-datetime.datetime.strptime("00:00:00", '%H:%M:%S')).seconds/60
            return current_value >= value
        

    @staticmethod
    def min_time_arrival(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index= int(data['value'][val_index]['leg_index'])-1 #the leg number is allways +1
            if not Itinerary_Filters.min_time_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_time_arrival_leg(leg, value):
        current_value = (datetime.datetime.strptime(leg['segments'][-1]['destination']['time'], '%H:%M:%S')-datetime.datetime.strptime("00:00:00", '%H:%M:%S')).seconds/60
        return current_value <= value
        # return datetime.datetime.strptime(leg['segments'][-1]['destination']['time'],
        #                               '%H:%M:%S') <= datetime.datetime.strptime(value, '%H:%M:%S')

    @staticmethod
    def max_time_arrival(itinerary, data):
        if 1: return True
        for val_index in range(len(data['value'])):
            leg_index= int(data['value'][val_index]['leg_index'])-1 #the leg number is allways +1
            if not Itinerary_Filters.max_time_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def min_time_departure_leg(leg, value):
        current_value = (datetime.datetime.strptime(leg['segments'][0]['destination']['time'], '%H:%M:%S')-datetime.datetime.strptime("00:00:00", '%H:%M:%S')).seconds/60
        return current_value >= value
        # return datetime.datetime.strptime(leg['segments'][0]['destination']['time'],
        #                               '%H:%M:%S') >= datetime.datetime.strptime(value, '%H:%M:%S')

    @staticmethod
    def min_time_departure(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_time_departure_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_time_departure_leg(leg, value):
        current_value = (datetime.datetime.strptime(leg['segments'][0]['destination']['time'], '%H:%M:%S')-datetime.datetime.strptime("00:00:00", '%H:%M:%S')).seconds/60
        return current_value <= value
        # return datetime.datetime.strptime(leg['segments'][0]['destination']['time'],
        #                               '%H:%M:%S') <= datetime.datetime.strptime(value, '%H:%M:%S')

    @staticmethod
    def max_time_departure(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_time_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def date_arrival_leg(leg, value):
        return leg['segments'][-1]['destination']['date'] in value

    @staticmethod
    def date_arrival(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.date_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']):
                return False
        return True

    @staticmethod
    def date_departure_leg(leg, value):
        return leg['segments'][0]['origin']['date'] in value

    @staticmethod
    def date_departure(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.date_departure_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']):
                return False
        return True

    @staticmethod
    def min_hour_departure_leg(leg, value):
        [leg_time_hh, leg_time_mm, leg_time_ss]= leg['segments'][0]['origin']['time'].split(':')
        leg_time_in_min=int(leg_time_hh)*60+int(leg_time_mm)
        return leg_time_in_min >= value

    @staticmethod
    def min_hour_departure(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_hour_departure_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_hour_departure_leg(leg, value):
        [leg_time_hh, leg_time_mm, leg_time_ss]= leg['segments'][0]['origin']['time'].split(':')
        leg_time_in_min=int(leg_time_hh)*60+int(leg_time_mm)
        return leg_time_in_min <= value

    @staticmethod
    def max_hour_departure(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_hour_departure_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def min_hour_arrival_leg(leg, value):
        [leg_time_hh, leg_time_mm, leg_time_ss]= leg['segments'][-1]['destination']['time'].split(':')
        leg_time_in_min=int(leg_time_hh)*60+int(leg_time_mm)
        return leg_time_in_min >= value

    @staticmethod
    def min_hour_arrival(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_hour_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_hour_arrival_leg(leg, value):
        [leg_time_hh, leg_time_mm, leg_time_ss]= leg['segments'][-1]['destination']['time'].split(':')
        leg_time_in_min=int(leg_time_hh)*60+int(leg_time_mm)
        return leg_time_in_min <= value

    @staticmethod
    def max_hour_arrival(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_hour_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_stops_leg(leg, value):
        numofstops = len(leg['segments'])-1
        return numofstops <= value

    @staticmethod
    def max_stops(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_stops_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def stops_range_leg(leg, value):
        numofstops = len(leg['segments'])-1
        return numofstops in value

    @staticmethod
    def stops_range(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.stops_range_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']):
                return False
        return True

    @staticmethod
    def min_stop_time_segment(segment, value):
        if len(segment['flight']['stop_time']) <=5: # 00:00 is without seconds
            return True
        [seg_time_hh, seg_time_mm, seg_time_ss] = segment['flight']['stop_time'].split(':')
        seg_time_in_min = int(seg_time_hh) * 60 + int(seg_time_mm)
        return seg_time_in_min >= value

    @staticmethod
    def min_stop_time_leg(leg, value):
        for segment_index in range(len(leg['segments']) -1):
            if not Itinerary_Filters.min_stop_time_segment(leg['segments'][segment_index], value):  #the data is per leg for all the stops in the leg
                return False
        return True

    @staticmethod
    def min_stop_time(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_stop_time_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_stop_time_segment(segment, value):
        if len(segment['flight']['stop_time']) <=5: # 00:00 is without seconds
            return True
        [seg_time_hh, seg_time_mm, seg_time_ss] = segment['flight']['stop_time'].split(':')
        seg_time_in_min = int(seg_time_hh) * 60 + int(seg_time_mm)
        return seg_time_in_min <= value

    @staticmethod
    def max_stop_time_leg(leg, value):
        for segment_index in range(len(leg['segments']) - 1):
            if not Itinerary_Filters.max_stop_time_segment(leg['segments'][segment_index], value):
                return False
        return True

    @staticmethod
    def max_stop_time(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_stop_time_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def max_sum_time_all_stops_leg(itinerary, data): #todo: delete filter
        return True
        leg = itinerary['legs'][data['leg_index']]
        return sum(segment['flight']['stop_time'] for segment in leg['segments']) <= data['value']

    @staticmethod
    def airport_arrival_leg(leg, value):
        return leg['segments'][-1]['destination']['airport']['name'] in value  # values

    @staticmethod
    def airport_arrival(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.airport_arrival_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']): # values
                return False
        return True

    @staticmethod
    def airport_departure_leg(leg, value):
        return leg['segments'][0]['origin']['airport']['name'] in value  # values

    @staticmethod
    def airport_departure(itinerary, data):
        #if 1: return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.airport_departure_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']): # values
                return False
        return True

    @staticmethod
    def airport_connection_segment(segment, value):
        return segment['destination']['airport']['name'] in value  # empty

    @staticmethod
    def airport_connection_leg(leg, value):
        for segment_index in range(len(leg['segments']) - 1): # without last leg
            if not Itinerary_Filters.airport_connection_segment(leg['segments'][segment_index], value):
                return False
        return True  # empty

    @staticmethod
    def airport_connection(itinerary, data): # empty

        return True  # todo: gets wrong data
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.airport_connection_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']): # values
                # logging.info('airport_connection data: %s ' % data)
                return False
        return True

    @staticmethod
    def min_flight_drive_duration(itinerary, data): # todo: tbd if it's in use
        #return True
        return itinerary['calculations']['total_drives_and_flights_time'] >= data['value']

    @staticmethod
    def max_flight_drive_duration(itinerary, data):
        return itinerary['calculations']['total_drives_and_flights_time'] <= data['value']

    @staticmethod
    def min_flight_duration_leg_leg(clac_per_leg, value):
        return clac_per_leg['total_flights_time'] >= value


    @staticmethod
    def min_flight_duration_leg(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_flight_duration_leg_leg(itinerary['calculations_per_legs'][leg_index], data['value'][val_index]['value']): # values
                return False
        return True

    @staticmethod
    def max_flight_duration_leg_leg(clac_per_leg, value):
        return clac_per_leg['total_flights_time'] <= value


    @staticmethod
    def max_flight_duration_leg(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_flight_duration_leg_leg(itinerary['calculations_per_legs'][leg_index], data['value'][val_index]['value']): # values
                return False
        return True

    @staticmethod
    def min_flight_duration(itinerary, data):
        return itinerary['calculations']['total_flights_time'] >= data['value']

    @staticmethod
    def max_flight_duration(itinerary, data):
        return itinerary['calculations']['total_flights_time'] <= data['value']

    @staticmethod
    def min_flight_drive_traffic_duration(itinerary, data):
        #return True
        return itinerary['calculations']['total_drives_and_flights_time_with_traffic'] >= data['value']

    @staticmethod
    def max_flight_drive_traffic_duration(itinerary, data):
        #return True
        return itinerary['calculations']['total_drives_and_flights_time_with_traffic'] <= data['value']

    @staticmethod
    def max_flight_public_transp_duration(itinerary, data): #todo - add it to calculations
        return True
        return itinerary['calculations']['total_drives_time_public_transp']   <= data['value']

    @staticmethod
    def min_flight_public_transp_duration(itinerary, data):
        return True
        return itinerary['calculations']['total_drives_time_public_transp'] >= data['value']

    @staticmethod
    def pos_airlines_segment(segment, value):
        # if not segment['carrier']['operating']['name'] in value:
        #     return False
        if not segment['carrier']['marketing']['name'] in value:
            return False
        return True

    @staticmethod
    def pos_airlines_leg(leg, value):
        for segment_index in range(len(leg['segments'])):
            if not Itinerary_Filters.pos_airlines_segment(leg['segments'][segment_index], value):
                return False
        return True  # empty

    @staticmethod
    def pos_airlines(itinerary, data): # only airlines in the values are allowed
        for leg in itinerary['legs']:
            if not Itinerary_Filters.pos_airlines_leg(leg, data['values']): # values
                return False
        return True

    @staticmethod
    def neg_airlines_segment(segment, value):
        if segment['carrier']['operating']['name'] in value:
            return False
        if segment['carrier']['marketing']['name'] in value:
            return False
        return True

    @staticmethod
    def neg_airlines_leg(leg, value):
        for segment_index in range(len(leg['segments'])):
            if not Itinerary_Filters.neg_airlines_segment(leg['segments'][segment_index], value):
                return False
        return True  # empty

    @staticmethod
    def neg_airlines(itinerary, data): # airlines in the values aren't allowed
        return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.neg_airlines_leg(itinerary['legs'][leg_index], data['value'][val_index]['values']): # values
                return False
        return True

    @staticmethod
    def min_drive_time_leg_leg(leg, value):
        if leg.get('drive_to_ap'):
            if not leg['drive_to_ap']['duration_value'] >= value:
                return False
        if leg.get('drive_from_ap'):
            if not leg['drive_from_ap']['duration_value'] >= value:
                return False
        return True

    @staticmethod
    def min_drive_time_leg(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.min_drive_time_leg_leg(itinerary['legs'][leg_index],
                                                        data['value'][val_index]['value']):
                return False
        return True


    @staticmethod
    def max_drive_time_leg_leg(leg, value):
        if leg.get('drive_to_ap'):
            if not leg['drive_to_ap']['duration_value'] <= value:
                return False
        if leg.get('drive_from_ap'):
            if not leg['drive_from_ap']['duration_value'] <= value:
                return False
        return True

    @staticmethod
    def max_drive_time_leg(itinerary, data):
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_drive_time_leg_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True

    @staticmethod
    def min_drive_time(itinerary, data):
        return itinerary['calculations']['total_drives_time'] >= data['value']

    @staticmethod
    def max_drive_time(itinerary, data):
        return itinerary['calculations']['total_drives_time'] <= data['value']



    @staticmethod
    def max_drive_distance_leg(leg, value):
        if not leg['drive_to_ap']['drive_distance'] <= value:
            return False
        if not leg['drive_from_ap']['drive_distance'] <= value:
            return False
        return True

    @staticmethod
    def max_drive_distance(itinerary, data):
        return True
        for val_index in range(len(data['value'])):
            leg_index = int(data['value'][val_index]['leg_index']) - 1  # the leg number is allways +1
            if not Itinerary_Filters.max_drive_distance_leg(itinerary['legs'][leg_index], data['value'][val_index]['value']):
                return False
        return True


    @staticmethod
    def conv_epoch_to_date_time(epoch):
        return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(epoch/1000)) #the client works in ms- /1000 covert to sec'

    @staticmethod
    def conv_date_time_to_epoch(date1,time1):
        d_h = map(int,date1.split('-')) + map(int,time1.split(':'))
        return (datetime.datetime(d_h[0], d_h[1], d_h[2], d_h[3], d_h[4], d_h[5]) - datetime.datetime(1970, 1, 1)).total_seconds()

    @staticmethod
    def combined_filters(filter_data, itinerary):
        """ filter_data has this structure:
        filter_data = {
            'min_price': {'value': 4000},
            'max_price': {'value': 20000},
            'stops_range': {'values': [1,2]},
                       }
        where the keys of filter_data are the name of the functions in Itinerary_Filters calss,
        the the value is a dict with one or more of the keys: value, values (a list), leg_index, segment_index.
        """
        if not filter_data:
            # logging.info('not filter_data - filter: %s' %filter_data )
            return True
        for key in filter_data.keys():
            result = eval("Itinerary_Filters.%s" % key)(itinerary, filter_data[key])
            if not result:
                # logging.info('filter: %s, result: %s' % (key, result))  # print only false filters
                # logging.info("filter_data[key]: %s" % filter_data[key])
                # logging.info("itinerary: %s" %itinerary)
                return False
        return True


    @staticmethod
    def Create_itineraries_limits_and_ranges(list_legs, itineraries):
        min_time_departure = {'value': []}  # leg
        max_time_departure = {'value': []}  # leg
        min_time_arrival = {'value': []}  # leg
        max_time_arrival = {'value': []}  # leg
        min_price = {'value': []}  # flight
        max_price = {'value': []}  # flight
        min_flight_drive_traffic_duration = {'value': []}  # flight
        max_flight_drive_traffic_duration = {'value': []}  # flight
        min_flight_drive_traffic_duration_leg = {'value': []}  # leg
        max_flight_drive_traffic_duration_leg = {'value': []}  # leg
        min_flight_drive_duration = {'value': []}  # flight
        max_flight_drive_duration = {'value': []}  # flight
        min_flight_drive_duration_leg = {'value': []}  # leg
        max_flight_drive_duration_leg = {'value': []}  # leg
        min_drive_time = {'value': []}  # flight
        max_drive_time = {'value': []}  # flight
        min_drive_time_dep_leg = {'value': []}  # leg
        max_drive_time_dep_leg = {'value': []}  # leg
        min_drive_time_arr_leg = {'value': []}  # leg
        max_drive_time_arr_leg = {'value': []}  # leg
        min_drive_traf_time_dep_leg = {'value': []}  # leg
        max_drive_traf_time_dep_leg = {'value': []}  # leg
        min_drive_traf_time_arr_leg = {'value': []}  # leg
        max_drive_traf_time_arr_leg = {'value': []}  # leg
        max_flight_duration = {'value': []}  # flight
        min_flight_duration = {'value': []}  # flight
        max_flight_duration_leg = {'value': []}  # leg
        min_flight_duration_leg = {'value': []}  # leg
        date_departure = {'value': []}  # leg
        date_arrival = {'value': []}  # leg
        airport_departure = {'value': []}  # leg
        airport_arrival = {'value': []}  # leg
        pos_airlines = {'value': []}  # flight
        airport_connection = {'value': []}  # leg
        stops_range = {'value': []}  # leg
        pos_airlines_list = []  # flight

        #####################
        # values per flight #
        #####################
        if len(itineraries)>0:
            min_price_data = {
                'value': min(itinerary['price'] for itinerary in itineraries)
            }
            min_price['value'].append(min_price_data)

            max_price_data = {
                'value': max(itinerary['price'] for itinerary in itineraries)
            }
            max_price['value'].append(max_price_data)

            min_flight_drive_duration_data = {
                'value': min(itinerary['calculations']['total_drives_and_flights_time'] for itinerary in itineraries)
            }
            min_flight_drive_duration['value'].append(min_flight_drive_duration_data)

            max_flight_drive_duration_data = {
                'value': max(itinerary['calculations']['total_drives_and_flights_time'] for itinerary in itineraries)
            }
            max_flight_drive_duration['value'].append(max_flight_drive_duration_data)

            min_flight_drive_traffic_duration_data = {
                'value': min(
                    itinerary['calculations']['total_drives_and_flights_time_with_traffic'] for itinerary in itineraries)
            }
            min_flight_drive_traffic_duration['value'].append(min_flight_drive_traffic_duration_data)

            max_flight_drive_traffic_duration_data = {
                'value': max(
                    itinerary['calculations']['total_drives_and_flights_time_with_traffic'] for itinerary in itineraries)
            }

            min_drive_time_data = {
                'value': min(
                    itinerary['calculations']['total_drives_time'] for itinerary in itineraries)
            }
            min_drive_time['value'].append(min_drive_time_data)

            max_drive_time_data = {
                'value': max(itinerary['calculations']['total_drives_time'] for itinerary in itineraries)
            }
            max_drive_time['value'].append(max_drive_time_data)

            min_flight_duration_data = {
                'value': min(itinerary['calculations']['total_flights_time'] for itinerary in itineraries)
            }
            min_flight_duration['value'].append(min_flight_duration_data)

            max_flight_duration_data = {
                'value': max(itinerary['calculations']['total_flights_time'] for itinerary in itineraries)
            }
            max_flight_duration['value'].append(max_flight_duration_data)

            ##################
            # values per leg #
            ##################

            for leg_index in range(len(list_legs)):
                base_time = datetime.datetime.strptime("00:00:00", '%H:%M:%S')

                minimum_arrival_time = min(itinerary['legs'][leg_index]['segments'][0]['destination']['time'] for itinerary in itineraries)
                minimum_arrival_time = datetime.datetime.strptime(minimum_arrival_time, '%H:%M:%S')
                min_arrival_minutes = (minimum_arrival_time - base_time).seconds / 60

                maximum_time_arrival = max(itinerary['legs'][leg_index]['segments'][0]['destination']['time'] for itinerary in itineraries)
                maximum_time_arrival = datetime.datetime.strptime(maximum_time_arrival, '%H:%M:%S')
                max_arrival_minutes = (maximum_time_arrival - base_time).seconds / 60

                minimum_departure_time = min(itinerary['legs'][leg_index]['segments'][-1]['destination']['time'] for itinerary in itineraries)
                minimum_departure_time = datetime.datetime.strptime(minimum_departure_time, '%H:%M:%S')
                min_departure_minutes = (minimum_departure_time - base_time).seconds / 60

                maximum_time_departure = max(itinerary['legs'][leg_index]['segments'][-1]['destination']['time'] for itinerary in itineraries)
                maximum_time_departure = datetime.datetime.strptime(maximum_time_departure, '%H:%M:%S')
                max_departure_minutes = (maximum_time_departure - base_time).seconds / 60


                min_time_departure_data = {
                    'leg_index': leg_index + 1,
                    'value': min_arrival_minutes
                }
                min_time_departure['value'].append(min_time_departure_data)

                max_time_departure_data = {
                    'leg_index': leg_index + 1,
                    'value': max_departure_minutes
                }
                max_time_departure['value'].append(max_time_departure_data)

                min_time_arrival_data = {
                    'leg_index': leg_index + 1,
                    'value': min_arrival_minutes
                }
                min_time_arrival['value'].append(min_time_arrival_data)


                max_time_arrival_data = {
                    'leg_index': leg_index + 1,
                    'value': max_arrival_minutes
                }
                max_time_arrival['value'].append(max_time_arrival_data)

                min_flight_drive_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(itinerary['calculations_per_legs'][leg_index]['total_drives_and_flights_time'] for itinerary in
                                 itineraries)
                }
                min_flight_drive_duration_leg['value'].append(min_flight_drive_duration_leg_data)

                max_flight_drive_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(itinerary['calculations_per_legs'][leg_index]['total_drives_and_flights_time'] for itinerary in
                                 itineraries)
                }
                max_flight_drive_duration_leg['value'].append(max_flight_drive_duration_leg_data)

                min_flight_drive_traffic_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(itinerary['calculations_per_legs'][leg_index]['total_drives_and_flights_time_with_traffic'] for
                                 itinerary in itineraries)
                }
                min_flight_drive_traffic_duration_leg['value'].append(min_flight_drive_traffic_duration_leg_data)

                max_flight_drive_traffic_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(itinerary['calculations_per_legs'][leg_index]['total_drives_and_flights_time_with_traffic'] for
                                 itinerary in itineraries)
                }
                max_flight_drive_traffic_duration_leg['value'].append(max_flight_drive_traffic_duration_leg_data)

                min_drive_time_arr_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(
                        itinerary['legs'][leg_index]['drive_to_ap']['duration_value'] for itinerary in itineraries)
                }
                min_drive_time_arr_leg['value'].append(min_drive_time_arr_leg_data)

                max_drive_time_arr_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(
                        itinerary['legs'][leg_index]['drive_to_ap']['duration_value'] for itinerary in itineraries)
                }
                max_drive_time_arr_leg['value'].append(max_drive_time_arr_leg_data)

                min_drive_time_dep_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(
                        itinerary['legs'][leg_index]['drive_from_ap']['duration_value'] for itinerary in itineraries)
                }
                min_drive_time_dep_leg['value'].append(min_drive_time_dep_leg_data)

                max_drive_time_dep_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(
                        itinerary['legs'][leg_index]['drive_from_ap']['duration_value'] for itinerary in itineraries)
                }
                max_drive_time_dep_leg['value'].append(max_drive_time_dep_leg_data)

                min_drive_traf_time_arr_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(
                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap']['duration_value'] for itinerary in itineraries)
                }
                min_drive_traf_time_arr_leg['value'].append(min_drive_traf_time_arr_leg_data)

                max_drive_traf_time_arr_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(
                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap']['duration_value'] for itinerary in itineraries)
                }
                max_drive_traf_time_arr_leg['value'].append(max_drive_traf_time_arr_leg_data)

                min_drive_traf_time_dep_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(
                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap']['duration_value'] for itinerary in itineraries)
                }
                min_drive_traf_time_dep_leg['value'].append(min_drive_traf_time_dep_leg_data)

                max_drive_traf_time_dep_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(
                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap']['duration_value'] for itinerary in itineraries)
                }
                max_drive_traf_time_dep_leg['value'].append(max_drive_traf_time_dep_leg_data)

                min_flight_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': min(
                        itinerary['calculations_per_legs'][leg_index]['total_flights_time'] for itinerary in itineraries)
                }
                min_flight_duration_leg['value'].append(min_flight_duration_leg_data)

                max_flight_duration_leg_data = {
                    'leg_index': leg_index + 1,
                    'value': max(
                        itinerary['calculations_per_legs'][leg_index]['total_flights_time'] for itinerary in itineraries)
                }
                max_flight_duration_leg['value'].append(max_flight_duration_leg_data)

                # few values per leg
                airport_departure_list = []
                airport_arrival_list = []

                airport_connection_list = []
                stops_range_list = []

                date_departure_list = []
                date_arrival_list = []

                for itinerary in itineraries:
                    airport_departure_list.append(itinerary['legs'][leg_index]['segments'][0]['origin']['airport']['name'])
                    airport_arrival_list.append(itinerary['legs'][leg_index]['segments'][-1]['destination']['airport']['name'])
                    date_departure_list.append(itinerary['legs'][leg_index]['segments'][0]['origin']['date'])
                    date_arrival_list.append(itinerary['legs'][leg_index]['segments'][-1]['destination']['date'])

                    for seg_index, segment in enumerate(itinerary['legs'][leg_index]['segments']):
                        pos_airlines_list.append(segment['carrier']['marketing']['name'])
                        # pos_airlines_list.append(segment['carrier']['operating']['name'])#todo: TBD if there is data about the oporator
                        stops_range_list.append(len(itinerary['legs'][leg_index]['segments']) - 1)  # how many stops in the leg
                        if seg_index < (len(itinerary['legs'][leg_index]['segments']) - 1):  # if it's not the last segment
                            airport_connection_list.append(segment['destination']['airport']['name'])

                airport_departure_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(airport_departure_list))
                }
                airport_departure['value'].append(airport_departure_data)

                airport_arrival_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(airport_arrival_list))
                }
                airport_arrival['value'].append(airport_arrival_data)

                date_departure_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(date_departure_list))
                }
                date_departure['value'].append(date_departure_data)

                date_arrival_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(date_arrival_list))
                }
                date_arrival['value'].append(date_arrival_data)

                airport_connection_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(airport_connection_list))
                }
                airport_connection['value'].append(airport_connection_data)

                stops_range_data = {
                    'leg_index': leg_index + 1,
                    'value': list(set(stops_range_list))
                }
                stops_range['value'].append(stops_range_data)

                pos_airlines_list += pos_airlines_list

            pos_airlines_data = {
                'value': list(set(pos_airlines_list))
            }
            pos_airlines['value'].append(pos_airlines_data)

            itineraries_limits_and_ranges = {
                'min_time_departure': min_time_departure,
                'max_time_departure': max_time_departure,
                'min_time_arrival': min_time_arrival,
                'max_time_arrival': max_time_arrival,
                'min_price': min_price_data,  # data
                'max_price': max_price_data,  # data
                'min_flight_drive_traffic_duration': min_flight_drive_traffic_duration_data,  # data
                'max_flight_drive_traffic_duration': max_flight_drive_traffic_duration_data,  # data
                'min_flight_drive_traffic_duration_leg': min_flight_drive_traffic_duration_leg,
                'max_flight_drive_traffic_duration_leg': max_flight_drive_traffic_duration_leg,
                'min_flight_drive_duration': min_flight_drive_duration_data,  # data
                'max_flight_drive_duration': max_flight_drive_duration_data,  # data
                'min_flight_drive_duration_leg': min_flight_drive_duration_leg,
                'max_flight_drive_duration_leg': max_flight_drive_duration_leg,
                'min_drive_time': min_drive_time_data,
                'max_drive_time': max_drive_time_data,
                # 'min_drive_time_leg': min_drive_time_leg,
                # 'max_drive_time_leg': max_drive_time_leg,
                'min_drive_time_dep_leg' : min_drive_time_dep_leg,
                'max_drive_time_dep_leg' : max_drive_time_dep_leg,
                'min_drive_time_arr_leg' : min_drive_time_arr_leg,
                'max_drive_time_arr_leg' : max_drive_time_arr_leg,
                'min_drive_traf_time_dep_leg': min_drive_traf_time_dep_leg,
                'max_drive_traf_time_dep_leg': max_drive_traf_time_dep_leg,
                'min_drive_traf_time_arr_leg': min_drive_traf_time_arr_leg,
                'max_drive_traf_time_arr_leg': max_drive_traf_time_arr_leg,
                'max_flight_duration': max_flight_duration_data,
                'min_flight_duration': min_flight_duration_data,
                'max_flight_duration_leg': max_flight_duration_leg,
                'min_flight_duration_leg': min_flight_duration_leg,
                'date_departure': date_departure,
                'date_arrival': date_arrival,
                'airport_departure': airport_departure,
                'airport_arrival': airport_arrival,
                'pos_airlines': pos_airlines_data,
                'airport_connection': airport_connection,
                'stops_range': stops_range
            }
        else:
            logging.info("WARNNING: no itineraries in limits and ranges" )
            itineraries_limits_and_ranges = {}
        logging.info("itineraries_limits_and_ranges: %s" % itineraries_limits_and_ranges)
        return itineraries_limits_and_ranges


class convert_string_minutes:
    @staticmethod
    def time_from_string_to_minutes(time_as_string):
        return (int(time_as_string.split(':')[0]) * 60) + int(time_as_string.split(':')[1])

        # @staticmethod
        # def time_from_minutes_to_string(by_minutes):
        #     return "%02d:%02d" % (by_minutes / 60, by_minutes % 60)


class TBS_Functions:
    @staticmethod
    def get_flights_reservation(ReservationID):
        query_booking_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
           <soapenv:Header>
              <ns:AuthHeader>
                 <ns:ResellerCode>V2CC</ns:ResellerCode>
                 <ns:Username>tomer</ns:Username>
                 <ns:Password>tomer1</ns:Password>
                 <!--Optional:-->
                 <ns:ApplicationId>?</ns:ApplicationId>
              </ns:AuthHeader>
           </soapenv:Header>
           <soapenv:Body>
              <ns:ReservationGetDetailsRQ>
                 <ns:ReservationID>{ReservationID}</ns:ReservationID>
              </ns:ReservationGetDetailsRQ>
           </soapenv:Body>
        </soapenv:Envelope>"""
        # if not "ReservationID" in itinerary.keys():
        #     continue
        msg = query_booking_msg.format(ReservationID=ReservationID)
        output = TBS_Functions.Send_TBS_Query(msg, "http://tbs.dcsplus.net/ws/Reservation_GetDetails")
        return xmltodict.parse(output)['SOAP-ENV:Envelope']['SOAP-ENV:Body'][
            'ns1:ReservationGetDetailsRS']

    @staticmethod
    def Arr_Dep_var(leg_uniq_data):
        if (leg_uniq_data['arr_or_dep'] == 'depart'):
            dep_arr_time_mode = 'Departure time'
        else:
            dep_arr_time_mode = 'Arrival time'
        return dep_arr_time_mode

    @staticmethod
    def generate_TBS_queries(api_data):
        # 'Coordinates: {latitude}, {longitude}'.format(latitude='37.24N', longitude='-115.81W')
        # < ns:TimeWindow   Mode = "departure"         Start = "10:00:00"         End = "18:00:00" / >
        route_template = """                    <ns:Route>
                           <ns:Origin Code="{origin_code}"/>
                           <ns:Destination Code="{destination_code}"/>
                           <ns:Date>{date_string}</ns:Date>
                           <!--Optional:-->
                        </ns:Route>"""
        passenger_template = """<ns:Passenger PTC="{ptc}" Count="{count}"/>"""
        query_template = """
                <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
               <soapenv:Header>
                  <ns:AuthHeader>
                     <ns:ResellerCode>V2CC</ns:ResellerCode>
                     <ns:Username>tomer</ns:Username>
                     <ns:Password>tomer1</ns:Password>
                  </ns:AuthHeader>
               </soapenv:Header>
               <soapenv:Body>
                  <ns:Air_LowFareSearchRQ>
                     <ns:Itinerary>
    {routes}
                     </ns:Itinerary>
                     <ns:Passengers>
                        {passengers}
                     </ns:Passengers>
                     <ns:Filters>
                        <ns:DirectFlightsOnly>{direct_flights_only}</ns:DirectFlightsOnly>
                        <ns:Class>{Class}</ns:Class>
                    </ns:Filters>
                  </ns:Air_LowFareSearchRQ>
               </soapenv:Body>
            </soapenv:Envelope>"""

        cabin = {'economy': 'Y', 'premEcon':'W','business':'C', 'first': 'F'}[api_data['legs_common_details']['cabin']]
        logging.info("Cabin: %s" % cabin)

        tbs_queries = []
        for dates in api_data['dates_combinations']:
            for airports in api_data['airport_combinations']:
                # creating qpx query using specific dates and airports:
                legs = []
                persons = []
                dep_arr_time_mode = []
                for i, leg_data in enumerate(api_data['legs_unique_data']):
                    # logging.debug("creating date-airports %s-%s combinations for %s legs" % (dates[i], airports[i], len(dates)))
                    legs.append(route_template.format(origin_code=airports[i][0], destination_code=airports[i][1], \
                                                      date_string=dates[i][0]))
                persons.append(passenger_template.format(ptc="ADT", count=api_data['legs_common_details']['adult']))
                tbs_queries.append(query_template.format(direct_flights_only=0, Class=cabin, routes='\n'.join(legs),
                                                         passengers='\n'.join(persons)))  # search all flights
                #tbs_queries.append(query_template.format(direct_flights_only=1, Class="", routes='\n'.join(legs), passengers='\n'.join(persons))) # search only direct flights
        return tbs_queries


    @staticmethod
    def generate_TBS_hotel_query(api_data):
        hotel_search_template = """
            <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
               <soapenv:Header>
                  <ns:AuthHeader>
                     <ns:ResellerCode>V2CC</ns:ResellerCode>
                     <ns:Username>tomer</ns:Username>
                     <ns:Password>tomer1</ns:Password>
                  </ns:AuthHeader>
               </soapenv:Header>
               <soapenv:Body>
                  <ns:HotelGetAvailabilityRQ Timeout="%(timeout)s" AddHotelDetails="true" AddHotelMinPrice="false" LimitResults="%(limit_results)s" IgnoreHotelOffers="false">
                     <ns:Location>
                        <ns:Position Latitude="%(latitude)s" Longitude="%(longitude)s" Radius="%(radius)s"/>
                     </ns:Location>
                     <ns:DateRange Start="%(start)s" End="%(end)s"/>
                     <ns:Rooms>
                        <!--1 or more repetitions:-->
                        %(rooms)s
                           <!--0 to 2 repetitions:-->
                     <!--      <ns:ChildAge>0</ns:ChildAge>-->
                     </ns:Rooms>
                     <ns:Filters>
                        <ns:Nationality ID="%(natinality_id)s"/>
                        <ns:AvailableOnly>0</ns:AvailableOnly>
                        <!--Optional:-->
                        <!--Optional:-->
                        <ns:HotelStars>
                           <!--Zero or more repetitions:-->
                           %(rating)s
                        </ns:HotelStars>
                        <!--Optional:-->
                        <ns:CompleteOffersOnly>0</ns:CompleteOffersOnly>
                        <!--Optional:-->
                        <!--<ns:System>?</ns:System>-->
                     </ns:Filters>
                  </ns:HotelGetAvailabilityRQ>
               </soapenv:Body>
            </soapenv:Envelope>
        """
        logging.info( "api_data['stars']: ")
        logging.info(api_data['stars'])
        if not api_data['stars']:
            rating = ""
        else:
            rating = "\n".join(["<ns:Rating>%s</ns:Rating>" % star for star in api_data['stars']])
            logging.info("In else rating - ")
            logging.info("rating: " + rating)

        rooms = "\n".join(["<ns:Room Adults=\"%s\" Children=\"0\"></ns:Room>" % api_data['rooms'][room]['adults'] for room in api_data['rooms'].keys()])
        api_data['rating'] = rating
        api_data['rooms'] = rooms


        return hotel_search_template % api_data


    @staticmethod
    def stop_time_calc(itinerary): # asssumption : same time zone !!
        import time
        for leg in itinerary['legs']:
            for segment_index in range(len(leg['segments']) - 1):
                #logging.info('stop time is: %s' % leg['segments'][segment_index]["flight"]["stop_time"])
                if leg['segments'][segment_index]["flight"]["stop_time"] == '00:00':
                    stop_time_sec = leg['segments'][segment_index + 1]["origin"]["epoch"] - \
                                leg['segments'][segment_index]["destination"]["epoch"]
                    stop_time_str = time.strftime("%H:%M:%S", time.gmtime(stop_time_sec)) # hh:mm:ss
                    #logging.info('stop time str is: %s sec is %s' %(stop_time_str,stop_time_sec) )
                    leg['segments'][segment_index]["flight"]["stop_time"] = stop_time_str

    @staticmethod
    def add_local_epoch(itineraries):
        unique_ap_codes = []
        for itinerary in itineraries:
            for leg in itinerary['legs']:
                data = {'code': "", 'offset': ""}
                offset = -1
                code = leg['segments'][0]['origin']['airport']['code']
                if unique_ap_codes:
                    for item in unique_ap_codes:
                        if item['code'] == code:
                            offset = item['offset']
                            break
                if offset == -1:
                    timezone = AP_DBs_main.get_timezone_from_db(code)
                    data['code'] = code
                    if timezone != -1:
                        offset = timezone['dstOffset'] + timezone['rawOffset']
                    else:
                        offset = 0
                    data['offset'] = offset
                    unique_ap_codes.append(data)
                leg['segments'][0]['origin']['local_offset'] = offset
                data = {'code': "", 'offset': ""}
                offset = -1
                code = leg['segments'][-1]['destination']['airport']['code']
                if unique_ap_codes:
                    for item in unique_ap_codes:
                        if item['code'] == code:
                            offset = item['offset']
                            break
                if offset == -1:
                    timezone = AP_DBs_main.get_timezone_from_db(code)
                    data['code'] = code
                    if timezone != -1:
                        offset = timezone['dstOffset'] + timezone['rawOffset']
                    else:
                        offset = 0
                    data['offset'] = offset
                    unique_ap_codes.append(data)
                leg['segments'][-1]['destination']['local_offset'] = offset


    @staticmethod
    def Convert_TBS_to_Internal_Format(results,num_legs):

        """
        Takes TBS results and returns a JSON which the calculator can read.
        """
        logging.info('running Convert_TBS_to_Inner_Format()')
        itineraries = []
        for query_results in results:  # results has answers from multiple queries
            root = ET.fromstring(query_results)
            if len(root[0][0]) == 0:
                    logging.info("Query returned no errors")
                    continue
            elif 'Errors' in root[0][0][0].tag:
                for error in root[0][0][0]:
                    logging.info("Query returned an error: %s" % error[0].text)
                continue
            PricedItineraries = root[0][0][1]
            ResultCode = PricedItineraries.get("ResultCode")  # this code is required for booking
            for PricedItinerary in PricedItineraries:  # flights are grouped by price
                Price = PricedItinerary[0]
                FareDetails = PricedItinerary[1]
                AllRoutes = PricedItinerary.findall('{http://tbs.dcsplus.net/ws/1.0/}Routes')
                Itineraries = PricedItinerary.findall('{http://tbs.dcsplus.net/ws/1.0/}Itinerary')

                # get all legs in searchable data structure
                legs = {}
                for Routes in AllRoutes:
                    Index = Routes.get("Index")
                    legs[Index] = {}
                    for Route in Routes:  # Routes
                        Ref = Route.get('Ref')
                        Duration = int(Route.get("Duration").split(':')[0]) * 60 + int(Route.get("Duration").split(':')[1]) \
                                    if "Duration" in Route.keys() else "N/A"
                        leg = {'duration': Duration, 'segments': []}
                        legs[Index][Ref] = leg
                        for Segment in Route:  # Route
                            Origin = Segment[0]
                            O_Airport = Segment[0][0]
                            Destination = Segment[1]
                            D_Airport = Segment[1][0]
                            Carrier = Segment[2]
                            Flight = Segment[3]
                            Aircraft = Segment[4]

                            epoch_o = Itinerary_Filters.conv_date_time_to_epoch(str(Origin.get('Date')), str(Origin.get('Time')))
                            epoch_d = Itinerary_Filters.conv_date_time_to_epoch(str(Destination.get('Date')),str(Destination.get('Time')))

                            segment = {"origin": {
                                "date": str(Origin.get('Date')),
                                "time": str(Origin.get('Time')),
                                "epoch": epoch_o,
                                # "local_epoch": 0,
                                "terminal:": Origin.get('Terminal', 'N/A'),
                                "airport": {"name": O_Airport.text,
                                            "city": O_Airport.get('City'),
                                            "code": O_Airport.get('Code'),
                                            "city_code": O_Airport.get('CityCode'),
                                            "city_id": O_Airport.get('CityId')}
                            },
                                "destination": {
                                    "date": str(Destination.get('Date')),
                                    "time": str(Destination.get('Time')),
                                     "epoch": epoch_d,
                                    # "local_epoch": 0,
                                    "terminal:": Destination.get('Terminal', 'N/A'),
                                    "airport": {"name": D_Airport.text,
                                                "city": D_Airport.get('City'),
                                                "code": D_Airport.get('Code'),
                                                "city_code": D_Airport.get('CityCode'),
                                                "city_id": D_Airport.get('CityId')}
                                },
                                "flight": {
                                    "duration": Flight.get("Duration", "N/A"),
                                    "meal": Flight.get("Meal", "N/A"),
                                    "number": Flight.get("Number"),
                                    "class": Flight.get("Class"),
                                    "cabin_type": Flight.get("CabinType"),
                                    "stop_time": Flight.get("StopTime", "00:00"),
                                    "number_of_seats": Flight.get("NumberOfSeats"),
                                    # "CodeShareInfo": tbs_segment.Flight._CodeShareInfo,
                                },
                                "aircraft": {"name": Aircraft.text,
                                             "code": Aircraft.get('Code')
                                             },
                                "carrier": {"marketing": {"name": Carrier[0].text, "code": Carrier[0].get("Code")},
                                            "operating": {"name": Carrier[1].text, "code": Carrier[1].get("Code")},
                                            },
                                #                 "mileage": "N/A",
                            }
                            leg['segments'].append(segment)

                        # move the stop time from the next leg to the current leg
                        FlightsMain.fix_stop_time_in_leg(leg)

                for Itinerary in Itineraries:
                    if len(Itinerary) < num_legs:
                        logging.error("Route has %s legs instead of %s" % (len(Routes), num_legs))
                        break
                    itinerary = {'api': 'TBS',
                                 'id': Itinerary.get("ItineraryCode"),
                                 'resultCode': ResultCode,
                                 'price': float(Price.get("Amount")),
                                 'currency': Price.get("Currency"),
                                 'fare_details': {"IsAutoTicketable": FareDetails.get("IsAutoTicketable"),
                                                  "Currency": FareDetails.get("Currency"),
                                                  "BaseFare": FareDetails.get("BaseFare"),
                                                  "FullFare": FareDetails.get("FullFare"),
                                                  "ServiceFee": FareDetails.get("ServiceFee"),
                                                  "ValidatingCarrier": FareDetails[2].text if len(FareDetails)==3 else "N/A",
                                                  "ValidatingCarrierCode": FareDetails[2].get("Code") if len(FareDetails)==3 else "N/A",
                                                  "TimeLimit": FareDetails[1].text,
                                                  "PaxFare": [{"count": pax.get("Count"),
                                                               "FullFare": pax.get("FullFare"),
                                                               "BaseFare": pax.get("BaseFare"),
                                                               "ServiceFee": pax.get("ServiceFee"),
                                                               "Currency": pax.get("Currency"),
                                                               "PTC": pax.get("PTC")} for pax in FareDetails.findall('{http://tbs.dcsplus.net/ws/1.0/}PaxFare')],
                                                  },
                                 'legs': [],
                                 }
                    for tbs_leg in Itinerary:
                        # logging.info("Leg: Index %s, Ref: %s" % (tbs_leg.get("Index"), tbs_leg.get("Ref")))

                        itinerary['legs'].append(legs[tbs_leg.get("Index")][tbs_leg.get("Ref")])
                    TBS_Functions.stop_time_calc(itinerary)
                    itineraries.append(itinerary)
                        # Itineraries(parent=ancestor_key, index=0, itinerary=json.dumps(itinerary)).put()
        TBS_Functions.add_local_epoch(itineraries)
        return itineraries


    @staticmethod
    def Send_TBS_Query(query, action):
        from api.FlightsArea import TBS_Functions
        #logging.info("Sending query to TBS server")
        encoded_request = query.encode('utf-8')
        url = "https://www.talmatravel.co.il/tbs/reseller/ws/"
        headers = {"Host": "www.talmatravel.co.il",
                   "Content-Type": "text/xml;charset=UTF-8",
                   "Content-Length": str(len(encoded_request)),
                   "SOAPAction": action,
                   "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
                   "Connection": "Keep-Alive"}
        urlfetch.set_default_fetch_deadline(120)
        for i in range(5):
            try:
                response = urlfetch.fetch(url=url, payload=encoded_request, headers=headers, method=urlfetch.POST)
                logging.info("Got response from TBS server")
                return response.content
                break
            except Exception,e :
                logging.error(e)

        return """<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
	xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:ns1="http://tbs.dcsplus.net/ws/1.0/">
	<SOAP-ENV:Body>
		<ns1:Air_LowFareSearchRS TimeStamp="2016-11-27T20:46:31+02:00" Version="1.0.22" RequestHost="84.108.34.70" TransactionID="20161127204631547811">
			<ns1:Errors>
				<ns1:Error Code="E.AIR1.006">
					<ns1:Message>Connection reset by peer - could not connect to TBS server.</ns1:Message>
				</ns1:Error>
			</ns1:Errors>
		</ns1:Air_LowFareSearchRS>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>"""


    @staticmethod
    def Send_TBS_Query_Wrapper(query, queue, action):
        queue.put(TBS_Functions.Send_TBS_Query(query, action))

    @staticmethod
    def Query_Flights_TBS_Servers(queries,self):
        # ToDo: create a threadpool using this code: https://www.metachris.com/2016/04/python-threadpool/
        from api.FlightsArea import TBS_Functions
        import threading
        import Queue
        q = Queue.Queue()
        threads = []
        def chunks(l, n):
            """Yield successive n-sized chunks from l."""
            for i in range(0, len(l), n):
                yield l[i:i + n]

        api_results = self.app.config.get('api_results')
        user = self.user_info['token_ts']
        api_results.clear_progress(user)
        api_results.add_progress(user,"Sending total of %s queries" % len(queries))
        logging.info("Sending total of %s queries" % len(queries))
        chunk_size=50
        grouped_queries = chunks(queries,chunk_size) # divide queries to groups of 10 elements each
        total_groups = int(len(queries)/chunk_size)
        for i,group in enumerate(grouped_queries,1):
            logging.info("PROGRESS: %s" % api_results.get_progress(user))
            if not api_results.get_progress(user)['stop']:
                logging.info("Sending group %s of %s" % (i,total_groups+1))
                api_results.add_progress(user, "Sending group %s of %s" % (i,total_groups+1))
                for query in group:
                    #logging.info("Sending query to TBS server (using threads)")
                    #logging.info(query)
                    t = threading.Thread(target=TBS_Functions.Send_TBS_Query_Wrapper, args=(query, q,"http://tbs.dcsplus.net/ws/Air_LowFareSearch"))
                    t.daemon = True
                    t.start()
                    threads.append(t)
                logging.info("joining threads")
                for t in threads:
                    t.join()
            else:
                logging.info("Forced stopping queries")
                break


        logging.info("joining threads finished")
        results = []
        for i, query in enumerate(queries):
            logging.debug("Waiting for result %s" % i)
            res = q.get()
            logging.info("Adding result %s" % i)
            results.append(res)
        return results

    @staticmethod
    def Create_Booking_XML(QueryResultCode, itineraryID, passengers, passenger_lead_index):
        logging.info("Passengers: %s" % passengers)
        """ Book an itinerary in the TBS servers.
            Params:
                itinerary: an itinerary from our data
                passengers: a list of passengers in the following format:
                    {   'PaxRef': 'Passenger unique ID',
                        'Title': 'mr/mrs/mss',
                        'FirstName': 'FirstName',
                        'LastName': 'LastName',
                        'BirthDate': 'BirthDate',
                        'Email': 'Email',
                        'Type': 'adult/child',
                        'PTC': 'ADT/YTH/CHD/SEN/INS/INF',
                    }
                passenger_lead_index: the index of the lead passenger. 0 for the 1st one.
        """
        # Using string format for templating. ExternalRef and FrequentTraveller are not mandatory
        booking_template = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                            xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
                            <soapenv:Header>
                            <ns:AuthHeader>
                                     <ns:ResellerCode>V2CC</ns:ResellerCode>
                                     <ns:Username>tomer</ns:Username>
                                     <ns:Password>tomer1</ns:Password>
                            </ns:AuthHeader>
                            </soapenv:Header>
                            <soapenv:Body>
                            <ns:Air_MakeReservationRQ>
                            <ns:ExternalRef>{ExternalRef}</ns:ExternalRef>
                            <ns:Passengers>
                            {Passengers}
                            </ns:Passengers>
                            <ns:ServiceConfig>
                                <ns:AirService ResultCode="{AirServiceCode}">
                                    <ns:ItineraryCode>{ItineraryCode}</ns:ItineraryCode>
                                    <ns:Passengers>
                                    {PaxRefList}
                                    <ns:SeatRequests>
                                        {SeatRequestsList}
                                    </ns:SeatRequests>
                                    </ns:Passengers>
                                    <ns:Comments>{Comments}</ns:Comments>
                                </ns:AirService>
                            </ns:ServiceConfig>
                            </ns:Air_MakeReservationRQ>
                            </soapenv:Body>
                            </soapenv:Envelope>"""

        # Using dict based templating, witth the %(key)s format.
        passenger_template = """<ns:Passenger PaxID="%(PaxID)s" PaxRef="%(PaxRef)s" Type="%(Type)s" Lead="%(Lead)s">
                                <ns:Title>%(Title)s</ns:Title>
                                <ns:FirstName>%(FirstName)s</ns:FirstName>
                                <ns:LastName>%(LastName)s</ns:LastName>
                                <ns:BirthDate>%(BirthDate)s</ns:BirthDate>
                                <ns:Email>%(Email)s</ns:Email>
                                <ns:IdentificationDocument Type="%(document_type)s">
                                  <ns:Number>%(document_number)s</ns:Number>
                                  <ns:ExpiryDate>%(document_expiry_date)s</ns:ExpiryDate>
                                  <ns:IssuingCountry>%(document_issue_country)s</ns:IssuingCountry>
                                  <ns:PaxNationality>%(document_nationality)s</ns:PaxNationality>
                                </ns:IdentificationDocument>
                            </ns:Passenger> """

        pax_ref = """<ns:PaxRef PTC="{PTC}">{PaxRef}</ns:PaxRef>"""

        seat_request = """<ns:SeatRequest PaxRef="{PaxRef}" Preference="{Preference}"> </ns:SeatRequest>"""




        passengers_xml = []
        pax_ref_xml = []
        seat_requests_xml = []

        for i, passenger in enumerate(passengers, 1):
            # I don't know that PaxID is, so I'll use index
            passenger['PaxID'] = i
            passenger['PaxRef'] = i

            if i == passenger_lead_index:
                passenger['Lead'] = "true"
            else:
                passenger['Lead'] = "false"
            passengers_xml.append(passenger_template % passenger)  # formatting using the "passenger" dict.


            pax_ref_xml.append(pax_ref.format(PTC=passenger['PTC'], PaxRef=passenger['PaxRef']))
            if passenger['seat_preference'] == "W" or passenger['seat_preference'] == "A":
                seat_requests_xml.append(seat_request.format(PaxRef=passenger['PaxRef'],Preference=passenger['seat_preference']))

        booking = booking_template.format(Passengers='\n'.join(passengers_xml),
                                          AirServiceCode=QueryResultCode,
                                          PaxRefList='\n'.join(pax_ref_xml),
                                          SeatRequestsList='\n'.join(seat_requests_xml),
                                          Comments="webservice reservation - comments",
                                          ExternalRef="",
                                          ItineraryCode=itineraryID)
        return booking

    @staticmethod
    def Cancle_Booked_Itinerary(ReservationID):
        template = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
   <soapenv:Header>
      <ns:AuthHeader>
         <ns:ResellerCode>V2CC</ns:ResellerCode>
         <ns:Username>tomer</ns:Username>
         <ns:Password>tomer1</ns:Password>
         <!--Optional:-->
         <ns:ApplicationId>?</ns:ApplicationId>
      </ns:AuthHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns:ReservationCancelRQ>
         <ns:ReservationID>{ReservationID}</ns:ReservationID>
      </ns:ReservationCancelRQ>
   </soapenv:Body>
</soapenv:Envelope>"""

        return template.format(ReservationID=ReservationID)


    @staticmethod
    def create_list_data(itineraries, or_dest): #, user_inputs):
        list1 = []
        for leg_index in range(len(itineraries[0]['legs'])):
            list_per_leg = []
            if or_dest == "origin":# and user_inputs["list_legs"][leg_index]["origin_input"]: #if there is address
                for itinerary in itineraries:
                    dict = {'airport': "", 'date': "", 'time': "", 'offset': ""}
                    # if or_dest == "origin":
                    airport = itinerary['legs'][leg_index]['segments'][0]['origin']['airport']['code']
                    flight_date = itinerary['legs'][leg_index]['segments'][0]['origin']['date']
                    flight_time = itinerary['legs'][leg_index]['segments'][0]['origin']['time']
                    ap_time_offset = itinerary['legs'][leg_index]['segments'][0]['origin']['local_offset']
                    # else:
                    #     airport = itinerary['legs'][leg_index]['segments'][-1]['destination']['airport']['code']
                    #     flight_date = itinerary['legs'][leg_index]['segments'][-1]['destination']['date']
                    #     flight_time = itinerary['legs'][leg_index]['segments'][-1]['destination']['time']
                    dict['airport'] = airport
                    dict['date'] = flight_date
                    dict['time'] = flight_time
                    dict['offset'] = ap_time_offset
                    list_per_leg.append(dict)
                list1.append(list_per_leg)
            if or_dest == "destination":# and user_inputs["list_legs"][leg_index]["destination_input"]:
                for itinerary in itineraries:
                    dict = {'airport': "", 'date': "", 'time': "", 'offset': ""}
                    # if or_dest == "origin":
                    # airport = itinerary['legs'][leg_index]['segments'][0]['origin']['airport']['code']
                    # flight_date = itinerary['legs'][leg_index]['segments'][0]['origin']['date']
                    # flight_time = itinerary['legs'][leg_index]['segments'][0]['origin']['time']
                    # else:
                    airport = itinerary['legs'][leg_index]['segments'][-1]['destination']['airport']['code']
                    flight_date = itinerary['legs'][leg_index]['segments'][-1]['destination']['date']
                    flight_time = itinerary['legs'][leg_index]['segments'][-1]['destination']['time']
                    ap_time_offset = itinerary['legs'][leg_index]['segments'][-1]['destination']['local_offset']
                    dict['airport'] = airport
                    dict['date'] = flight_date
                    dict['time'] = flight_time
                    dict['offset'] = ap_time_offset
                    list_per_leg.append(dict)
                list1.append(list_per_leg)
        return list1

    @staticmethod
    def create_unique_airports_list(list1):
        airports_list = []
        for leg_index in range(len(list1)):
            unique_airports_set_per_leg = []
            for dict in list1[leg_index]:
                unique_airports_set_per_leg.append(dict['airport'])
            unique_airports_set_per_leg = list(set(unique_airports_set_per_leg))
            airports_list.append(unique_airports_set_per_leg)

        return airports_list

    @staticmethod
    def min_gap_list(epoch_uniq):
        gap = 1800
        epoch_uniq_min_gap = [epoch_uniq[0]]
        last_entered = epoch_uniq[0]
        for epoch_uniq_index in range(len(epoch_uniq)):
            if (last_entered + gap) <= epoch_uniq[epoch_uniq_index]:
                epoch_uniq_min_gap.append(epoch_uniq[epoch_uniq_index])
                last_entered = epoch_uniq[epoch_uniq_index]
        return epoch_uniq_min_gap

    @staticmethod
    def create_time_date_per_leg_per_ap(list1, list_uniq):
        list_times_per_airports = []
        for leg_index in range(len(list_uniq)):
            times_per_ap_per_leg = []
            for ap_code in list_uniq[leg_index]:
                dict_ap_time = {'airport': ap_code, 'epoch': [], 'epoch_uniq': [], 'epoch_uniq_min_gap': [], 'drive_to_ap': [],'drive_with_traffic_to_ap':[], 'pub_trans_to_ap': [], 'drive_from_ap': [],'drive_with_traffic_from_ap':[],'pub_trans_from_ap':[] }
                for leg in list1[leg_index]:
                    if leg['airport'] == ap_code:
                        epoch = Itinerary_Filters.conv_date_time_to_epoch(leg['date'], leg['time'])
                        epoch = epoch + leg['offset']
                        dict_ap_time['epoch'].append(epoch)
                dict_ap_time['epoch_uniq'] = sorted(list(set(dict_ap_time['epoch'])))
                dict_ap_time['epoch_uniq_min_gap'] = TBS_Functions.min_gap_list(dict_ap_time['epoch_uniq'])
                times_per_ap_per_leg.append(dict_ap_time)
            list_times_per_airports.append(times_per_ap_per_leg)
        return list_times_per_airports

    @staticmethod
    def internal_JSON_land_calc(itineraries, user_inputs):
        gap = 1800
        #drive with traffic : don't calculate more than once in this gap between 2 uniq epoches
        if len(itineraries) == 0:
            return itineraries

        #logging.info("origin_ap new %s" % origin_ap)
        GM_API_key= "AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg"
        from traffic_calculation import traffic_calculation
        from datetime import datetime, timedelta
        import json

        fixed_value = 0

        ######################################################################
        # Create list with unique airports and all flights times per airport #
        ######################################################################
        logging.info("start land_calc")
        list_data_orig = copy.deepcopy(TBS_Functions.create_list_data(itineraries, "origin"))#,user_inputs))
        list_data_dest = copy.deepcopy(TBS_Functions.create_list_data(itineraries, "destination"))#,user_inputs))
        list_data_orig_forsend = copy.deepcopy(list_data_orig)
        list_data_dest_forsend = copy.deepcopy(list_data_dest)
        list_data_uniq_orig = copy.deepcopy(TBS_Functions.create_unique_airports_list(list_data_orig_forsend))
        list_data_uniq_dest = copy.deepcopy(TBS_Functions.create_unique_airports_list(list_data_dest_forsend))
        list_ap_times_comb_orig=TBS_Functions.create_time_date_per_leg_per_ap(list_data_orig_forsend, list_data_uniq_orig)
        list_ap_times_comb_dest=TBS_Functions.create_time_date_per_leg_per_ap(list_data_dest_forsend, list_data_uniq_dest)

        ################################################################
        # Add results of drive w/o traffic to the list - 1 per airport #
        ################################################################
        for leg_index in range(len(list_ap_times_comb_orig)):

            origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
            addr_geo = google_maps.get_geo_string_result(origin_address)

            for ap_data_index in range(len(list_ap_times_comb_orig[leg_index])):

                land_calc_res = {'drive_to_ap':{}}

                land_calc_res['drive_to_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value #todo + TBD : fix the fixed_value
                }


                if origin_address:
                    if addr_geo != '0,0': #'0,0' if there isn't geocode
                        origin_ap = list_ap_times_comb_orig[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(origin_ap)

                        drive_json = traffic_calculation.drive(addr_geo, ap_location, GM_API_key, "", "", 0)
                         # fills drive to airport
                        land_calc_res['drive_to_ap'] = {
                            'duration_value': drive_json['drive'][0].get('duration_value', fixed_value) / 60,
                        # todo + TBD: fix the fixed_value
                            'distance_value': drive_json['drive'][0].get('distance_value', fixed_value)
                        # todo + TBD : fix the fixed_value
                        }
                        list_ap_times_comb_orig[leg_index][ap_data_index]['drive_to_ap']=land_calc_res['drive_to_ap']


        for leg_index in range(len(list_ap_times_comb_dest)):

            dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
            addr_geo = google_maps.get_geo_string_result(dest_address)

            for ap_data_index in range(len(list_ap_times_comb_dest[leg_index])):

                land_calc_res = {'drive_from_ap':{}}

                land_calc_res['drive_from_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value #todo + TBD : fix the fixed_value
                }


                if dest_address:
                    if addr_geo != '0,0': #'0,0' if there isn't geocode
                        dest_ap = list_ap_times_comb_dest[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(dest_ap)

                        drive_json = traffic_calculation.drive(ap_location,addr_geo , GM_API_key, "", "", 0)
                         # fills drive to airport
                        land_calc_res['drive_from_ap'] = {
                            'duration_value': drive_json['drive'][0].get('duration_value', fixed_value) / 60,
                        # todo + TBD: fix the fixed_value
                            'distance_value': drive_json['drive'][0].get('distance_value', fixed_value)
                        # todo + TBD : fix the fixed_value
                        }
                        list_ap_times_comb_dest[leg_index][ap_data_index]['drive_from_ap']=land_calc_res['drive_from_ap']

        #################################################
        # Add results of drive with traffic to the list #
        #################################################
        for leg_index in range(len(list_ap_times_comb_orig)):
            origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
            addr_geo = google_maps.get_geo_string_result(origin_address)

            for ap_data in list_ap_times_comb_orig[leg_index]:

                if origin_address:
                    if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                        origin_ap = ap_data['airport']
                        ap_location = AP_DBs_main.get_ap_location(origin_ap)

                        for epoch_uniq_index in range(len(ap_data['epoch_uniq_min_gap'])):

                            drive_time_wo_traffic = ap_data['drive_to_ap']['duration_value']
                            time_to_arrive_before_flight_to_ap= 150*60  # 2.5h in seconds. todo : get parameter from user
                            # todo : can create another query if the traffic is bigger than ~15 min for improving time of search
                            epoch_start_drive = int(ap_data['epoch_uniq_min_gap'][epoch_uniq_index] - time_to_arrive_before_flight_to_ap -drive_time_wo_traffic) # calculate 2.5 hours before flight minus the drive time w/o traffic
                            land_calc_res = {'drive_with_traffic_to_ap':{}}

                            land_calc_res['drive_with_traffic_to_ap'] = {
                                'epoch': fixed_value,
                                'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                                'distance_value': fixed_value, #todo + TBD : fix the fixed_value
                                'est_start_drive_traf': fixed_value #todo + TBD : fix the fixed date
                            }

                            drive_traffic_json = traffic_calculation.drive(addr_geo, ap_location, GM_API_key,
                                                                           epoch_start_drive, "best_guess", 1)
                            # when start driving:
                            if drive_traffic_json['drive'][0]["traffic_duriation"].get('traffic_duriation_value'):
                                estimate_time_start_drive = ap_data['epoch_uniq_min_gap'][epoch_uniq_index] - time_to_arrive_before_flight_to_ap - drive_traffic_json['drive'][0]["traffic_duriation"]['traffic_duriation_value']
                            else:
                                estimate_time_start_drive = 0

                             # fills drive to airport
                            land_calc_res['drive_with_traffic_to_ap'] = {
                                                'epoch': ap_data['epoch_uniq_min_gap'][epoch_uniq_index],
                                                "duration_value": drive_traffic_json['drive'][0]["traffic_duriation"].get('traffic_duriation_value',fixed_value)/60, #todo + TBD: fix the fixed_value
                                                "distance_value": drive_traffic_json['drive'][0].get('distance_value',fixed_value) ,
                                                "est_start_drive_traf": estimate_time_start_drive
                            }

                            ap_data['drive_with_traffic_to_ap'].append(land_calc_res['drive_with_traffic_to_ap'])

        for leg_index in range(len(list_ap_times_comb_dest)):  #each leg
            dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
            addr_geo = google_maps.get_geo_string_result(dest_address)

            for ap_data in list_ap_times_comb_dest[leg_index]: #each ap in a leg

                if dest_address:
                    if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                        dest_ap = ap_data['airport']
                        ap_location = AP_DBs_main.get_ap_location(dest_ap)

                        for epoch_uniq_index in range(len(ap_data['epoch_uniq_min_gap'])):

                            time_to_get_car_after_flight = 60 * 60  # 1h in seconds. todo : get parameter from user
                            epoch_start_drive = int(ap_data['epoch_uniq_min_gap'][epoch_uniq_index] + time_to_get_car_after_flight)  # calculate 1 hours after flight
                            land_calc_res = {'drive_with_traffic_from_ap': {}}

                            land_calc_res['drive_with_traffic_from_ap'] = {
                                'epoch': fixed_value,
                                'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'est_finish_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                            }

                            drive_traffic_json = traffic_calculation.drive( ap_location,addr_geo, GM_API_key,
                                                                            epoch_start_drive, "best_guess", 1)
                            # when finish driving:
                            if drive_traffic_json['drive'][0]["traffic_duriation"].get('traffic_duriation_value'):
                                estimated_time_finish_driving = epoch_start_drive + drive_traffic_json['drive'][0]["traffic_duriation"]['traffic_duriation_value']
                            else:
                                estimated_time_finish_driving = 0

                                # fills drive from airport
                            land_calc_res['drive_with_traffic_from_ap'] = {
                                'epoch': ap_data['epoch_uniq_min_gap'][epoch_uniq_index],
                                "duration_value": drive_traffic_json['drive'][0]["traffic_duriation"].get(
                                    'traffic_duriation_value', fixed_value) / 60,  # todo + TBD: fix the fixed_value
                                "distance_value": drive_traffic_json['drive'][0].get('distance_value', fixed_value),
                                "est_finish_drive_traf": estimated_time_finish_driving
                            }

                            ap_data['drive_with_traffic_from_ap'].append(land_calc_res['drive_with_traffic_from_ap'])

        #################################################
        # Add results of public transp' to the list     #
        #################################################

        for leg_index in range(len(list_ap_times_comb_orig)):
            if user_inputs["list_legs"][leg_index]['origin_public'] and user_inputs["list_legs"][leg_index]['origin_input']:
                origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
                addr_geo = google_maps.get_geo_string_result(origin_address)
                # use_pub_trans

                for ap_data in list_ap_times_comb_orig[leg_index]:

                    if origin_address:
                        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                            origin_ap = ap_data['airport']
                            ap_location = AP_DBs_main.get_ap_location(origin_ap)

                            for epoch_uniq_index in range(len(ap_data['epoch_uniq'])):

                                #drive_time_wo_traffic = ap_data['drive_to_ap']['duration_value']
                                time_to_arrive_before_flight_to_ap= 150*60  # 2.5h in seconds. todo : get parameter from user
                                # todo : can create another query if the traffic is bigger than ~15 min for improving time of search
                                epoch_arrive_ap = int(ap_data['epoch_uniq'][epoch_uniq_index] - time_to_arrive_before_flight_to_ap ) # calculate 2.5 hours before flight minus the drive time w/o traffic
                                land_calc_res = {'pub_trans_to_ap':{}}
                                land_calc_res['pub_trans_to_ap'] = {
                                    "transit": [{
                                        "start_address": origin_address,
                                        "end_address": origin_ap,
                                        "start_geo": {"lng": 0,
                                                      "lat": 0
                                                      },
                                        "end_geo": {"lng": 0,
                                                    "lat": 0
                                                    },
                                        "duration_value": 0,
                                        "distance_value": 0,
                                        "departure_time": {
                                            "date": 0,
                                            "time": 0,
                                            "departure_value": 0,
                                            "departure_text": 0,
                                            "time_zone": 0
                                        },
                                        "arrival_time": {
                                            "date": 0,
                                            "time": 0,
                                            "arrival_text": 0,
                                            "arrival_value": 0},

                                        "step": {},
                                        "number_of_walking_steps": 0,
                                        "number_of_transit_rides": 0,
                                        "total_walking_duration": 0,
                                        "total_transit_duration": 0,
                                    }]}

                                transit_json = traffic_calculation.transit_details(addr_geo, ap_location,
                                                                                   epoch_arrive_ap,
                                                                                   'transit',
                                                                                   GM_API_key, 0)
                                # # when start driving:
                                # if drive_traffic_json['drive'][0]["traffic_duriation"].get('traffic_duriation_value'):
                                #     estimate_time_start_drive =  ap_data['epoch_uniq'][epoch_uniq_index] - time_to_arrive_before_flight_to_ap - drive_traffic_json['drive'][0]["traffic_duriation"]['traffic_duriation_value']
                                # else:
                                #     estimate_time_start_drive = 0

                                 # fills drive to airport
                                land_calc_res['pub_trans_to_ap'] = {
                                    'epoch': ap_data['epoch_uniq'][epoch_uniq_index],
                                    "duration_value": int(transit_json["transit"][0]['duration_value'])/60,
                                    "distance_value": transit_json["transit"][0]['distance_value'],
                                    "start_address": transit_json['transit'][0]['start_address'],
                                    "end_address": transit_json['transit'][0]['end_address'],
                                    "start_geo": transit_json["transit"][0]['start_geo'],
                                    "end_geo": transit_json["transit"][0]['end_geo'],
                                    "departure_time": transit_json["transit"][0]['departure_time'],
                                    "arrival_time": transit_json["transit"][0]['arrival_time'],
                                    "total_walking_duration": transit_json['transit'][0]['total_walking_duration'],
                                    "number_of_walking_steps": transit_json['transit'][0]['number_of_walking_steps'],
                                    "number_of_transit_rides": transit_json['transit'][0]['number_of_transit_rides']
                                }

                                ap_data['pub_trans_to_ap'].append(land_calc_res['pub_trans_to_ap'])


        for leg_index in range(len(list_ap_times_comb_dest)):
            if user_inputs["list_legs"][leg_index]['destination_public'] and user_inputs["list_legs"][leg_index][
                'destination_input']:
                dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
                addr_geo = google_maps.get_geo_string_result(dest_address)

                for ap_data in list_ap_times_comb_dest[leg_index]:

                    if dest_address:
                        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                            dest_ap = ap_data['airport']
                            ap_location = AP_DBs_main.get_ap_location(dest_ap)

                            for epoch_uniq_index in range(len(ap_data['epoch_uniq'])):

                                time_to_get_car_after_flight = 60 * 60  # 1h in seconds. todo : get parameter from user
                                epoch_start_pub_trans = int(ap_data['epoch_uniq'][epoch_uniq_index] + time_to_get_car_after_flight)  # calculate 1 hours after flight
                                land_calc_res = {'pub_trans_from_ap': {}}
                                land_calc_res['drive_with_traffic_from_ap'] = {
                                    'epoch': fixed_value,
                                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                                    'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                                    'est_finish_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                                }

                                transit_json = traffic_calculation.transit_details(ap_location, addr_geo,
                                                                                   epoch_start_pub_trans,
                                                                                   'transit',
                                                                                   GM_API_key, 1)

                                # when finish driving:
                                # if drive_traffic_json['drive'][0]["traffic_duriation"].get('traffic_duriation_value'):
                                #     estimated_time_finish_diriving = epoch_start_drive + drive_traffic_json['drive'][0]["traffic_duriation"]['traffic_duriation_value']
                                # else:
                                #     estimated_time_finish_diriving = 0

                                    # fills drive from airport
                                land_calc_res['pub_trans_from_ap'] = {
                                    'epoch': ap_data['epoch_uniq'][epoch_uniq_index],
                                    "duration_value": int(transit_json["transit"][0]['duration_value'])/60,
                                    "distance_value": transit_json["transit"][0]['distance_value'],
                                    "start_address": transit_json['transit'][0]['start_address'],
                                    "end_address": transit_json['transit'][0]['end_address'],
                                    "start_geo": transit_json["transit"][0]['start_geo'],
                                    "end_geo": transit_json["transit"][0]['end_geo'],
                                    "departure_time": transit_json["transit"][0]['departure_time'],
                                    "arrival_time": transit_json["transit"][0]['arrival_time'],
                                    "total_walking_duration": transit_json['transit'][0]['total_walking_duration'],
                                    "number_of_walking_steps": transit_json['transit'][0]['number_of_walking_steps'],
                                    "number_of_transit_rides": transit_json['transit'][0]['number_of_transit_rides']
                                }

                                ap_data['pub_trans_from_ap'].append(land_calc_res['pub_trans_from_ap'])




        ########################################
        # original - all queries per itinerary #
        ########################################

        # logging.info("list_ap_times_comb_orig : %s" %list_ap_times_comb_orig)
        # logging.info("list_ap_times_comb_dest : %s" %list_ap_times_comb_dest)

        new_itineraries = []
        for itinerary in itineraries:
            for leg_index, leg in enumerate(itinerary['legs']):
                # set json's fields (orig)
                leg['drive_with_traffic_to_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value, #todo + TBD : fix the fixed_value
                    'est_start_drive_traf': fixed_value #todo + TBD : fix the fixed date
                }
                itinerary['legs'][leg_index]['drive_to_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value #todo + TBD : fix the fixed_value
                }

                leg['drive_with_traffic_from_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value, #todo + TBD : fix the fixed_value
                    'est_finish_drive_traf': fixed_value #todo + TBD : fix the fixed date
                }
                itinerary['legs'][leg_index]['drive_from_ap'] = {
                    'duration_value': fixed_value, #todo + TBD : fix the fixed_value
                    'distance_value': fixed_value #todo + TBD : fix the fixed_value
                }

                itinerary['legs'][leg_index]['pub_trans_to_ap'] = {
                    'epoch': fixed_value,
                    "duration_value": fixed_value,
                    "distance_value": fixed_value,
                    "start_address": fixed_value,
                    "end_address": fixed_value,
                    "start_geo": fixed_value,
                    "end_geo": fixed_value,
                    "departure_time": fixed_value,
                    "arrival_time": fixed_value,
                    "total_walking_duration": fixed_value,
                    "number_of_walking_steps": fixed_value,
                    "number_of_transit_rides": fixed_value
                }

                itinerary['legs'][leg_index]['pub_trans_from_ap'] = {
                    'epoch': fixed_value,
                    "duration_value": fixed_value,
                    "distance_value": fixed_value,
                    "start_address": fixed_value,
                    "end_address": fixed_value,
                    "start_geo": fixed_value,
                    "end_geo": fixed_value,
                    "departure_time": fixed_value,
                    "arrival_time": fixed_value,
                    "total_walking_duration": fixed_value,
                    "number_of_walking_steps": fixed_value,
                    "number_of_transit_rides": fixed_value
                }

                origin_address = user_inputs["list_legs"][leg_index]["origin_input"]

                if origin_address:
                    origin_ap = itinerary['legs'][leg_index]['segments'][0]['origin']['airport']['code']

                    for ap_uniq_index, ap_uniq_data in enumerate(list_ap_times_comb_orig[leg_index]):
                        itinerary_epoch = 0
                        if ap_uniq_data['airport'] == origin_ap: #'0,0' if there isn't geocode
                            if len(ap_uniq_data['drive_to_ap'])==2:  # there is data to use => there is valid orig address

                             # todo: fix it: leg['arriving_to_ap_time'] = arriving_to_ap_time
                            #estimated_duration_time = str(arriving_to_ap_time - timedelta(seconds=seconds)).split(" ")
                            # todo: fix it itinerary['legs'][i]['arriving_to_ap_time'] = arriving_to_ap_time

                                # fills drive to airport
                                itinerary['legs'][leg_index]['drive_to_ap'] = ap_uniq_data['drive_to_ap']
                                # fills drive with traffic to airport
                                itinerary_epoch = Itinerary_Filters.conv_date_time_to_epoch(itinerary['legs'][leg_index]['segments'][0]['origin']['date'], itinerary['legs'][leg_index]['segments'][0]['origin']['time'])
                                offset = itinerary['legs'][leg_index]['segments'][0]['origin']['local_offset']
                                itinerary_epoch = itinerary_epoch + offset
                                for drive_w_traf_uniq_index, drive_w_traf_uniq_data in enumerate(ap_uniq_data['epoch_uniq_min_gap']):
                                    difference_epoch = itinerary_epoch - drive_w_traf_uniq_data
                                    if abs(difference_epoch) < gap:
                                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap'] = \
                                        ap_uniq_data['drive_with_traffic_to_ap'][drive_w_traf_uniq_index]
                                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap'][
                                            'est_start_drive_traf'] = \
                                        ap_uniq_data['drive_with_traffic_to_ap'][drive_w_traf_uniq_index][
                                            'est_start_drive_traf'] + difference_epoch
                                        if itinerary['legs'][leg_index]['drive_with_traffic_to_ap']['duration_value'] < \
                                                itinerary['legs'][leg_index]['drive_to_ap']['duration_value']:
                                            itinerary['legs'][leg_index]['drive_to_ap']['duration_value'] = \
                                            itinerary['legs'][leg_index]['drive_with_traffic_to_ap']['duration_value']
                                        break
                                # fills public trans' to airport
                                # if (use_pub_trans == 1):
                                for pub_trans_uniq_index, pub_trans_uniq_data in enumerate(
                                            ap_uniq_data['pub_trans_to_ap']):
                                        if pub_trans_uniq_data['epoch'] == itinerary_epoch:
                                            leg['pub_trans_to_ap'] = pub_trans_uniq_data
                                            break
                    #logging.info("itinerary['legs'][leg_index] origin: %s" % itinerary['legs'][leg_index])

                dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
                if dest_address:
                    dest_ap = itinerary['legs'][leg_index]['segments'][-1]['destination']['airport']['code']
                    for ap_uniq_index, ap_uniq_data in enumerate(list_ap_times_comb_dest[leg_index]):
                        if ap_uniq_data['airport'] == dest_ap:  # '0,0' if there isn't geocode
                            if len(ap_uniq_data['drive_from_ap']) == 2:  # there is data to use => there is valid dest address

                                # todo: fix it: leg['arriving_to_ap_time'] = arriving_to_ap_time
                                # estimated_duration_time = str(arriving_to_ap_time - timedelta(seconds=seconds)).split(" ")
                                # todo: fix it itinerary['legs'][i]['arriving_to_ap_time'] = arriving_to_ap_time

                                # fills drive from airport
                                itinerary['legs'][leg_index]['drive_from_ap'] = ap_uniq_data[
                                    'drive_from_ap']
                                # fills drive with traffic from airport
                                itinerary_epoch = Itinerary_Filters.conv_date_time_to_epoch(itinerary['legs'][leg_index]['segments'][-1]['destination']['date'],
                                    itinerary['legs'][leg_index]['segments'][-1]['destination']['time'])
                                for drive_w_traf_uniq_index, drive_w_traf_uniq_data in enumerate(
                                        ap_uniq_data['epoch_uniq_min_gap']):
                                    difference_epoch = itinerary_epoch - drive_w_traf_uniq_data
                                    if abs(difference_epoch) < gap:
                                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap'] = \
                                        ap_uniq_data['drive_with_traffic_from_ap'][drive_w_traf_uniq_index]
                                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap'][
                                            'est_finish_drive_traf'] = \
                                        ap_uniq_data['drive_with_traffic_from_ap'][drive_w_traf_uniq_index][
                                            'est_finish_drive_traf'] + difference_epoch
                                        if itinerary['legs'][leg_index]['drive_with_traffic_from_ap'][
                                            'duration_value'] < itinerary['legs'][leg_index]['drive_from_ap'][
                                            'duration_value']:
                                            itinerary['legs'][leg_index]['drive_from_ap']['duration_value'] = \
                                            itinerary['legs'][leg_index]['drive_with_traffic_from_ap']['duration_value']
                                        break
                                # fills public trans' from airport
                                # if (use_pub_trans == 1):
                                for pub_trans_uniq_index, pub_trans_uniq_data in enumerate(
                                            ap_uniq_data['pub_trans_from_ap']):
                                        if pub_trans_uniq_data['epoch'] == itinerary_epoch:
                                            leg['pub_trans_from_ap'] = pub_trans_uniq_data
                                            break
                    #logging.info("itinerary['legs'][leg_index] dest: %s" % itinerary['legs'][leg_index])

                # # details from last flight in leg
                # leg['origin_details'] = {
                #         "origin_airport": origin_ap,
                #         "origin_address": origin_address,
                # }


                #         # fills drive with traffic from airport
                #         leg['drive_with_traffic_from_ap'] = {
                #             'distance_value': drive_traffic_json['drive'][0].get('distance_value', fixed_value),
                #             'duration_value': drive_traffic_json['drive'][0].get('duration_value',fixed_value)/60
                #         }

                #         leg['dest_details'] = {
                #             'dest_ap': dest_ap,
                #             'dest_address': dest_addr
                #         }
                #         leg['out_of_ap_time'] = str(out_of_ap_time[0]) + " " + str(out_of_ap_time[1])

            new_itineraries.append(itinerary)
        logging.info("stop land_calc")
        return new_itineraries

    @staticmethod
    def internal_JSON_land_calc_parallel(itineraries, user_inputs):
        gap = 1800
        # drive with traffic : don't calculate more than once in this gap between 2 uniq epoches
        if len(itineraries) == 0:
            return itineraries

        # logging.info("origin_ap new %s" % origin_ap)
        GM_API_key = "AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg"
        # GM_API_key = "AIzaSyBOvQwz7JTtll6MhH_8-MJ0ukCNeQiKv1Y"
        from traffic_calculation import traffic_calculation
        from datetime import datetime, timedelta
        import json

        fixed_value = 0
        all_traffic_data = {
            'with_traffic': [],
            'without_traffic': [],
            'public_transp': []
        }
        ######################################################################
        # Create list with unique airports and all flights times per airport #
        ######################################################################
        logging.info("start land_calc_new")
        list_data_orig = copy.deepcopy(TBS_Functions.create_list_data(itineraries, "origin"))  # ,user_inputs))
        list_data_dest = copy.deepcopy(TBS_Functions.create_list_data(itineraries, "destination"))  # ,user_inputs))
        list_data_orig_forsend = copy.deepcopy(list_data_orig)
        list_data_dest_forsend = copy.deepcopy(list_data_dest)
        list_data_uniq_orig = copy.deepcopy(TBS_Functions.create_unique_airports_list(list_data_orig_forsend))
        list_data_uniq_dest = copy.deepcopy(TBS_Functions.create_unique_airports_list(list_data_dest_forsend))
        list_ap_times_comb_orig = TBS_Functions.create_time_date_per_leg_per_ap(list_data_orig_forsend, list_data_uniq_orig)
        list_ap_times_comb_dest = TBS_Functions.create_time_date_per_leg_per_ap(list_data_dest_forsend, list_data_uniq_dest)

        ################################################################
        # Add results of drive w/o traffic to the list - 1 per airport #
        ################################################################
        for leg_index in range(len(list_ap_times_comb_orig)):

            origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
            addr_geo = google_maps.get_geo_string_result(origin_address)

            for ap_data_index in range(len(list_ap_times_comb_orig[leg_index])):

                land_calc_res = {'drive_to_ap': {}}

                land_calc_res['drive_to_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value  # todo + TBD : fix the fixed_value
                }

                if origin_address:
                    if addr_geo != '0,0':  # '0,0' if there isn't geocode
                        origin_ap = list_ap_times_comb_orig[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(origin_ap)
                        traffic_data = drive_calc.traffic_data(leg_index, ap_data_index, 0, 0, addr_geo, ap_location,
                                                                  GM_API_key, "", "", 0)
                        all_traffic_data['without_traffic'].append(traffic_data)

        for leg_index in range(len(list_ap_times_comb_dest)):

            dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
            addr_geo = google_maps.get_geo_string_result(dest_address)

            for ap_data_index in range(len(list_ap_times_comb_dest[leg_index])):

                land_calc_res = {'drive_from_ap': {}}

                land_calc_res['drive_from_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value  # todo + TBD : fix the fixed_value
                }

                if dest_address:
                    if addr_geo != '0,0':  # '0,0' if there isn't geocode
                        dest_ap = list_ap_times_comb_dest[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(dest_ap)
                        traffic_data = drive_calc.traffic_data(leg_index, ap_data_index, 0, 1, ap_location, addr_geo,
                                                                  GM_API_key, "", "", 0)
                        all_traffic_data['without_traffic'].append(traffic_data)

        all_traffic_data['without_traffic'] = drive_calc.get_url(all_traffic_data['without_traffic'])
        for data in all_traffic_data['without_traffic']:

            leg_index = data['leg_index']
            ap_data_index = data['ap_data_index']
            if data['origin_or_dest'] == 0:
                land_calc_res['drive_to_ap'] = {
                    'duration_value': data['result']['drive'].get('duration_value', 0) / 60,
                    'distance_value': data['result']['drive']['distance_value']
                }

                list_ap_times_comb_orig[leg_index][ap_data_index]['drive_to_ap'] = land_calc_res['drive_to_ap']
            else:
                land_calc_res['drive_from_ap'] = {
                    'duration_value': data['result']['drive'].get('duration_value', 0) / 60,
                    'distance_value': data['result']['drive']['distance_value']
                }
                list_ap_times_comb_dest[leg_index][ap_data_index]['drive_from_ap'] = land_calc_res['drive_from_ap']

        time_to_arrive_before_flight_to_ap = 150 * 60  # 2.5h in seconds. todo : get parameter from user
        time_to_get_car_after_flight = 60 * 60  # 1h in seconds. todo : get parameter from user
        for leg_index in range(len(list_ap_times_comb_orig)):
            origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
            addr_geo = google_maps.get_geo_string_result(origin_address)

            for ap_data_index in range(len(list_ap_times_comb_orig[leg_index])):

                if origin_address:
                    if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                        origin_ap = list_ap_times_comb_orig[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(origin_ap)

                        for epoch_uniq_index in range(len(list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'])):
                            drive_time_wo_traffic = list_ap_times_comb_orig[leg_index][ap_data_index]['drive_to_ap'][
                                'duration_value']

                            # todo : can create another query if the traffic is bigger than ~15 min for improving time of search
                            epoch_start_drive = int(list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'][
                                    epoch_uniq_index] - time_to_arrive_before_flight_to_ap - drive_time_wo_traffic)  # calculate 2.5 hours before flight minus the drive time w/o traffic
                            land_calc_res = {'drive_with_traffic_to_ap': {}}

                            land_calc_res['drive_with_traffic_to_ap'] = {
                                'epoch': fixed_value,
                                'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'est_start_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                            }
                            traffic_data = drive_calc.traffic_data(leg_index, ap_data_index, epoch_uniq_index, 0,
                                                                      addr_geo, ap_location, GM_API_key,
                                                                      epoch_start_drive, "best_guess", 1)
                            all_traffic_data['with_traffic'].append(traffic_data)

        for leg_index in range(len(list_ap_times_comb_dest)):  # each leg
            dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
            addr_geo = google_maps.get_geo_string_result(dest_address)

            for ap_data_index in range(len(list_ap_times_comb_dest[leg_index])):  # each ap in a leg

                if dest_address:
                    if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                        dest_ap = list_ap_times_comb_dest[leg_index][ap_data_index]['airport']
                        ap_location = AP_DBs_main.get_ap_location(dest_ap)

                        for epoch_uniq_index in range(
                                len(list_ap_times_comb_dest[leg_index][ap_data_index]['epoch_uniq_min_gap'])):
                            epoch_start_drive = int(
                                list_ap_times_comb_dest[leg_index][ap_data_index]['epoch_uniq_min_gap'][
                                    epoch_uniq_index] + time_to_get_car_after_flight)  # calculate 1 hours after flight
                            land_calc_res = {'drive_with_traffic_from_ap': {}}

                            land_calc_res['drive_with_traffic_from_ap'] = {
                                'epoch': fixed_value,
                                'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                                'est_finish_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                            }

                            traffic_data = drive_calc.traffic_data(leg_index, ap_data_index, epoch_uniq_index, 1,
                                                                      ap_location, addr_geo, GM_API_key,
                                                                      epoch_start_drive, "best_guess", 1)
                            all_traffic_data['with_traffic'].append(traffic_data)

        all_traffic_data['with_traffic'] = drive_calc.get_url(all_traffic_data['with_traffic'])

        for data in all_traffic_data['with_traffic']:
            leg_index = data['leg_index']
            ap_data_index = data['ap_data_index']
            epoch_uniq_index = data['epoch_uniq_index']
            if data['origin_or_dest'] == 0:
                if data['result']['drive']['duration_value']:
                    estimate_time_start_drive = list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'][
                                                    epoch_uniq_index] - time_to_arrive_before_flight_to_ap - \
                                                data['result']['drive']['duration_value']
                else:
                    estimate_time_start_drive = 0
                land_calc_res['drive_with_traffic_to_ap'] = {
                    'epoch': list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'][epoch_uniq_index],
                    'duration_value': data['result']['drive'].get('duration_value', 0) / 60,
                    'distance_value': data['result']['drive']['distance_value'],
                    "est_start_drive_traf": estimate_time_start_drive
                }
                list_ap_times_comb_orig[leg_index][ap_data_index]['drive_with_traffic_to_ap'].append(
                    land_calc_res['drive_with_traffic_to_ap'])
                epoch_start_drive = data['epoch_time']
                if data['result']['drive']['duration_value']:
                    estimated_time_finish_driving = epoch_start_drive + data['result']['drive']['duration_value']
                else:
                    estimated_time_finish_driving = 0
            else:
                land_calc_res['drive_with_traffic_from_ap'] = {
                    'epoch': list_ap_times_comb_dest[leg_index][ap_data_index]['epoch_uniq_min_gap'][epoch_uniq_index],
                    'duration_value': data['result']['drive'].get('duration_value', 0) / 60,
                    'distance_value': data['result']['drive']['distance_value'],
                    "est_finish_drive_traf": 0
                }
                list_ap_times_comb_dest[leg_index][ap_data_index]['drive_with_traffic_from_ap'].append(
                    land_calc_res['drive_with_traffic_from_ap'])

        # pub tranc
        for leg_index in range(len(list_ap_times_comb_orig)):
            if user_inputs["list_legs"][leg_index]['origin_public'] and user_inputs["list_legs"][leg_index][
                'origin_input']:
                origin_address = user_inputs["list_legs"][leg_index]["origin_input"]
                addr_geo = google_maps.get_geo_string_result(origin_address)
                for ap_data_index in range(len(list_ap_times_comb_orig[leg_index])):

                    if origin_address:
                        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                            origin_ap = list_ap_times_comb_orig[leg_index][ap_data_index]['airport']
                            ap_location = AP_DBs_main.get_ap_location(origin_ap)

                            for epoch_uniq_index in range(len(list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'])):
                                # drive_time_wo_traffic = ap_data['drive_to_ap']['duration_value']
                                time_to_arrive_before_flight_to_ap = 150 * 60  # 2.5h in seconds. todo : get parameter from user
                                # todo : can create another query if the traffic is bigger than ~15 min for improving time of search
                                epoch_arrive_ap = int(list_ap_times_comb_orig[leg_index][ap_data_index]['epoch_uniq_min_gap'][epoch_uniq_index] - time_to_arrive_before_flight_to_ap)  # calculate 2.5 hours before flight minus the drive time w/o traffic
                                land_calc_res = {'pub_trans_to_ap': {}}
                                land_calc_res['pub_trans_to_ap'] = {
                                    "transit": [{
                                        "start_address": origin_address,
                                        "end_address": origin_ap,
                                        "start_geo": {"lng": 0,
                                                      "lat": 0
                                                      },
                                        "end_geo": {"lng": 0,
                                                    "lat": 0
                                                    },
                                        "duration_value": 0,
                                        "distance_value": 0,
                                        "departure_time": {
                                            "date": 0,
                                            "time": 0,
                                            "departure_value": 0,
                                            "departure_text": 0,
                                            "time_zone": 0
                                        },
                                        "arrival_time": {
                                            "date": 0,
                                            "time": 0,
                                            "arrival_text": 0,
                                            "arrival_value": 0},

                                        "step": {},
                                        "number_of_walking_steps": 0,
                                        "number_of_transit_rides": 0,
                                        "total_walking_duration": 0,
                                        "total_transit_duration": 0,
                                    }]}

                                traffic_data = transit_calc.traffic_data(leg_index, ap_data_index,
                                                                         epoch_uniq_index, addr_geo,
                                                                         ap_location, epoch_arrive_ap,
                                                                         'transit', GM_API_key, 0)
                                all_traffic_data['public_transp'].append(traffic_data)

        for leg_index in range(len(list_ap_times_comb_dest)):
            if user_inputs["list_legs"][leg_index]['destination_public'] and \
                    user_inputs["list_legs"][leg_index][
                        'destination_input']:
                dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
                addr_geo = google_maps.get_geo_string_result(dest_address)

                for ap_data_index in range(len(list_ap_times_comb_dest[leg_index])):

                    if dest_address:
                        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
                            dest_ap = list_ap_times_comb_dest[leg_index][ap_data_index]['airport']
                            ap_location = AP_DBs_main.get_ap_location(dest_ap)

                            for epoch_uniq_index in range(
                                    len(list_ap_times_comb_dest[leg_index][ap_data_index]['epoch_uniq_min_gap'])):
                                time_to_get_car_after_flight = 60 * 60  # 1h in seconds. todo : get parameter from user
                                epoch_start_pub_trans = int(list_ap_times_comb_dest[leg_index][ap_data_index]['epoch_uniq_min_gap'][epoch_uniq_index] + time_to_get_car_after_flight)  # calculate 1 hours after flight
                                land_calc_res = {'pub_trans_from_ap': {}}
                                land_calc_res['pub_trans_from_ap'] = {
                                    "transit": [{
                                        "start_address": dest_address,
                                        "end_address": dest_ap,
                                        "start_geo": {"lng": 0,
                                                      "lat": 0
                                                      },
                                        "end_geo": {"lng": 0,
                                                    "lat": 0
                                                    },
                                        "duration_value": 0,
                                        "distance_value": 0,
                                        "departure_time": {
                                            "date": 0,
                                            "time": 0,
                                            "departure_value": 0,
                                            "departure_text": 0,
                                            "time_zone": 0
                                        },
                                        "arrival_time": {
                                            "date": 0,
                                            "time": 0,
                                            "arrival_text": 0,
                                            "arrival_value": 0},

                                        "step": {},
                                        "number_of_walking_steps": 0,
                                        "number_of_transit_rides": 0,
                                        "total_walking_duration": 0,
                                        "total_transit_duration": 0}]
                                }

                                traffic_data = transit_calc.traffic_data(leg_index, ap_data_index,
                                                                         epoch_uniq_index, ap_location,
                                                                         addr_geo, epoch_start_pub_trans,
                                                                         'transit', GM_API_key, 1)
                                all_traffic_data['public_transp'].append(traffic_data)

        all_traffic_data['public_transp'] = transit_calc.get_url(all_traffic_data['public_transp'])

        for data in all_traffic_data['public_transp']:
            leg_index = data['leg_index']
            ap_data_index = data['ap_data_index']
            epoch_uniq_index = data['epoch_uniq_index']
            if data['origin_or_dest'] == 0:
                land_calc_res['pub_trans_to_ap'] = {
                    'epoch': data['epoch_time'],
                    "duration_value": int(data['result']['transit'][0]['duration_value']) / 60,
                    "distance_value": data['result']["transit"][0]['distance_value'],
                    "start_address": data['result']['transit'][0]['start_address'],
                    "end_address": data['result']['transit'][0]['end_address'],
                    "start_geo": data['result']["transit"][0]['start_geo'],
                    "end_geo": data['result']["transit"][0]['end_geo'],
                    "departure_time": data['result']["transit"][0]['departure_time'],
                    "arrival_time": data['result']["transit"][0]['arrival_time'],
                    "total_walking_duration": data['result']['transit'][0]['total_walking_duration'],
                    "number_of_walking_steps": data['result']['transit'][0]['number_of_walking_steps'],
                    "number_of_transit_rides": data['result']['transit'][0]['number_of_transit_rides']
                }

                list_ap_times_comb_orig[leg_index][ap_data_index]['pub_trans_to_ap'].append(land_calc_res[
                    'pub_trans_to_ap'])

            else:
                land_calc_res['pub_trans_from_ap'] = {
                    'epoch': data['epoch_time'],
                    "duration_value": int(data['result']["transit"][0]['duration_value']) / 60,
                    "distance_value": data['result']["transit"][0]['distance_value'],
                    "start_address": data['result']['transit'][0]['start_address'],
                    "end_address": data['result']['transit'][0]['end_address'],
                    "start_geo": data['result']["transit"][0]['start_geo'],
                    "end_geo": data['result']["transit"][0]['end_geo'],
                    "departure_time": data['result']["transit"][0]['departure_time'],
                    "arrival_time": data['result']["transit"][0]['arrival_time'],
                    "total_walking_duration": data['result']['transit'][0]['total_walking_duration'],
                    "number_of_walking_steps": data['result']['transit'][0]['number_of_walking_steps'],
                    "number_of_transit_rides": data['result']['transit'][0]['number_of_transit_rides']
                }
                list_ap_times_comb_dest[leg_index][ap_data_index]['pub_trans_from_ap'].append(land_calc_res['pub_trans_from_ap'])

        ########################################
        # original - all queries per itinerary #
        ########################################

        #logging.info("list_ap_times_comb_orig : %s" %list_ap_times_comb_orig)
        #logging.info("list_ap_times_comb_dest : %s" %list_ap_times_comb_dest)

        new_itineraries = []
        for itinerary in itineraries:
            for leg_index, leg in enumerate(itinerary['legs']):
                # set json's fields (orig)
                itinerary['legs'][leg_index]['drive_with_traffic_to_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'est_start_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                }
                itinerary['legs'][leg_index]['drive_to_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value  # todo + TBD : fix the fixed_value
                }

                itinerary['legs'][leg_index]['drive_with_traffic_from_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'est_finish_drive_traf': fixed_value  # todo + TBD : fix the fixed date
                }
                itinerary['legs'][leg_index]['drive_from_ap'] = {
                    'duration_value': fixed_value,  # todo + TBD : fix the fixed_value
                    'distance_value': fixed_value  # todo + TBD : fix the fixed_value
                }

                itinerary['legs'][leg_index]['pub_trans_to_ap'] = {
                    'epoch': fixed_value,
                    "duration_value": fixed_value,
                    "distance_value": fixed_value,
                    "start_address": fixed_value,
                    "end_address": fixed_value,
                    "start_geo": fixed_value,
                    "end_geo": fixed_value,
                    "departure_time": fixed_value,
                    "arrival_time": fixed_value,
                    "total_walking_duration": fixed_value,
                    "number_of_walking_steps": fixed_value,
                    "number_of_transit_rides": fixed_value
                }

                itinerary['legs'][leg_index]['pub_trans_from_ap'] = {
                    'epoch': fixed_value,
                    "duration_value": fixed_value,
                    "distance_value": fixed_value,
                    "start_address": fixed_value,
                    "end_address": fixed_value,
                    "start_geo": fixed_value,
                    "end_geo": fixed_value,
                    "departure_time": fixed_value,
                    "arrival_time": fixed_value,
                    "total_walking_duration": fixed_value,
                    "number_of_walking_steps": fixed_value,
                    "number_of_transit_rides": fixed_value
                }

                origin_address = user_inputs["list_legs"][leg_index]["origin_input"]

                if origin_address:
                    origin_ap = itinerary['legs'][leg_index]['segments'][0]['origin']['airport']['code']
                    for ap_uniq_index, ap_uniq_data in enumerate(list_ap_times_comb_orig[leg_index]):
                        itinerary_epoch = 0
                        if ap_uniq_data['airport'] == origin_ap:  # '0,0' if there isn't geocode
                            if len(ap_uniq_data[
                                       'drive_to_ap']) == 2:  # there is data to use => there is valid orig address

                                # todo: fix it: leg['arriving_to_ap_time'] = arriving_to_ap_time
                                # estimated_duration_time = str(arriving_to_ap_time - timedelta(seconds=seconds)).split(" ")
                                # todo: fix it itinerary['legs'][i]['arriving_to_ap_time'] = arriving_to_ap_time

                                # fills drive to airport
                                itinerary['legs'][leg_index]['drive_to_ap'] = ap_uniq_data['drive_to_ap']
                                # fills drive with traffic to airport
                                itinerary_epoch = Itinerary_Filters.conv_date_time_to_epoch(
                                    itinerary['legs'][leg_index]['segments'][0]['origin']['date'],
                                    itinerary['legs'][leg_index]['segments'][0]['origin']['time'])
                                offset = itinerary['legs'][leg_index]['segments'][0]['origin']['local_offset']
                                itinerary_epoch = itinerary_epoch + offset
                                for drive_w_traf_uniq_index, drive_w_traf_uniq_data in enumerate(
                                        ap_uniq_data['epoch_uniq_min_gap']):
                                    difference_epoch = itinerary_epoch - drive_w_traf_uniq_data

                                    if abs(difference_epoch) < gap:

                                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap'] = \
                                            ap_uniq_data['drive_with_traffic_to_ap'][drive_w_traf_uniq_index]
                                        itinerary['legs'][leg_index]['drive_with_traffic_to_ap'][
                                            'est_start_drive_traf'] = \
                                            ap_uniq_data['drive_with_traffic_to_ap'][drive_w_traf_uniq_index][
                                                'est_start_drive_traf'] + difference_epoch
                                        if itinerary['legs'][leg_index]['drive_with_traffic_to_ap']['duration_value'] < \
                                                itinerary['legs'][leg_index]['drive_to_ap']['duration_value']:
                                            itinerary['legs'][leg_index]['drive_to_ap']['duration_value'] = \
                                                itinerary['legs'][leg_index]['drive_with_traffic_to_ap'][
                                                    'duration_value']

                                        break
                                # fills public trans' to airport
                                if(user_inputs["list_legs"][leg_index]['origin_public'] == 'TRUE'):
                                    for pub_trans_uniq_index, pub_trans_uniq_data in enumerate(ap_uniq_data['epoch_uniq_min_gap']):
                                        difference_epoch = itinerary_epoch - pub_trans_uniq_data
                                        if abs(difference_epoch) < gap:
                                            itinerary['legs'][leg_index]['pub_trans_to_ap'] = ap_uniq_data['pub_trans_to_ap'][pub_trans_uniq_index]
                                            break

                dest_address = user_inputs["list_legs"][leg_index]["destination_input"]
                if dest_address:

                    dest_ap = itinerary['legs'][leg_index]['segments'][-1]['destination']['airport']['code']
                    for ap_uniq_index, ap_uniq_data in enumerate(list_ap_times_comb_dest[leg_index]):
                        if ap_uniq_data['airport'] == dest_ap:  # '0,0' if there isn't geocode
                            if len(ap_uniq_data[
                                       'drive_from_ap']) == 2:  # there is data to use => there is valid dest address

                                # todo: fix it: leg['arriving_to_ap_time'] = arriving_to_ap_time
                                # estimated_duration_time = str(arriving_to_ap_time - timedelta(seconds=seconds)).split(" ")
                                # todo: fix it itinerary['legs'][i]['arriving_to_ap_time'] = arriving_to_ap_time

                                # fills drive from airport
                                itinerary['legs'][leg_index]['drive_from_ap'] = ap_uniq_data[
                                    'drive_from_ap']
                                # fills drive with traffic from airport
                                itinerary_epoch = Itinerary_Filters.conv_date_time_to_epoch(
                                    itinerary['legs'][leg_index]['segments'][-1]['destination']['date'],
                                    itinerary['legs'][leg_index]['segments'][-1]['destination']['time'])
                                offset = itinerary['legs'][leg_index]['segments'][-1]['destination']['local_offset']
                                itinerary_epoch = itinerary_epoch + offset
                                for drive_w_traf_uniq_index, drive_w_traf_uniq_data in enumerate(
                                        ap_uniq_data['epoch_uniq_min_gap']):
                                    difference_epoch = itinerary_epoch - drive_w_traf_uniq_data

                                    if abs(difference_epoch) < gap:
                                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap'] = \
                                            ap_uniq_data['drive_with_traffic_from_ap'][drive_w_traf_uniq_index]
                                        itinerary['legs'][leg_index]['drive_with_traffic_from_ap'][
                                            'est_finish_drive_traf'] = \
                                            ap_uniq_data['drive_with_traffic_from_ap'][drive_w_traf_uniq_index][
                                                'est_finish_drive_traf'] + difference_epoch
                                        if itinerary['legs'][leg_index]['drive_with_traffic_from_ap'][
                                            'duration_value'] < itinerary['legs'][leg_index]['drive_from_ap'][
                                            'duration_value']:
                                            itinerary['legs'][leg_index]['drive_from_ap']['duration_value'] = \
                                                itinerary['legs'][leg_index]['drive_with_traffic_from_ap'][
                                                    'duration_value']

                                        break
                                # fills public trans' from airport
                                if(user_inputs["list_legs"][leg_index]['destination_public'] == 'TRUE'):
                                    for pub_trans_uniq_index, pub_trans_uniq_data in enumerate(ap_uniq_data['epoch_uniq_min_gap']):
                                        difference_epoch = itinerary_epoch - pub_trans_uniq_data
                                        if abs(difference_epoch) < gap:
                                            itinerary['legs'][leg_index]['pub_trans_from_ap'] = ap_uniq_data['pub_trans_from_ap'][pub_trans_uniq_index]
                                            break
                                        # logging.info("itinerary['legs'][leg_index] dest: %s" % itinerary['legs'][leg_index])

                                        # # details from last flight in leg
                                        # leg['origin_details'] = {
                                        #         "origin_airport": origin_ap,
                                        #         "origin_address": origin_address,
                                        # }


                                        #         # fills drive with traffic from airport
                                        #         leg['drive_with_traffic_from_ap'] = {
                                        #             'distance_value': drive_traffic_json['drive'][0].get('distance_value', fixed_value),
                                        #             'duration_value': drive_traffic_json['drive'][0].get('duration_value',fixed_value)/60
                                        #         }

                                        #         leg['dest_details'] = {
                                        #             'dest_ap': dest_ap,
                                        #             'dest_address': dest_addr
                                        #         }
                                        #         leg['out_of_ap_time'] = str(out_of_ap_time[0]) + " " + str(out_of_ap_time[1])

            new_itineraries.append(itinerary)
        logging.info("stop land_calc_new")
        return new_itineraries

class QPX_Functions:
    @staticmethod
    def generate_QPX_queries(api_data):
        qpx_queries = []
        for dates in api_data['dates_combinations']:
            for airports in api_data['airport_combinations']:
                # creating qpx query using specific dates and airports:
                legs = []
                for i, leg_data in enumerate(api_data['legs_unique_data']):
                    logging.debug(
                        "creating date-airports %s-%s combinations for %s legs" % (dates[i], airports[i], len(dates)))
                    legs.append({
                        "kind": "qpxexpress#sliceInput",
                        "origin": airports[i][0],
                        "destination": airports[i][1],
                        "date": dates[i],
                        "maxStops": api_data['legs_unique_data'][i]['max_stop'],
                        "maxConnectionDuration": api_data['legs_unique_data'][i]['max_connection_duration'],
                        "preferredCabin": api_data['legs_unique_data'][i]['cabin'],
                        "permittedDepartureTime": {
                            "kind": "qpxexpress#timeOfDayRange",
                            "earliestTime": api_data['legs_unique_data'][i]['earliest_time'],
                            "latestTime": api_data['legs_unique_data'][i]['latest_time']
                        }
                    })

                qpx_queries.append({"request": {
                    "passengers": {
                        "kind": "qpxexpress#passengerCounts",
                        "adultCount": api_data['legs_common_details']['adult'],
                        "childCount": api_data['legs_common_details']['child'],
                        "infantInLapCount": api_data['legs_common_details']['infant_in_lap'],
                        "infantInSeatCount": api_data['legs_common_details']['infant_in_seat'],
                        "seniorCount": api_data['legs_common_details']['senior']
                    },
                    "slice": legs,
                    "maxPrice": api_data['legs_common_details']['max_price'],
                    "refundable": api_data['legs_common_details']['refundable'],
                    "solutions": api_data['legs_common_details']['solutions']
                }
                })
        return qpx_queries

    @staticmethod
    def qpx_flights_new(qpx_queries):
        # google apis, type:server - https://console.cloud.google.com/apis/library?project=ultra-mediator-129910
        # enable apis on 4/5/16: Google Maps Directions API , Google Maps Distance Matrix API , Google Maps Elevation API  , Google Maps Geocoding API , Google Maps Geolocation API , Google Maps JavaScript API , Google Places API Web Service , QPX Express Airfare API
        api_keys = [
            'AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg',  # user:omerkapp
            'AIzaSyDOLCuId9IzaciG0OF8SletcGcEKIQuSu8',  # user:omerkapp
            'AIzaSyBZi1wmfN1-9RIITJDJkwgWiyCsu0O_dzc',  # user:omerkapp
            'AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04',  # by elad/itamar
            'AIzaSyCS8PTaJ7f2VEJWzk-qvLhaYPH3KHeQVh0',  # by elad/itamar
            'AIzaSyCC1Ne42exQs66bT4euID-anESedD2vhts',  # user:ofircz
            'AIzaSyAXaQgGhIhIc3vahqaSbt8mBSJp_JNQIu8',  # user:ofircz
            'AIzaSyA3GOYFsJfmwXldLt8VMQphahXg-xfEli8',  # user:omerkapp
            'AIzaSyA46afJXewqOsUA04GaKjK54lUaoRw86RA',  # user:omerkapp
            'AIzaSyAHoMotgKgvLYosjhpUwp7SZ8QxS8lbuDM',  # user:omerkapp
            'AIzaSyC_scKLIqXvCZEn1A-Y57844x-9oHJJDcQ',  # user:omerkapp
            'AIzaSyCAMVScVSvWiZiMHlhaKB7nDoTMPTUiVgI',  # user:ofircz
            'AIzaSyBooLEDRhXqBu-9tNApAN9MP9eoFay7yX0',  # user:ofircz
            'AIzaSyC9GHR3v0tKi544FUHxnnc8d7iFCpS30aY'  # user:ofircz
        ]

        maps_api_key = api_keys[3]

        def sending_one_query_to_qpx(query, key):
            data = json.dumps(query)
            try:
                logging.info("Sending query to QPX server")
                logging.debug(query)
                url = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=' + api_qpx
                headers = {'Content-Type': 'application/json'}
                req = urllib2.Request(url, data, headers)
                u = urllib2.urlopen(req)
                read_url = u.read()
                results = json.loads(read_url)
            except Exception, e:
                logging.exception("Got EXCEPTION: %s" % e)
                return ['ERROR', None]
            return ['OK', results]

        key_index = 0
        api_qpx = api_keys[key_index]
        logging.info("\nSelected key: %s" % api_qpx)
        results = []
        for query in qpx_queries:
            logging.info("Sending query to QPX server")
            ans = sending_one_query_to_qpx(query, api_qpx)
            while (ans[0] == "ERROR"):
                if len(api_keys) > key_index - 2:
                    logging.info("Running again with a different key")
                    key_index = key_index + 1
                    api_qpx = api_keys[key_index]
                    logging.info("\nSelected key: %s" % api_qpx)
                    ans = sending_one_query_to_qpx(query, api_qpx)
                else:
                    logging.error("\nERROR: No more available keys!")
                    sys.exit()
            results.append(ans[1])
        return results

    @staticmethod
    def Convert_QPX_to_Our_Format(results):
        """
        Takes qpx results and returns a JSON which the calculator can read
        Params:
            results - list of dict object, a dict for each query which was sent to the API. Using QPX Format.
        Example of a structure of a leg:

            # segment is a one flight from point A to point B.
            segment = {"destination": "MAD",
                       "origin": "TLV",
                       "aircraft": "738",
                       "mileage": 2201,
                       "id": "LgK502nPCWk3Jskq",
                       "duration": 325,
                       "originTerminal": "3",
                       "arrivalTime": "2016-08-29T20:10+02:00",
                       "destinationTerminal": "1",
                       "meal": "Meal",
                       "departureTime": "2016-08-29T15:45+03:00",
                       "flight_number": "1302",
                       "flight_carrier": "UX",
                       "cabin": "COACH",
                       "bookingCode": "T",
                       "connectionDuration": 225
                       }
            # a leg is a combination of segments, which will result in a route from user's origin to destination request
            leg = { 'duration': None,
                    'segments': [segment],
                  }

            # itinerary is a combination of legs, according to user's input
            itinerary = { 'api': 'QPX',
                          'id': "KPbw3xYfdSBTExUnNqHTnG003",
                         'price': "USD1183.43",
                         'passengers': {'adults': None },
                         legs: [leg],
                        }
        """

        itineraries = []  # list of all itineraries in our format
        for query_result in results:
            query_result = json.loads(
                query_result.itinerary)  # getting results from database, need to access only flights part
            for qpx_itinerary in query_result['trips']['tripOption']:
                itinerary = {'api': 'QPX',
                             'id': qpx_itinerary['id'],
                             'price': qpx_itinerary['saleTotal'],
                             'passengers': {'adults': qpx_itinerary['pricing'][0]['passengers']['adultCount']},
                             'legs': [],
                             }
                itineraries.append(itinerary)

                for qpx_leg in qpx_itinerary['slice']:
                    leg = {'duration': qpx_leg['duration'],
                           'segments': []
                           }
                    itinerary['legs'].append(leg)

                    for qpx_slice in qpx_leg['segment']:
                        logging.debug(qpx_slice['leg'][0])
                        segment = {"destination": qpx_slice['leg'][0]['destination'],
                                   "origin": qpx_slice['leg'][0]['origin'],
                                   "id": qpx_slice['id'],
                                   "duration": qpx_slice['duration'],
                                   "originTerminal": qpx_slice['leg'][0].get('originTerminal', "N/A"),
                                   "arrivalTime": qpx_slice['leg'][0]['arrivalTime'],
                                   "destinationTerminal": qpx_slice['leg'][0].get('destinationTerminal', "N/A"),
                                   "meal": qpx_slice['leg'][0].get('meal', "N/A"),
                                   "departureTime": qpx_slice['leg'][0]['departureTime'],
                                   "flight_number": qpx_slice['flight']['number'],
                                   "flight_carrier": qpx_slice['flight']['carrier'],
                                   "cabin": qpx_slice['cabin'],
                                   "bookingCode": qpx_slice['bookingCode'],
                                   "connectionDuration": qpx_slice.get('connectionDuration', 0),
                                   "aircraft": qpx_slice['leg'][0]['aircraft'],
                                   "mileage": qpx_slice['leg'][0]['mileage'],
                                   }
                        leg['segments'].append(segment)
        return itineraries


class FlightsMain:
    @staticmethod
    def fix_arrive_dates(leg):  # add 2 dates before if by arrival is chosen in a leg
        arrival_dates = []
        for single_date in leg['dates_options']:
            date_only = single_date.split('T')[0]
            [year, month, day] = date_only.split('-')
            dateformat_single_date = datetime.datetime(int(year), int(month), int(day))
            date1 = (dateformat_single_date + datetime.timedelta(-1))
            date2 = (dateformat_single_date + datetime.timedelta(-2))
            arrival_dates.append(dateformat_single_date.isoformat())
            arrival_dates.append(date1.isoformat())
            arrival_dates.append(date2.isoformat())
        dates_options_uniq = list(set(arrival_dates))  #
        # Not order preserving - from https://www.peterbe.com/plog/uniqifiers-benchmark
        dates_options_uniq.sort()  # TBD -sort the dates. must do when using the dates_options as a list after expaning the date rnage by +/- few days. lists must be in the same length if we want to use it in a make sense way. the unique and sort should save the lenghs equal and in the right order
        return arrival_dates

    @staticmethod
    def fix_stop_time_in_leg(leg):
        for index in range(len(leg['segments'])):
            if index > 0:
                tmp = leg['segments'][index - 1]['flight']['stop_time']
                leg['segments'][index - 1]['flight']['stop_time'] = leg['segments'][index]['flight']['stop_time']
                leg['segments'][index]['flight']['stop_time'] = tmp

    # create a list with all combination of the optional origin airports and the optional destination airports
    # TBD- add the distance data for each (airport & address)
    @staticmethod
    def dates_combination(list_legs):
        def chunks(l, n):
            """Yield successive n-sized chunks from l."""
            for i in range(0, len(l), n):
                yield l[i:i + n]

        dates_list = []
        for leg in list_legs:
            if leg['arrive_depart'] == "arrive":
                # logging.info("fix by arrive")
                leg['dates_options_arrival'] = leg['dates_options']
                leg['dates_options'] = FlightsMain.fix_arrive_dates(leg) #add 2 dates before for each date
            dates_list.append(map(lambda x: x.split('T')[0], leg['dates_options']))
        logging.info("Dates List: %s" % dates_list)
        grouped_combinations = []
        all_combinations = itertools.product(*dates_list)
        for element in all_combinations:
            grouped_combinations.append([x for x in chunks(element, 1)])
        logging.info("All iterations: %s" % grouped_combinations)
        return grouped_combinations

    # create a list with all combination of the optional origin airports and the optional destination airports
    # TBD- add the distance data for each (airport & address)
    @staticmethod
    def airport_combination(list_legs,legs_common_details):
        def chunks(l, n):
            """Yield successive n-sized chunks from l."""
            for i in range(0, len(l), n):
                yield l[i:i + n]

        airport_list = []
        for leg in list_legs:
            # airport_list.append(leg['origin_airports'])
            # airport_list.append(leg['destination_airports'])
            airport_list.append(leg['origin'])
            airport_list.append(leg['destination'])
        logging.info("Airports List: %s" % airport_list)
        grouped_combinations = []
        all_combinations = itertools.product(*airport_list)
        logging.info(all_combinations)
        logging.info(legs_common_details)
        if legs_common_details['same_airport']:
            if len(list_legs) == 2:
                all_combinations = filter(lambda x: x[1]==x[2], all_combinations)
                logging.info("Filtered combinations: %s " % all_combinations)
            elif len(list_legs) == 3:
                all_combinations = filter(lambda x: x[1]==x[2] and x[3]==x[4], all_combinations)
                logging.info("Filtered combinations: %s " % all_combinations)
            elif len(list_legs) == 4:
                all_combinations = filter(lambda x: x[1] == x[2] and x[3] == x[4] and x[5]==x[6], all_combinations)
                logging.info("Filtered combinations: %s " % all_combinations)
            elif len(list_legs) == 5:
                all_combinations = filter(lambda x: x[1] == x[2] and x[3] == x[4] and x[5] == x[6] and x[7] == x[8], all_combinations)
                logging.info("Filtered combinations: %s " % all_combinations)

        for element in all_combinations:
            grouped_combinations.append([x for x in chunks(element, 2)])
        logging.info("All iterations: %s" % grouped_combinations)
        return grouped_combinations

    @staticmethod
    def GetAllFlightsCombinations(user_input, round_trip=False, return_date=''):

        legs_common_details = user_input['legs_common_details']
        list_legs = user_input['list_legs']
        # depricate the door to door feature:
        # relevant_airports = FlightsMain.get_relevant_airports(list_legs)  # Relevant airports
        # update_list_legs_airports= FlightsMain.add_relevant_airports_to_list_legs(list_legs,relevant_airports)
        # update_list_legs_distance_to_airports = FlightsMain.get_distance_one_to_many(update_list_legs_airports)
        # airport_combinations = FlightsMain.airport_combination(update_list_legs_distance_to_airports,relevant_airports)  #create all the combinations of airports
        airport_combinations = FlightsMain.airport_combination(list_legs,legs_common_details)  # create all the combinations of airports
        logging.info("airport_combinations:")
        logging.info(airport_combinations)
        # dates_combinations = FlightsMain.dates_combination(list_legs)
        dates_combinations = FlightsMain.main_dates_combination(list_legs)
        # update_list_legs_dates_days_after_before=FlightsMain.dates_days_after_before(list_legs)  #this is the only funtion that update the list leg. it uses the dates_options and for each option add all the dates in the range days_after_dates & days_before_dates
        # dates_combinations = FlightsMain.dates_combination(update_list_legs_distance_to_airports)  #create all the combinations of dates
        # dates_combinations = FlightsMain.main_dates_combination(update_list_legs_dates_days_after_before)  #create all the combinations of dates

        logging.info("dates_combinations: \n%s" % dates_combinations)
        legs_unique_data = []
        for leg in user_input['list_legs']:
            legs_unique_data.append({'preferred_earliest_time': leg['preferred_earliest_time'],
                                     'preferred_latest_time': leg['preferred_latest_time'],
                                     'max_connection_duration': leg['max_connection_duration'],
                                     # 'min_connection_duration': leg['min_connection_duration'],
                                     'cabin': leg['cabin'],  # error - cabin onnly per flight and nor per leg
                                     'earliest_time': leg['earliest_time'],
                                     'latest_time': leg['latest_time'],
                                     'origin_taxi': leg['origin_taxi'],
                                     'origin_private': leg['origin_private'],
                                     'origin_rental': leg['origin_rental'],
                                     'origin_public': leg['origin_public'],
                                     'destination_taxi': leg['destination_taxi'],
                                     'destination_private': leg['destination_private'],
                                     'destination_rental': leg['destination_rental'],
                                     'destination_public': leg['destination_public'],
                                     'origin_address': leg['origin_input'],
                                     'destination_address': leg['destination_input'],
                                     'origin_input': leg['origin_input'],
                                     'arr_or_dep': leg['arrive_depart'],
                                     'max_stop': leg['max_stop']})
        data_for_api = {'legs_unique_data': legs_unique_data,
                        'airport_combinations': airport_combinations,
                        'dates_combinations': dates_combinations,
                        'legs_common_details': legs_common_details}
        return data_for_api
        # return [update_list_legs_distance_to_airports,airport_combinations, dates_combinations, dict_details]

    @staticmethod
    def compress(uncompressed):
        """Compress a string to a list of output symbols."""

        # Build the dictionary.
        dict_size = 256
        dictionary = dict((chr(i), i) for i in xrange(dict_size))
        # in Python 3: dictionary = {chr(i): i for i in range(dict_size)}

        w = ""
        result = []
        for c in uncompressed:
            wc = w + c
            if wc in dictionary:
                w = wc
            else:
                result.append(dictionary[w])
                # Add wc to the dictionary.
                dictionary[wc] = dict_size
                dict_size += 1
                w = c

        # Output the code for w.
        if w:
            result.append(dictionary[w])
        return result
    @staticmethod
    def Convert_Our_Format_to_Client_Format(itineraries_data,all_server_filters_limit,sort_by, limit, list_legs):
        all_itineraries = {'all': [], 'shortest': [], 'best': [], 'cheapest': [],'filters': {'limit':[], 'sort_by':[],'server_all':[],'client_all':[]}}
        all_itineraries['filters']['server_all'] = all_server_filters_limit
        all_itineraries['filters']['limit'] = limit
        all_itineraries['filters']['sort_by'] = sort_by
        all_itineraries['filters']['total_results'] = itineraries_data['len']
        all_itineraries['filters']['total_filtered_results'] = itineraries_data['len_filtered']
        itineraries = itineraries_data['itineraries']
        for itinerary in itineraries:
            calculations_per_legs = {
                "total_drives_and_flights_time": 0,
                "total_drives_time": 0,
                "total_stops_time": 0,
                "total_stops": 0,
                "total_flights_time": 0,
                "total_drives_time_with_traffic": 0,
                "total_drives_and_flights_time_with_traffic": 0
            }
            calculations_per_flight = {
                "total_drives_and_flights_time": 0,
                "total_drives_time": 0,
                "total_stops_time": 0,
                "total_stops": 0,
                "total_flights_time": 0,
                "total_drives_time_with_traffic": 0,
                "total_drives_and_flights_time_with_traffic": 0
            }
            new_itinerary = {'id': itinerary['id'],
                             'groups': {},  # 8.9 -was group
                             'num_of_legs': len(itinerary['legs']),
                             # replace this with PaxFare dict. Update in client
                             "travelers_details": {
                                 "child_amount": 0,
                                 "youth_amount": 0,
                                 "baby_amount": 0,
                                 "adult_amount": 0 ,
                                 "senior_amount": 0,
                             },
                             'legs': [],
                             'price': itinerary['price'],
                             'algorithms_scores': {},
                             'calculations': [],  # itinerary['calculations'],
                             'calculations_per_legs': [],  # itinerary['calculations_per_legs'],
                             'calculations': itinerary['calculations'] if itinerary.get('calculations') else calculations_per_flight,
                             'calculations_per_legs': itinerary['calculations_per_legs'] if itinerary.get('calculations_per_legs') else  calculations_per_legs,
                             'currency': itinerary['currency'],
                             'validating_carrier': itinerary['fare_details']['ValidatingCarrier'],
                             'query_id': itinerary['id'],
                             'query_resultCode': itinerary['resultCode'],
                             'time_limit': itinerary['fare_details']['TimeLimit'],
                             }
            for PaxFare in itinerary['fare_details']['PaxFare']:
                if PaxFare['PTC'] == "ADT":
                    new_itinerary['travelers_details']['adult_amount'] += 1
                if PaxFare['PTC'] == "SEN":
                    new_itinerary['travelers_details']['senior_amount'] += 1
                if PaxFare['PTC'] == "YTH":
                    new_itinerary['travelers_details']['youth_amount'] += 1
                if PaxFare['PTC'] == "CHD":
                    new_itinerary['travelers_details']['child_amount'] += 1
                if PaxFare['PTC'] == "INF":
                    new_itinerary['travelers_details']['baby_amount'] += 1

            for leg in itinerary['legs']:
                new_leg = {"duration": leg['duration'],
                           # int(leg['duration'].split(':')[0])*60+int(leg['duration'].split(':')[1]),
                           "direct_flights": []}
                for segment in leg['segments']:
                    # logging.debug("Flight Duration: %s" % segment['flight']['duration'])
                    if segment['flight']['duration'] == "N/A" or len(segment['flight']['duration']) < 5:
                        flight_duration = "N/A"
                    else:
                        flight_duration = int(segment['flight']['duration'].split(':')[0]) * 60 + int(
                            segment['flight']['duration'].split(':')[1])
                    if segment['flight']['stop_time'] == "N/A":
                        stop_duration = "N/A"
                    else:
                        stop_duration = int(segment['flight']['stop_time'].split(':')[0]) * 60 + int(
                            segment['flight']['stop_time'].split(':')[1])
                    base_time = datetime.datetime.strptime("00:00:00",'%H:%M:%S')
                    arrival_time = datetime.datetime.strptime(segment['destination']['time'],'%H:%M:%S')
                    arrival_minutes = (arrival_time-base_time).seconds/60
                    departure_time = datetime.datetime.strptime(segment['origin']['time'],'%H:%M:%S')
                    departure_minutes = (departure_time-base_time).seconds/60
                    new_segment = {"arrival_airport_city": segment['destination']['airport']['city'],
                                   "flight_num": segment['flight']['number'],
                                   "departure_airport_code": segment['origin']['airport']['code'],
                                   "departure_airport_city": segment['origin']['airport']['city'],
                                   "flight_duration": flight_duration,
                                   "departure_airport": segment['origin']['airport']['name'],
                                   "arrival_airport_code": segment['destination']['airport']['code'],
                                   "plane_type": segment['aircraft']['name'],
                                   "departure_date": segment['origin']['date'],
                                   "arrival_date": segment['destination']['date'],
                                   "stop_length": stop_duration,
                                   "arrival_time": arrival_minutes,
                                   "arrival_airport": segment['destination']['airport']['name'],
                                   "departure_time": departure_minutes,
                                   "airline": segment['carrier']['marketing']['name'],
                                   "airline_operator": segment['carrier']['operating']['name'],
                                   "details": {
                                       "class": segment['flight']['class'],
                                       "meal": segment['flight']['meal']
                                   }
                                   }
                    new_leg['direct_flights'].append(new_segment)
                # from some reason, the [flight['airline'] for flight in new_leg['direct_flights']] not working as expected! Doing other way:
                new_leg["airlines"] = []
                for flight in new_leg['direct_flights']:
                    new_leg["airlines"].append(flight['airline'])
                    #new_leg["airlines"].append(flight['airline_operator']) #todo: for now need to check what to do with this data
                new_leg["airlines"] = list(set(new_leg["airlines"]))
                new_leg['origin'] = copy.deepcopy(new_leg['direct_flights'][0])
                new_leg['origin']['time'] = new_leg['origin']['departure_time']
                new_leg['origin']['date'] = new_leg['origin']['departure_date']
                new_leg['origin']['airport_code'] = new_leg['origin']['departure_airport_code']
                new_leg['origin']['state'] = new_leg['origin']['departure_airport_city']
                new_leg['origin']['airport'] = new_leg['origin']['departure_airport']
                new_leg['destination'] = copy.deepcopy(new_leg['direct_flights'][-1])
                new_leg['destination']['time'] = new_leg['destination']['arrival_time']
                new_leg['destination']['date'] = new_leg['destination']['arrival_date']
                new_leg['destination']['state'] = new_leg['destination']['arrival_airport_city']
                new_leg['destination']['airport_code'] = new_leg['destination']['arrival_airport_code']
                new_leg['destination']['airport'] = new_leg['destination']['arrival_airport']
                if leg.get('drive_to_ap'):
                    new_leg['origin']['drive_distance'] = leg['drive_to_ap']['distance_value']
                    new_leg['origin']['drive_time'] = leg['drive_to_ap']['duration_value']
                else:
                    new_leg['origin']['drive_distance'] = 0
                    new_leg['origin']['drive_time'] = 0
                if leg.get('drive_from_ap'):
                    new_leg['destination']['drive_distance'] = leg['drive_from_ap']['distance_value']
                    new_leg['destination']['drive_time'] = leg['drive_from_ap']['duration_value']
                else:
                    new_leg['destination']['drive_distance'] = 0
                    new_leg['destination']['drive_time'] = 0
                if leg.get('drive_with_traffic_to_ap'):
                    new_leg['origin']['drive_time_traf'] =  leg['drive_with_traffic_to_ap']['duration_value']
                    new_leg['origin']['est_start_drive_traf'] = leg['drive_with_traffic_to_ap']['est_start_drive_traf']
                else:
                    new_leg['origin']['drive_time_traf'] = 0
                    new_leg['origin']['est_start_drive_traf'] = 0

                if leg.get('drive_with_traffic_from_ap'):
                    new_leg['destination']['drive_time_traf'] = leg['drive_with_traffic_from_ap']['duration_value']
                    new_leg['destination']['est_finish_drive_traf'] = leg['drive_with_traffic_from_ap']['est_finish_drive_traf']

                else:
                    new_leg['destination']['drive_time_traf'] = 0
                    new_leg['destination']['est_finish_drive_traf'] = 0

                if leg.get('pub_trans_to_ap'):
                    new_leg['origin']['pub_trans_to_ap'] = leg['pub_trans_to_ap']
                else:
                    new_leg['origin']['pub_trans_to_ap'] = {
                        "duration_value":0
                    }

                if leg.get('pub_trans_from_ap'):
                    new_leg['destination']['pub_trans_from_ap'] = leg['pub_trans_from_ap']
                else:
                    new_leg['destination']['pub_trans_from_ap'] = {
                        "duration_value": 0
                    }

                # del new_leg['origin']['arrival_time']
                # del new_leg['origin']['arrival_date']
                # del new_leg['origin']['arrival_airport_city']
                # del new_leg['origin']['departure_airport_code']
                # del new_leg['origin']['stop_length']
                # del new_leg['origin']['arrival_airport']
                # del new_leg['origin']['arrival_airport_code']
                # del new_leg['origin']['arrival_time']
                # del new_leg['origin']['arrival_time']
                # del new_leg['origin']['details']
                #
                # del new_leg['destination']['departure_airport_city']
                # del new_leg['destination']['departure_airport_code']
                # del new_leg['destination']['departure_airport']
                # del new_leg['destination']['arrival_date']
                # del new_leg['destination']['arrival_time']
                # del new_leg['destination']['departure_time']
                # del new_leg['destination']['arrival_airport_code']
                # del new_leg['destination']['departure_date']
                # del new_leg['destination']['arrival_airport']
                # del new_leg['destination']['details']


                new_itinerary['legs'].append(new_leg)
            new_itinerary['airlines'] = []
            for my_leg in new_itinerary["legs"]:
                airlines = my_leg['airlines']
                for airline in airlines:
                    new_itinerary['airlines'].append(airline)
            new_itinerary['airlines'] = list(set(new_itinerary['airlines']))
            all_itineraries['all'].append(new_itinerary)

        client_all_filters_limit = Itinerary_Filters.Create_itineraries_limits_and_ranges(
            list_legs, itineraries)
        all_itineraries['filters']['client_all'] = client_all_filters_limit
        return all_itineraries

    @staticmethod
    def flights_calculations_per_legs(leg):
        # input: internal (our) json format
        calculations_per_legs = {
            "total_drives_and_flights_time": 0,
            "total_drives_time": 0,
            "total_stops_time": 0,
            "total_stops": 0,
            "total_flights_time": 0,
            "total_drives_time_with_traffic": 0,
            "total_drives_and_flights_time_with_traffic": 0
        }
        total_stops_time = 0
        total_flights_time = 0
        for segment in leg['segments']:
            #logging.info("segment %s " % segment)
            total_stops_time += convert_string_minutes.time_from_string_to_minutes(segment['flight']['stop_time'])
            # if segment['flight']['duration'] == "N/A" or len(segment['flight']['duration']) < 5:
            #      total_flights_time = "N/A"
            #     # total_flights_time = 150  # ofir temp' fix
            # elif total_flights_time != "N/A":
            #     total_flights_time += convert_string_minutes.time_from_string_to_minutes(segment['flight']['duration'])

        calculations_per_legs['total_stops_time'] = total_stops_time
        calculations_per_legs['total_flights_time'] = leg['duration']

        if leg.get('drive_to_ap') or leg.get('drive_from_ap'):
            if leg['drive_to_ap']['duration_value'] != "N/A":
                if calculations_per_legs['total_drives_time'] != "N/A":
                    calculations_per_legs['total_drives_time'] += (leg['drive_to_ap']['duration_value'])  #/ 60
            else:
                calculations_per_legs['total_drives_time'] = 0  #todo : the value is don't care. but if it's because one ap and another ap get value, we don't want to "harm" the ap which got value

            if leg['drive_from_ap']['duration_value'] != "N/A":
                if calculations_per_legs['total_drives_time'] != "N/A":
                    calculations_per_legs['total_drives_time'] += (leg['drive_from_ap']['duration_value'])  #/ 60  # changed to minutes
            else:
                calculations_per_legs['total_drives_time'] = 0
        else:
            calculations_per_legs['total_drives_time'] = 0

        if leg.get('drive_with_traffic_to_ap') or leg.get('drive_with_traffic_from_ap'):
            if leg['drive_with_traffic_to_ap']['duration_value'] != "N/A":
                if calculations_per_legs['total_drives_time_with_traffic'] != "N/A":
                    calculations_per_legs['total_drives_time_with_traffic'] += leg['drive_with_traffic_to_ap']['duration_value'] #/60
            else:
                calculations_per_legs['total_drives_time_with_traffic'] = "N/A"
            if leg['drive_with_traffic_from_ap']['duration_value'] != "N/A":
                if calculations_per_legs['total_drives_time_with_traffic'] != "N/A":
                    calculations_per_legs['total_drives_time_with_traffic'] +=  leg['drive_with_traffic_from_ap']['duration_value'] #/60
            else:
                calculations_per_legs['total_drives_time_with_traffic'] = 0
        else:
            calculations_per_legs['total_drives_time_with_traffic'] = 0

        calculations_per_legs['total_stops'] = len(leg['segments']) - 1

        if calculations_per_legs['total_stops_time'] != "N/A" and calculations_per_legs['total_flights_time'] != "N/A" \
                and calculations_per_legs['total_drives_time'] != "N/A":
            calculations_per_legs['total_drives_and_flights_time'] = calculations_per_legs['total_flights_time'] + \
                                                                     calculations_per_legs['total_drives_time']
        else:
            calculations_per_legs['total_drives_and_flights_time'] = 0

        if calculations_per_legs['total_stops_time'] != "N/A" and calculations_per_legs['total_flights_time'] != "N/A" \
                and calculations_per_legs['total_drives_time_with_traffic'] != "N/A":
            calculations_per_legs['total_drives_and_flights_time_with_traffic'] = \
                calculations_per_legs['total_flights_time'] + \
                calculations_per_legs['total_drives_time_with_traffic']
        else:
            calculations_per_legs['total_drives_and_flights_time_with_traffic'] = 0

        # logging.info("calculations_per_legs %s \n" % calculations_per_legs)
        return calculations_per_legs

    @staticmethod
    def flights_calculations_per_flight(itineraries):

        if len(itineraries) == 0:
            return itineraries

        new_itineraries = []
        for itinerary in itineraries:
            calculations_per_flight = {
                "total_drives_and_flights_time": 0,
                "total_drives_time": 0,
                "total_stops_time": 0,
                "total_stops": 0,
                "total_flights_time": 0,
                "total_drives_time_with_traffic": 0,
                "total_drives_and_flights_time_with_traffic": 0
            }
            itinerary['calculations_per_legs'] = []
            itinerary['calculations'] = calculations_per_flight
            for leg in itinerary['legs']:
                # logging.info("leg %s " % leg)
                calculations_per_leg = FlightsMain.flights_calculations_per_legs(leg)
                itinerary['calculations_per_legs'].append(calculations_per_leg)
                if calculations_per_leg['total_drives_time'] != "N/A" and calculations_per_flight[
                    'total_drives_time'] != "N/A":
                    calculations_per_flight['total_drives_time'] += calculations_per_leg['total_drives_time']
                else:
                    calculations_per_flight['total_drives_time'] = 0

                if calculations_per_leg['total_stops_time'] != "N/A" and calculations_per_flight[
                    'total_stops_time'] != "N/A":
                    calculations_per_flight['total_stops_time'] += calculations_per_leg['total_stops_time']
                else:
                    calculations_per_flight['total_stops_time'] = "N/A"

                if calculations_per_leg['total_stops'] != "N/A" and calculations_per_flight['total_stops'] != "N/A":
                    calculations_per_flight['total_stops'] += calculations_per_leg['total_stops']
                else:
                    calculations_per_flight['total_stops'] = "N/A"

                if calculations_per_leg['total_flights_time'] != "N/A" and calculations_per_flight[
                    'total_flights_time'] != "N/A":
                    calculations_per_flight['total_flights_time'] += calculations_per_leg['total_flights_time']
                else:
                    calculations_per_flight['total_flights_time'] = "N/A"

                if calculations_per_leg['total_drives_and_flights_time'] != "N/A" \
                        and calculations_per_flight['total_drives_and_flights_time'] != "N/A":
                    calculations_per_flight['total_drives_and_flights_time'] += calculations_per_leg[
                        'total_drives_and_flights_time']
                else:
                    calculations_per_flight['total_drives_and_flights_time'] = "N/A"

                if calculations_per_leg['total_drives_time_with_traffic'] != "N/A" \
                        and calculations_per_flight['total_drives_time_with_traffic'] != "N/A":
                    calculations_per_flight['total_drives_time_with_traffic'] += calculations_per_leg[
                        'total_drives_time_with_traffic']
                else:
                    calculations_per_flight['total_drives_time_with_traffic'] = "N/A"

                if calculations_per_leg['total_drives_and_flights_time_with_traffic'] != "N/A" \
                        and calculations_per_flight['total_drives_and_flights_time_with_traffic'] != "N/A":
                    calculations_per_flight['total_drives_and_flights_time_with_traffic'] \
                        += calculations_per_leg['total_drives_and_flights_time_with_traffic']
                else:
                    calculations_per_flight['total_drives_and_flights_time_with_traffic'] = "N/A"

            # logging.info("calculations_per_flight %s \n" % calculations_per_flight)
            itinerary['calculations'] = calculations_per_flight
            new_itineraries.append(itinerary)
        return new_itineraries

    # IMPORTANT: At the moment not in use, we might need to take parts of it later on
    # @staticmethod
    # def Add_Calculations_on_Results(results):
    #     """
    #     Takes results and returns a JSON which the algorithm can read
    #     Params:
    #         results - list of itineraries, received from the Convert_QPX_to_Our_Format function
    #     """
    #
    #     # Method that build the origin section for each flight that in the json file.
    #     def origin_build(leg, general_data):
    #         origin = {}
    #         # state = self.get_state(
    #         #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('originAddress'),
    #         #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
    #         #         'origin'))
    #         Address_GMAP = None
    #         origin['state'] = None
    #         # origin['airport_code'] = leg['segment'][0]['flight']['carrier']
    #         origin['airport_code'] = leg['segment'][0]['leg'][0]['origin']
    #         origin['airport'] = filter(lambda x: x['code'] == origin['airport_code'], general_data['airport'])[0][
    #             'name']
    #         origin['drive_time'] = 0
    #         # origin['drive_time'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get(
    #         #     'timeToAirport')
    #         # date_and_time = self.get_time_and_date(
    #         #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
    #         #         'departureTime'))  # split by 'T', example : 2016-08-21T04:50+03:00
    #         origin['time'] = leg['segment'][0]['leg'][0]['departureTime'].split('T')[1][
    #                          :5]  # example ; 2016-08-20T16:05+03:00
    #         origin['date'] = leg['segment'][0]['leg'][0]['departureTime'].split('T')[0]
    #         origin['timezone'] = leg['segment'][0]['leg'][0]['departureTime'][-6:]
    #         origin['address'] = None
    #         origin['drive_distance'] = 0
    #         # origin['address'] = Address_GMAP
    #         # # origin['address'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('originAddress')
    #         # origin['drive_distance'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get(
    #         #     'distanceToAirport')
    #         return origin

        # Method that build the origin section for each flight that in the json file.
        # def destination_build(leg, general_data):
        #     destination = {}
        #     # state = self.get_state(
        #     #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('destinationAddress'),
        #     #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
        #     #         'destination'))
        #     Address_GMAP = None
        #     destination['state'] = None
        #     # destination['airport_code'] = leg['segment'][0]['flight']['carrier']
        #     destination['airport_code'] = leg['segment'][-1]['leg'][0]['destination']
        #     destination['airport'] = \
        #     filter(lambda x: x['code'] == destination['airport_code'], general_data['airport'])[0]['name']
        #     destination['drive_time'] = 0
        #     # destination['drive_time'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get(
        #     #     'timeToAirport')
        #     # date_and_time = self.get_time_and_date(
        #     #     json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
        #     #         'departureTime'))  # split by 'T', example : 2016-08-21T04:50+03:00
        #     destination['time'] = leg['segment'][-1]['leg'][0]['departureTime'].split('T')[1][
        #                           :5]  # example ; 2016-08-20T16:05+03:00
        #     destination['date'] = leg['segment'][-1]['leg'][0]['departureTime'].split('T')[0]
        #     destination['timezone'] = leg['segment'][-1]['leg'][0]['departureTime'][-6:]
        #     destination['address'] = None
        #     destination['drive_distance'] = 0
        #     # destination['address'] = Address_GMAP
        #     # # destination['address'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('destinationAddress')
        #     # destination['drive_distance'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get(
        #     #     'distanceToAirport')
        #     return destination

        # Method that build the direct flights section for each flight that in the json file.
        # def direct_flights_build(leg, general_data):
        #     direct_flights = []
        #     for flight in leg['segment']:
        #         direct_flight = {}
        #         details = {}
        #         direct_flight['arrival_airport_code'] = flight['leg'][0]['destination']
        #         direct_flight['arrival_airport'] = \
        #             filter(lambda x: x['code'] == direct_flight['arrival_airport_code'], general_data['airport'])[0][
        #                 'name']
        #         direct_flight['departure_airport_code'] = flight['leg'][0]['origin']
        #         direct_flight['departure_airport'] = \
        #             filter(lambda x: x['code'] == direct_flight['departure_airport_code'], general_data['airport'])[0][
        #                 'name']
        #         dep_date = flight['leg'][0]['departureTime']
        #         arr_date = flight['leg'][0]['arrivalTime']
        #         direct_flight['departure_date'] = dep_date.split('T')[0]
        #         direct_flight['arrival_date'] = arr_date.split('T')[0]
        #         direct_flight['departure_time'] = dep_date.split('T')[1][:5]
        #         direct_flight['arrival_time'] = arr_date.split('T')[1][:5]
        #         direct_flight['flight_num'] = flight['flight']['number']
        #         direct_flight['flight_duration'] = flight['leg'][0]['duration']
        #         direct_flight['plane_type'] = None
        #         direct_flight['departure_airport_city'] = None
        #         direct_flight['arrival_airport_city'] = None
        #         direct_flight['stop_length'] = 0
        #         direct_flight['airline'] = flight['flight']['carrier']
        #         if "meal" in flight['leg'][0].keys():
        #             details['meal'] = flight['leg'][0]['meal']
        #         else:
        #             details['meal'] = "N/A"
        #         details['class'] = flight['cabin']
        #         direct_flight['details'] = details
        #         direct_flights.append(direct_flight)
        #     return direct_flights
        #
        # # begining of the function
        # list_flights = []
        # for itinerary in results:
        #     for leg in itinerary['legs']:
        #         if leg['duration'] == "N/A":
        #             leg['duration'] = sum([x['duration'] for x in leg['segments']])
        #         for segment in leg['segments']:
        #             pass
        #
        #     result = json.loads(result)
        #     best_flights = []
        #     for flight in enumerate(result['trips']['tripOption']):
        #         best_data = {'price': None, 'legs': None, 'travelers_details': None, 'num_of_legs': None,
        #                      'calculations': None}
        #         travelers_details = {'senior_amount': None, 'adult_amount': None, 'kids_amount': None,
        #                              'baby_amount': None}
        #         groups = {'group_by_price': None, 'group_by_duration_flight_time_and_travel_time': None,
        #                   'group_by_duration_travel_time': None, 'group_by_duration_flight_time': None,
        #                   'group_by_mileage': None, 'group_by_amount_stop': None,
        #                   'group_by_duration_stop_time': None,
        #                   'group_jump_by_difference_favorite_time_leg1': None}
        #         algorithms_scores = {'cheapest1': None, 'cheapest2': None, 'cheapest3': None, 'cheapest4': None,
        #                              'best1': None, 'best2': None, 'best3': None, 'best4': None,
        #                              'shortest1': None, 'shortest2': None, 'shortest3': None, 'shortest4': None,}
        #         calculations = {}
        #         total_flights_time = 0
        #         total_stops_time = 0
        #         total_mileage = 0
        #         total_flight_with_drive_time = 0
        #         total_stops = 0
        #         airlines_per_flight = {}
        #         list_airlines_per_flight = []
        #         airlines_per_flight_counter = 0
        #         travelers_details['adult_amount'] = flight['pricing'][0]['passengers']['adultCount']
        #         best_data['flight_id'] = str(out_index) + '-' + str(in_index)
        #         best_data['external_id'] = out_index
        #         best_data['internal_id'] = in_index
        #         best_data['price'] = flight['saleTotal']
        #
        #         legs_list = []
        #         calculations_legs_list = []
        #         for leg in flight[
        #             'slice']:  # in QPX every leg represents as a Slice, and every leg could have some connections
        #             direct_flights = []
        #             legs_data = {}
        #             calculations_legs_data = {}
        #             list_airlines_per_leg = []
        #             airlines_per_leg = {}
        #             airlines_per_leg_counter = 0
        #             flights_time = leg['duration']
        #             total_flights_time += flights_time
        #             calculations_legs_data['total_flights_time'] = flights_time
        #             stops_time = leg['duration'] - sum([x['leg'][0]['duration'] for x in leg['segment']])
        #             total_stops_time += stops_time
        #             calculations_legs_data['total_stops_time'] = stops_time
        #             mileage = sum([x['leg'][0]['mileage'] for x in leg['segment']])
        #             calculations_legs_data['total_mileage'] = mileage
        #             total_mileage += mileage
        #             duration_with_drive = flights_time  # no drive time for now
        #             total_flight_with_drive_time += duration_with_drive
        #             calculations_legs_data['total_drives_and_flights_time'] = duration_with_drive
        #             stops = len(leg['segment']) - 1
        #             total_stops += stops
        #             calculations_legs_data['total_stops'] = stops
        #             calculations_legs_data['total_drives_time'] = calculations_legs_data[
        #                                                               'total_drives_and_flights_time'] - flights_time
        #             legs_data['duration'] = leg['duration']
        #             legs_data['origin'] = origin_build(leg, result['trips']['data'])
        #             legs_data['destination'] = destination_build(leg, result['trips']['data'])
        #             #
        #             # self.airline_build(flight, airlines_per_leg, airlines_per_leg_counter, list_airlines_per_leg,
        #             #                    specific_flight, leg)
        #             # legs_data['airlines'] = list_airlines_per_leg
        #             # self.airline_build(flight, airlines_per_flight, airlines_per_flight_counter,
        #             #                    list_airlines_per_flight, specific_flight, leg)
        #             #
        #             legs_data['direct_flights'] = direct_flights_build(leg, result['trips']['data'])
        #             legs_list.append(legs_data)
        #             calculations_legs_list.append(calculations_legs_data)
        #
        #             calculations['total_flights_time'] = total_flights_time
        #             calculations['total_stops_time'] = total_stops_time
        #             calculations['total_mileage'] = total_mileage
        #             calculations['total_drives_and_flights_time'] = total_flight_with_drive_time
        #             calculations['total_stops'] = total_stops
        #             calculations['total_drives_time'] = total_flight_with_drive_time - total_flights_time
        #             best_data['calculations'] = calculations
        #             best_data['calculations_per_legs'] = calculations_legs_list
        #             best_data['legs'] = legs_list
        #             # best_data['num_of_legs'] = int(
        #             #     json_data[flight]['trips']['tripOption'][specific_flight].get('number_of_legs'))
        #             # best_data['travelers_details'] = travelers_details
        #             # best_data['airlines'] = list_airlines_per_flight
        #             # best_data['groups'] = groups
        #             # best_data['algorithms_scores'] = algorithms_scores
        #             best_flights.append(best_data)
        #     list_flights.append(best_flights)
        # return list_flights

    # NOTE: can be used later to nearby airports feature
    # @staticmethod
    # def add_relevant_airports_to_list_legs(list_legs,relevant_airports):
    #
    #     origin_relevant_airports = relevant_airports ['origin_relevant_airports']
    #     destination_relevant_airports = relevant_airports ['destination_relevant_airports']
    #     # print "list_legs - add_relevant_airports_to_list_legs"
    #     # print list_legs
    #     for i in range(len(list_legs)):
    #         # list_legs.append({
    #         #     "origin_airports": relevant_airports ['origin_relevant_airports'][i],
    #         #     "dest_airports": relevant_airports ['destination_relevant_airports'][i]
    #         # })
    #         list_legs[i]['origin_airports']=origin_relevant_airports[i]
    #         list_legs[i]['destination_airports']=destination_relevant_airports[i]
    #     print "list_legs - update"
    #     print list_legs
    #
    #     return list_legs





    # NOTE: can be used later
    # @staticmethod
    # def get_relevant_airports(list_legs):
    #     # example: list_legs = [{'destination': 'berlin germany', 'date': '2016-08-20', 'cabin': '', 'latest_time': '22:00', 'max_connection_duration': 2000, 'origin': 'london, england', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': 'london, england', 'date': '2016-09-15', 'cabin': '', 'latest_time': '23:00', 'max_connection_duration': 2000, 'origin': 'dam square, amsterdam', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': '86-88 Highland Ave Jersey City, NJ 07306, USA', 'date': '2016-10-20', 'cabin': '', 'latest_time': '19:00', 'max_connection_duration': 2000, 'origin': 'milan italy', 'max_stop': 2, 'earliest_time': '05:00'}]
    #     # TBD - add check if the airport query is empty for few reasons: no airports, wrong API key, anything else
    #     print_debug = 1
    #     if (print_debug):
    #         print "def get_relevant_airports(list_legs)"
    #         print "list_legs"
    #         print list_legs
    #
    #     origin_relevant_airports = []
    #     destination_relevant_airports  = []
    #     for legs in list_legs: #TBD - need to upgrade the relavent airport function, after finding the nearest airport - add more airports bt database (or by anything else)
    #         origin_relevant_airports.append(amadeus.nearest_relevant_airport(legs['origin_input']))
    #         destination_relevant_airports.append(amadeus.nearest_relevant_airport(legs['destination_input']))
    #
    #         # removing SDV (sde dov in israel) - TBD need to create real function for that
    #         # TBD- what to do with few airports in a cuty like NYC, LON and more, what is the 3 letter codes for each one of them
    #         # SDV- was removed in amadeus.nearest_relevant_airport
    #
    #
    #     relevant_airports = {'origin_relevant_airports': origin_relevant_airports, 'destination_relevant_airports': destination_relevant_airports}
    #     return relevant_airports



    # NOTE: can be used later to nearby airports feature
    # # get by the list_legs : list of origin and destination airports, and origin and destination addresses
    # # create list of geo location (lat,lng) for each airport of each address (the data is already in the list_legs)
    # # list of addresses or geocodes is seperated by "|" between items (see The Google Maps Distance Matrix API)
    # # get for each address the driving (TBD - public trans') time and distance from the relevant airports
    # # add the information to the list_legs
    # # TBD - try to ask only 1 time for data between specific address and specific airport (for example if the same address is in the origin and in the destination)
    #
    # @staticmethod
    # def get_distance_one_to_many(list_legs):
    #
    #     print_debug = 1
    #     if (print_debug):
    #         print "def get_distance_one_to_many"
    #     #TBD - add test to check good inputs
    #     # TBD - compare destination and origins for saving duplactions
    #     # if not ? or not ?:
    #     #     print "not address or not list_dest"
    #     #     return None
    #
    #     # for - each leg
    #     for leg_num in range(len(list_legs)):
    #
    #         if (print_debug):
    #             print "leg_num"
    #
    #             print leg_num
    #         str_origin_address = list_legs[leg_num]['origin_input']
    #         str_destination_address = list_legs[leg_num]['destination_input']
    #         str_origin_address_enc = urllib.quote(str_origin_address.encode('utf-8'))
    #         str_destination_address_enc = urllib.quote(str_destination_address.encode('utf-8'))
    #
    #         # origin airports
    #         # for - each origin relevant airport
    #         str_origin_airports = ''  #create string of geocode seperated by "|" for each airport
    #         for num_of_origin_airports in range(len(list_legs[leg_num]['origin_airports'])):
    #             if num_of_origin_airports > 0:  # don't put the "|" in the first time. the | is for seperating between few origins or between few destinations in the same query
    #                 str_origin_airports = str_origin_airports + "|"
    #                 if (print_debug):
    #                     print str_origin_airports
    #
    #             if (print_debug):
    #                 print 'num_of_origin_airports'
    #             lat = ''
    #             lng= ''
    #             lat=list_legs[leg_num]['origin_airports'][num_of_origin_airports]['location']['latitude']
    #             lng=list_legs[leg_num]['origin_airports'][num_of_origin_airports]['location']['longitude']
    #             if lat=='' or lng =='':  #TBD - check if this is the error for empty lan and lng
    #                 print "error - get_distance_one_to_many lat or lng are emapty"
    #
    #             if (print_debug):
    #                 print leg_num
    #                 print num_of_origin_airports
    #
    #             str_origin_airports= str_origin_airports + str(lat) + ',' + str(lng)
    #
    #         if (print_debug):
    #             print 'str_origin_airports'
    #             print str_origin_airports
    #         # TBD test if need to be encoded : str_origin_airports_encoded = urllib.quote(str_origin_airports.encode('utf-8'))
    #         url_attributes_encoded = str_origin_address_enc + '&destinations=' + str_origin_airports + '&key=' + maps_api_key + '&language=en'
    #         # url_attributes = urllib.quote(url_attributes.encode('utf-8'))
    #         url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + url_attributes_encoded
    #         if (print_debug):
    #             print "def get_distance_one_to_many - url"
    #             print url
    #
    #         urlopen = urllib.urlopen(url)
    #         read_url = urlopen.read()
    #         result = []
    #         result = json.loads(read_url)
    #         if (print_debug):
    #             print 'url result'
    #             print result
    #         # result example for 1 address and 3 airports (london UK): {'status': 'OK', 'destination_addresses': ['450 Northern Perimeter Rd W, Longford, Hounslow, Greater London TW6 2RR, UK', 'Cargo Forecourt Rd, Horley, Gatwick, West Sussex RH6, UK', '94 Newland St, London E16 2HN, UK'], 'rows': [{'elements': [{'status': 'OK', 'distance': {'value': 28221, 'text': '28.2 km'}, 'duration': {'value': 2363, 'text': '39 mins'}}, {'status': 'OK', 'distance': {'value': 47838, 'text': '47.8 km'}, 'duration': {'value': 4583, 'text': '1 hour 16 mins'}}, {'status': 'OK', 'distance': {'value': 14442, 'text': '14.4 km'}, 'duration': {'value': 1833, 'text': '31 mins'}}]}], 'origin_addresses': ['London, UK']}
    #         response = []
    #         for i in range(len(result['origin_addresses'])): #15/5/16 - can be only 1 origin_addresses for now, need to be changed when adding more
    #             # TBD fix bug - encode well the  result['origin_addresses'][0] (there was a problem with origin_input/ destination = "paris france")
    #             list_legs[leg_num]['origin_G_MAP_address']=result['origin_addresses'][0]
    #             if (len(result['destination_addresses'])) != len(list_legs[leg_num]['origin_airports']):
    #                 print "ERROR - num of airports != num of results distances"
    #             for j in range(len(result['destination_addresses'])):
    #
    #                 # TBD fix bug - encode well the  result['destination_addresses'][j]
    #                 # str_result_destination_addresses = result['destination_addresses'][j]
    #                 # airport_G_MAP_address = urllib.quote(str_result_destination_addresses.encode('utf-8'))
    #                 # list_legs[leg_num]['origin_airports'][j]['airport_G_MAP_address']=airport_G_MAP_address
    #                 list_legs[leg_num]['origin_airports'][j]['distance_G_MAP_text'] = result['rows'][i]['elements'][j]['distance']['text']
    #                 list_legs[leg_num]['origin_airports'][j]['distance_G_MAP_value'] = result['rows'][i]['elements'][j]['distance']['value']
    #                 list_legs[leg_num]['origin_airports'][j]['duration_G_MAP_text'] = result['rows'][i]['elements'][j]['duration']['text']
    #                 list_legs[leg_num]['origin_airports'][j]['duration_G_MAP_value'] = result['rows'][i]['elements'][j]['duration']['value']
    #
    #         print 'list_legs'
    #         print list_legs
    #
    #
    #
    #         # for - each destination relevant airport
    #         str_destination_airports = ''
    #         for num_of_destination_airports in range(len(list_legs[leg_num]['destination_airports'])):
    #             if num_of_destination_airports > 0:  # don't put the "|" in the first time. the | is for seperating between few origins or between few destinations in the same query
    #                 str_destination_airports = str_destination_airports + "|"
    #                 if (print_debug):
    #                     print str_destination_airports
    #
    #             if (print_debug):
    #                 print 'num_of_destination_airports'
    #                 print num_of_destination_airports
    #             lat = ''
    #             lng= ''
    #             lat=list_legs[leg_num]['destination_airports'][num_of_destination_airports]['location']['latitude']
    #             lng=list_legs[leg_num]['destination_airports'][num_of_destination_airports]['location']['longitude']
    #             if lat=='' or lng =='':  #TBD - check if this is the error for empty lan and lng
    #                 print "error - get_distance_one_to_many lat or lng are emapty"
    #                 print leg_num
    #                 print num_of_destination_airports
    #             str_destination_airports= str_destination_airports + str(lat) + ',' + str(lng)
    #         if (print_debug):
    #             print 'str_destination_airports'
    #             print str_destination_airports
    #
    #         # TBD test if need to be encoded : str_origin_airports_encoded = urllib.quote(str_origin_airports.encode('utf-8'))
    #
    #         url_attributes_encoded = str_destination_address_enc + '&destinations=' + str_destination_airports + '&key=' + maps_api_key + '&language=en'
    #         # url_attributes = urllib.quote(url_attributes.encode('utf-8'))
    #         url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + url_attributes_encoded
    #         if (print_debug):
    #             print "def get_distance_one_to_many - url"
    #             print url
    #
    #         urlopen = urllib.urlopen(url)
    #         read_url = urlopen.read()
    #         result = []
    #         result = json.loads(read_url)
    #         if (print_debug):
    #             print 'url result'
    #             print result
    #         # result example for 1 address and 3 airports (london UK): {'status': 'OK', 'destination_addresses': ['450 Northern Perimeter Rd W, Longford, Hounslow, Greater London TW6 2RR, UK', 'Cargo Forecourt Rd, Horley, Gatwick, West Sussex RH6, UK', '94 Newland St, London E16 2HN, UK'], 'rows': [{'elements': [{'status': 'OK', 'distance': {'value': 28221, 'text': '28.2 km'}, 'duration': {'value': 2363, 'text': '39 mins'}}, {'status': 'OK', 'distance': {'value': 47838, 'text': '47.8 km'}, 'duration': {'value': 4583, 'text': '1 hour 16 mins'}}, {'status': 'OK', 'distance': {'value': 14442, 'text': '14.4 km'}, 'duration': {'value': 1833, 'text': '31 mins'}}]}], 'origin_addresses': ['London, UK']}
    #         response = []
    #         for i in range(len(result['origin_addresses'])): #15/5/16 - can be only 1 origin_addresses for now, need to be changed when adding more
    #             # TBD fix bug - encode well the  result['origin_addresses'][0]
    #             list_legs[leg_num]['destination_G_MAP_address']=result['origin_addresses'][0]
    #             if (len(result['destination_addresses'])) != len(list_legs[leg_num]['destination_airports']):
    #                 print "ERROR - num of airports != num of results distances"
    #             for j in range(len(result['destination_addresses'])):
    #
    #                 # TBD fix bug - encode well the  result['destination_addresses'][j]
    #                 # str_result_destination_addresses = result['destination_addresses'][j]
    #                 # airport_G_MAP_address = urllib.quote(str_result_destination_addresses.encode('utf-8'))
    #                 #
    #                 # list_legs[leg_num]['destination_airports'][j]['airport_G_MAP_address']=airport_G_MAP_address
    #                 list_legs[leg_num]['destination_airports'][j]['distance_G_MAP_text'] = result['rows'][i]['elements'][j]['distance']['text']
    #                 list_legs[leg_num]['destination_airports'][j]['distance_G_MAP_value'] = result['rows'][i]['elements'][j]['distance']['value']
    #                 list_legs[leg_num]['destination_airports'][j]['duration_G_MAP_text'] = result['rows'][i]['elements'][j]['duration']['text']
    #                 list_legs[leg_num]['destination_airports'][j]['duration_G_MAP_value'] = result['rows'][i]['elements'][j]['duration']['value']
    #
    #         if (print_debug):
    #             print 'list_legs'
    #             print list_legs
    #
    #     return list_legs

    # days_after_dates & days_before_dates
    # This function can be before other dates combinations functions and therefore it writes to list leg to list_legs[n_legs]['dates_options']


    @staticmethod
    def dates_days_after_before(list_legs,date_field):
        # Example
        #  >>> now2 = datetime.datetime(2016, 8, 4)
        # >>> print now2.isoformat()
        # 2016-08-04T00:00:00
        # >>> print (now2.isoformat()).split('T')[0]
        # 2016-08-04
        # >>> new1=now2+datetime.timedelta(days=10)
        # >>> print new1
        # 2016-08-14 00:00:00
        import datetime
        print_debug = 1
        num_of_legs = len(list_legs)

        dates_options = list()
        for n_legs in range(num_of_legs):
            if list_legs[n_legs]['days_after_dates']:
                days_after_dates = int(list_legs[n_legs]['days_after_dates'])
            if list_legs[n_legs]['days_before_dates']:
                days_before_dates = int(list_legs[n_legs]['days_before_dates'])
                neg_days_before_dates = -days_before_dates  # negative value, works even if it 0

            # if (days_after_dates != 0 or days_before_dates != 0):
            dates_to_add = list() # dates by days_after_dates & days_before_dates
            logging.info('dates_options %s' % list_legs[n_legs][date_field])

            for n_dates in range(len(list_legs[n_legs][date_field])):
                logging.info('n_dates %s' % list_legs[n_legs][date_field][n_dates])
                #ddate = list_legs[n_legs][date_field][n_dates].split('T')[0]
                ddate = list_legs[n_legs][date_field][n_dates]

                logging.info('ddate :%s' % ddate)
                [year, month, day] = ddate.split('-')
                cur_date = datetime.datetime(int(year), int(month), int(day))
                if (print_debug):
                    print 'cur_date:', cur_date
                    print '[year,month,day]:', [year, month, day]
                    print 'neg_days_before_dates:', neg_days_before_dates
                    print 'days_after_dates:', days_after_dates

                for add_days in range(neg_days_before_dates,
                                      days_after_dates + 1):  # all the dates in the leg for the cur_date are calculated here
                    dates_to_add.append((cur_date + datetime.timedelta(add_days)).isoformat().split('T')[0])
                    #print add_days
                    # dates_options.append(dates_to_add)  # add the range of dates for each date in the list_legs[n_legs]['dates_options']
                    # print 'dates_options: ', dates_options
            dates_options_uniq = list(
                set(dates_to_add))  # Not order preserving - from https://www.peterbe.com/plog/uniqifiers-benchmark
            dates_options_uniq.sort()  # TBD -sort the dates. must do when using the dates_options as a list after expaning the date rnage by +/- few days. lists must be in the same length if we want to use it in a make sense way. the unique and sort should save the lenghs equal and in the right order
            if (print_debug):
                print 'dates_to_add:', dates_to_add
                print 'dates_options_uniq:', dates_options_uniq
            list_legs[n_legs][date_field] = dates_options_uniq
        return list_legs

    # The first date set in 'dates_options' of leg 0 (can be few options)
    # After that all the dates are by 'stay_duration' (can be few options)
    # all the mix duration options are calculate
    # The last leg doesn't have 'stay_duration'
    @staticmethod
    def dates_stay_durations_comb(list_legs):
        import datetime
        print_debug = 0
        num_of_legs = len(list_legs)

        dates_options = list()
        # Example
        #  >>> now2 = datetime.datetime(2016, 8, 4)
        # >>> print now2.isoformat()
        # 2016-08-04T00:00:00
        # >>> print (now2.isoformat()).split('T')[0]
        # 2016-08-04
        # >>> new1=now2+datetime.timedelta(days=10)
        # >>> print new1
        # 2016-08-14 00:00:00
        dates_combinations_list = list()
        for n_dates in range(
                len(list_legs[0]['dates_options'])):  # todo: tbd - is it OK that the first date is only from leg 0?
            [year, month, day] = list_legs[0]['dates_options'][n_dates].split(
                '-')  # TBD - is it OK that the first date is only from leg 0?
            cur_date = datetime.datetime(int(year), int(month), int(day))
            # cur_date_str = str(cur_date)
            cur_date_str = cur_date.isoformat().split('T')[0]

            if num_of_legs == 1:  # not relevant
                dates_combinations_list = list_legs[0]['dates_options']
            if num_of_legs == 2:
                for i1 in range(len(list_legs[0]['stay_duration'])):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    dates_combinations_list.append(cur_date_str + "," + date2_str)
            if num_of_legs == 3:
                for i1 in range(len(list_legs[0]['stay_duration'])):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    for i2 in range(len(list_legs[1]['stay_duration'])):
                        duration2 = int(list_legs[1]['stay_duration'][i2])
                        date3 = (date2 + datetime.timedelta(duration2))
                        date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                        dates_combinations_list.append(cur_date_str + "," + date2_str + "," + date3_str)
            if num_of_legs == 4:
                for i1 in range(len(list_legs[0]['stay_duration'])):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    for i2 in range(len(list_legs[1]['stay_duration'])):
                        duration2 = int(list_legs[1]['stay_duration'][i2])
                        date3 = (date2 + datetime.timedelta(duration2))
                        date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                        for i3 in range(len(list_legs[2]['stay_duration'])):
                            duration3 = int(list_legs[2]['stay_duration'][i3])
                            date4 = (date3 + datetime.timedelta(duration3))
                            date4_str = (date3 + datetime.timedelta(duration3)).isoformat().split('T')[0]
                            dates_combinations_list.append(
                                cur_date_str + "," + date2_str + "," + date3_str + "," + date4_str)
            if num_of_legs == 5:
                for i1 in range(len(list_legs[0]['stay_duration'])):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    for i2 in range(len(list_legs[1]['stay_duration'])):
                        duration2 = int(list_legs[1]['stay_duration'][i2])
                        date3 = (date2 + datetime.timedelta(duration2))
                        date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                        for i3 in range(len(list_legs[2]['stay_duration'])):
                            duration3 = int(list_legs[2]['stay_duration'][i3])
                            date4 = (date3 + datetime.timedelta(duration3))
                            date4_str = (date3 + datetime.timedelta(duration3)).isoformat().split('T')[0]
                            for i4 in range(len(list_legs[3]['stay_duration'])):
                                duration4 = int(list_legs[3]['stay_duration'][i4])
                                date5 = (date4 + datetime.timedelta(duration4))
                                date5_str = (date4 + datetime.timedelta(duration4)).isoformat().split('T')[0]
                                dates_combinations_list.append(
                                    cur_date_str + "," + date2_str + "," + date3_str + "," + date4_str + "," + date5_str)
        if (print_debug):
            print "dates_combinations_list"
            print dates_combinations_list

        # exmaple: change from ['2016-08-23,2016-09-23', '2016-08-23,2016-09-24', '2016-08-24,2016-09-23', '2016-08-24,2016-09-24'] to [['2016-08-27', '2016-10-26'], ['2016-08-27', '2016-10-27'], ['2016-08-27', '2016-10-28']...
        dates_combinations_list_tmp = []
        for one_comb in dates_combinations_list:
            dates = one_comb.split(",")
            dates_combinations_list_tmp.append(map(lambda x:[x], dates))
        dates_combinations_list_split = dates_combinations_list_tmp
        if (print_debug):
            print "after split: dates_combinations_list_split"
            print dates_combinations_list_split
        return dates_combinations_list_split

    # The first date set in 'date' of leg 0 (can be few options)
    # After that all the dates are by 'stay_duration' (can be few options)
    # all the duration options are calculate as a list: 1st duration of leg 0 with 1st duration of leg 1 ..
    # The last leg doesn't have 'stay_duration'
    @staticmethod
    def dates_stay_durations_list(list_legs,date_field):
        import datetime
        print_debug = 0
        num_of_legs = len(list_legs)

        # Example
        #  >>> now2 = datetime.datetime(2016, 8, 4)
        # >>> print now2.isoformat()
        # 2016-08-04T00:00:00
        # >>> print (now2.isoformat()).split('T')[0]
        # 2016-08-04
        # >>> new1=now2+datetime.timedelta(days=10)
        # >>> print new1
        # 2016-08-14 00:00:00
        dates_combinations_list = list()
        for n_dates in range(len(list_legs[0][date_field])):
            # todo: tbd - is it OK that the first date is only from leg 0?
            [year, month, day] = list_legs[0][date_field][n_dates].split('-')
            cur_date = datetime.datetime(int(year), int(month), int(day))
            # cur_date_str = str(cur_date)
            cur_date_str = cur_date.isoformat().split('T')[0]
            # get the minimal list length of 'dates_options' in case they aren't in the same size
            length_dates_options_list =[]
            for k in range(num_of_legs-1): #todo: add warnining to fix input, not support len=0
                length_dates_options_list.append(len(list_legs[k]['stay_duration']))
            min_num_of_durations = min(length_dates_options_list)

            if num_of_legs == 1:  # not relevant
                dates_combinations_list = list_legs[0][date_field]
            if num_of_legs == 2:
                for i1 in range(min_num_of_durations):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    dates_combinations_list.append(cur_date_str + "," + date2_str)
            if num_of_legs == 3:
                for i1 in range(min_num_of_durations):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    duration2 = int(list_legs[1]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                    dates_combinations_list.append(cur_date_str + "," + date2_str + "," + date3_str)
            if num_of_legs == 4:
                for i1 in range(min_num_of_durations):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    duration2 = int(list_legs[1]['stay_duration'][i1])
                    duration3 = int(list_legs[2]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    date3 = (date2 + datetime.timedelta(duration2))
                    date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                    date4_str = (date3 + datetime.timedelta(duration3)).isoformat().split('T')[0]
                    dates_combinations_list.append(cur_date_str + "," + date2_str + "," + date3_str + "," + date4_str)
            if num_of_legs == 5:
                for i1 in range(min_num_of_durations):
                    duration1 = int(list_legs[0]['stay_duration'][i1])
                    duration2 = int(list_legs[1]['stay_duration'][i1])
                    duration3 = int(list_legs[2]['stay_duration'][i1])
                    duration4 = int(list_legs[3]['stay_duration'][i1])
                    date2 = (cur_date + datetime.timedelta(duration1))
                    date2_str = (cur_date + datetime.timedelta(duration1)).isoformat().split('T')[0]
                    date3 = (date2 + datetime.timedelta(duration2))
                    date3_str = (date2 + datetime.timedelta(duration2)).isoformat().split('T')[0]
                    date4 = (date3 + datetime.timedelta(duration3))
                    date4_str = (date3 + datetime.timedelta(duration3)).isoformat().split('T')[0]
                    date5_str = (date4 + datetime.timedelta(duration4)).isoformat().split('T')[0]
                    dates_combinations_list.append(cur_date_str + "," + date2_str + "," + date3_str + "," + date4_str + "," + date5_str)
        if (print_debug):
            print "dates_combinations_list"
            print dates_combinations_list

        # exmaple: change from ['2016-08-23,2016-09-23', '2016-08-23,2016-09-24', '2016-08-24,2016-09-23', '2016-08-24,2016-09-24'] to [['2016-08-27', '2016-10-26'], ['2016-08-27', '2016-10-27'], ['2016-08-27', '2016-10-28']...
        dates_combinations_list_tmp = []
        for one_comb in dates_combinations_list:
            dates = one_comb.split(",")
            dates_combinations_list_tmp.append(map(lambda x:[x], dates))
        dates_combinations_list_split = dates_combinations_list_tmp
        if (print_debug):
            print "after split: dates_combinations_list_split"
            print dates_combinations_list_split
        return dates_combinations_list_split


    # Create all the dates combinations in list_legs[]['dates_options'] with all the other legs
    @staticmethod
    def dates_full_combination(list_legs,date_field):
        import datetime
        print_debug = 1
        num_of_legs = len(list_legs)

        # number of combinations:
        # num_of_dates_combination = 1
        # for k in range(num_of_legs):
        #     num_of_leg_dates = len(list_legs[k]['dates_options'])
        #     num_of_dates_combination = num_of_dates_combination * len(list_legs[k]['dates_options'])
        # if (print_debug):
        #     print "num_of_leg_dates:", num_of_leg_dates
        #     print "num_of_dates_combination:", num_of_dates_combination

        dates_combinations_list = []
        if num_of_legs == 1:
            dates_combinations_list = list_legs[0][date_field]
        if num_of_legs == 2:
            for i1 in range(len(list_legs[0][date_field])):
                date1 = list_legs[0][date_field][i1]
                for i2 in range(len(list_legs[1][date_field])):
                    date2 = list_legs[1][date_field][i2]
                    dates_combinations_list.append(date1 + "," + date2)
        if num_of_legs == 3:
            for i1 in range(len(list_legs[0][date_field])):
                date1 = list_legs[0][date_field][i1]
                for i2 in range(len(list_legs[1][date_field])):
                    date2 = list_legs[1][date_field][i2]
                    for i3 in range(len(list_legs[2][date_field])):
                        date3 = list_legs[2][date_field][i3]
                        dates_combinations_list.append(date1 + "," + date2 + "," + date3)
        if num_of_legs == 4:
            for i1 in range(len(list_legs[0][date_field])):
                date1 = list_legs[0][date_field][i1]
                for i2 in range(len(list_legs[1][date_field])):
                    date2 = list_legs[1][date_field][i2]
                    for i3 in range(len(list_legs[2][date_field])):
                        date3 = list_legs[2][date_field][i3]
                        for i4 in range(len(list_legs[3][date_field])):
                            date4 = list_legs[3][date_field][i4]
                            dates_combinations_list.append(date1 + "," + date2 + "," + date3 + "," + date4)
        if num_of_legs == 5:
            for i1 in range(len(list_legs[0][date_field])):
                date1 = list_legs[0][date_field][i1]
                for i2 in range(len(list_legs[1][date_field])):
                    date2 = list_legs[1][date_field][i2]
                    for i3 in range(len(list_legs[2][date_field])):
                        date3 = list_legs[2][date_field][i3]
                        for i4 in range(len(list_legs[3][date_field])):
                            date4 = list_legs[3][date_field][i4]
                            for i5 in range(len(list_legs[4][date_field])):
                                date4 = list_legs[4][date_field][i5]
                            dates_combinations_list.append(
                                date1 + "," + date2 + "," + date3 + "," + date4 + "," + date5)
        if (print_debug):
            print "dates_combinations_list"
            print dates_combinations_list

        # exmaple: change from ['2016-08-23,2016-09-23', '2016-08-23,2016-09-24', '2016-08-24,2016-09-23', '2016-08-24,2016-09-24'] to [['2016-08-27', '2016-10-26'], ['2016-08-27', '2016-10-27'], ['2016-08-27', '2016-10-28']...
        dates_combinations_list_tmp = []
        for one_comb in dates_combinations_list:
            dates = one_comb.split(",")
            dates_combinations_list_tmp.append(map(lambda x:[x], dates))
        dates_combinations_list_split = dates_combinations_list_tmp
        if (print_debug):
            print "after split: dates_combinations_list_split"
            print dates_combinations_list_split
        return dates_combinations_list_split

    # create dates combinations by match ['dates_options'][0] from all the legs, ['dates_options'][1] from all the legs ....
    # TBD - for now we don't not inform that the len(list_legs[?]['dates_options']) aren't match between legs. we stop by the shortest one
    @staticmethod
    def dates_as_list(list_legs,date_field):

        print_debug = 1
        num_of_legs = len(list_legs)

        length_dates_options_list = list()

        # get the minimal list length of 'dates_options' in case they aren't in the same size
        for k in range(num_of_legs):#todo: cover missing date (send error msg)
            length_dates_options_list.append(len(list_legs[k][date_field]))
        num_of_dates_combination = min(length_dates_options_list)

        dates_combinations_list = []
        if num_of_legs == 1:
            dates_combinations_list = list_legs[0][date_field]
        if num_of_legs == 2:
            for i1 in range(num_of_dates_combination):
                date1 = list_legs[0][date_field][i1]
                date2 = list_legs[1][date_field][i1]
                dates_combinations_list.append(date1 + "," + date2)
        if num_of_legs == 3:
            for i1 in range(num_of_dates_combination):
                date1 = list_legs[0][date_field][i1]
                date2 = list_legs[1][date_field][i1]
                date3 = list_legs[2][date_field][i1]
                dates_combinations_list.append(date1 + "," + date2 + "," + date3)

        if num_of_legs == 4:
            for i1 in range(num_of_dates_combination):
                date1 = list_legs[0][date_field][i1]
                date2 = list_legs[1][date_field][i1]
                date3 = list_legs[2][date_field][i1]
                date4 = list_legs[3][date_field][i1]
                dates_combinations_list.append(date1 + "," + date2 + "," + date3 + "," + date4)
        if num_of_legs == 5:
            for i1 in range(num_of_dates_combination):
                date1 = list_legs[0][date_field][i1]
                date2 = list_legs[1][date_field][i1]
                date3 = list_legs[2][date_field][i1]
                date4 = list_legs[3][date_field][i1]
                date5 = list_legs[4][date_field][i1]
                dates_combinations_list.append(date1 + "," + date2 + "," + date3 + "," + date4 + "," + date5)
        if (print_debug):
            logging.info("dates_combinations_list")
            logging.info(dates_combinations_list)

        # exmaple: change from ['2016-08-23,2016-09-23', '2016-08-23,2016-09-24', '2016-08-24,2016-09-23', '2016-08-24,2016-09-24'] to [['2016-08-27', '2016-10-26'], ['2016-08-27', '2016-10-27'], ['2016-08-27', '2016-10-28']...
        dates_combinations_list_tmp = []
        for one_comb in dates_combinations_list:
            dates = one_comb.split(",")
            dates_combinations_list_tmp.append(map(lambda x:[x], dates))
        dates_combinations_list_split = dates_combinations_list_tmp
        if (print_debug):
            print "after split: dates_combinations_list_split"
            print dates_combinations_list_split

        return dates_combinations_list_split

    @staticmethod
    def main_dates_combination(list_legs):
        print_debug = 1

        # right now read dates_combination_method value only from leg 0!!!! 0 - do any combination of 'dates_options' , 2 - 'dates_options' is a list and use each item with the same place in the list of the other legs, 3 - from each dates_options values in leg 0 calculate the length stay by 'stay_duration' (stay_duration can have few options, all the combinations are calculated)
        if list_legs[0]['dates_combination_method'] == '0':
            if list_legs[0]['date']: #if there is 'date' use date, else use dates_options
                list_legs = FlightsMain.dates_days_after_before(list_legs, 'date')
                dates_comb = FlightsMain.dates_full_combination(list_legs, 'date')
            else:
                logging.info('main_dates_combination - dates_combination by uri')
                list_legs = FlightsMain.dates_days_after_before(list_legs, 'dates_options')
                # dates_comb = FlightsMain.dates_full_combination(list_legs, 'dates_options')
                dates_comb = FlightsMain.dates_combination(list_legs)  # by uri , include fix by arrival
        elif list_legs[0]['dates_combination_method'] == '2':
            dates_comb = FlightsMain.dates_as_list(list_legs, 'date')#use 'date' only
        elif list_legs[0]['dates_combination_method'] == '3':
            if list_legs[0]['date']:
                list_legs = FlightsMain.dates_days_after_before(list_legs, 'date')
                dates_comb = FlightsMain.dates_stay_durations_list(list_legs, 'date')
            # else:
            #     list_legs = FlightsMain.dates_days_after_before(list_legs, 'dates_options')
            #     dates_comb = FlightsMain.dates_stay_durations_list(list_legs, 'date')
        else:  # default is dates_as_list which use the dates_options as (all first values of the legs, all sec' values of the legs..). if there is only 1 date in any leg there is only 1 date combination
            logging.info('dates_combination_method is not 0,2,3')
            dates_comb = FlightsMain.dates_as_list(list_legs, 'date')
        logging.info('dates_comb %s' %dates_comb)
        return dates_comb


# if leg['arrive_depart'] == "arrive":
#     # logging.info("fix by arrive")
#     leg['dates_options'] = FlightsMain.fix_arrive_dates(leg)  # add 2 dates before for each date
