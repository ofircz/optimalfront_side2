import json
import logging
import os
import platform

from lib.bs4 import BeautifulSoup
import urllib2, sys
from lib import requests
import pickle


class apiHandler():
    #getting list of hotels from trip advisor api using geo coding google maps api
    @staticmethod
    def get_hotels_by_address(address):
        from api.trip_advisor import tripAdvisor
        from api.google_maps import GoogleMaps
        g = GoogleMaps
        t = tripAdvisor()
        params = g.get_geo(address)
        hotels = t.get_hotel_list(params)
        return hotels


#pick the best hotels from the selection of trip advisor
    @staticmethod
    def hotels_selection(address):
        api = apiHandler
        all_hotels = api.get_hotels_by_address(address)
        selection = []
        for hotel in all_hotels:
            if hotel['rating']['rate'] != None and float(hotel['rating']['rate']) >= 4.0:
                selection.append(hotel)
        return selection
