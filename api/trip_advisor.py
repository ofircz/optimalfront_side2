from lib import requests
import urllib
import json
tripiadvisorAPI = "d863ef6d884e4d76856c876e5930988f"


class tripAdvisor():
	def get_hotel_list(self, params):
		api_url = "http://api.tripadvisor.com/api/partner/2.0/map/"
		url = api_url+str(params['lat'])+","+str(params['long'])+"/hotels?key="+tripiadvisorAPI
		request = self.api_call(url)
		dict = json.loads(request.read())['data']
		response = set_dictionaty(dict)
		return response

	def mapper(self, params):
		api_url = "http://api.tripadvisor.com/api/partner/2.0/location_mapper/"
		url = api_url+str(params['lat'])+','+str(params['lng'])+'?key='+tripiadvisorAPI+"-mapper&catergory=hotels"
		if params['hotel_name']:
			url = url+'&q='+params['hotel_name']
		request = self.api_call(url)
		dict = json.loads(request.read())['data']
		return dict

	def api_call(self, url):
		return urllib.urlopen(url)


def set_dictionaty(dict):
	hotels = []
	for d in dict:
		hotel = {}
		hotel['name'] = d['name']
		hotel['photos'] = d["see_all_photos"]
		hotel['web_url'] = d['web_url']
		hotel['long'] = d['longitude']
		hotel['lat'] = d['latitude']
		hotel['ranking'] = d['ranking_data']		
		hotel['rating'] = {
		'rate': d['rating'],
		'reviews': d['num_reviews']
		}
		hotel['distance'] = d['distance']
		hotels.append(hotel)
	return hotels
