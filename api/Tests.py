from api.QpxExpress import QpxExpress


class QpxTest:
    def __init__(self):
        pass

    @staticmethod
    def qpx():
        print "******** Tests -def qpx********"
        dict_details = {
            'adult': 1,
            'child': 0,
            'senior': 0,
            'infant_in_lap': 0,
            'infant_in_seat': 0,
            'max_price': '',
            'refundable': False,
            'solutions': 30
        }
        list_legs = [
            {
                #'origin': 'SFO airport',
                'origin': 'london, england',
                'destination': 'berlin germany',
                'date': '2016-08-20',
                'max_stop': 1,
                'max_connection_duration': 2000,
                'cabin': '',
                'earliest_time': '05:00',
                'latest_time': '22:00'
            }
            ,
            {
                'origin': 'dam square, amsterdam',
                'destination': 'london, england',
                'date': '2016-09-15',
                'max_stop': 1,
                'max_connection_duration': 2000,
                'cabin': '',
                'earliest_time': '05:00',
                'latest_time': '23:00'
            }
            ,
            {
               'origin': 'milan italy',
               'destination': '86-88 Highland Ave Jersey City, NJ 07306, USA',
               'date': '2016-10-20',
               'max_stop': 2,
               'max_connection_duration': 2000,
               'cabin': '',
               'earliest_time': '05:00',
               'latest_time': '19:00'
            }
        ]

        qpx = QpxExpress
        print 'Tests - list_legs'
        print list_legs
        print 'Tests - dict_details'
        print dict_details
        print 'Tests qpx.qpx_wrapping(list_legs, dict_details)'
        wrap_qpx = qpx.qpx_wrapping(list_legs, dict_details)
        #  normal_qpx = qpx.qpx_flights(list_legs, dict_details, True, 2016-01-28)
        return wrap_qpx


