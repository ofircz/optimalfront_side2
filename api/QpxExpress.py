from lib import requests
import urllib2
import json
from api.Amadeus import Amadeus
from api.google_maps import GoogleMaps
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(45)

api_key = 'AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04'
api_key2 = 'AIzaSyCS8PTaJ7f2VEJWzk-qvLhaYPH3KHeQVh0'
api_key2 = 'AIzaSyAXaQgGhIhIc3vahqaSbt8mBSJp_JNQIu8'
amadeus = Amadeus
google_maps = GoogleMaps


class QpxExpress:
    @staticmethod
    def qpx_flights(list_legs, dict_details, round_trip=False, return_date=None):
        print 'qpx_flights - list_legs'
        print list_legs


        if return_date is None:
            return_date = ''
        legs = []
        for leg in list_legs:
            legs.append({
                "kind": "qpxexpress#sliceInput",
                "origin": leg['origin'],
                "destination": leg['destination'],
                "date": leg['date'],
                "maxStops": leg['max_stop'],
                "maxConnectionDuration": leg['max_connection_duration'],
                "preferredCabin": leg['cabin'],
                "permittedDepartureTime": {
                    "kind": "qpxexpress#timeOfDayRange",
                    "earliestTime": leg['earliest_time'],
                    "latestTime": leg['latest_time']
                }
            })

        if round_trip:
            legs.append({
                "kind": "qpxexpress#sliceInput",
                "origin": list_legs[-1]['destination'],  # Last element
                "destination": list_legs[0]['origin'],
                "date": return_date,
                "maxStops": list_legs[0]['max_stop'],
                "maxConnectionDuration": list_legs[0]['max_connection_duration'],
                "preferredCabin": list_legs[0]['cabin'],
                "permittedDepartureTime": {
                    "kind": "qpxexpress#timeOfDayRange",
                    "earliestTime": list_legs[0]['earliest_time'],
                    "latestTime": list_legs[0]['latest_time']
                }
            })

        url = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=' + api_key2
        headers = {'Content-Type': 'application/json'}

        params = {"request": {
            "passengers": {
                "kind": "qpxexpress#passengerCounts",
                "adultCount": dict_details['adult'],
                "childCount": dict_details['child'],
                "infantInLapCount": dict_details['infant_in_lap'],
                "infantInSeatCount": dict_details['infant_in_seat'],
                "seniorCount": dict_details['senior']
            },
            "slice": legs,
            "maxPrice": dict_details['max_price'],
            "refundable": dict_details['refundable'],
            "solutions": dict_details['solutions']
        }
        }
        # params = {
        #   "request": {
        #     "passengers": {
        #       "kind": "qpxexpress#passengerCounts",
        #       "adultCount": 1,
        #     },
        #     "slice": [
        #       {
        #         "kind": "qpxexpress#sliceInput",
        #         "origin": "DCA",
        #         "destination": "NYC",
        #         "date": "2016-01-20",
        #       }
        #     ],
        #     "refundable": "false",
        #     "solutions": 5
        #   }
        # }

        '''urllib2'''
        data = json.dumps(params)
        print 'data'
        print data
        req = urllib2.Request(url, data, headers)
        u = urllib2.urlopen(req)
        read_url = u.read()
        result = json.loads(read_url)
        return result

    @staticmethod
    def qpx_fix_json(flights, vector_distance):
        if 'tripOption' not in flights['trips']:
            print "Error Flights"
            return []
        airport_data = []
        carrier_data = []
        for airports in flights['trips']['data']['airport']:
            airport_data.append({
                'code': airports['code'],
                'name': airports['name']
            })

        for carriers in flights['trips']['data']['carrier']:
            carrier_data.append({
                'code': carriers['code'],
                'name': carriers['name']
            })

        new_flights = flights
        del new_flights['kind']
        del new_flights['trips']['requestId']
        del new_flights['trips']['data']
        del new_flights['trips']['kind']
        for option in new_flights['trips']['tripOption']:
            del option['id']
            del option['kind']
            option['saleTotalQpx'] = option.pop('saleTotal')
            option.update({
                'saleTotalAmadeus': ''
            })
            option['legs'] = option.pop('slice')
            count_legs = 0
            for legs in option['legs']:
                del legs['kind']
                legs['part'] = legs.pop('segment')
                count_parts = 0
                legs.update({  # Adding new Data to json
                    'originAddress': '',
                    'distanceToAirport': '',
                    'timeToAirport': '',
                    'destinationAddress': '',
                    'distanceFromAirport': '',
                    'timeFromAirport': ''
                })
                for parts in legs['part']:
                    del parts['marriedSegmentGroup']
                    del parts['id']
                    del parts['kind']
                    del parts['bookingCodeCount']
                    parts['flight'].update({
                        'name': ''
                    })
                    parts['leg_details'] = parts.pop('leg')
                    for details in parts['leg_details']:
                        del details['id']
                        del details['kind']
                        del details['duration']
                        details.update({
                            'destination_airport': '',
                            'origin_airport': ''
                        })
                    count_parts += 1
                    for vector in vector_distance:  # Adding distance vector to json
                        for distance in vector:
                            if parts['leg_details'][0]['origin'] == distance['airport']:
                                legs['originAddress'] = distance['origin_address']
                                legs['timeToAirport'] = distance['duration']['text']
                                legs['distanceToAirport'] = distance['distance']['text']
                            if parts['leg_details'][0]['destination'] == distance['airport']:
                                legs['destinationAddress'] = distance['origin_address']
                                legs['timeFromAirport'] = distance['duration']['text']
                                legs['distanceFromAirport'] = distance['distance']['text']
                legs.update({
                    'stops': (count_parts-1),
                    'durationWithDrive': legs['duration']+int(legs['timeFromAirport'].split(' ', 1)[0])+int(legs['timeToAirport'].split(' ', 1)[0])
                })
                print '########################'
                print legs['timeFromAirport'].split(' ', 1)[0]
                count_legs += 1
            option.update({
                'number_of_legs': count_legs,
                'passengersDetails': option['pricing'][0]['passengers']
            })
            del option['passengersDetails']['kind']

        QpxExpress.fix_airport_data(new_flights, airport_data, carrier_data)
        return new_flights

    @staticmethod
    def fix_airport_data(new_flights, airport_data, carrier_data):

        for flights in new_flights['trips']['tripOption']:
            for legs in flights['legs']:
                for parts in legs['part']:
                    for a_data in airport_data:
                        if parts['leg_details'][0]['destination'] == a_data['code']:
                            parts['leg_details'][0]['destination_airport'] = a_data['name']
                        elif parts['leg_details'][0]['origin'] == a_data['code']:
                            parts['leg_details'][0]['origin_airport'] = a_data['name']

                    for c_data in carrier_data:
                        if parts['flight']['carrier'] == c_data['code']:
                            parts['flight']['name'] = c_data['name']

    '''
    1-getting list of address legs
    2-find for each address nearest relevant airports
    3-measure time and distance from each airport to origin address and add to json
    4-matrix query from all relevant airports
    '''
    #  TODO complete fill data to json
    @staticmethod
    def qpx_wrapping(list_legs, dict_details, round_trip=False, return_date=''):
        vector_distance = []
        relevant_airports = QpxExpress.get_relevant_airports(list_legs)  # Relevant airports

        for i in range(len(list_legs)):
            vector_distance.append(QpxExpress.get_distance_airport(list_legs[i]['origin'], relevant_airports[i]))
            # Distance Vector
        vector_distance.append(QpxExpress.get_distance_airport(list_legs[-1]['destination'], relevant_airports[-1]))
        # Distance Vector

        clear_relevant_airport = QpxExpress.clear_relevant_airport_data(relevant_airports)
        permute = QpxExpress.permute_airports(clear_relevant_airport)
        print 'permute'
        print permute
        new_permute = QpxExpress.string_to_list(permute)  # All possible airports trips
        print 'new_permute'
        print new_permute
        all_fixed_queries = QpxExpress.fix_leg_address(list_legs, new_permute, dict_details, vector_distance,
                                                       round_trip, return_date)
        print 'all_fixed_queries'
        print all_fixed_queries
        return all_fixed_queries  # TODO fill missing data(distances)

    '''Private Method - getting the right airport for a specific address'''
    @staticmethod
    def get_relevant_airports(list_legs):
        relevant_airports = []
        for legs in list_legs:
            relevant_airports.append(amadeus.nearest_relevant_airport(legs['origin']))

        relevant_airports.append(amadeus.nearest_relevant_airport(list_legs[-1]['destination']))

        return relevant_airports

    '''gets distance matrix for specific address and many airports'''
    @staticmethod
    def get_distance_airport(address, relevant_airports):
        # airports = [airport['airport'] for airport in relevant_airports]
        airports = []
        for airport in relevant_airports:
                airports.append(airport['location'])

        vector_distance = google_maps.get_distance_one_to_many(address, airports)

        for i in range(len(relevant_airports)):
                vector_distance[i]['airport'] = relevant_airports[i]['airport']

        return vector_distance

    '''Recursion method , permute all optional trips from all optional airports '''
    @staticmethod
    def permute_airports(vector_distance):
        if len(vector_distance) == 1:
            return vector_distance[0]
        else:
            l_new = []
            for a in vector_distance[0]:
                for b in QpxExpress.permute_airports(vector_distance[1:]):
                    l_new.append(a + "," + b)
            return l_new

    '''Clear data from relevant airports and keeps the airports code'''
    @staticmethod
    def clear_relevant_airport_data(relevant_airports):
        new_l = []
        if relevant_airports is None:
            return new_l

        new_l = relevant_airports
        for legs in new_l:
            for airport in legs:
                del airport['city']
                del airport['distance']
                del airport['city_name']
                del airport['airport_name']
                del airport['aircraft_movements']
                del airport['location']
                del airport['timezone']

        no_dict = []
        trip = []

        for legs in new_l:
            for airport in legs:
                trip.append(airport['airport'])
            no_dict.append(trip)
            trip = []

        return no_dict

    '''Private method splits string and inserts to array'''
    @staticmethod
    def string_to_list(permute):
        new_l = []
        if permute is None:
            return new_l

        for airport in permute:
            airports = airport.split(",")
            new_l.append(airports)

        return new_l

    '''Private Method - Fix all addresses(origin and destination) in flights leg, for making correct queries'''
    @staticmethod
    def fix_leg_address(list_legs, new_permute, dict_details, vector_distance, round_trip=False, return_date=''):
        print "def fix_leg_address(list_legs, new_permute, dict_details, vector_distance, round_trip=False, return_date='')"
        print "list_legs: ", list_legs
        print "new_permute: ", new_permute
        print "dict_details: ", dict_details
        print "vector_distance: ", vector_distance
        new_l = []
        if list_legs is None or new_permute is None:
            print "Arguments Error"
            return new_l

        for i in range(len(new_permute)):
            new_l.append(list(list_legs))

        new_flights = []
        for i in range(len(new_permute)):
            for j in range(len(new_permute[i])-1):
                one = new_permute[i][j]
                two = new_permute[i][j+1]

                new_l[i][j]['origin'] = one
                new_l[i][j]['destination'] = two
            query = QpxExpress.qpx_flights(new_l[i], dict_details, round_trip, return_date)
            fix = QpxExpress.qpx_fix_json(query, vector_distance)
            new_flights.append(fix)

        return new_flights

    @staticmethod
    def create_qpx_json_from_request(request_json):
        # read form personal data
        personal = request_json['personal']
        adult = personal['numAdults']
        senior = personal['numElder']
        children = request_json['presonal']['numKids'] #there is a bug in the form --> need to put it personal

        # read form flight data
        flights = request_json['flights']
        flight1 = flights['0']
        origin = flight1['origin']
        destination = flight1['dest']
        date = flight1['firstDate'] #need to change it to correct format. e.g: '2016-08-20'
        max_stops = flight1['maxStops']
        start_hour = flight1['prefStartHour'] #need to change it to '03:00' instead of '3' that is returned in the form
        end_hour = flight1['prefEndHour'] #need to change it to '03:00' instead of '3' that is returned in the form

        dict_details = {
            'adult': adult,
            'child': children,
            'senior': senior,
            'infant_in_lap': 0,
            'infant_in_seat': 0,
            'max_price': '',
            'refundable': False,
            'solutions': 3
        }
        list_legs = [
            {
                'origin': origin,
                'destination': destination,
                'date': '2016-08-20',
                'max_stop': max_stops,
                'max_connection_duration': 2000,
                'cabin': '',
                'earliest_time': '05:00', # should be start_hour but need to parse it correctly
                'latest_time': '22:00'  # should be end_hour but need to parse it correctly
            }
        ]

        qpx_data = {'dict_details': dict_details, 'list_legs': list_legs}

        return qpx_data

