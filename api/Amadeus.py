import json
import urllib2
from api.google_maps import GoogleMaps

api_key = '9XJUXCbPBHaUL7czwbA1UCsaFPAvGUGt'


class Amadeus:
    @staticmethod
    def get_flights():
        params = {
            'origin': 'TLV',
            'destination': 'BCN',
            'departure_date': '2016-01-01',
            'return_date': '2016-01-05',
            'arrive_by': '',
            'return_by': '',
            'adults': 1,
            'children': 0,
            'infants': 0,
            'include_airlines': '',
            'exclude_airlines': '',
            'non-stop': False,
            'max_price': '',
            'currency': '',
            'travel_class': '',
            'number_of_results': 10
        }
        #Handle arrive by and return by parameters #TODO complete
        if not params['arrive_by'] and not params['return_by']:
            url = 'https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search?' \
                  'origin='+params['origin']+'&destination='+params['destination']+'' \
                  '&departure_date='+params['departure_date']+'&return_date='+params['return_date']+''\
                  '&adults='+str(params['adults'])+'&children='+str(params['children'])+'&infants='+str(params['infants'])+'' \
                  '&non-stop='+str(params['non-stop'])+'&number_of_results='+str(params['number_of_results'])+'&apikey='+ api_key
        elif params['arrive_by'] and not params['return_by']:
            url = 'https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search?' \
                  'origin='+params['origin']+'&destination='+params['destination']+'' \
                  '&departure_date='+params['departure_date']+'&return_date='+params['return_date']+''\
                  '&arrive_by='+params['arrive_by']+''\
                  '&adults='+str(params['adults'])+'&children='+str(params['children'])+'&infants='+str(params['infants'])+'' \
                  '&non-stop='+str(params['non-stop'])+'&number_of_results='+str(params['number_of_results'])+'&apikey='+ api_key
        elif not params['arrive_by'] and params['return_by']:
            url = 'https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search?' \
                  'origin='+params['origin']+'&destination='+params['destination']+'' \
                  '&departure_date='+params['departure_date']+'&return_date='+params['return_date']+''\
                  '&return_by='+params['return_by']+''\
                  '&adults='+str(params['adults'])+'&children='+str(params['children'])+'&infants='+str(params['infants'])+'' \
                  '&non-stop='+str(params['non-stop'])+'&number_of_results='+str(params['number_of_results'])+'&apikey='+ api_key
        else:
            url = 'https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search?' \
                  'origin='+params['origin']+'&destination='+params['destination']+'' \
                  '&departure_date='+params['departure_date']+'&return_date='+params['return_date']+''\
                  '&arrive_by='+params['arrive_by']+'&return_by='+params['return_by']+'' \
                  '&adults='+str(params['adults'])+'&children='+str(params['children'])+'&infants='+str(params['infants'])+'' \
                  '&non-stop='+str(params['non-stop'])+'&number_of_results='+str(params['number_of_results'])+'&apikey='+ api_key
        ##################

        req = urllib2.Request(url)
        u_open = urllib2.urlopen(req)
        read_url = u_open.read()
        result = json.loads(read_url)

        return result

    @staticmethod
    def nearest_relevant_airport(address, km=500):

        print "def nearest_relevant_airport(address, km=500):"
        print "address"
        print address
        relevant_airports = []
        g_maps = GoogleMaps
        geo_location = g_maps.get_geo(address)
        print "geo_location"
        print geo_location
        url = 'http://api.sandbox.amadeus.com/v1.2/airports/nearest-relevant?latitude='+str(geo_location['lat'])+'' \
              '&longitude='+str(geo_location['lng'])+'&apikey=' + api_key
        print "url"
        print url

        req = urllib2.Request(url)
        u_open = urllib2.urlopen(req)
        read_url = u_open.read()
        result = json.loads(read_url)
        print "result airports nearest-relevant"
        print result

        for airport in result:
            if float(airport['distance']) <= float(km):
                # ofir - remove SDV
                if airport['airport'] != 'SDV':
                    relevant_airports.append(airport)

        print "relevant_airports"
        print relevant_airports
        return relevant_airports

    @staticmethod
    def airport_autocomplete(term):
        url = 'http://api.sandbox.amadeus.com/v1.2/airports/autocomplete?apikey='+api_key+'&term='+term
        req = urllib2.Request(url)
        u_open = urllib2.urlopen(req)
        read_url = u_open.read()
        result = json.loads(read_url)
        return result
