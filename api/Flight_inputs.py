__author__ = 'Ofir & Elad & Doron'
from api.QpxExpress import QpxExpress
import logging


class QpxInputs:
    def __init__(self):
        pass

    @staticmethod
    # Input for running the QPX Express API from the server
    # QPX reference: https://developers.google.com/qpx-express/v1/trips/search#request-body
    # options to add: 1. two dates per leg (with earliest & latest per date) , location order flexability (TBD - how to get the data from client?),
    def ServerQpxInputsBasic():
        one_way = {"list_legs":[{"dates_combination_method":3,"dates_options":["2016-11-30T21:00:00.000Z","2016-12-30T21:00:00.000Z"],"destination":["sfo","mia"],"origin_input":"Ben Yehuda 21 Tel Aviv Israel","days_after_dates":0,"destination_input":"West Atlantic Boulevard, Pompano Beach, FL 33069, USA","origin":["lax","tlv"],"stay_duration":[],"days_before_dates":0,"destination_change_airport_possibility":1,"date":"2016-08-21","origin_change_airport_possibility":1,"origin_taxi":True,"origin_private":True,"origin_rental":True,"origin_public":True,"destination_taxi":True,"destination_private":True,"destination_rental":True,"destination_public":True,"arrive_depart":"depart","preferred_earliest_time":"00:00","preferred_latest_time":"23:59","max_connection_duration":1000,"cabin":None,"earliest_time":"00:00","latest_time":"23:59","max_stop":3,"$$hashKey":"object:5247"}],"legs_common_details":{"cabin":"economy","max_price":"usd99999","infant_in_lap":0,"refundable":True,"senior":0,"adult":1,"child":0,"saleCountry":"IL","infant_in_seat":0,"solutions":500,"alliance":"","corporate":False},"passengers":[4828780191285248,5110255167995904]}
        #one_way = {"list_legs":[{"dates_combination_method":3,"dates_options":["2016-10-30T21:00:00.000Z","2016-10-28T21:00:00.000Z","2016-10-27T21:00:00.000Z","2016-10-20T21:00:00.000Z"],"destination":["mia","sfo","cor", "lax","lon", "par"],"origin_input":"Ben Yehuda 21 Tel Aviv Israel","days_after_dates":0,"destination_input":"West Atlantic Boulevard, Pompano Beach, FL 33069, USA","origin":["tlv"],"stay_duration":[],"days_before_dates":0,"destination_change_airport_possibility":1,"date":"2016-08-21","origin_change_airport_possibility":1,"origin_taxi":True,"origin_private":True,"origin_rental":True,"origin_public":True,"destination_taxi":True,"destination_private":True,"destination_rental":True,"destination_public":True,"arrive_depart":"depart","preferred_earliest_time":"00:00","preferred_latest_time":"23:59","max_connection_duration":1000,"cabin":None,"earliest_time":"00:00","latest_time":"23:59","max_stop":3,"$$hashKey":"object:5247"}],"legs_common_details":{"cabin":"economy","max_price":"usd99999","infant_in_lap":0,"refundable":True,"senior":0,"adult":1,"child":0,"saleCountry":"IL","infant_in_seat":0,"solutions":500,"alliance":"","corporate":False},"passengers":[4828780191285248,5110255167995904]}
        round_trip = {"list_legs":[{"dates_combination_method":3,"dates_options":["2016-09-30T21:00:00.000Z"],"destination":["mia"],"origin_input":"Ben Yehuda 21 Tel Aviv Israel","days_after_dates":0,"destination_input":"","origin":["tlv"],"stay_duration":[],"days_before_dates":0,"destination_change_airport_possibility":1,"date":"2016-08-21","origin_change_airport_possibility":1,"origin_taxi":True,"origin_private":True,"origin_rental":True,"origin_public":True,"destination_taxi":True,"destination_private":True,"destination_rental":True,"destination_public":True,"arrive_depart":"depart","preferred_earliest_time":"00:00","preferred_latest_time":"23:59","max_connection_duration":1000,"cabin":None,"earliest_time":"00:00","latest_time":"23:59","max_stop":3,"$$hashKey":"object:5087"},{"dates_combination_method":3,"dates_options":["2016-11-12T21:00:00.000Z"],"destination":["bcn"],"origin_input":"","days_after_dates":0,"destination_input":"Ben Yehuda 21 Tel Aviv Israel","origin":["mia"],"stay_duration":[],"days_before_dates":0,"destination_change_airport_possibility":1,"date":"2016-08-21","origin_change_airport_possibility":1,"origin_taxi":True,"origin_private":True,"origin_rental":True,"origin_public":True,"destination_taxi":True,"destination_private":True,"destination_rental":True,"destination_public":True,"arrive_depart":"depart","preferred_earliest_time":"00:00","preferred_latest_time":"23:59","max_connection_duration":1000,"cabin":None,"earliest_time":"00:00","latest_time":"23:59","max_stop":3,"$$hashKey":"object:5107"}],"legs_common_details":{"cabin":"economy","max_price":"usd99999","infant_in_lap":0,"refundable":True,"senior":0,"adult":1,"child":0,"saleCountry":"IL","infant_in_seat":0,"solutions":500,"alliance":"","corporate":False},"passengers":[4828780191285248,5110255167995904]}
        round_trip = {"list_legs": [{"dates_combination_method": 1,
                                     "dates_options": ["2016-09-29T21:00:00.000Z"],
                                     "destination": ["MIA"], "origin_input": "Boyle Avenue, Fontana, CA 92337, USA",
                                     "days_after_dates": 0,
                                     "destination_input": "West Atlantic Boulevard, Pompano Beach, FL 33069, USA",
                                     "origin": ["LAX"], "stay_duration": [],
                                     "days_before_dates": 0, "destination_change_airport_possibility": 1,
                                     "date": "2016-09-15",
                                     "origin_change_airport_possibility": 1, "origin_taxi": True,
                                     "origin_private": True,
                                     "origin_rental": True, "origin_public": True, "destination_taxi": True,
                                     "destination_private": True, "destination_rental": True,
                                     "destination_public": True,
                                     "arrive_depart": "depart", "preferred_earliest_time": "00:00",
                                     "preferred_latest_time": "23:59",
                                     "max_connection_duration": 1000, "cabin": None, "earliest_time": "00:00",
                                     "latest_time": "23:59", "max_stop": 3,
                                     "$$hashKey": "object:5087"}
            ,
                                    {"dates_combination_method": 3, "dates_options": ["2016-09-12T21:00:00.000Z"],
                                     "destination": ["LAX"],
                                     "origin_input": "West Atlantic Boulevard, Pompano Beach, FL 33069, USA",
                                     "days_after_dates": 0, "destination_input": "Boyle Avenue, Fontana, CA 92337, USA",
                                     "origin": ["MIA"], "stay_duration": [],
                                     "days_before_dates": 0, "destination_change_airport_possibility": 1,
                                     "date": "2016-09-20", "origin_change_airport_possibility": 1,
                                     "origin_taxi": True, "origin_private": True, "origin_rental": True,
                                     "origin_public": True, "destination_taxi": True, "destination_private": True,
                                     "destination_rental": True, "destination_public": True, "arrive_depart": "depart",
                                     "preferred_earliest_time": "00:00", "preferred_latest_time": "23:59",
                                     "max_connection_duration": 1000, "cabin": None,
                                     "earliest_time": "00:00", "latest_time": "23:59", "max_stop": 3,
                                     "$$hashKey": "object:5107"}],
                      "legs_common_details": {"cabin": "economy", "max_price": "usd99999", "infant_in_lap": 0,
                                              "refundable": True, "senior": 0, "adult": 1, "child": 0,
                                              "saleCountry": "IL", "infant_in_seat": 0, "solutions": 500,
                                              "alliance": "", "corporate": False},
                      "passengers": [4828780191285248, 5110255167995904]}
        return one_way

    @staticmethod
    def ServerQpxInputsBasic2():
        print "******** def QpxInputsBasic********"

        legs_common_details = {
            'adult': 1,
            'child': 0,
            'senior': 0,
            'infant_in_lap': 0,
            'infant_in_seat': 0,
            'max_price': 'usd99999',
            'saleCountry': 'IL' ,  #TBD -can we use it? what are the input options? see codes at : http://www.fedex.com/gb/tracking/codes.html
            'refundable': False,  #TBD - search for both options. (boolean	Return only solutions with refundable fares.)
            'solutions': 5,  # max 500 - ERROR - there was an error
            'alliance': '' #TBD request.slice[].alliance - Slices with only the carriers in this alliance should be returned; do not use this field with permittedCarrier. Allowed values are ONEWORLD, SKYTEAM, and STAR.
            # TBD - add request.slice[].permittedCarrier[] & request.slice[].prohibitedCarrier[]
        }

        list_legs = [
            {
                #Airports and addresses
                'origin_input': 'TLV airport',
                # 3 airports: 'destination_input': '601 E Fairfield Rd, High Point, NC 27263, USA',
                'destination_input': 'Villa Carlos Paz cordoba argentina',
                'origin': ['TLV'],#TBD
                'destination': ['SFO'],
                'origin_change_airport_possibility': 1,
                'destination_change_airport_possibility': 1,
                # TBD - max_drive_time/max_distance (absolute from address or add from closest airport

                #Dates
                'date': '2016-08-21',  # TBD- leave empty and use dates combinations for filling
                'dates_options': ['2016-09-29','2016-10-01'],  #dates_options & dates_combination_method & stay_duration: if dates_combination_method is '0' - any combination of the dates in 'dates_options' of each leg. if '1' - the Nth item in the 'dates_options' list is only with the other Nth items in the other legs
                'dates_combination_method': 3,  #right now read this value only from leg 0!!!! 1 - do any combination of 'dates_options' , 2 - 'dates_options' is a list and use each item with the same place in the list of the other legs, 3 - from each dates_options values in leg 0 calculate the length stay by 'stay_duration' (stay_duration can have few options, all the combinations are calculated)
                'stay_duration':['7','8'],  # TBD- what about weekends (not working days)? can be stay duration of working days
                'days_after_dates': 0,  #check this num of dates after each date option. This is expand the 'dates_options' before using dates_combination_method
                'days_before_dates': 0,  #check this num of dates before each date option. This is expand the 'dates_options' before using dates_combination_method

                #Stops
                'max_stop': 3,
                'max_connection_duration': 1000,  #mintues. TBD- IS IT for all the leg or for each stop?
                # TBD - 'min_connection_duration'

                'cabin': '',  #Prefer solutions that book in this cabin for this slice. Allowed values are COACH, PREMIUM_COACH, BUSINESS, and FIRST.

                # Hours
                'earliest_time': '00:00',
                'latest_time': '23:00',
                'preferred_earliest_time': '00:00',
                'preferred_latest_time': '22:00',

                # Preparations
                'origin_airports': ['TLV'],
                'destination_airports': ['COR','AEP'],
                #'origin_airports': [{'airport':'TLV'}],
                #'destination_airports': [{'airport':'COR'},{'airport':'AEP'}],
            },
            {
                #Airports and addresses
                'origin_input': 'TLV airport',
                # 3 airports: 'destination_input': '601 E Fairfield Rd, High Point, NC 27263, USA',
                'destination_input': 'Villa Carlos Paz cordoba argentina',
                'origin': ['SFO'],#TBD
                'destination': ['TLV'],
                'origin_change_airport_possibility': 1,
                'destination_change_airport_possibility': 1,
                # TBD - max_drive_time/max_distance (absolute from address or add from closest airport

                #Dates
                'date': '2016-08-21',  # TBD- leave empty and use dates combinations for filling
                'dates_options': ['2016-10-29','2016-11-01'],  #dates_options & dates_combination_method & stay_duration: if dates_combination_method is '0' - any combination of the dates in 'dates_options' of each leg. if '1' - the Nth item in the 'dates_options' list is only with the other Nth items in the other legs
                'dates_combination_method': 3,  #right now read this value only from leg 0!!!! 1 - do any combination of 'dates_options' , 2 - 'dates_options' is a list and use each item with the same place in the list of the other legs, 3 - from each dates_options values in leg 0 calculate the length stay by 'stay_duration' (stay_duration can have few options, all the combinations are calculated)
                'stay_duration':['7','8'],  # TBD- what about weekends (not working days)? can be stay duration of working days
                'days_after_dates': 0,  #check this num of dates after each date option. This is expand the 'dates_options' before using dates_combination_method
                'days_before_dates': 0,  #check this num of dates before each date option. This is expand the 'dates_options' before using dates_combination_method

                #Stops
                'max_stop': 2,
                'max_connection_duration': 2000,  #mintues. TBD- IS IT for all the leg or for each stop?
                # TBD - 'min_connection_duration'

                'cabin': '',  #Prefer solutions that book in this cabin for this slice. Allowed values are COACH, PREMIUM_COACH, BUSINESS, and FIRST.

                # Hours
                'earliest_time': '00:00',
                'latest_time': '23:59',
                'preferred_earliest_time': '00:00',
                'preferred_latest_time': '23:59',

                # Preparations
                'origin_airports': ['COR','AEP'],
                'destination_airports': ['TLV'],
                #'origin_airports': [{'airport':'COR'},{'airport':'AEP'}],
                #'destination_airports': [{'airport':'TLV'}]
            }
           ,
           # {
           #      #Airports and addresses
           #      'origin_input': 'TLV airport',
           #      # 3 airports: 'destination_input': '601 E Fairfield Rd, High Point, NC 27263, USA',
           #      'destination_input': 'Villa Carlos Paz cordoba argentina',
           #      'origin': 'empty',#TBD
           #      'destination': 'empty',
           #      'origin_change_airport_possibility': 1, #
           #      'destination_change_airport_possibility': 1,
           #      # TBD - max_drive_time/max_distance (absolute from address or add from closest airport
           #
           #      #Dates
           #      'date': '2016-08-21',  # TBD- leave empty and use dates combinations for filling
           #      'dates_options': ['2016-10-29','2016-12-31'],  #dates_options & dates_combination_method & stay_duration: if dates_combination_method is '0' - any combination of the dates in 'dates_options' of each leg. if '1' - the Nth item in the 'dates_options' list is only with the other Nth items in the other legs
           #      'dates_combination_method': 'dontcare',  #right now read only from leg 0!!!! 0 - do any combination of 'dates_options' , 1 - 'dates_options' is a list and use each item with the same place in the list of the other legs, 2 - from each dates_options items calculate the length stay by 'stay_duration' (stay_duration can have few options)
           #      'stay_duration':['4'],  # TBD- what about weekends (not working days)? can be stay duration of working days
           #      'days_after_dates': 1,  #check this num of dates after each date option. This is expand the 'dates_options' before using dates_combination_method
           #      'days_before_dates': 0,  #check this num of dates before each date option. This is expand the 'dates_options' before using dates_combination_method
           #
           #      #Stops
           #      'max_stop': 2,
           #      'max_connection_duration': 2000,  #mintues. TBD- IS IT for all the leg or for each stop?
           #      # TBD - 'min_connection_duration'
           #
           #      'cabin': '',  #Prefer solutions that book in this cabin for this slice. Allowed values are COACH, PREMIUM_COACH, BUSINESS, and FIRST.
           #
           #      # Hours
           #      'earliest_time': '00:00',
           #      'latest_time': '23:59',
           #      'preferred_earliest_time': '00:00',
           #      'preferred_latest_time': '23:59',
           #
           #      # Preparations
           #      'origin_airports': {},
           #      'destination_airports': {}
           # }
           # ,
           # {
           #      #Airports and addresses
           #      'origin_input': 'TLV airport',
           #      # 3 airports: 'destination_input': '601 E Fairfield Rd, High Point, NC 27263, USA',
           #      'destination_input': 'Villa Carlos Paz cordoba argentina',
           #      'origin': 'empty',#TBD
           #      'destination': 'empty',
           #      'origin_change_airport_possibility': 1, #
           #      'destination_change_airport_possibility': 1,
           #      # TBD - max_drive_time/max_distance (absolute from address or add from closest airport
           #
           #      #Dates
           #      'date': '2016-08-21',  # TBD- leave empty and use dates combinations for filling
           #      'dates_options': ['2017-10-29','2017-12-31'],  #dates_options & dates_combination_method & stay_duration: if dates_combination_method is '0' - any combination of the dates in 'dates_options' of each leg. if '1' - the Nth item in the 'dates_options' list is only with the other Nth items in the other legs
           #      'dates_combination_method': 'dontcare',  #right now read only from leg 0!!!! 0 - do any combination of 'dates_options' , 1 - 'dates_options' is a list and use each item with the same place in the list of the other legs, 2 - from each dates_options items calculate the length stay by 'stay_duration' (stay_duration can have few options)
           #      'stay_duration':['4'],  # TBD- what about weekends (not working days)? can be stay duration of working days
           #      'days_after_dates': 0,  #check this num of dates after each date option. This is expand the 'dates_options' before using dates_combination_method
           #      'days_before_dates': 1,  #check this num of dates before each date option. This is expand the 'dates_options' before using dates_combination_method
           #
           #      #Stops
           #      'max_stop': 2,
           #      'max_connection_duration': 2000,  #mintues. TBD- IS IT for all the leg or for each stop?
           #      # TBD - 'min_connection_duration'
           #
           #      'cabin': '',  #Prefer solutions that book in this cabin for this slice. Allowed values are COACH, PREMIUM_COACH, BUSINESS, and FIRST.
           #
           #      # Hours
           #      'earliest_time': '00:00',
           #      'latest_time': '23:59',
           #      'preferred_earliest_time': '00:00',
           #      'preferred_latest_time': '23:59',
           #
           #      # Preparations
           #      'origin_airports': {},
           #      'destination_airports': {}
           # }
        ]

        # qpx_server_data = {'dict_details': dict_details, 'list_legs': list_legs}
        # print qpx_server_data
        # return qpx_server_data

        #######
        # 3/5/16
        # dict_details = {'senior': 0, 'solutions': 30, 'child': 0, 'adult': 1, 'alliance': '', 'refundable': False, 'infant_in_lap': 0, 'infant_in_seat': 0, 'max_price': ''}
        # list_legs = [{'destination': 'berlin germany', 'date': '2016-08-20', 'cabin': '', 'latest_time': '22:00', 'max_connection_duration': 2000, 'origin': 'london, england', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': 'london, england', 'date': '2016-09-15', 'cabin': '', 'latest_time': '23:00', 'max_connection_duration': 2000, 'origin': 'dam square, amsterdam', 'max_stop': 1, 'earliest_time': '05:00'}, {'destination': '86-88 Highland Ave Jersey City, NJ 07306, USA', 'date': '2016-10-20', 'cabin': '', 'latest_time': '19:00', 'max_connection_duration': 2000, 'origin': 'milan italy', 'max_stop': 2, 'earliest_time': '05:00'}]
        #######

        logging.debug('Tests - list_legs')
        logging.debug(list_legs)
        logging.debug('Tests - dict_details')
        logging.debug(legs_common_details)
        ServerQpxInputs = {'legs_common_details': legs_common_details, 'list_legs': list_legs}
        return ServerQpxInputs


    # TBD - fix the function below with the new flight form, add missing inputs
    # TBD - get input "number of legs" (or count how many legs) and create automatic form with flexibale No. of legs
    def FormQpxInputsBasic(request_json):
        # read form personal data
        personal = request_json['personal']
        adult = personal['numAdults']
        senior = personal['numElder']
        children = request_json['presonal']['numKids'] #there is a bug in the form --> need to put it personal

        # read form flight data
        flights = request_json['flights']
        flight1 = flights['0']
        origin = flight1['origin']
        destination = flight1['dest']
        date = flight1['firstDate'] #need to change it to correct format. e.g: '2016-08-20'
        max_stops = flight1['maxStops']
        start_hour = flight1['prefStartHour'] #need to change it to '03:00' instead of '3' that is returned in the form
        end_hour = flight1['prefEndHour'] #need to change it to '03:00' instead of '3' that is returned in the form

        dict_details = {
            'adult': adult,
            'child': children,
            'senior': senior,
            'infant_in_lap': 0,
            'infant_in_seat': 0,
            'max_price': '',
            'refundable': False,
            'solutions': 3
        }
        list_legs = [
            {
                'origin': origin,
                'destination': destination,
                'date': '2016-08-20',
                'max_stop': max_stops,
                'max_connection_duration': 2000,
                'cabin': '',
                'earliest_time': '05:00', # should be start_hour but need to parse it correctly
                'latest_time': '22:00'  # should be end_hour but need to parse it correctly
            }
        ]

        qpx_input_data = {'dict_details': dict_details, 'list_legs': list_legs}

        return qpx_input_data
