from __future__ import unicode_literals
import urllib
import re
import json
import time
import datetime
import logging
#maps_api_key = 'AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04'
#maps_api_key = 'AIzaSyDJIGLVjLrcqgCOToYiZBIZEmzwCAN4VdM'
#maps_api_key = 'AIzaSyCC1Ne42exQs66bT4euID-anESedD2vhts'
maps_api_key = 'AIzaSyDQbSs-uUPre7tqm9aggv3Oint47eJtbyE'
#maps_api_key = 'AIzaSyAy6G00RPTmSEGOa5ZOknDpqfuAINTqUqc'
#maps_api_key = 'AIzaSyCRHrZYPwIxEjZ4lcVhFs1jXyyIxB4ZbnQ'
#maps_api_key = 'AIzaSyAv1pWS9M3lGWo_6JemCMEyDxaFCLAUQ5Q'

class GoogleMaps:
    # google maps geocoding api = return geocoding for airport address
    @staticmethod
    def get_geo_airport(address):
        print 'def get_geo_airport(address)'
        print 'address'
        print address
        if not address:
            print "def get_geo_airport: not address"
            return None
        if address.isspace():
            new_address = address.replace(' ', '+')
        else:
            new_address = address

        new_address = re.sub(r'[^\x00-\x7F]+', '', new_address)
        new_address = new_address.replace(',', '+')

        zero = {'lat': 0, 'lng': 0}
        url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + new_address + 'airport&key=' + maps_api_key
        print "def get_geo_airport: url"
        print url
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        print "def get_geo_airport: result"
        print result
        status = result['status']
        if status == 'OK':
            geometry = result['results'][0]['geometry']['location']
            print "def get_geo_airport:geometry"
            print geometry
            return geometry
        else:
            print "def get_geo_airport:status != OK "
            return zero  # new

    # google maps geocoding api = return geocoding for general addresses
    @staticmethod
    def get_geo(address):
        #print "start def get_geo"
        if not address:
            #print "def get_geo - not address"
            return None
        if address.isspace():
            new_address = address.replace(' ', '+')
        else:
            new_address = address

        new_address = re.sub(r'[^\x00-\x7F]+', '', new_address)
        new_address = new_address.replace(',', '+')

        zero = {'lat': 0, 'lng': 0}
        url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + new_address + '&key=' + maps_api_key
        print "def get_geo: url"
        print url
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        status = result['status']
        if status == 'OK':
            geometry = result['results'][0]['geometry']['location']
            return geometry
            print "def get_geo:geometry"
            print geometry
        else:
            print "def get_geo:status != OK "
            return zero  # new

        # google maps geocoding api = return geocoding for general addresses

    @staticmethod
    def get_geo_string_result(address):
        print "start def get_geo"
        if not address:
            print "def get_geo - not address"
            return '0,0'
        if address.isspace():
            new_address = address.replace(' ', '+')
        else:
            new_address = address

        new_address = re.sub(r'[^\x00-\x7F]+', '', new_address)
        new_address = new_address.replace(',', '+')

        zero = '0,0'
        url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + new_address + '&key=' + maps_api_key
        # print "def get_geo: url"
        print url
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        status = result['status']
        if status == 'OK':
            geometry = str(result['results'][0]['geometry']['location']['lat']) + ',' + str(result['results'][0]['geometry']['location']['lng'])
            return geometry
            # print "def get_geo:geometry"
            # print geometry
        else:
            # print "def get_geo:status != OK "
            return zero  # new


    # google maps reverse geocoding api = return addresses for geocoding
    @staticmethod
    def get_reverse_geo(lat, lng):
        print "def get_reverse_geo(lat, lng):"
        print "lat"
        print lat
        print "lng"
        print lng
        if not lng or not lat:
            print "def get_reverse_geo - not lng or not lat"
            return None

        new_lat = str(lat)
        new_lng = str(lng)

        url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + new_lat + ',' + new_lng + '&key=' + maps_api_key
        print "def get_reverse_geo - url"
        print url
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        print "def get_reverse_geo - result"
        print result
        geometry = result['results'][0]['formatted_address']
        print "def get_reverse_geo - geometry"
        print geometry
        return geometry

    # google maps distanceMatrix api = return distance info for two lists of latLng(origins and destinations)
    @staticmethod
    def get_distance_info(list_origin, list_dest):
        print "def get_distance_info(list_origin, list_dest):"
        print 'get_distance_info - list_origin'
        print list_origin
        print 'get_distance_info - list_dest'
        print list_dest
        if not list_origin or not list_dest:
            print "def get_distance_info - not list_origin or not list_dest"
            return None

        origins = []
        dests = []

        for i in range(len(list_origin)):
            origins.append(GoogleMaps.get_reverse_geo(list_origin[i]['lat'], list_origin[i]['lng']))
        print "def get_distance_info - origins"
        print origins
        for i in range(len(list_dest)):
            dests.append(GoogleMaps.get_reverse_geo(list_dest[i]['lat'], list_dest[i]['lng']))
        print "def get_distance_info - dests"
        print dests
        # for i in origins:
        #     for ch in [' ','-','//']:
        #         if ch in i:
        #             i = i.replace(ch,'+')
        #     i = re.sub(r'[^\x00-\x7F]+','', i)
        #
        # for i in dests:
        #     for ch in [' ','-','//']:
        #         if ch in i:
        #             i = i.replace(ch,'+')
        #     i = re.sub(r'[^\x00-\x7F]+','', i)

        str_origin = origins[0]
        str_dest = dests[0]

        for i in range(1, len(origins)):
            str_origin = str_origin + '|' + origins[i]

        for i in range(1, len(dests)):
            str_dest = str_dest + '|' + dests[i]

        print "def get_distance_info - str_origin"
        print str_origin
        print "def get_distance_info - str_dest"
        print str_dest

        url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + str_origin + '&destinations=' + str_dest + '' \
                                                                                                              '&key=' + maps_api_key + '&language=en'
        print "def get_distance_info - url"
        print url
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        print "def get_distance_info - result"
        print result

        response = []
        for i in range(len(result['origin_addresses'])):
            for j in range(len(result['destination_addresses'])):
                response.append({'origin_address': result['origin_addresses'][i],
                                 'dest_address': result['destination_addresses'][j],
                                 'distance': {
                                     'text': result['rows'][i]['elements'][j]['distance']['text'],
                                     'value': result['rows'][i]['elements'][j]['distance']['value']
                                 },
                                 'duration': {
                                     'text': result['rows'][i]['elements'][j]['duration']['text'],
                                     'value': result['rows'][i]['elements'][j]['duration']['value']
                                 }})
        print "def get_distance_info - response"
        print response
        return response

    # google maps distanceMatrix api = return distance info for one to many addresses
    @staticmethod
    def get_distance_one_to_many(address, list_dest):
        print "def get_distance_one_to_many"
        if not address or not list_dest:
            print "not address or not list_dest"
            return None

        dests = []

        for i in range(len(list_dest)):
            dests.append(GoogleMaps.get_reverse_geo(list_dest[i]['latitude'], list_dest[i]['longitude']))

        str_dest = dests[0]

        for i in range(1, len(dests)):
            str_dest = str_dest + '|' + dests[i]

        address_encoded = urllib.quote(address.encode('utf-8'))
        str_dest_encoded = urllib.quote(str_dest.encode('utf-8'))
        # maps_api_key_encoded = urllib.quote(maps_api_key.encode('utf-8'))

        url_attributes = address + '&destinations=' + str_dest + '&key=' + maps_api_key + '&language=en'
        url_attributes_encoded = address_encoded + '&destinations=' + str_dest_encoded + '&key=' + maps_api_key + '&language=en'
        # url_attributes = urllib.quote(url_attributes.encode('utf-8'))
        url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + url_attributes_encoded
        print "def get_distance_one_to_many - url"
        print url

        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        print "def get_distance_one_to_many - result"
        print result
        response = []
        for i in range(len(result['origin_addresses'])):
            for j in range(len(result['destination_addresses'])):
                response.append({'origin_address': result['origin_addresses'][i],
                                 'dest_address': result['destination_addresses'][j],
                                 'distance': {
                                     'text': result['rows'][i]['elements'][j]['distance']['text'],
                                     'value': result['rows'][i]['elements'][j]['distance']['value']
                                 },
                                 'duration': {
                                     'text': result['rows'][i]['elements'][j]['duration']['text'],
                                     'value': result['rows'][i]['elements'][j]['duration']['value']
                                 }})
        print "def get_distance_one_to_many - response"
        print response
        return response
    @staticmethod
    def get_time_zone(ap_location):
        url = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + ap_location + '&timestamp=' + str(time.time()) + '&key=' + maps_api_key
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        logging.info("result['status']: %s" % result['status'])
        if result['status'] == 'OK':
            return result
        else:
            return -1
