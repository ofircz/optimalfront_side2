import requests
import urllib3

error_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">

               <soapenv:Header>

                  <ns:AuthHeader>

                     <ns:ResellerCode>V2CC</ns:ResellerCode>

                     <ns:Username>tomer</ns:Username>

                     <ns:Password>tomer1</ns:Password>

                  </ns:AuthHeader>

               </soapenv:Header>

               <soapenv:Body>

                  <ns:Air_LowFareSearchRQ>

                     <ns:Itinerary>

                        <ns:Route>

                           <ns:Origin Code="ams.par"/>

                           <ns:Destination Code="sfo"/>

                           <ns:Date>2016-11-08</ns:Date>

                           <!--Optional:-->

                        </ns:Route>

                    <ns:Route>

                           <ns:Origin Code="sfo"/>

                           <ns:Destination Code="TLV"/>

                           <ns:Date>2016-11-23</ns:Date>

                           <!--Optional:-->

                        </ns:Route>

                     </ns:Itinerary>

                     <ns:Passengers>

                        <ns:Passenger PTC="ADT" Count="1"/>

                     </ns:Passengers>

                     <ns:Filters>

                        <ns:DirectFlightsOnly>0</ns:DirectFlightsOnly>

                        <ns:Class></ns:Class>

                    </ns:Filters>

                  </ns:Air_LowFareSearchRQ>

               </soapenv:Body>

            </soapenv:Envelope>"""

one_way_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
   <soapenv:Header>
      <ns:AuthHeader>
         <ns:ResellerCode>V2CC</ns:ResellerCode>
         <ns:Username>tomer</ns:Username>
         <ns:Password>tomer1</ns:Password>
      </ns:AuthHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns:Air_LowFareSearchRQ>
         <ns:Itinerary>
            <ns:Route>
               <ns:Origin_Code="TLV"/>
               <ns:Destination Code="COR"/>
               <ns:Date>2015-11-29</ns:Date>
               <!--Optional:-->
               <ns:TimeWindow />
            </ns:Route>
            <ns:Route>
               <ns:Origin Code="COR"/>
               <ns:Destination Code="TLV"/>
               <ns:Date>2016-12-29</ns:Date>
               <!--Optional:-->
               <ns:TimeWindow />
            </ns:Route>
         </ns:Itinerary>
         <ns:Passengers>
            <ns:Passenger PTC="ADT" Count="1"/>
            <ns:Passenger PTC="CHD" Count="1"/>
         </ns:Passengers>
         <ns:Filters>
        </ns:Filters>
      </ns:Air_LowFareSearchRQ>
   </soapenv:Body>
</soapenv:Envelope>"""

msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
               <soapenv:Header>
                  <ns:AuthHeader>
                     <ns:ResellerCode>V2CC</ns:ResellerCode>
                     <ns:Username>tomer</ns:Username>
                     <ns:Password>tomer1</ns:Password>
                  </ns:AuthHeader>
               </soapenv:Header>
               <soapenv:Body>
                  <ns:Air_LowFareSearchRQ>
                     <ns:Itinerary>
                        <ns:Route>
                           <ns:Origin Code="ams"/>
                           <ns:Destination Code="mad"/>
                           <ns:Date>2016-11-21</ns:Date>
                           <!--Optional:-->
                        </ns:Route>
                    <ns:Route>
                           <ns:Origin Code="MAD"/>
                           <ns:Destination Code="par"/>
                           <ns:Date>2016-11-30</ns:Date>
                           <!--Optional:-->
                        </ns:Route>
                     </ns:Itinerary>
                     <ns:Passengers>
                        <ns:Passenger PTC="ADT" Count="1"/>
                     </ns:Passengers>
                     <ns:Filters>
                        <ns:DirectFlightsOnly>0</ns:DirectFlightsOnly>
                        <ns:Class></ns:Class>
                    </ns:Filters>
                  </ns:Air_LowFareSearchRQ>
               </soapenv:Body>
            </soapenv:Envelope>"""

request_json = {'timeout': 60,
                'limit_results': 2,
                'latitude': 41.3850639,
                'longitude': 2.1734035,
                'radius': 10,
                'start': '2017-01-10',
                'end': '2017-01-12',
                'adults': 2,
                'natinality_id': 1,
                'stars': [3, 4]}

hotel_search_template = """
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
       <soapenv:Header>
          <ns:AuthHeader>
             <ns:ResellerCode>V2CC</ns:ResellerCode>
             <ns:Username>tomer</ns:Username>
             <ns:Password>tomer1</ns:Password>
          </ns:AuthHeader>
       </soapenv:Header>
       <soapenv:Body>
          <ns:HotelGetAvailabilityRQ Timeout="%(timeout)s" AddHotelDetails="false" AddHotelMinPrice="false" LimitResults="%(limit_results)s" IgnoreHotelOffers="false">
             <ns:Location>
                <ns:Position Latitude="%(latitude)s" Longitude="%(longitude)s" Radius="%(radius)s"/>
             </ns:Location>
             <ns:DateRange Start="%(start)s" End="%(end)s"/>
             <ns:Rooms>
                <!--1 or more repetitions:-->
                <ns:Room Adults="%(adults)s" Children="0">
                   <!--0 to 2 repetitions:-->
             <!--      <ns:ChildAge>0</ns:ChildAge>-->
                </ns:Room>
             </ns:Rooms>
             <ns:Filters>
                <ns:Nationality ID="%(natinality_id)s"/>
                <ns:AvailableOnly>1</ns:AvailableOnly>
                <!--Optional:-->
                <!--Optional:-->
                <ns:HotelStars>
                   <!--Zero or more repetitions:-->
                   %(rating)s
                </ns:HotelStars>
                <!--Optional:-->
                <ns:CompleteOffersOnly>0</ns:CompleteOffersOnly>
                <!--Optional:-->
                <!--<ns:System>?</ns:System>-->
             </ns:Filters>
          </ns:HotelGetAvailabilityRQ>
       </soapenv:Body>
    </soapenv:Envelope>
"""

rating = "\n".join(["<ns:Rating>%s</ns:Rating>" % star for star in request_json['stars']])
request_json['rating'] = rating

msg = hotel_search_template % request_json

encoded_request = msg.encode('utf-8')
url = "http://www.talmatravel.co.il/tbs/reseller/ws/"
headers = {"Host": "www.talmatravel.co.il",
           "Content-Type": "text/xml;charset=UTF-8",
           "Content-Length": str(len(encoded_request)),
           "SOAPAction": "http://tbs.dcsplus.net/ws/Hotel_GetAvailability",
           "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
           "Connection": "Keep-Alive"}

response = requests.post(url,data=encoded_request,headers=headers,verify=False)
#print response.content
open('response.xml','w').write(response.content)
# import threading
# import Queue
#
#
# def wsdl_wrapper(something, queue):
#
#     #response = requests.post(url, data=encoded_request, headers=headers, verify=False)
#     response = urlfetch.fetch(url=url, headers=headers, payload=encoded_request,method=urlfetch.POST)
#     print "Got result from query"
#     queue.put(response.content)
#
#
# q = Queue.Queue()
#
# for i in range(1):
#     print "Sending query to TBS server (using threads)"
#     t = threading.Thread(target=wsdl_wrapper, args=(dict, q))
#     t.daemon = True
#     t.start()
#
# results = []
# for i in range(1):
#     print "Waiting for result %s" % i
#     res = q.get()
#     print "Adding result %s" % i
#     results.append(res)
#
# # write the first response to file
# fd = open('response.xml', 'r')
# fd.write(results[0])
# fd.close()
