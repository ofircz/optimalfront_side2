# from models import models
from models import models
from google.appengine.ext import ndb
import logging
import os
import platform


class DAL():

    @staticmethod
    def save_airport(city, code, airport_name, country, abbrev, world_area_code):
        from models.models import Airports
        _airport = Airports()
        qry = _airport.check_airport_exist(
            city, code, airport_name, country, abbrev, world_area_code)
        if not qry:
            _airport = Airports(city_name=city, code=code, airport_name=airport_name,
                                abbrevcountry=country, abbrev=abbrev, world_area_code=world_area_code)
            _airport.put()
            return True
        return False

    @staticmethod
    def save_airport_from_dict(dict, dict_latlng):
        from models.models import Airports
        _airport = Airports()
        city_name = dict['City name']
        code = dict['Airport Code']
        airport_name = dict['Airport Name']
        country = dict['Country name']
        abbrev = str(dict['Country Abbrev.'])
        world_area_code = int(float(dict['World Area Code']))
        lat = float(dict_latlng['lat'])
        lng = float(dict_latlng['lng'])
        qry = _airport.check_airport_exist(
            city_name, code, airport_name, country, abbrev, world_area_code, lat, lng)
        if not qry:
            _airport = Airports(city_name=city_name, code=code, airport_name=airport_name,
                                country=country, abbrev=abbrev, world_area_code=world_area_code, lat=lat, lng=lng)
            _airport.put()
            return True
        return False

    @staticmethod
    def save_flight(title, date, airline, departure, arrival, time, plane,
                    plane_class):
        from models.models import Flights
        _flight = Flights()
        qry = _flight.check_flight_exist(
            title, date, airline, departure, arrival, time, plane)
        if not qry:
            _flight = Flights(title=title, date=date, airline=airline,
                              departure=departure, arrival=arrival, time=time, plane=plane)
            _flight.put()
            return True
        return False

    @staticmethod
    def save_flight_from_dict(dict):
        # from models.models import Airports
        from models.models import Flights
        from models.models import Connection

        _connection = Connection()
        _connection.check_connection_exist(
            title=dict['title'], date=dict['date'], price=dict['price'])
        _connection = Connection(
            title=dict['title'], date=dict['date'], price=dict['price'])
        _connection.put()
        for inner_dict in dict['flights']:
            title = inner_dict['title']
            airline = inner_dict['airline']
            date = inner_dict['date']
            departure = inner_dict['departure']
            arrival = inner_dict['arrival']
            time = inner_dict['time']
            plane = inner_dict['plane']
            plane_class = inner_dict['class']
            _flight = Flights()
            qry = _flight.check_flight_exist(
                title, date, airline, departure, arrival, time, plane)
            if not qry:
                _flight = Flights(title=title, date=date, airline=airline, departure=departure,
                                  arrival=arrival, time=time, plane=plane, plane_class=plane_class)
                _flight.put()

    @staticmethod
    def save_csv_to_db(list):
        j = -1
        for i in list:
            j += 1
            if j < 5:
                add_csv_to_models(list[j])
                logging.info(list[j])
            else:
                break
                j += 1

    @staticmethod
    def findCitiesByCountry(country):
            from models.models import Cities
            cities = Cities.query(Cities.country == country).get()
            return cities

    @staticmethod
    def hotelLocation(hotel):
        from models.models import Cities
        lst = []
        temp = []
        lst.append(hotel.city)
        temp.append(Cities.query(Cities.city == hotel.city).get())
        for i in range(1):
            lst.append(temp[0].country)
        return lst

    @staticmethod
    def get_location(name):
        from models.models import Hotels
        hotel = Hotels.query(Hotels.name == name).get()
        return hotel

    @staticmethod
    def save_hotels(hotels_list):
        # for hotel in hotels_list:
            # save_hotel(hotel)
        return

    @staticmethod
    def csvParser():
        import csv
        dirname = os.path.abspath("csv")
        if (platform.system() == "Linux"):
            fold = "/"
        else:
            fold = "\\"
        return_list = []
        for file in os.listdir(dirname):
            with open(dirname+fold+file, 'r') as f:
                hotels = csv.reader(f)
                list = []
                i = 0
                return_list.append(file)
                for row in hotels:
                    if i > 10:
                        break
                    if i > 0:
                        list.append(row)
                    if i == 1:
                        i += 1
                return_list.append(list)
        DAL.save_csv_to_db(list)
        return return_list

#changed = getting lat and lng from xlsx and save all new airport details
    @staticmethod
    def xlsReader():
        from api.google_maps import GoogleMaps
        g = GoogleMaps
        from lib import xlrd
        fname = os.path.abspath("xls/airports.xlsx") #get the full path to file
        flights_book = xlrd.open_workbook(filename=fname) #open the xls file
        sheet_names = flights_book.sheet_names() #get all sheets names
        sheet = flights_book.sheet_by_name(sheet_names[0]) #take the first one ,
        keys = [sheet.cell(0, col_index).value for col_index in xrange(sheet.ncols)]

        dict_list = []  #create list
        dal = DAL()
        for row_index in xrange(1, sheet.nrows):
            d = {}
            counter = 0
            flag = False
            for col_index in xrange(sheet.ncols):
                if sheet.cell(row_index, col_index).value != "" and keys[col_index] != sheet.cell(row_index, col_index).value:
                    d[keys[col_index]] = sheet.cell(row_index, col_index).value
                else:
                    flag = True
                    break
            if d and not flag:
                if counter < 2:
                    airport = '' + d['Airport Name'] + d['Country name'] #new
                    geo = g.get_geo_airport(airport) #new
                    dal.save_airport_from_dict(d, geo) #new
                    counter += 1
                    dict_list.append(d)
        return dict_list

# def save_hotel(hotel):
#     from models.models import Hotels
#     hotel = Hotels()
#     hotel_dict = {}
#     # hotel_dict['hotel_id'] = 
#     hotel_dict['hotel_name'] = hotel['headline ']
#     source = 'hotwire'
#     # hotel_dict['hotel_address'] = 
#     hotel_dict['city'] = hotel['city']
#     hotel_dict['latitude'] = 
#     hotel_dict['longitude'] = 
#     hotel_dict['url'] = 
#     hotel_dict['longitude'] = 


def add_csv_to_models(list):
    hotel_dict = {
        "hotel_id": int(list[0]),
        "hotel_name": list[5],
        "address": list[8],
        "city": list[11],
        "rating": float(list[15]),
        "longitude": list[16],
        "latitude": list[17],
        "url": list[18],
        "checking": list[19],
        "checkout": list[19],
        "corrency": list[-1]
    }
    add_to_hotels(hotel_dict)
    country = {
        "state": list[12],
        "country": list[13],
        "countryisocode": list[14]
    }
    add_to_countries(country)

    city = {
        "zipcode": list[10],
        "city": list[11],
        "country": list[13]
    }
    add_to_cities(city)


def add_to_hotels(hotel_dict):
    from models.models import Hotels
    hotels = Hotels()
    qry = hotels.check_if_exist(hotel_dict)
    if not qry:
        hotels = Hotels(hotel_id=hotel_dict["hotel_id"], hotel_name=hotel_dict["hotel_name"],
                        address=hotel_dict[
            "address"], city=hotel_dict["city"],
            rating=hotel_dict["rating"], location=ndb.GeoPt(
            hotel_dict["latitude"], hotel_dict["longitude"]),
            url=hotel_dict["url"], checking=hotel_dict[
            "checking"],
            checkout=hotel_dict[
            "checkout"], corrency=hotel_dict["corrency"]
        )
        hotels.put()


def add_to_countries(country):
    from models.models import Countries
    countries = Countries()
    qry = countries.check_if_exist(country)
    if not qry:
        countries = Countries(country=country["country"],
                              state=country["state"],
                              countryisocode=country["countryisocode"]
                              )
        countries.put()


def add_to_cities(city):
    from models.models import Cities
    cities = Cities()
    qry = cities.check_if_exist(city)
    if not qry:
        cities = Cities(country=city["country"], city=city["city"],
                        zipcode=city["zipcode"]
                        )
        cities.put()
