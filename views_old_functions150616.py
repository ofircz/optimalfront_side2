__author__ = 'ad'

class TripSummary(webapp2.RequestHandler):
    def get(self):
        from api.Tests import QpxTest
        import parsers.Creating_json_file_for_client_side as csf
        t = QpxTest
        wrp = t.qpx()
        csf.json_data = wrp
        csf.main()
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(csf.my_data, indent=2))

def set_hotels():
    from api.trip_advisor import tripAdvisor
    t = tripAdvisor()
    main_hotels = [] # added row - tal
    params = {
    'lat': '42.33141',
    'long': '-71.099396'
    }
    hotels = t.get_hotel_list(params)
    json = {
              "coords": [
          {
            "latitude": "32.830593",
            "longitude": "-79.825432",
            "id": ""
          },
          {
            "latitude": "32.863488",
            "longitude": "-80.023833",
            "id": ""
          },
          {
            "latitude": "32.853438",
            "longitude": "-80.056785",
            "id": ""
          }
        ],
          'area_details': {'area': 'Lake Forest', 'State': 'California',
            'continent': 'America'},
          'order_details': {'start_date':'14.8','end_date':'18.8', 'duration':'4',
                            'adults': '1',
                            'children': '0',
                            'senior:':'0',
                            'breakfast': 'included',
                            'refundable': 'yes'},
                            'link_to_hotels_in_the_area': ['‫http://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799‬‬',
                                                   '‪http://www.booking.com/searchresults.he.html?city‬‬ ‫‪=20013943&aid=825774‬‬'],
          'highest_ranked': [{
          'name': '‫‪Staybridge‬‬ ‫‪Suites‬‬ ‫‪Irvine‬‬ ‫‪East‬‬ ‫‪-‬‬ ‫‪Lake‬‬ ‫‪Forest‬‬' ,
          'price': '946',
          'tax_to_pay_in_hotel':'10%',
          'rank': ranks('name'),
          'address':'2 Orchard, Lake Forest, California, United States',
          'link': 'https://www.hotelscombined.co.il/Hotel/Staybridge_Suites_Irvine_East_Lake_Forest.htm?a_aid=139799',
          'pension': 'breakfast',
          'reviews':reviews(),
          'hotel_details':hotel_details(hotels[0]),
          'added_device':hotel_addons(),
          'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg',
          },{
          'name': '‫‪Holiday‬‬ ‫‪Inn‬‬ ‫‪Irvine‬‬ ‫‪Spectrum‬‬' ,
          'price': ranks('name'),
          'tax_to_pay_in_hotel':'10%',
          'rank': '8.9' ,
          'address':'23131 Lake Center Drive, Lake Forest, California, United States',
          'link': 'https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Irvine_Spectrum.htm?a_aid=139799',
          'pension': 'breakfast',
                    'reviews':reviews(),
          'hotel_details':hotel_details(hotels[1]),
          'added_device':hotel_addons(),
          'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg',
          },{
          'name': '‫‪Prominence‬‬ ‫‪Hotel‬‬ ‫&‬ ‫‪Suites‬‬' ,
          'price': '465' ,
          'tax_to_pay_in_hotel':'10%',
          'rank': ranks('name'),
          'address':'20768 Lake Forest Drive, Lake Forest, California, United States',
          'link': 'https://www.hotelscombined.co.il/Hotel/Prominence_Hotel_Suites.htm?a_aid=139799',
          'pension': 'breakfast',
                    'reviews':reviews(),
          'hotel_details':hotel_details(hotels[2]),
          'added_device':hotel_addons(),
              'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg',
          }]
          },{
          'area':'North Carolina',
          'order_details': {'dates':'19-24.8', 'adults':'1', 'children': '0', 'breakfast': 'included', 'refundable':'yes'},
          'hotels_in_the_area':['‫‪‫‪http://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799‬‬'],
          'highest_ranked': [{
          'name': '‫‪‫‪Hawthorne‬‬ ‫‪Inn‬‬ ‫‪Winston‬‬ ‫‪Salem‬‬' ,
          'price': '601',
          'tax_to_pay_in_hotel':'0',
          'rank': ranks('name'),
          'link': 'https://www.hotelscombined.co.il/Hotel/Hawthorne_Inn_Winston_Salem.htm?a_aid=139799',
          'address':'460 North Cherry Street, Winston-Salem, North Carolina, United States',
          'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg',

                    'reviews':reviews(),
          'hotel_details':hotel_details(hotels[3]),
          'added_device':hotel_addons()
          },{
          'name': '‫‪‫‪Embassy‬‬ ‫‪Suites‬‬ ‫‪Hotel‬‬ ‫‪Winston‬‬ ‫‪-‬‬ ‫‪Salem‬‬' ,
          'price': '834' ,
          'tax_to_pay_in_hotel':'0',
          'rank': ranks('name'),
          'address':'460 North Cherry Street, Winston-Salem, North Carolina, United States',
          'link': 'https://www.hotelscombined.co.il/Hotel/Embassy_Suites_Hotel_Winston_Salem.htm?a_aid=139799',
               'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg'
          },{
          'name': '‫‪‫‪Holiday‬‬ ‫‪Inn‬‬ ‫‪Express‬‬ ‫‪Winston‬‬ ‫‪-‬‬ ‫‪Salem‬‬' ,
          'price': '685' ,
          'tax_to_pay_in_hotel':'0',
          'rank': ranks('name'),
          'address':'2520 Peters Creek Pkwy, Winston-Salem, North Carolina, United States',
          'link': 'https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Express_Winston_Salem.htm?a_aid=139799',
               'pictures':[
          'https://media.datahc.com/HI104237475.jpg',
          'https://media.datahc.com/HI104237501.jpg',
          'https://media.datahc.com/HI104237478.jpg',
          'https://media.datahc.com/HI104237497.jpg'
          ],
          'header':'https://media.datahc.com/HI104237497.jpg',

                    'reviews':reviews(),
          'hotel_details':hotel_details(hotels[5]),
          'added_device':hotel_addons()
          }
          ]}
    main_hotels.append(json) # added row - tal
    return main_hotels #added row - tal
    #return json

def reviews(hotel = None):
    reviews = {
    'source0':'this is a review',
    'source1': 'this is another review',
    'source2': 'third review'
    }
    return reviews


def hotel_addons(hotel = None):
    devices = [
    'first device',
    'second device',
    'third device'
    ]
    return devices


def hotel_details(hotel = None):
    hotel_dict = {
    'check_in': '12:00',
    'check_out': '14:00',
    'reception': {'from':'7:00', 'to':'23:00'},
    'availeable_rooms': '250',
    'laggage_keeping':'yes',
    'parking':'yes',
    'stars':hotel['rating']['rate'],
    'rooms':[{
    'type':'single',
    'price_per_night':'60'
    }],
    'lat': hotel['lat'],
    'long': hotel['long'],
    'distances':{
    'from_address':{
    'drive':'10',
    'aerial': '10',
    'driving_time':'25'
    },
    'from_airport':{
    'drive':'10',
    'aerial': '10',
    'driving_time':'25'
    },
    }
    }
    return hotel_dict

def ranks(hotel):
    ranks = {
        'tripadvisor':{
        'rank':'9',
        'ranks_num':'1000'
        },
        'site1':{
        'rank':'9',
        'ranks_num':'1000'
        },
        'site2':{
        'rank':'9',
        'ranks_num':'1000'
        },
        'average_rank':'9',
        'ranks_sum':'3000'
    }
    return ranks

# createdb page to parse the xls
class CreateBb(webapp2.RequestHandler):
    def get(self):
        d = DAL()
        output = json.dumps(d.xlsReader(), indent=4)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(output)


# class csvParser(webapp2.RequestHandler):
#     def get(self):
#         dal = DAL()
#         self.response.headers['Content-Type'] = 'application/json'
#         self.response.write(json.dumps(dal.csvParser()))
#
#
# class FileUploadFormHandler(webapp2.RequestHandler):
#     def get(self):
#         fix_path()
#         upload_url = blobstore.create_upload_url('/upload_file')
#         template_values = {
#             'url': upload_url,
#         }
#         # blobstore.delete(blob_key)
#         jinja = jinja2.get_jinja2(app=self.app)
#
#         template = jinja.render_template("upload.html", **template_values)
#
#         # template = jinja.get_template('web/upload.html')
#         self.response.write(template)


# class FileUploadHandler(blobstore_handlers.BlobstoreUploadHandler):
#
#     def post(self):
#         try:
#             from models.models import UserFiles
#             upload = self.get_uploads()[0]
#             user_file = UserFiles(blob_key=upload.key())
#             user_file.put()
#             self.redirect('/view_file/%s' % upload.key())
#         except:
#             self.error(500)
#
#
# class ViewFileHandler(blobstore_handlers.BlobstoreDownloadHandler):
#     def get(self, blob_key):
#         from parsers.parser import Parser
#         p = Parser()
#         list = p.main(blob_key)
#         template_values = {
#             'data': list,
#         }
#         # blobstore.delete(blob_key)
#         # template = JINJA_ENVIRONMENT.get_template('admin.html')
#         # self.response.write(template.render(template_values))
#         jinja = jinja2.get_jinja2(app=self.app)
#
#         template = jinja.render_template("admin.html", **template_values)
#
#         # template = jinja.get_template('web/upload.html')
#         self.response.write(template)
#
#         # self.response.headers['Content-Type'] = 'application/json'
#         # self.response.write(json.dumps(list, indent=4))


# class UpdateFlight(webapp2.RequestHandler):
#     """Update the selected data from JS"""
#     def post(self):
#         global Flights
#         blob = self.request.body
#         stuff = json.loads(blob)
#         self.response.headers['Content-Type'] = 'application/json'
#         self.response.write(json.dumps(stuff, indent=4))


class TripAdvisor(webapp2.RequestHandler):
    def get(self):
        from DAL import DAL
        from api.trip_advisor import tripAdvisor
        t = tripAdvisor()
        params = {
        'lat': '42.33141',
        'long': '-71.099396',
        'hotel_name': 'enVision Hotel Boston'
        }
        dict = t.mapper(params)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(dict, indent=2))

class apiTester(webapp2.RequestHandler):

    def get(self):
      from google.appengine.api import urlfetch
      url = "https://www.hotel.de/home.aspx?han=8701967&pcon=1&hs_destination=Berlin"
      try:
        result = urlfetch.fetch(url,  method=urlfetch.GET)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(result.content)

      except urllib2.URLError, e:
        handleError(e)


class GetHotelsTripAdvisor(webapp2.RequestHandler):
    #return hotels
    @staticmethod
    def get(address):
        api = apiHandler()
        hotels = api.get_hotels_by_address(address)
        return hotels

    #return best hotels
    @staticmethod
    def get_best(address):
        api = apiHandler()
        best_hotels = api.hotels_selection(address)
        return best_hotels


class TestFlightsFromClient(webapp2.RequestHandler):
    def post(self):
        from api.Flight_inputs import QpxInputs
        # from api.QpxExpress import QpxExpress
        from api.FlightsArea import FlightsMain
        import parsers.FlightAlg as FAlg

        data = json.loads(self.request.body)
        # read form data:
        obj = data['obj']

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        # dict_details = server_qpx_inputs['dict_details']
        # list_legs = server_qpx_inputs['list_legs']
        # wrap_qpx = QpxExpress.qpx_wrapping(list_legs, dict_details)
        wrap_qpx = FlightsMain.GetAllFlights(server_qpx_inputs)
        FAlg.json_data = wrap_qpx
        selected_flights1 = FAlg.main(wrap_qpx, server_qpx_inputs)
        # flights_for_client = FlightsMain.FlightsParseNCalc(wrap_qpx,server_qpx_inputs)

        self.response.headers['Content-Type'] = 'application/json'
        res1 = {'QPX_res': wrap_qpx, 'alg_res': FAlg.my_data}
        # self.response.write(json.dumps(res1, indent=2))
        self.response.write(json.dumps(FAlg.my_data, indent=2))

        # from api.QpxExpress import QpxExpress
        # qpx = QpxExpress
        #
        # qpx_request = qpx.create_qpx_json_from_request(obj)
        #
        #
        # dict_details = qpx_request['dict_details']
        # list_legs = qpx_request['list_legs']
        #
        # wrap_qpx = qpx.qpx_wrapping(list_legs, dict_details)
        #
        # self.response.write("qpx response:")
        # self.response.write(json.dumps(wrap_qpx, indent=4))
        #
        # return self.response
