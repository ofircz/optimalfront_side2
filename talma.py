import suds
import sys
import json
import logging


class wsdl_client():
    """ This client is needed to send queries to the TBS API.
        The class allows us to pass around this object, and initiate it
        only when needed and only once. Also, we use a cached version which
        lays locally. If original changes, we need to update as well, or to direct
        to the original: http://www.talmatravel.co.il/tbs/reseller/ws/?wsdl """
    def __init__(self):
        self.client = None

    def get_client(self):
        if self.client:
            return self.client
        else:
            import suds
            url = 'http://www.talmatravel.co.il/tbs/reseller/ws/?wsdl'
            print "Loading WSDL client"
            self.client = suds.client.Client(url)
            print "Client is loaded"
            return self.client

one_way_msg = """"<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
   <soapenv:Header>
      <ns:AuthHeader>
         <ns:ResellerCode>V2CC</ns:ResellerCode>
         <ns:Username>tomer</ns:Username>
         <ns:Password>tomer1</ns:Password>
      </ns:AuthHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns:Air_LowFareSearchRQ>
         <ns:Itinerary>
            <ns:Route>
               <ns:Origin Code="TLV"/>
               <ns:Destination Code="COR"/>
               <ns:Date>2016-11-29</ns:Date>
               <!--Optional:-->
               <ns:TimeWindow />
            </ns:Route>
         </ns:Itinerary>
         <ns:Passengers>
            <ns:Passenger PTC="ADT" Count="1"/>
         </ns:Passengers>
         <ns:Filters>
        </ns:Filters>
      </ns:Air_LowFareSearchRQ>
   </soapenv:Body>
</soapenv:Envelope>"""

roudtrip_msg = """  <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
           <soapenv:Header>
              <ns:AuthHeader>
                 <ns:ResellerCode>V2CC</ns:ResellerCode>
                 <ns:Username>tomer</ns:Username>
                 <ns:Password>tomer1</ns:Password>
              </ns:AuthHeader>
           </soapenv:Header>
           <soapenv:Body>
              <ns:Air_LowFareSearchRQ>
                 <ns:Itinerary>
                    <ns:Route>
                       <ns:Origin Code="TLV"/>
                       <ns:Destination Code="COR"/>
                       <ns:Date>2016-08-29</ns:Date>
                       <!--Optional:-->
                       <ns:TimeWindow />
                    </ns:Route>
                    <ns:Route>
                       <ns:Origin Code="COR"/>
                       <ns:Destination Code="TLV"/>
                       <ns:Date>2016-09-12</ns:Date>
                       <!--Optional:-->
                       <ns:TimeWindow />
                    </ns:Route>
                 </ns:Itinerary>
                 <ns:Passengers>
                    <ns:Passenger PTC="ADT" Count="2"/>
                 </ns:Passengers>
                 <ns:Filters>
                </ns:Filters>
              </ns:Air_LowFareSearchRQ>
           </soapenv:Body>
        </soapenv:Envelope>"""


booking_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                            xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
                            <soapenv:Header>
                            <ns:AuthHeader>
                                     <ns:ResellerCode>V2CC</ns:ResellerCode>
                                     <ns:Username>tomer</ns:Username>
                                     <ns:Password>tomer1</ns:Password>
                            </ns:AuthHeader>
                            </soapenv:Header>
                            <soapenv:Body>
                            <ns:Air_MakeReservationRQ>
                            <ns:ExternalRef></ns:ExternalRef>
                            <ns:Passengers>
                            <ns:Passenger PaxID="0" PaxRef="Passenger unique ID" Type="adult" Lead="true">
                                <ns:FirstName>Uri</ns:FirstName>
                                <ns:LastName>Milman</ns:LastName>
                                <ns:BirthDate>1980-02-15</ns:BirthDate>
                                <ns:Email>uri@twohavefun.com</ns:Email>
                            </ns:Passenger>
                            </ns:Passengers>
                            <ns:ServiceConfig>
                                <ns:AirService ResultCode="AIR:WS:V2CC:7b6b1c89091297c43a3d5c2479e5582e">
                                    <ns:ItineraryCode>2f54786446455f293a43e6a863ab86d41cd3d2a3:0</ns:ItineraryCode>
                                    <ns:Passengers>
                                    <ns:PaxRef PTC="ADT">1</ns:PaxRef>
                                    </ns:Passengers>
                                    <ns:Comments>webservice reservation - comments</ns:Comments>
                                </ns:AirService>
                            </ns:ServiceConfig>
                            </ns:Air_MakeReservationRQ>
                            </soapenv:Body>
                            </soapenv:Envelope>"""

#
#

query_booking_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
   <soapenv:Header>
      <ns:AuthHeader>
         <ns:ResellerCode>V2CC</ns:ResellerCode>
         <ns:Username>tomer</ns:Username>
         <ns:Password>tomer1</ns:Password>
         <!--Optional:-->
         <ns:ApplicationId>?</ns:ApplicationId>
      </ns:AuthHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns:ReservationGetDetailsRQ>
         <ns:ReservationID>84260</ns:ReservationID>
      </ns:ReservationGetDetailsRQ>
   </soapenv:Body>
</soapenv:Envelope>"""
## start Parsing

def Convert_TBS_to_Our_Format(results):

    # one-way query, only one leg per itinerary
    itineraries = []
    for query_results in results:  # results has answers from multiple queries
        ResultCode = query_results.PricedItineraries._ResultCode  # this code is required for booking
        for element in query_results.PricedItineraries.PricedItinerary:  # flights are grouped by price
            logging.debug("Price Group: %s%s" % (element.Price._Currency, element.Price._Amount))
            # get all legs in searchable data structure
            legs = {}
            for route_outer in element.Routes:  # 1st index
                logging.debug("Outer Index: ", route_outer._Index)
                legs[route_outer._Index] = {}
                for route in route_outer.Route:  # 2nd index
                    leg = {'duration': route._Duration if "_Duration" in route.__keylist__ else "N/A",
                           'segments': [],
                           }
                    legs[route_outer._Index][route._Ref] = leg
                    for tbs_segment in route['Segment']:
                        logging.debug("Building segment: %s -> %s" % (
                        tbs_segment.Origin.Airport._Code, tbs_segment.Destination.Airport._Code))
                        segment = {"origin": {
                            "date": str(tbs_segment.Origin._Date),
                            "time": str(tbs_segment.Origin._Time),
                            "terminal:": tbs_segment.Origin._Terminal if "_Terminal" in tbs_segment.Origin.__keylist__ else "N/A",
                            "airport": {"name": tbs_segment.Origin.Airport.value,
                                        "city": tbs_segment.Origin.Airport._City,
                                        "code": tbs_segment.Origin.Airport._Code,
                                        "city_code": tbs_segment.Origin.Airport._CityCode,
                                        "city_id": tbs_segment.Origin.Airport._CityId}
                        },
                            "destination": {
                                "date": str(tbs_segment.Destination._Date),
                                "time": str(tbs_segment.Destination._Time),
                                "terminal:": tbs_segment.Destination._Terminal if "_Terminal" in tbs_segment.Destination.__keylist__ else "N/A",
                                "airport": {"name": tbs_segment.Destination.Airport.value,
                                            "city": tbs_segment.Destination.Airport._City,
                                            "code": tbs_segment.Destination.Airport._Code,
                                            "city_code": tbs_segment.Destination.Airport._CityCode,
                                            "city_id": tbs_segment.Destination.Airport._CityId}
                            },
                            "flight": {
                                "duration": tbs_segment.Flight._Duration if "_Duration" in tbs_segment.Flight.__keylist__ else "N/A",
                                "meal": tbs_segment.Flight._Meal if "_Meal" in tbs_segment.Flight.__keylist__ else "N/A",
                                "number": tbs_segment.Flight._Number,
                                "class": tbs_segment.Flight._Class,
                                "stop_time": tbs_segment.Flight._StopTime if "_StopTime" in tbs_segment.Flight.__keylist__ else "00:00",
                                "number_of_seats": tbs_segment.Flight._NumberOfSeats,
                                #"CodeShareInfo": tbs_segment.Flight._CodeShareInfo,
                                },
                            "aircraft": {"name": tbs_segment.Aircraft.value,
                                         "code": tbs_segment.Aircraft._Code,
                                         },
                            "carrier": {"marketing": {"name": tbs_segment.Carrier.Marketing.value,
                                                      "code": tbs_segment.Carrier.Marketing._Code},
                                        "operating": {"name": tbs_segment.Carrier.Operating.value,
                                                      "code": tbs_segment.Carrier.Operating._Code},
                                        },
                            "mileage": "N/A",
                        }
                        leg['segments'].append(segment)
            for tbs_itinerary in element.Itinerary:
                logging.debug("Itinerary %s" % tbs_itinerary._ItineraryCode)
                itinerary = {'api': 'TBS',
                             'id': tbs_itinerary._ItineraryCode,
                             'resultCode': ResultCode,
                             'price': element.Price._Amount,
                             'currency': element.Price._Currency,
                             'fare_details': {"IsAutoTicketable": element.FareDetails._IsAutoTicketable,
                                              "Currency": element.FareDetails._Currency,
                                              "BaseFare": element.FareDetails._BaseFare,
                                              "FullFare": element.FareDetails._FullFare,
                                              "ServiceFee": element.FareDetails._ServiceFee,
                                              "ValidatingCarrier": element.FareDetails.ValidatingCarrier.value,
                                              "ValidatingCarrierCode": element.FareDetails.ValidatingCarrier._Code,
                                              "PaxFare": [{"count": pax._Count,
                                                           "FullFare": pax._FullFare,
                                                           "BaseFare": pax._BaseFare,
                                                           "ServiceFee": pax._ServiceFee,
                                                           "Currency": pax._Currency,
                                                           "PTC": pax._PTC} for pax in element.FareDetails.PaxFare],
                                              },
                             'legs': [],
                             }
                for tbs_leg in tbs_itinerary.RouteRef:
                    logging.debug("Leg: Index %s, Ref: %s" % (tbs_leg._Index, tbs_leg._Ref))
                    itinerary['legs'].append(legs[tbs_leg._Index][tbs_leg._Ref])
                    itineraries.append(itinerary)
                    #Itineraries(parent=ancestor_key, index=0, itinerary=json.dumps(itinerary)).put()
    return itineraries


def wsdl_wrapper(function, query, queue):
    ans = function(__inject={'msg': query})
    logging.info("Got result from query")
    queue.put(ans)




import threading
import Queue

client = wsdl_client().get_client()
q = Queue.Queue()

for i in range(1):
    logging.info("Sending query to TBS server (using threads)")
    logging.info(one_way_msg)
    t = threading.Thread(target=wsdl_wrapper, args=(client.service.Air_LowFareSearch, one_way_msg, q))
    t.daemon = True
    t.start()

results = []
for i in range(1):
    print "Waiting for result %s" % i
    res = q.get()
    print "Adding result %s" % i
    results.append(res)

#output = client.service.Air_LowFareSearch(__inject={'msg':roudtrip_msg})


#output = client.service.Reservation_GetDetails(__inject={'msg':query_booking_msg})

# print output
#
# passengers = []
# for passenger in output.Reservation.Passengers.Passenger:
#     passengers.append({'firstname': passenger.FirstName, 'lastname': passenger.LastName})
#
# pax_fare = []
# for pax in output.Reservation.Services.AirService.FareDetails.PaxFare:
#     allowance = []
#     for baggage in pax.BaggageAllowance:
#         tbs_segment = baggage.Segment
#         allowance.append({'baggage_allowed':baggage.Baggage._Allowed,
#                           'segemnt': {
#                             "origin": {
#                             "date": str(tbs_segment.Origin._Date),
#                             #"time": str(tbs_segment.Origin._Time),
#                             "terminal:": tbs_segment.Origin._Terminal if "_Terminal" in tbs_segment.Origin.__keylist__ else "N/A",
#                             "airport": {"name": tbs_segment.Origin.Airport.value,
#                                         "city": tbs_segment.Origin.Airport._City,
#                                         "code": tbs_segment.Origin.Airport._Code,
#                                         "city_code": tbs_segment.Origin.Airport._CityCode,
#                                         "city_id": tbs_segment.Origin.Airport._CityId}
#                         },
#                             "destination": {
#                                 "date": str(tbs_segment.Destination._Date),
#                                # "time": str(tbs_segment.Destination._Time),
#                                 "terminal:": tbs_segment.Destination._Terminal if "_Terminal" in tbs_segment.Destination.__keylist__ else "N/A",
#                                 "airport": {"name": tbs_segment.Destination.Airport.value,
#                                             "city": tbs_segment.Destination.Airport._City,
#                                             "code": tbs_segment.Destination.Airport._Code,
#                                             "city_code": tbs_segment.Destination.Airport._CityCode,
#                                             "city_id": tbs_segment.Destination.Airport._CityId}
#                             },
#                             "flight": {
#                                 "duration": tbs_segment.Flight._Duration if "_Duration" in tbs_segment.Flight.__keylist__ else "N/A",
#                                 "meal": tbs_segment.Flight._Meal if "_Meal" in tbs_segment.Flight.__keylist__ else "N/A",
#                                 "number": tbs_segment.Flight._Number,
#                                 #"class": tbs_segment.Flight._Class,
#                                 "stop_time": tbs_segment.Flight._StopTime if "_StopTime" in tbs_segment.Flight.__keylist__ else "00:00",
#                                 #"number_of_seats": tbs_segment.Flight._NumberOfSeats,
#                                 # "CodeShareInfo": tbs_segment.Flight._CodeShareInfo,
#                             },
#                             "carrier": {"marketing": {"name": tbs_segment.Carrier.Marketing.value,
#                                                       "code": tbs_segment.Carrier.Marketing._Code},
#                                         # "operating": {"name": tbs_segment.Carrier.Operating.value,
#                                         #               "code": tbs_segment.Carrier.Operating._Code},
#                                         }}})
#     pax_fare.append({'BaggageAllowance': allowance})
#
# ReservationDetails = { 'agency': {'code':  output.Reservation.Agency._Code,
#                                   'id': output.Reservation.Agency._ID,
#                                    'name': output.Reservation.Agency._Name },
#                        'passengers': passengers,
#                        'services': {'air_service':
#                                         {'fair_details': {
#                                              'pax_fare':pax_fare,
#                                              'time_limit':output.Reservation.Services.AirService.FareDetails.TimeLimit._Date,
#                                              'validating_carrier': {
#                                                  'name': output.Reservation.Services.AirService.FareDetails.ValidatingCarrier.value,
#                                                  'code': output.Reservation.Services.AirService.FareDetails.ValidatingCarrier._Code,
#                                              },
#                                             'online_check_url': output.Reservation.Services.AirService.OnlineCheckInUrl,
#                                             'pnr_lines': None, # not sure we need it
#                                             'account': None, # we don't need it
#                                             'documents': output.Reservation.Services.AirService.Documents,
#                                             'price': {
#                                                     'currency': output.Reservation.Services.AirService.Price._Currency,
#                                                     'amount': output.Reservation.Services.AirService.Price._Amount,
#                                             },
#                                             'segments': output.Reservation.Services.AirService.Segments,
#                                             'service_period': {
#                                                 'start_date': output.Reservation.Services.AirService.ServicePeriod.StartDate,
#                                                 'end_date': output.Reservation.Services.AirService.ServicePeriod.EndDate,
#                                                 'duration': output.Reservation.Services.AirService.ServicePeriod.Duration,
#                                                 'duration_type':  output.Reservation.Services.AirService.ServicePeriod.DurationType
#
#                                             }
#                                          }
#                                     }
#                        }}
#
# # for passenger in output.Reservation.Passengers:
# #     print passenger[1][0].FirstName
# #     print passenger[1][0].LastName
#
# #itineraries = Convert_TBS_to_Our_Format([output])
# #print json.dumps(itineraries[0])
#
#
# #output = client.service.Air_MakeReservation(__inject={'msg':booking_msg})
#
