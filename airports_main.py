__author__ = 'Ofir'

import json
import logging
import webapp2
from google.appengine.ext import ndb
from lib import requests
import urllib2
import urllib
import json
from math import cos, asin, sqrt
from api.Amadeus import Amadeus
from api.google_maps import GoogleMaps
from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(120)
from airports_codes_and_amadeus_db import AirportCodesDB
from ap_db_locations import AirportDbLocations
from ap_db_loc_wo_doubles import AirportDbLocationsWoDoubles
from ap_db_with_timezone import AirportDbTimeZoneWoDoubles
from ap_nearby_400km import AirportDbNearby400KM

amadeus = Amadeus
g_maps = GoogleMaps

class AP_DBs_main(webapp2.RequestHandler):

########################################
#todo : Allways change tlv geocode to 32.005532, 34.885400 (for transit calculations)
########################################

    @staticmethod
    def get_timezone_from_db(ap_code):
        ap_db_details = AirportDbTimeZoneWoDoubles.ApDbWITHTimeZoneWoDoubles()
        for ap in ap_db_details:
            if ap['ap_code'] == ap_code:  # if the same airport
                if ap and ap['ap_details']['timezone']:
                    # logging.info("ap['ap_details']['timezone'] %s: ", ap['ap_details']['timezone'])
                    return ap['ap_details']['timezone']
                # logging.info("ap %s: ", ap)
        return -1


    @staticmethod
    def get_ap_location(ap_code):
        ap = AP_DBs_main.get_ap_by_code(ap_code)
        if ap:
            if len(ap['ap_details']) == 1:
                lat = ap['ap_details'][0]['location']['latitude']
                lon = ap['ap_details'][0]['location']['longitude']
            else:
                lat = ap['ap_details']['location']['latitude']
                lon = ap['ap_details']['location']['longitude']
            ap_location = str(lat) + ',' + str(lon)
            return ap_location
        else:
            return 'NA'


    @staticmethod
    def get_ap_by_code(ap_code):
        ap_db_details = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()
        for ap in ap_db_details:
            if ap['ap_code'] == ap_code:  # if the same airport
                return ap


    @staticmethod
    def add_timezone_to_airport(ap_code):
        ap = AP_DBs_main.get_ap_by_code(ap_code)
        timezone = AP_DBs_main.get_timezone_from_GoogleMaps(AP_DBs_main.get_ap_location(ap_code))
        if len(ap['ap_details']) == 1:
            ap_details = ap['ap_details'][0]
        else:
            ap_details = ap['ap_details']
        if timezone != -1:
            ap_details['timezone'] = timezone
        return ap_details


    @staticmethod
    def add_timezone_to_all_airports(self):
        all_details = []
        ap_db_details = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()[2000:]
        for ap in ap_db_details:
            ap_details = AP_DBs_main.add_timezone_to_airport(ap['ap_code'])
            ap['ap_details'] = ap_details
            all_details.append(ap)
        self.response.write(json.dumps(all_details, indent=2))


    @staticmethod
    def get_timezone_from_GoogleMaps(ap_location):
        timezone_url = GoogleMaps.get_time_zone(ap_location)
        if timezone_url == -1:
            return -1
        timezone = {'dstOffset': timezone_url['dstOffset'],
                    'rawOffset': timezone_url['rawOffset']}
        return timezone

    @staticmethod
    def testAPnearby(self):
        # create basic data about airport that start with the input_term for autocomplete

        ap_db_nearby = AirportDbNearby400KM.ApDbNearby400KM()
        #element: {'ap_parent_code': 'YUY', 'lat': 48.20611, 'APDist400km': [{'CodeDestPar': 'YSB', 'movements': 25221, 'CodeDest': 'YSB', 'CodeOrig': 'YUY', 'dist': 229}, {'CodeDestPar': 'YKQ', 'movements': 33948, 'CodeDest': 'YKQ', 'CodeOrig': 'YUY', 'dist': 363}, {'CodeDestPar': 'YVO', 'movements': 21219, 'CodeDest': 'YVO', 'CodeOrig': 'YUY', 'dist': 79}, {'CodeDestPar': 'YTS', 'movements': 45399, 'CodeDest': 'YTS', 'CodeOrig': 'YUY', 'dist': 191}, {'CodeDestPar': 'YTM', 'movements': 6246, 'CodeDest': 'YTM', 'CodeOrig': 'YUY', 'dist': 365}, {'CodeDestPar': 'YY', 'movements': 7775, 'CodeDest': 'YY', 'CodeOrig': 'YUY', 'dist': 297}, {'CodeDestPar': 'YYB', 'movements': 16123, 'CodeDest': 'YYB', 'CodeOrig': 'YUY', 'dist': 209}, {'CodeDestPar': 'YMT', 'movements': 14341, 'CodeDest': 'YMT', 'CodeOrig': 'YUY', 'dist': 359}, {'CodeDestPar': 'YMO', 'movements': 29551, 'CodeDest': 'YMO', 'CodeOrig': 'YUY', 'dist': 365}], 'ap_code': 'YUY', 'lng': -78.83556}
        ap_db_details = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()#todo: add the needed values from ap_db_details to the nearby db
        #element: {'ap_parent_code': 'YUY', 'ap_details': [{'location': {'longitude': -78.83556, 'latitude': 48.20611}, 'city_code': 'YUY', 'aircraft_movements': 12986, 'name': 'Rouyn-Noranda Airport', 'code': 'YUY', 'city_name': 'Rouyn-Noranda', 'timezone': 'America/Toronto', 'state': '', 'country': 'CA'}], 'ap_code': 'YUY'}
        # ap_input = 'jfk'  # send part of the airport code
        # max_distance=150

        data = {
            'ap_code': 'nyc',
            'max_distance': 250
        }
        ap_input = data['ap_code']
        max_distance = data.get('max_distance')
        if not max_distance:
            max_distance = 150

        matched_aps = []
        len_input = len(ap_input)
        if len_input !=3:
            self.response.write("Error: please insert airport code with 3 letters")# todo: this check should be in the client
        else:
            for ap in ap_db_nearby:
                if ap_input.lower() == ap['ap_code'].lower():
                    all_nearby_ap_sorted = sorted(ap['APDist400km'],key=lambda k: k['dist'])
                    # todo: add count of amount of aps and using the 'movment'
                    # todo: need to change the way 'parent' ap shows - with few aps
                    for ap_nearby in all_nearby_ap_sorted:
                        if ap_nearby['dist'] < max_distance:# if in the required distance. todo: can add tests how many aps in the range and add / delete aps by movements
                            for ap_det in ap_db_details:
                                if ap_det['ap_code'] == ap_nearby['CodeDest']: #if the same airport
                                    if ap_nearby['CodeDest'] == ap_nearby['CodeDestPar']:# no different parent
                                        if len(ap_det['ap_details']) == 1:  # there is '0'
                                            ap_to_add = ap_det['ap_details'][0]['city_name']+ " " + ap_det['ap_details'][0]['country'] + ' [' + ap_det['ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                        else:
                                            ap_to_add = ap_det['ap_details']['city_name']+ " " + ap_det['ap_details']['country'] + ' [' + ap_det['ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                    else:
                                        if len(ap_det['ap_details']) == 1:  # there is '0'
                                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + ap_det['ap_details'][0]['country'] +' ' + ap_det['ap_code'] +' [' + ap_det['ap_parent_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                        else:
                                            ap_to_add = ap_det['ap_details']['city_name'] + " " + ap_det['ap_details']['country'] +' ' + ap_det['ap_code'] +' [' + ap_det['ap_parent_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                    matched_aps.append(ap_to_add)
                                    break
                        else:
                            break # sorted by distance, the next aps will have bigger distance

                ##############################################################
                # workaround for multy airports cities like LON,NYC(and TLV) #
                # if ap wasn't found, use the first airport that has the     #
                # ap_parent_code                                             #
                ##############################################################


            if len(matched_aps)==0:
                for ap in ap_db_nearby:
                    if ap_input.lower() == ap['ap_parent_code'].lower():
                        all_nearby_ap_sorted = sorted(ap['APDist400km'], key=lambda k: k['dist'])
                        # todo: add count of amount of aps and using the 'movment'
                        # todo: need to change the way 'parent' ap shows - with few aps

                        ############
                        # only in the workaround - add the current ap to the results
                        ############

                        ap_to_add = ap['ap_code'] + ' [' + ap['ap_parent_code'] + '] - ' +  '0 KM'
                        matched_aps.append(ap_to_add)


                        for ap_nearby in all_nearby_ap_sorted:
                            if ap_nearby[
                                'dist'] < max_distance:  # if in the required distance. todo: can add tests how many aps in the range and add / delete aps by movements
                                for ap_det in ap_db_details:
                                    if ap_det['ap_code'] == ap_nearby['CodeDest']:  # if the same airport
                                        if ap_nearby['CodeDest'] == ap_nearby['CodeDestPar']:  # no different parent
                                            if len(ap_det['ap_details']) == 1:  # there is '0'
                                                ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                                            ap_det['ap_details'][0]['country'] + ' [' + ap_det[
                                                                'ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                            else:
                                                ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                                            ap_det['ap_details']['country'] + ' [' + ap_det[
                                                                'ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM'
                                        else:
                                            if len(ap_det['ap_details']) == 1:  # there is '0'
                                                ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                                            ap_det['ap_details'][0]['country'] + ' ' + ap_det[
                                                                'ap_code'] + ' [' + ap_det[
                                                                'ap_parent_code'] + '] - ' + str(
                                                    ap_nearby['dist']) + 'KM'
                                            else:
                                                ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                                            ap_det['ap_details']['country'] + ' ' + ap_det[
                                                                'ap_code'] + ' [' + ap_det[
                                                                'ap_parent_code'] + '] - ' + str(
                                                    ap_nearby['dist']) + 'KM'
                                        matched_aps.append(ap_to_add)
                                        break
                            else:
                                break  # sorted by distance, the next aps will have bigger distance

            self.response.write(json.dumps(matched_aps, indent=2))

    @staticmethod
    def CreateApDbDistanceCombTill400km(self):
# DB created : ApDbNearby400KM in ap_nearby_400km.py
        airportDB = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()
        airports_full_comb_distances = []
        for air1 in airportDB:  #

            if len(air1['ap_details'])==1: #there is '0'
                lat1 = air1['ap_details'][0]['location']['latitude']
                lon1 = air1['ap_details'][0]['location']['longitude']
            else:
                lat1 = air1['ap_details']['location']['latitude']
                lon1 = air1['ap_details']['location']['longitude']

            ap_code_1 = air1['ap_code']
            ap_parent_code_1 = air1['ap_parent_code']
            one_full_airport = {'ap_code': ap_code_1,'ap_parent_code': ap_parent_code_1, 'lng': lon1, 'lat': lat1, 'APDist400km': []}

            for air2 in airportDB:  # len(airportDB)
                ap_code_2 = air2['ap_code']
                ap_parent_code_2 = air2['ap_parent_code']

                if len(air2['ap_details']) == 1: #there is '0'
                    lat2 = air2['ap_details'][0]['location']['latitude']
                    lon2 = air2['ap_details'][0]['location']['longitude']
                    aircraft_movements_2 = air2['ap_details'][0]['aircraft_movements']
                else:
                    lat2 = air2['ap_details']['location']['latitude']
                    lon2 = air2['ap_details']['location']['longitude']
                    aircraft_movements_2 = air2['ap_details']['aircraft_movements']

                # calculate air distance: http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
                p = 0.017453292519943295  # PI / 180
                a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
                distance = 12742 * asin(sqrt(a))  # 2 * R; R = 6371 km (radius of earth)
                if distance < 400:
                    one_dest_airport = {'CodeOrig': ap_code_1, 'CodeDest': ap_code_2,'CodeDestPar': ap_parent_code_2, 'dist': int(distance), 'movements':aircraft_movements_2}

                    # print '2: ', one_dest_airport
                    if ap_code_1 != ap_code_2:
                        one_full_airport['APDist400km'].append(one_dest_airport)
            airports_full_comb_distances.append(one_full_airport)  # TBD- add it to DB? too big file to move to client side

        self.response.write("airports_full_comb_distances %s" %airports_full_comb_distances)


    @staticmethod
    def TestNearbyByAddress(self):

        from api.google_maps import GoogleMaps
        google_maps = GoogleMaps

        apDB_locations_and_det = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()
        #data = json.loads(self.request.body)
        # input_address = data['address']  # send part of the airport code
        #input_address = 'a--12a12'
        input_address = 'mevaseret zion, israel'
        addr_geo = google_maps.get_geo_string_result(input_address)
        logging.info("++++++++++++++++++++++++++++++++++++++++++++++++'")
        logging.info("addr_geo %s" % addr_geo)
        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
            lat2 = float(addr_geo.split(',')[0])
            lng2 = float(addr_geo.split(',')[1])
            print 'lat, lng :' , lat2,' ', lng2
            max_distance = 400  # km


            airports_in_distance = []
            for ap_det in apDB_locations_and_det:  #

                if len(ap_det['ap_details']) == 1:  # there is '0'
                    lat1 = ap_det['ap_details'][0]['location']['latitude']
                    lng1 = ap_det['ap_details'][0]['location']['longitude']
                else:
                    lat1 = ap_det['ap_details']['location']['latitude']
                    lng1 = ap_det['ap_details']['location']['longitude']

                ap_code_1 = ap_det['ap_code']
                ap_parent_code_1 = ap_det['ap_parent_code']

                # calculate air distance: http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-lnggitude-points-haversine-formula
                p = 0.017453292519943295  # PI / 180
                a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lng2 - lng1) * p)) / 2
                distance = 12742 * asin(sqrt(a))  # 2 * R; R = 6371 km (radius of earth)
                if distance < max_distance:

                    if ap_det['ap_code'] == ap_det[
                        'ap_parent_code']:  # no different parent (nyc is the parent of jfk and ewr for example)
                        #print "ap_det ", ap_det
                        if len(ap_det['ap_details']) == 1:  # there is '0'
                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                        ap_det['ap_details'][0]['country'] + ' [' + ap_det[
                                            'ap_code'] + '] - ' + str(int(distance)) + ' KM'
                        else:
                            ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                        ap_det['ap_details']['country'] + ' [' + ap_det[
                                            'ap_code'] + '] - ' + str(int(distance)) + ' KM'
                    else:
                        if len(ap_det['ap_details']) == 1:  # there is '0'
                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                        ap_det['ap_details'][0]['country'] + ' ' + ap_det[
                                            'ap_code'] + ' [' + ap_det[
                                            'ap_parent_code'] + '] - ' + str(int(distance)) + ' KM'
                        else:
                            ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                        ap_det['ap_details']['country'] + ' ' + ap_det[
                                            'ap_code'] + ' [' + ap_det[
                                            'ap_parent_code'] + '] - ' + str(int(distance)) + ' KM'

                    airports_in_distance.append([int(distance),ap_to_add])

            all_nearby_ap_sorted1 = sorted(airports_in_distance, key=lambda k: k[0])
            all_nearby_ap_sorted2=[]
            for ap in all_nearby_ap_sorted1:
                all_nearby_ap_sorted2.append(ap[1])
            self.response.write(json.dumps(all_nearby_ap_sorted2, indent=2))

        else:
            self.response.write("Error -No geocode [ERR]")

        #
        # apDB_locations_and_det = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()
        #
        # max_distance =400 #km
        # address_lan = 41
        # address_lng = -74.5
        #
        # lat2=address_lan
        # lng2=address_lng
        #
        # airports_in_distance = []
        # for ap_det in apDB_locations_and_det:  #
        #
        #     if len(ap_det['ap_details']) == 1:  # there is '0'
        #         lat1 = ap_det['ap_details'][0]['location']['latitude']
        #         lng1 = ap_det['ap_details'][0]['location']['longitude']
        #     else:
        #         lat1 = ap_det['ap_details']['location']['latitude']
        #         lng1 = ap_det['ap_details']['location']['longitude']
        #
        #     ap_code_1 = ap_det['ap_code']
        #     ap_parent_code_1 = ap_det['ap_parent_code']
        #
        #
        #     # calculate air distance: http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-lnggitude-points-haversine-formula
        #     p = 0.017453292519943295  # PI / 180
        #     a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lng2 - lng1) * p)) / 2
        #     distance = 12742 * asin(sqrt(a))  # 2 * R; R = 6371 km (radius of earth)
        #     if distance < max_distance:
        #
        #         if ap_det['ap_code'] == ap_det['ap_parent_code']:  # no different parent (nyc is the parent of jfk and ewr for example)
        #             print "ap_det ", ap_det
        #             if len(ap_det['ap_details']) == 1:  # there is '0'
        #                 ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
        #                             ap_det['ap_details'][0]['country'] + ' [' + ap_det[
        #                                 'ap_code'] + '] - ' + str(distance) + ' KM'
        #             else:
        #                 ap_to_add = ap_det['ap_details']['city_name'] + " " + \
        #                             ap_det['ap_details']['country'] + ' [' + ap_det[
        #                                 'ap_code'] + '] - ' + str(distance) + ' KM'
        #         else:
        #             if len(ap_det['ap_details']) == 1:  # there is '0'
        #                 ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
        #                             ap_det['ap_details'][0]['country'] + ' ' + ap_det[
        #                                 'ap_code'] + ' [' + ap_det[
        #                                 'ap_parent_code'] + '] - ' + str(distance) + ' KM'
        #             else:
        #                 ap_to_add = ap_det['ap_details']['city_name'] + " " + \
        #                             ap_det['ap_details']['country'] + ' ' + ap_det[
        #                                 'ap_code'] + ' [' + ap_det[
        #                                 'ap_parent_code'] + '] - ' + str(distance) + ' KM'
        #
        #
        #         airports_in_distance.append(ap_to_add)
        #
        # self.response.write("airports_full_comb_distances %s" % airports_in_distance)

    # @staticmethod
    # def ApWoLocationInGoogleMaps(self):

    # @staticmethod
    # def ApWoLocationInGoogleMaps(self):
    #
    #     ApWoLocationDb= AirportDbLocationsWoDoubles.ApDbWoLocationsWoDoubles()
    #     full_res =[]
    #     for ap in ApWoLocationDb[:5]:
    #         print ap
    #         res = {}
    #         address = ap['ap_code'] + " aiport"
    #         g_results = g_maps.get_address_reulsts(address)
    #         #self.response.write(str(g_results).replace("u'", '"').replace("'", '"'))
    #         res['ap_code'] = ap['ap_code']
    #         res['g_results'] = g_results
    #         full_res.append(res)
    #
    #     self.response.write("full_res %s " % full_res)


    @staticmethod
    def FirstAirportDBByAmadeus(self):
        # take list of ~4000 most popular airports and send the airport code to location API.
        # most of the request get airport details
        # json db is created: ap_code and if there are details: ap_details
        ap_basic_db=[]
        ap_code_list=AirportCodesDB.AirportcodebasicDB()
        #logging.info("Got basic_ap_code_db: %s" %ap_code_list)
        #self.response.write(" %s " % ap_code_list)

        #for ap in ap_code_list[:4]:
        for ap in ap_code_list:
            LocationInfo = AP_DBs_main.AmadeusLocationInfo(ap)
            if LocationInfo.get('airports'):
                new_ap_data ={
                    'ap_code' : ap,
                    'ap_details' : LocationInfo['airports']
                }
            else:
                new_ap_data = {
                    'ap_code' : ap
                }
            ap_basic_db.append(new_ap_data)

        #logging.info("ap_basic_db: %s" % ap_basic_db)
        self.response.write(" %s " % ap_basic_db)

    # send airport codes to get information from amadeus location API. state, cityname, location lat + lng, country, ap name, city code, timezone
    # create by the print AirportAmadeusData
    @staticmethod
    def AmadeusLocationInfo(location):
        # example: http://api.sandbox.amadeus.com/v1.2/location/AGP/?apikey=zHLpD3QdTGWkdzP3iDy0O7NERLdIzTwq
        url = 'http://api.sandbox.amadeus.com/v1.2/location/' + location + '/?apikey=zHLpD3QdTGWkdzP3iDy0O7NERLdIzTwq'
        req = urllib2.Request(url)
        u_open = urllib2.urlopen(req)
        read_url = u_open.read()
        result = json.loads(read_url)

        return(result)

    @staticmethod
    def AirportDBByAmadeusOnlyWithLOcations(self):
        # reusults are in AirportDbLocations.ApDbWoLocations() and AirportDbLocations.ApDbWithLocations()

        ap_db_with_locations = []
        ap_db_wo_locations = []
        AirportDBByAmadeus = AirportCodesDB.AirportAmadeusData()
        for ap in AirportDBByAmadeus:
            # print ap
            parent_code=ap['ap_code']
            if ap.get('ap_details'):
                if len(ap['ap_details']) == 1:
                    ap['ap_parent_code'] = parent_code
                    ap_db_with_locations.append(ap)
                else: #more than 1 airport
                    for ap1 in ap['ap_details']:
                        # print ap1
                        new_ap={}
                        new_ap['ap_parent_code'] = parent_code
                        new_ap['ap_code'] = ap1['code']
                        # print new_ap
                        new_ap['ap_details']=ap1
                        ap_db_with_locations.append(new_ap)
            else:
                ap['ap_parent_code'] = parent_code
                ap_db_wo_locations.append(ap)
        self.response.write("ap_db_with_locations %s " % ap_db_with_locations)
        self.response.write("ap_db_wo_locations %s " % ap_db_wo_locations)

    @staticmethod
    def check_doubles_ap_db_locations(self):
        #results are in AirportDbLocations.Apdoubles()

        ap_db_wo_locations = AirportDbLocations.ApDbWoLocations()
        ap_db_with_locations = AirportDbLocations.ApDbWithLocations()
        logging.info("Got ap_db_wo_locations and ap_db_with_locations")
        doubles1=[]
        doubles2=[]
        for ap_wo_loc in ap_db_wo_locations: #(10.10 - got empty result)
            for ap_with_loc in ap_db_with_locations:
                if ap_wo_loc['ap_code'] == ap_with_loc['ap_code'] or ap_wo_loc['ap_code'] == ap_with_loc['ap_parent_code']:
                    doubles1.append(ap_wo_loc)

# double are because airport are once alone and in once cause have another parent with few airports
# for now removing the ap that is alone if it has another same ap with another 'parent'
        for ap_with_loc1 in ap_db_with_locations:
            for ap_with_loc2 in ap_db_with_locations:
                if ap_with_loc1['ap_code'] == ap_with_loc2['ap_code']:
                    if ap_with_loc1['ap_parent_code'] != ap_with_loc2['ap_parent_code']:
                        if ap_with_loc1['ap_parent_code'] == ap_with_loc1['ap_code']:
                            doubles2.append(ap_with_loc1)

        self.response.write("doubles1 %s \n" % doubles1)
        self.response.write("doubles2 %s " % doubles2)

    @staticmethod
    def remove_doubles_ap_db_locations(self): #10.10 - only with location
        ap_db_with_locations_with_doubles = AirportDbLocations.ApDbWithLocations()
        ap_db_doubles = AirportDbLocations.Apdoubles()

        #self.response.write("ap_db_with_locations_with_doubles %s \n\n" % ap_db_with_locations_with_doubles)


        ap_db_wo_doubles =[]
        num_of_del=0
        print "len: ",len(ap_db_with_locations_with_doubles)
        for ap_index in range(len(ap_db_with_locations_with_doubles)):
            print "index : ", ap_index
            if ap_db_with_locations_with_doubles[ap_index-num_of_del] in ap_db_doubles:
                print "ap_code :", ap_db_with_locations_with_doubles[ap_index-num_of_del]['ap_code']#ap_index-num_of_del
                del ap_db_with_locations_with_doubles[ap_index-num_of_del]
                num_of_del +=1
                print "num_of_del: ", num_of_del
        self.response.write("num_of_del:%s" %num_of_del)
        self.response.write("ap_db_with_locations_with_doubles %s " % ap_db_with_locations_with_doubles)


    @staticmethod
    def testAPautocomplete(self):
        from airports_codes_and_amadeus_db import AirportCodesDB
        ap_code_list = AirportCodesDB.AirportcodebasicDB()
        input_term = 'tl'  # send part of the airport code
        matched_aps = []
        len_input = len(input_term)
        for ap in ap_code_list:
            # logging.info("ap: %s" % ap)
            if str(input_term).lower() == ap[:len_input].lower():
                matched_aps.append(ap)  # todo : add more airport details when possible

        self.response.write(json.dumps(matched_aps, indent=2))

    @staticmethod
    def testAPautocomplete_details(self):
        # create basic data about airport that start with the input_term for autocomplete
        from airports_codes_and_amadeus_db import AirportCodesDB
        ap_code_list_details = AirportCodesDB.AirportAmadeusData()
        input_term = 't'  # send part of the airport code
        matched_aps = []
        len_input = len(input_term)
        for ap in ap_code_list_details:
            # logging.info("ap: %s" % ap)
            if str(input_term).lower() == ap['ap_code'][:len_input].lower():
                if ap.get('ap_details'):
                    if len(ap['ap_details'])==1:
                        ap_det= ap['ap_code'] + "-" + ap['ap_details'][0]['name'] + "," + ap['ap_details'][0]['city_name']+ " " + ap['ap_details'][0]['country']
                    else: #more than 1 airport
                        ap_det = ap['ap_code'] + ": " + str(len(ap['ap_details'])) + " airports" + "," + ap['ap_details'][0]['city_name']+ " " + ap['ap_details'][0]['country']
                else:
                    ap_det = ap['ap_code']
                matched_aps.append(ap_det)

        self.response.write(json.dumps(matched_aps, indent=2))

                # @staticmethod
    # def create_nearby_airport(airports,radios):
    #
    #     nearby_airports_in_radios = []
    #     nearby_airport_db = AirportNearbyDB.APNearbyDB()
    #     for one_airport in airports:
    #         print "here "
    #         print airports
    #         for AP in nearby_airport_db:
    #              if AP["APCodeOrigin"] == one_airport :
    #                  #print AP
    #                  for nearbyAP in AP["APnearby400"] :
    #                      if nearbyAP["dist"] <= radios:
    #                          nearby_airports_in_radios.append(nearbyAP["APCodeDest"])
    #                      #print "here is nearby "
    #                      #print nearbyAP
    #                  break
    #         if (len(nearby_airports_in_radios) == 0):  # in case the airport isn't in the nearby airports DB
    #             nearby_airports_in_radios.append(one_airport)
    #     print  "nearby radios "
    #     print  nearby_airports_in_radios
    #     return nearby_airports_in_radios

    # @staticmethod
    # def Get_airport_location(airports):
    #     nearby_airport_db = AirportNearbyDB.APNearbyDB()
    #     location =[]
    #     for air_port in airports:
    #         if (len(air_port)==1):
    #             for AP in  nearby_airport_db:
    #               if (AP["APCodeOrigin"] == air_port):
    #
    #                    lng= AP["lng"]
    #                    lat =AP["lat"]
    #                    point=[lng , lat]
    #                    print lng + " "+lat + " " + air_port
    #                    return  point

    #get DB with airports, include LAN&LNG. Create for each airport all the air distances between all the airports
    # @staticmethod
    # def CreateDistanceComb(airportDB):
    #
    #     airports_full_comb_distances=[]
    #     for air1 in range(len(airportDB)):  #len(airportDB)
    #
    #
    #         lat1=airportDB[air1]['lan']
    #         lon1=airportDB[air1]['lng']
    #         airport_code_origin = airportDB[air1]['airport_code']
    #         origin_airport_num_in_names_list = airportDB[air1]['airport_num_in_list_name']
    #         one_full_airport = {'APCodeOrigin': airport_code_origin ,'lng': lon1, 'lat': lat1 , 'APDestDist': {} , 'OriginAPNumListName': origin_airport_num_in_names_list  }
    #         print '1: ', one_full_airport
    #         for air2 in range(len(airportDB)): #len(airportDB)
    #
    #             one_dest_airport = {'APCodeOrigin': None , 'APCodeDest': None , 'DestAPNumListName': None, 'dist': None }
    #             dest_airport_num_in_names_list = airportDB[air2]['airport_num_in_list_name']
    #             lat2=airportDB[air2]['lan']
    #             lon2=airportDB[air2]['lng']
    #
    #             # calculate air distance: http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula
    #             p = 0.017453292519943295  # PI / 180
    #             a = 0.5 - cos((lat2 - lat1) * p)/2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    #             distance= 12742 * asin(sqrt(a)) # 2 * R; R = 6371 km (radius of earth)
    #
    #             one_dest_airport['APCodeOrigin']=airport_code_origin
    #             one_dest_airport['APCodeDest']=airportDB[air2]['airport_code']
    #             one_dest_airport['DestAPNumListName']=airportDB[air2]['airport_num_in_list_name']
    #             one_dest_airport['dist']=int(distance)
    #
    #             #print '2: ', one_dest_airport
    #
    #             one_full_airport['APDestDist'][air2] = one_dest_airport
    #         airports_full_comb_distances.append(one_full_airport) # TBD- add it to DB? too big file to move to client side
    #
    #
    #     return airports_full_comb_distances


    # @staticmethod
    # def CreateNearbyAirportsDB(airportDB,MaxDist):
    #
    #     airports_nearby=[]
    #     for air1 in range(len(airportDB)):  #len(airportDB)
    #         lat1=airportDB[air1]['lat']
    #         lon1=airportDB[air1]['lng']
    #         OriginAPNumListName = airportDB[air1]['OriginAPNumListName']
    #         APCodeOrigin = airportDB[air1]['APCodeOrigin']
    #         #one_full_airport = {'APCodeOrigin': APCodeOrigin ,'OriginAPNumListName': OriginAPNumListName, 'APnearby500': {} , 'APnearby400': {} , 'APnearby300': {} , 'APnearby200': {} ,'APnearby100': {} ,'APnearby50': {}   }
    #         one_full_airport = {'APCodeOrigin': APCodeOrigin ,'OriginAPNumListName': OriginAPNumListName,'lng': lon1, 'lat': lat1, 'APnearby400': {} }
    #         print '3: ', one_full_airport
    #         AirportsMaxDist =[]
    #
    #         for air2 in range(len(airportDB[air1]['APDestDist'])): #len(airportDB[air1]['APDestDist'])
    #             if (airportDB[air1]['APDestDist'][air2]['dist'] < 400):
    #                 AirportsMaxDist.append(airportDB[air1]['APDestDist'][air2])
    #             #print '2: ', AirportsMaxDist
    #         one_full_airport['APnearby400'] = AirportsMaxDist
    #
    #         airports_nearby.append(one_full_airport)
    #
    #     print "airports_nearby"
    #     return airports_nearby
