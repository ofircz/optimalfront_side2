import urllib
import json
#from pytz import timezone
import time
from datetime import datetime, timedelta
from api.google_maps import GoogleMaps
import datetime
import logging

#from pytz import timezone, tzinfo

class traffic_calculation:

 @staticmethod
 def drive(origin_address, destination_address, key, epoch_time, traffic_model, use_traffic):#traffic_model options: best_guess, optimistic , passimistic, traffic model can get only month ahead

    url_start_str= 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='
    # case of driving with date & hour
    if epoch_time:

        if use_traffic:
            # url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + urllib.quote(
            # origin_address.encode('utf-8')) + '&destinations=' + urllib.quote(
            # destination_address.encode('utf-8')) + '&departure_time=' + str(converted_date) + '&traffic_model=' + traffic_model + '&key=' + str(key) + '&language=en'
            url = url_start_str + origin_address + '&destinations=' + destination_address + '&departure_time=' + str(epoch_time) + '&traffic_model=' + traffic_model + '&key=' + str(key) + '&language=en'

        else:
            url = url_start_str + origin_address + '&destinations=' + destination_address + '&departure_time=' + str(epoch_time) + '&key=' + str(key) + '&language=en'
    # case of driving without date & hour
    else:
        # url = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + urllib.quote(
        #         origin_address.encode('utf-8')) + '&destinations=' + urllib.quote(destination_address.encode('utf-8')) + '&key=' + str(key) + '&language=en'
        url = url_start_str + origin_address + '&destinations=' + destination_address + '&key=' + str(key) + '&language=en'
    urlopen = urllib.urlopen(url)
    read_url = urlopen.read()
    result = json.loads(read_url)

    status= result.get('status',0)
    unwanted_status_options = [0, 'ZERO_RESULTS', 'NOT_FOUND' , 'INVALID_REQUEST', 'OVER_QUERY_LIMIT', 'REQUEST_DENIED', 'UNKNOWN_ERROR']
    if status in unwanted_status_options:
        logging.info("unwanted_status_options - url with issue: %s " %url)
        logging.info("unwanted_status_options - status: %s " %status)
        drive_json = {"drive":
        [{
        "start_address": origin_address,
        "end_address": destination_address,
        "distance_text": "N/A",
        "distance_value": 0,
        "duration_text": url,
        "duration_value": 0,
        "traffic_duriation": {"traffic_duriation_text": "N/A" if use_traffic else "N/A" ,
                              "traffic_duriation_value": 0 if use_traffic else "N/A"}
        }]}
        return drive_json
    else:
     drive_json = {"drive":
        [{
        "start_address": origin_address,
        "end_address": destination_address,
        "distance_text": result['rows'][0]['elements'][0]['distance']['text'],
        "distance_value": result['rows'][0]['elements'][0]['distance']['value'],
        "duration_text": result['rows'][0]['elements'][0]['duration']['text'],
        "duration_value": result['rows'][0]['elements'][0]['duration']['value'],
        "traffic_duriation": {"traffic_duriation_text": result['rows'][0]['elements'][0]['duration_in_traffic']['text'] if use_traffic else "N/A" ,
                              "traffic_duriation_value": result['rows'][0]['elements'][0]['duration_in_traffic']['value'] if use_traffic else "N/A"}
        }]}

     return drive_json


 @staticmethod
 def transit_details(origin_address, destination_address, converted_date , transit="", API_KEY='', dep_or_arive=1): #Date untill month ahead. transit options:  bus, train , tram , subway, rail

    #from dateutil.tz import tzutc, tzlocal, tzoffset
    import time
    #from pytz import timezone
    #import pytz
    from time import strptime

    if transit == "transit":
        mode = '&mode=' + transit
    else:
        mode = '&mode=transit&transit_mode=' + transit
    # calculate transit with date and hour

    if converted_date != '':
        # print "with hour and date"
        # convert date&time to int

        # deside if arrivel or departure
        if dep_or_arive:
          url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + urllib.quote(origin_address.encode('utf-8')) + '&destination=' + \
          urllib.quote(destination_address.encode('utf-8'))+"&departure_time=" + str(int(converted_date)) + mode + "&key=" + API_KEY
        else:
          url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + urllib.quote(origin_address.encode('utf-8')) + '&destination=' + \
          urllib.quote(destination_address.encode('utf-8')) + '&arrival_time=' + str(int(converted_date)) + mode+'&key=' + API_KEY
    else: # calculate transit without date and hour
          url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + urllib.quote(
          origin_address.encode('utf-8')) + '&destination=' + urllib.quote(
          destination_address.encode('utf-8')) + mode + "&key=" + API_KEY
    #print url
    urlopen = urllib.urlopen(url)
    read_url = urlopen.read()
    result = json.loads(read_url)
    status= result.get('status',0)
    unwanted_status_options = [0, 'ZERO_RESULTS', 'NOT_FOUND' , 'INVALID_REQUEST', 'OVER_QUERY_LIMIT', 'REQUEST_DENIED', 'UNKNOWN_ERROR']
    if status in unwanted_status_options:
        logging.info("unwanted_status_options - url with issue: %s " % url)
        logging.info("unwanted_status_options - status: %s " % status)
        transit_json = {
           "transit": [{
               "start_address": origin_address,
               "end_address": destination_address,
               "start_geo": {"lng": 0,
                             "lat": 0
                             },
               "end_geo": {"lng": 0,
                           "lat": 0
                           },
               "duration_value": 0,
               "distance_value": 0,
               "departure_time": {
                   "date": 0,
                   "time": 0,
                   "departure_value": 0,
                   "departure_text": 0,
                   "time_zone": 0
               },
               "arrival_time": {
                   "date": 0,
                   "time": 0,
                   "arrival_text": 0,
                   "arrival_value": 0},

               "step": {},
               "number_of_walking_steps": 0,
               "number_of_transit_rides": 0,
               "total_walking_duration": 0,
               "total_transit_duration": 0,
           }]}
        return transit_json

    for i in range(len(result['routes'])):# this loop handels for the general details
        for j in range(len(result['routes'][i]['legs'])):
           date_time = (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['departure_time']['value']).timetuple())
           date_time = datetime.datetime(date_time.tm_year, date_time.tm_mon, date_time.tm_mday, date_time.tm_hour, date_time.tm_min, date_time.tm_sec)
           #departure_time_zone = pytz.timezone(result['routes'][i]['legs'][j]['departure_time']['time_zone'])
           #departure_date_time = date_time.astimezone(departure_time_zone)
           date_timeA = (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['arrival_time']['value']).timetuple())
           date_timeA = datetime.datetime(date_timeA.tm_year, date_timeA.tm_mon, date_timeA.tm_mday, date_timeA.tm_hour,
                                          date_timeA.tm_min, date_timeA.tm_sec)
           #arrival_time_zone = pytz.timezone(result['routes'][i]['legs'][j]['arrival_time']['time_zone'])
           #arrival_date_time = date_timeA.astimezone(arrival_time_zone)
           #print arrival_time_zone, "check"
           transit_json = {
               "transit": [{
                   "start_address": origin_address,
                   "end_address": destination_address,
                   "start_geo": {"lng": result['routes'][i]['legs'][j]['start_location']['lng'], "lat": result['routes'][i]['legs'][j]['start_location']['lat']},
                   "end_geo": {"lng": result['routes'][i]['legs'][j]['end_location']['lng'], "lat": result['routes'][i]['legs'][j]['end_location']['lat']},
                   "duration_value": result['routes'][i]['legs'][j]['duration']['value'],
                   "distance_value": result['routes'][i]['legs'][j]['distance']['value'],
                   "departure_time": {
                                      "date": str(date_time.date()),
                                      "time": str(date_time.time()),
                                      "departure_value": (result['routes'][i]['legs'][j]['departure_time']['value']),
                                      "departure_text": (result['routes'][i]['legs'][j]['departure_time']['text']),
                                      "time_zone": result['routes'][i]['legs'][j]['departure_time']['time_zone']
                                      },
                   "arrival_time": {
                                      "date": str(date_timeA.date()),
                                       "time": str(date_timeA.time()),
                                      "arrival_text": result['routes'][i]['legs'][j]['arrival_time']['text'],
                                      "arrival_value": result['routes'][i]['legs'][j]['arrival_time']['value']},

                   "step": {},
                   "number_of_walking_steps": "",
                   "number_of_transit_rides": "",
                   "total_walking_duration": "",
                   "total_transit_duration": "" ,
               }]}

    #dt = (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['departure_time']['value']).timetuple())
    #d = datetime.datetime(dt.tm_year, dt.tm_mon, dt.tm_mday, dt.tm_hour, dt.tm_min, dt.tm_sec, tzinfo=pytz.utc)
    #print d

    #t_z = pytz.timezone(result['routes'][i]['legs'][j]['departure_time']['time_zone'])
    #dams = d.astimezone(t_z)
    #print t_z
    #print dams.date(), " ", dams.time()
    #tzone = timezone(result['routes'][i]['legs'][j]['departure_time']['time_zone'])
    #utc = datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['departure_time']['value'])
    #print (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['departure_time']['value']))
    #print tzone
    #print utc.astimezone(ams)
    #    ny_dt = tz.normalize(utc.astimezone(tz))
    #print str(ny_dt)

    index = 0
    count_transit_ride = 0
    count_walking_steps = 0
    total_walking_dur = []# array to grab walking time
    total_transit_duriation = []
    # handle steps in directions
    for step_details in (result['routes'][0]['legs'][0]['steps']):
           #adding walking time
           if step_details['travel_mode'] == 'WALKING':

               total_walking_dur.append(step_details['duration']['value'])
               count_walking_steps = count_walking_steps + 1

           if step_details['travel_mode'] == 'TRANSIT':
               total_transit_duriation.append(step_details['duration']['value'])
               count_transit_ride = count_transit_ride + 1
               step_num = {
                   "type": step_details['transit_details']['line']['vehicle']['name'],
                   "departure_time_text": str(step_details['transit_details']['departure_time']['text']),
                   "duration_value": int(step_details['duration']['value'])/60,
                   "departure_time": str(step_details['transit_details']['departure_time']['text']),
                   "time_zone": str(step_details['transit_details']['departure_time']['time_zone']),
                   "departure_station": str(step_details['transit_details']['departure_stop']['name']),
                   "line":  str(step_details['transit_details']['line']['name'])
                   if 'Bus' in (step_details['transit_details']['line']['vehicle'].get('type')) else 'N/A',
                   "arrival_station": str(step_details['transit_details']['arrival_stop']['name']),
                   "company_site": str(step_details['transit_details']['line']['agencies'][0]['url'])
               }
               transit_json['transit'][0]['step'][index] = step_num
               index = index + 1


    total_walking = sum(total_walking_dur) / 60  # minutes
    transit_json['transit'][0]['total_walking_duration'] = total_walking
    transit_json['transit'][0]['number_of_walking_steps'] = count_walking_steps
    transit_json['transit'][0]['number_of_transit_rides'] = count_transit_ride
    transit_json['transit'][0]['total_transit_duration'] = str(sum(total_transit_duriation)/60)
    # print count_transit_ride, " num of rides"
    # print count_walking_steps, " num of walking steps"

    return transit_json


 @staticmethod
 def google_place(city, state):

        url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + city + ',%20airport%20' + state + '&key=AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04'
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)

        for i in range(len(result['results'])):
            for type in result['results'][i]['types']:

                if str(type) == 'airport':
                    place_id = result['results'][i]['place_id']
                    # airport_address = result['results'][i]['formatted_address']
        geo_ap = result['results'][i]['geometry']['location']

        # print result['results'][i]['geometry']['origin_addresses']
        url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=place_id:" + place_id + "&destinations=Villa Carlos Paz cordoba argentina&key=AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04"
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)

        # print result
        return place_id, geo_ap

 @staticmethod
 def googlePlaces():

        YOUR_API_KEY = 'AIzaSyAaK16eSZN8W07Mz8M0Q3SiT1Vruv_yv04'

        url= "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=5000&" \
             "type=establishmen&keyword=medicine&key="+YOUR_API_KEY
        url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=medicine+factory+in+Sydney&key="+YOUR_API_KEY

        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        # print result
        # for item in result['results']:
            # print item['name'], item['types']

# 03 Schwab Road, Melville, NY 11747, USA, "Boyle Avenue, Fontana, CA 92337, USA"
#traffic_calculation.transit_details("West Atlantic Boulevard, Pompano Beach, FL 33069, USA",'mia airport', '2016-09-10' , '12:00:00', "bus" ,'AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg', dep_or_arive=1)
#traffic_calculation.drive("West Atlantic Boulevard, Pompano Beach, FL 33069, USA",'mia airport','AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg','2016-09-20' , '12:00:00' ,"best_guess",1)
#traffic_alculation.transit_details('Ben Yehuda 21 Tel Aviv Israel','tlv', '2016-09-21' , '12:00:00','bus','AIzaSyD6HxG6blw_-Kr1mkDJRYUG3anB0NNAibg',1)

class drive_calc:
    @staticmethod
    def traffic_data(leg_index, ap_data_index, epoch_uniq_index, origin_or_dest, origin_address, destination_address, key,
                      epoch_time, traffic_model, use_traffic):
         traffic_data = {'traffic_data': {}}
         traffic_data['traffic_data'] = {
             'leg_index': leg_index,
             'ap_data_index': ap_data_index,
             'epoch_uniq_index': epoch_uniq_index,
             'origin_or_dest': origin_or_dest,
             'origin_address': origin_address,
             'destination_address': destination_address,
             'key': key,
             'epoch_time': epoch_time,
             'traffic_model': traffic_model,
             'use_traffic': use_traffic,
             'url': 0,
             'result': 0
         }
         return traffic_data

    @staticmethod
    def get_url(traffic_data):

         url_start_str = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins='

         for data in traffic_data:
             epoch_time = data['traffic_data']['epoch_time']
             use_traffic = data['traffic_data']['use_traffic']
             origin_address = data['traffic_data']['origin_address']
             destination_address = data['traffic_data']['destination_address']
             traffic_model = data['traffic_data']['traffic_model']
             key = data['traffic_data']['key']
             if epoch_time:
                 if use_traffic:
                     url = url_start_str + origin_address + '&destinations=' + destination_address + '&departure_time=' + str(
                         epoch_time) + '&traffic_model=' + traffic_model + '&key=' + str(key) + '&language=en'

                 else:
                     url = url_start_str + origin_address + '&destinations=' + destination_address + '&departure_time=' + str(
                         epoch_time) + '&key=' + str(key) + '&language=en'
             # case of driving without date & hour
             else:
                 url = url_start_str + origin_address + '&destinations=' + destination_address + '&key=' + str(
                     key) + '&language=en'
             data['traffic_data']['url'] = url

         return drive_calc.get_JSON_drive(traffic_data)

    @staticmethod
    def from_url_to_json(q, leg_index, ap_data_index, epoch_uniq_index, origin_or_dest, origin_address,
                          destination_address, key, epoch_time, traffic_model, use_traffic, url):
         urlopen = urllib.urlopen(url)
         read_url = urlopen.read()
         result = json.loads(read_url)
         logging.info(result)
         status = result['rows'][0]['elements'][0]['status']
         unwanted_status_options = [0, 'ZERO_RESULTS', 'NOT_FOUND', 'INVALID_REQUEST', 'OVER_QUERY_LIMIT',
                                    'REQUEST_DENIED', 'UNKNOWN_ERROR']
         if status in unwanted_status_options:
             logging.info("unwanted_status_options - url with issue: %s " % url)
             logging.info("unwanted_status_options - status: %s " % status)
             drive_json = {"drive":
                 {
                     "start_address": origin_address,
                     "end_address": destination_address,
                     "distance_text": "N/A",
                     "distance_value": 0,
                     "duration_text": url,
                     "duration_value": 0,
                     "traffic_duriation": {"traffic_duriation_text": "N/A" if use_traffic else "N/A",
                                           "traffic_duriation_value": 0 if use_traffic else "N/A"}
                 }}
         else:
             if 'duration_in_traffic' in result['rows'][0]['elements'][0].keys():
                 drive_json = {"drive":
                     {
                         "start_address": origin_address,
                         "end_address": destination_address,
                         "distance_text": result['rows'][0]['elements'][0]['distance']['text'],
                         "distance_value": result['rows'][0]['elements'][0]['distance']['value'],
                         "duration_text": result['rows'][0]['elements'][0]['duration']['text'],
                         "duration_value": result['rows'][0]['elements'][0]['duration']['value'],
                         "traffic_duriation": {
                             "traffic_duriation_text": result['rows'][0]['elements'][0]['duration_in_traffic'][
                                 'text'] if use_traffic else "N/A",
                             "traffic_duriation_value": result['rows'][0]['elements'][0]['duration_in_traffic'][
                                 'value'] if use_traffic else 0}
                     }}
             else:
                 drive_json = {"drive":
                     {
                         "start_address": origin_address,
                         "end_address": destination_address,
                         "distance_text": result['rows'][0]['elements'][0]['distance']['text'],
                         "distance_value": result['rows'][0]['elements'][0]['distance']['value'],
                         "duration_text": result['rows'][0]['elements'][0]['duration']['text'],
                         "duration_value": result['rows'][0]['elements'][0]['duration']['value'],
                         "traffic_duriation": {
                             "traffic_duriation_text":  "N/A",
                             "traffic_duriation_value": 0}
                     }}

         traffic_data = {
             'leg_index': leg_index,
             'ap_data_index': ap_data_index,
             'epoch_uniq_index': epoch_uniq_index,
             'origin_or_dest': origin_or_dest,
             'origin_address': origin_address,
             'destination_address': destination_address,
             'key': key,
             'epoch_time': epoch_time,
             'traffic_model': traffic_model,
             'use_traffic': use_traffic,
             'url': url,
             'result': drive_json
         }

         q.put(traffic_data)

    @staticmethod
    def get_JSON_drive(traffic_data):
         import threading
         import Queue
         q = Queue.Queue()
         threads = []

         def chunks(l, n):
             for i in range(0, len(l), n):
                 yield l[i:i + n]

         grouped_data = chunks(traffic_data, 150)

         for group in grouped_data:
             for all_data in group:
                 data = all_data['traffic_data']
                 t = threading.Thread(target=drive_calc.from_url_to_json, args=(q, data['leg_index'],
                                                                                   data['ap_data_index'],
                                                                                   data['epoch_uniq_index'],
                                                                                   data['origin_or_dest'],
                                                                                   data['origin_address'],
                                                                                   data['destination_address'],
                                                                                   data['key'],
                                                                                   data['epoch_time'],
                                                                                   data['traffic_model'],
                                                                                   data['use_traffic'],
                                                                                   data['url']))
                 t.daemon = True
                 t.start()
                 threads.append(t)
             for t in threads:
                 t.join()
         results = []
         # logging.info("traffic_data: %s" % traffic_data)
         for i, query in enumerate(traffic_data):
             res = q.get()
             results.append(res)
         # logging.info("results %s" % results)
         return results


class transit_calc:

    @staticmethod
    def traffic_data(leg_index, ap_data_index, epoch_uniq_index, origin_address, destination_address, converted_date , transit="", key='', dep_or_arive=1):
        if transit == "transit":
            mode = '&mode=' + transit
        else:
            mode = '&mode=transit&transit_mode=' + transit
        # calculate transit with date and hour

        traffic_data = {'traffic_data': {}}
        traffic_data['traffic_data'] = {
            'leg_index': leg_index,
            'ap_data_index': ap_data_index,
            'epoch_uniq_index': epoch_uniq_index,
            'origin_or_dest': dep_or_arive,
            'origin_address': origin_address,
            'destination_address': destination_address,
            'key': key,
            'epoch_time': converted_date,
            'mode': mode,
            'url': 0,
            'result': 0
        }
        return traffic_data

    @staticmethod
    def get_url(traffic_data):

        for data in traffic_data:
            epoch_time = data['traffic_data']['epoch_time']
            dep_or_arive = data['traffic_data']['origin_or_dest']
            origin_address = data['traffic_data']['origin_address']
            destination_address = data['traffic_data']['destination_address']
            mode = data['traffic_data']['mode']
            key = data['traffic_data']['key']
            if epoch_time != '':
                # print "with hour and date"
                # convert date&time to int

                # deside if arrivel or departure
                if dep_or_arive:
                    url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + urllib.quote(
                        origin_address.encode('utf-8')) + '&destination=' + \
                          urllib.quote(destination_address.encode('utf-8')) + "&departure_time=" + str(
                        int(epoch_time)) + mode + "&key=" + key
                else:
                    url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' + urllib.quote(
                        origin_address.encode('utf-8')) + '&destination=' + \
                          urllib.quote(destination_address.encode('utf-8')) + '&arrival_time=' + str(
                        int(epoch_time)) + mode + '&key=' + key
            else:  # calculate transit without date and hour
                url = "https://maps.googleapis.com/maps/api/directions/json?origin=" + urllib.quote(
                    origin_address.encode('utf-8')) + '&destination=' + urllib.quote(
                    destination_address.encode('utf-8')) + mode + "&key=" + key
            data['traffic_data']['url'] = url
        return transit_calc.get_JSON_drive(traffic_data)

    @staticmethod
    def from_url_to_json(q, leg_index, ap_data_index, epoch_uniq_index, dep_or_arive, origin_address, destination_address, key, converted_date, mode, url):
        transit_json = 0
        urlopen = urllib.urlopen(url)
        read_url = urlopen.read()
        result = json.loads(read_url)
        status = result.get('status', 0)
        unwanted_status_options = [0, 'ZERO_RESULTS', 'NOT_FOUND', 'INVALID_REQUEST', 'OVER_QUERY_LIMIT',
                                   'REQUEST_DENIED', 'UNKNOWN_ERROR']
        if status in unwanted_status_options:
            logging.info("unwanted_status_options - url with issue: %s " % url)
            logging.info("unwanted_status_options - status: %s " % status)
            transit_json = {
                "transit": [{
                    "start_address": origin_address,
                    "end_address": destination_address,
                    "start_geo": {"lng": 0,
                                  "lat": 0
                                  },
                    "end_geo": {"lng": 0,
                                "lat": 0
                                },
                    "duration_value": 0,
                    "distance_value": 0,
                    "departure_time": {
                        "date": 0,
                        "time": 0,
                        "departure_value": 0,
                        "departure_text": 0,
                        "time_zone": 0
                    },
                    "arrival_time": {
                        "date": 0,
                        "time": 0,
                        "arrival_text": 0,
                        "arrival_value": 0},

                    "step": {},
                    "number_of_walking_steps": 0,
                    "number_of_transit_rides": 0,
                    "total_walking_duration": 0,
                    "total_transit_duration": 0,
                }]}
        else:
            for i in range(len(result['routes'])):# this loop handels for the general details
                for j in range(len(result['routes'][i]['legs'])):
                   date_time = (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['departure_time']['value']).timetuple())
                   date_time = datetime.datetime(date_time.tm_year, date_time.tm_mon, date_time.tm_mday, date_time.tm_hour, date_time.tm_min, date_time.tm_sec)
                   #departure_time_zone = pytz.timezone(result['routes'][i]['legs'][j]['departure_time']['time_zone'])
                   #departure_date_time = date_time.astimezone(departure_time_zone)
                   date_timeA = (datetime.datetime.fromtimestamp(result['routes'][i]['legs'][j]['arrival_time']['value']).timetuple())
                   date_timeA = datetime.datetime(date_timeA.tm_year, date_timeA.tm_mon, date_timeA.tm_mday, date_timeA.tm_hour,
                                                  date_timeA.tm_min, date_timeA.tm_sec)
                   #arrival_time_zone = pytz.timezone(result['routes'][i]['legs'][j]['arrival_time']['time_zone'])
                   #arrival_date_time = date_timeA.astimezone(arrival_time_zone)
                   #print arrival_time_zone, "check"
                   transit_json = {
                       "transit": [{
                           "start_address": origin_address,
                           "end_address": destination_address,
                           "start_geo": {"lng": result['routes'][i]['legs'][j]['start_location']['lng'], "lat": result['routes'][i]['legs'][j]['start_location']['lat']},
                           "end_geo": {"lng": result['routes'][i]['legs'][j]['end_location']['lng'], "lat": result['routes'][i]['legs'][j]['end_location']['lat']},
                           "duration_value": result['routes'][i]['legs'][j]['duration']['value'],
                           "distance_value": result['routes'][i]['legs'][j]['distance']['value'],
                           "departure_time": {
                                              "date": str(date_time.date()),
                                              "time": str(date_time.time()),
                                              "departure_value": (result['routes'][i]['legs'][j]['departure_time']['value']),
                                              "departure_text": (result['routes'][i]['legs'][j]['departure_time']['text']),
                                              "time_zone": result['routes'][i]['legs'][j]['departure_time']['time_zone']
                                              },
                           "arrival_time": {
                                              "date": str(date_timeA.date()),
                                               "time": str(date_timeA.time()),
                                              "arrival_text": result['routes'][i]['legs'][j]['arrival_time']['text'],
                                              "arrival_value": result['routes'][i]['legs'][j]['arrival_time']['value']},

                           "step": {},
                           "number_of_walking_steps": "",
                           "number_of_transit_rides": "",
                           "total_walking_duration": "",
                           "total_transit_duration": ""
                       }]}
        traffic_data = {
           'leg_index': leg_index,
           'ap_data_index': ap_data_index,
           'epoch_uniq_index': epoch_uniq_index,
           'origin_or_dest': dep_or_arive,
           'origin_address': origin_address,
           'destination_address': destination_address,
           'key': key,
           'epoch_time': converted_date,
           'mode': mode,
           'url': url,
           'result': transit_json
        }
        q.put(traffic_data)

    @staticmethod
    def get_JSON_drive(traffic_data):
        import threading
        import Queue
        q = Queue.Queue()
        threads = []

        def chunks(l, n):
            for i in range(0, len(l), n):
                yield l[i:i + n]

        grouped_data = chunks(traffic_data, 150)

        for group in grouped_data:
            for all_data in group:
                data = all_data['traffic_data']
                t = threading.Thread(target=transit_calc.from_url_to_json, args=(q, data['leg_index'],
                                                                                        data['ap_data_index'],
                                                                                        data['epoch_uniq_index'],
                                                                                        data['origin_or_dest'],
                                                                                        data['origin_address'],
                                                                                        data['destination_address'],
                                                                                        data['key'],
                                                                                        data['epoch_time'],
                                                                                        data['mode'],
                                                                                        data['url']))
                t.daemon = True
                t.start()
                threads.append(t)
            for t in threads:
                t.join()
        results = []
        # logging.info("traffic_data: %s" % traffic_data)
        for i, query in enumerate(traffic_data):
            res = q.get()
            results.append(res)
        # logging.info("results %s" % results)
        return results
