/**
 * Created by lironil96 on 26/10/2016.
 */

'use strict';

app.directive('airportAutocomplete', function($parse, $http, ajaxService) {
    return {
        scope: {
            ngModel: '=',
            airportsNearby: '=',
            distanceNearby: '='
        },

        link: function(scope, element, attrs, model) {
            jQuery(function() {
                jQuery(element).autocomplete({
                    source: function( request, response ) {
                        if(scope.ngModel.length == 2) {
                            var requestToSend = {request_detail: request.term};
                            jQuery.ajax({
                                url: "/APCodeACdet",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json",
                                data: JSON.stringify(requestToSend),
                                success: function( data ) {
                                    var sourceObj = [];
                                    for(var airportData in data){
                                        if(data.hasOwnProperty(airportData)){
                                            sourceObj.push({
                                                value: data[airportData].substring(0,3),
                                                label: data[airportData]
                                            });
                                        }
                                    }
                                    response(sourceObj);
                                }
                            });
                        }
                        else {
                            jQuery.ajax({
                                url: "//api.sandbox.amadeus.com/v1.2/airports/autocomplete",
                                dataType: "json",
                                data: {
                                    apikey: "zHLpD3QdTGWkdzP3iDy0O7NERLdIzTwq",
                                    term: request.term
                                },
                                success: function( data ) {
                                    response( data );
                                }
                            });
                        }

                    },
                    minLength: 2,
                   select: function( event, ui ) {
                        scope.$apply(function() {
                        scope.ngModel = ui.item.value;
                             scope.airportsNearby = [];
                    if (scope.ngModel.length < 3)
                             return;
                    ajaxService.resolveAirPorts(scope.ngModel, scope.distanceNearby).then(function(data){
                     scope.airportsNearby = data;
                    });
                        });
                    },
                    open: function() {
                        jQuery( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
                    },
                    close: function() {
                        jQuery( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
                    }
                });
            });

        }
    };
});
