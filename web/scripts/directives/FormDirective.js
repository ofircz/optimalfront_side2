'use strict';
app.directive('formDirective', function() {
	return {
		restrict: 'AE',
		scope: true,
		template: '<button id="addDiv" ng-disabled="number >=5" class="btn btn-default" ng-click="click()">Add</button> <br></br> <button id="sendForm()"> send </button>',
		controller: function($scope, $element, $compile) {
			$scope.number = 1;
			$scope.name="leg";
			$scope.click = function() {
				$scope.name+=$scope.number;
				var clonedDiv = $('#flight-form-wrapper'+ $scope.number).clone();
				
				$scope.number++;
				clonedDiv.attr("id", 'flight-form-wrapper'+ $scope.number);
				
				clonedDiv.find('input:text, input:password, input:file, textarea').val('');
				clonedDiv.find('input:radio, input:checkbox')
         		.removeAttr('checked').removeAttr('selected');
				
				$('#flight-form-wrapper'+ ($scope.number-1)).append($compile($(clonedDiv))($scope));

			}
		}
	}
});











