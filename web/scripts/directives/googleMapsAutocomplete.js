/**
 * Created by user on 26/10/2016.
 */

'use strict';

app.directive('ngAutocomplete', function($parse,ajaxService) {
    return {

        scope: {
           // details: '=',
            ngAutocomplete: '=',
            addressNearby: '='
        },

        link: function(scope, element, attrs, model) {
            var newAutocomplete = function() {
                scope.gPlace = new google.maps.places.Autocomplete(element[0]);
                google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                    scope.$apply(function() {
                       // scope.details = scope.gPlace.getPlace();
                       scope.ngAutocomplete = scope.gPlace.getPlace().formatted_address;
                        if(!scope.gPlace.getPlace().geometry){
                            alert("Place contains no geometry. Please choose another.");
                            return;
                        }


                    });
                });


            };

            newAutocomplete();
        }
    };
});