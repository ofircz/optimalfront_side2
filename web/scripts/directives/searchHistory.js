'use strict';


app.directive('searchHistory', function (ajaxService, $mdSidenav, _){

    return{
            templateUrl:'views/directiveTemplates/searchHistory.html',
            replace: true,
            link: function (scope, elements, attr){
                    ajaxService.getSearchHistory().then(data =>{
                        scope.historyData = data;
                        scope.$apply();
                    });

                    scope.formatDateArrayString = dateArray =>{

                        if (dateArray==null){
                          return;
                        }
                        let formattedString;
                        formattedString = Array.from(dateArray, currentDate => currentDate.toLocaleDateString())
                        .join(", ");
                        return formattedString;



                    }
                    scope.itemClicked = item =>{

                      // copies the object in order prevent changing the history
                      let copy =  _.cloneDeep(item)
                        scope.selected({data:copy});

                    }

                    scope.showFilterButton = true;
                    scope.toggleRight = function(){
                         $mdSidenav('right').toggle();
                         scope.showFilterButton = false;
                    }

                    scope.closePanel = ()=>{
                        $mdSidenav('right').close();
                               scope.showFilterButton = true;
                    }

                    scope.getDisplayString = item =>{
                        let legsCount = item.list_legs.length;
                        let firstDate = item.list_legs[0].dates_options[0];
                        let firstDateString = firstDate.toLocaleDateString();
                        let legHasMultipleDates =getMultipleDatesString(item);
                        let legAirportsString ='';

                        for(let leg of item.list_legs){
                          legAirportsString += (getAirportDisplayString(leg) + ', ');
                        }

                        let displayString =
                        `${firstDateString}${legHasMultipleDates}: ${legAirportsString}`;

                        if (legsCount > 1) {
                            displayString +=`${legsCount} LEGS`;
                        }else{
                            displayString = displayString.slice(0,-2); //slices last comma and space (' ,')
                        }

                        return displayString;
                    }

                   let getMultipleDatesString = item =>{
                        let firstDate = item.list_legs[0].dates_options[0];
                        let legHasMultipleDates = '';
                         if (item.list_legs[0].dates_options.length > 1){
                            return "(M)";
                        }
                        else {
                            for (let leg of item.list_legs.slice(1)) {

                                if (legHasMultipleDates.length > 0){
                                    break;
                                }
                                if (leg.dates_options.length > 0){
                                        for (let date of leg.dates_options){
                                            if (date != firstDate){
                                                 return "(M)";

                                                }
                                    }

                                }
                            }
                        }
                        return '';

                   }
                   let getAirportDisplayString = leg =>{

                       if (leg.origin[0].length ===0 || leg.destination[0].length ===0)
                       {
                              return "No Data";
                       }

                       let origin = leg.origin[0].match(/[a-zA-Z]{3}/g)[0].toUpperCase();
                       let destination = leg.destination[0].match(/[a-zA-Z]{3}/g)[0].toUpperCase();

                       let nearByOriginCount = getCountString(
                       leg.airportsNearbyOrigion.getSelectedAirports().length +
                                    leg.addressesNearbyOrigion.getSelectedAirports().length);


                       let nearByDestinationCount = getCountString(
                       leg.addressesNearbyDestination.getSelectedAirports().length +
                                            leg.airportsNearbyDestination.getSelectedAirports().length);


                       let displayString = `${origin}${nearByOriginCount} - ${destination}${nearByDestinationCount}`;
                       return displayString;
                    }

                    let getCountString = count =>{
                        if (count > 0){
                            return `(${count})`
                        }
                        return '';

                    }

                },
             scope: {
                        selected:'&'

                    }
             }
   });
