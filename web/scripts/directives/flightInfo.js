'use strict';


app.directive('flightInfo', function ($mdDialog,localStorageService){

    return{
            templateUrl:'views/directiveTemplates/flightInfo.html',
            replace: true,
            link: function (scope, elements, attr){


                scope.convertMinutesToTimeString =  minutes => {
                    return commonHelpers.convertMinutesToHoursString(minutes);
                };

                scope.getClassOfFlightsString = flights =>{
                   let classStr = Array.prototype.map.call(flights, flight=> {return flight.details.class}).join(', ')
                    return classStr;
                };

                scope.chooseFlight =control =>
                {

                }


              scope.chooseFlight = function(flight) {
                     scope.chosenFlights = localStorageService.get('chosenFlight');

                    if (scope.chosenFlights == null || Object.keys(scope.chosenFlights).length == 0) {
                        scope.chosenFlights = [];
                    }


                var result = false;
                var index = 0;
                if (scope.chosenFlights !== null) {
                    scope.chosenFlights.forEach(function(flightFromArray, indexFromArray) {
                        if (flightFromArray.id === flight.id) {
                            result = true;
                            index = indexFromArray;
                        }
                    });
                } else {
                    scope.chosenFlights = [];
                }
            
                if (result === true) {
                    scope.chosenFlights.splice(index, 1);
                } else {
                    scope.chosenFlights.push(flight);
                }
            
                localStorageService.set('chosenFlight', scope.chosenFlights);
            };

                  scope.showAdvanced = function(ev, flight) {

                  flight.convertMinutesToTimeString = scope.convertMinutesToTimeString;
                    $mdDialog.show({
                      locals: { currentModalFlight: flight },
                      controller:  ['scope', '$mdDialog','currentModalFlight', DialogController],
                      templateUrl: 'views/directiveTemplates/moreFlightInfoDialog.html',
                      parent: angular.element(document.body),
                      targetEvent: ev,
                      clickOutsideToClose:true,

                    })
                    .then( answer => {
                       scope.status = 'You said the information was "' + answer + '".';
                    }, function() {
                       scope.status = 'You cancelled the dialog.';
                    });
                  };


                      function DialogController( scope, $mdDialog, currentModalFlight) {
                             scope.hide =  ()=> {
                              $mdDialog.hide();
                            };

                            scope.currentModalFlight = currentModalFlight;
                            scope.convertMinutesToTimeString = currentModalFlight.convertMinutesToTimeString;

                             scope.cancel = ()=> {
                              $mdDialog.cancel();
                            };

                             scope.answer = answer => {
                              $mdDialog.hide(answer);
                            };
                        };


                },
             scope: {
                        flight:'=',
                        chosenFlights:"="

                    }
             }
 });