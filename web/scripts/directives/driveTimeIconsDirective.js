/**
 * Created by lironil96 on 26/10/2016.
 */

// Description
// USE - @allFlights.html -> Blue square flight info -> drive time icons (<div class="row"></div>
/*
* Calling a function using ng-if to check if the drive time equals zero makes the div disappear,
* this causes the destination icon to pop over to the origin section - Problem.
* The simplest solution would be to change the opacity of the zero drive time icon to 0
* which means accessing that element, but angular doesn't support this, and jQuery would
* require to label all icons with classes or unique ids to be identified by the index.. Again, problem.
* Therefore I'm creating this directive to have straight access to the div element wrapping the icon/s
* */
'use strict';

app.directive('iconFix', function($parse) {
    return {
        scope: {
            hoursOrigin: '=',
            minutesOrigin: '=',
            hoursDestination: '=',
            minutesDestination: '='
        },

        link: function(scope, element, attrs, model) {
            if(scope.hoursOrigin == 0 && scope.minutesOrigin == 0 && scope.hoursDestination == 0 && scope.minutesDestination == 0) {
                element.html('');
            }
            else {
                if(scope.hoursOrigin == 0 && scope.minutesOrigin == 0) {
                    jQueryUse(element).children('div.origin').css("cssText", "opacity: 0;");
                }
                if(scope.hoursDestination == 0 && scope.minutesDestination == 0) {
                    jQueryUse(element).children('div.destination').css("cssText", "opacity: 0;");
                }
            }
        }
    };
});