'use strict';


app.directive('multipleSelectMenu', function (ajaxService){

    return{
            templateUrl:'views/directiveTemplates/multipleSelectMenu.html',
            replace: true,
            link: function (scope, elements, attr){
                   let prevSearch = scope.searchValue;
                   let prevDistance =  scope.model.distance;
                    let prevSelected = scope.model.selected.map(item => item.airportText);

                  if (scope.searchValue !=null){
                     scope.menuOptions = scope.model.cachedAirports;
                    }

                   let selectId = elements[0].querySelector(".select-control").id;
                   let id =  elements[0].querySelector(".md-select-value").id;


                    let isAllSelected = false;
                    let selectedCount;

                    // Load select all status from cache (on back button press)
                    if ((scope.model.cachedAirports !=null) && (scope.model.cachedAirports.length > 0)){
                            if (scope.model.selected.length >= scope.model.cachedAirports.length)
                            {
                                     isAllSelected  = true;
                            }
                        }

                    scope.setSelected = option => {
                        if (prevSelected != null){
                            if (prevSelected.indexOf(option.airportText) != -1)
                            {
                              return true;
                            }

                        }

                        return false;
                    }
                     // Function that loads the airport data from the server
                    scope.loadFunction = ()=>{

                        let performServerCall = true;

                        // Checking if the search value defined
                        if (scope.searchValue == null){
                            performServerCall = false;
                            }
                         // Checking if the length at least 3 chars
                        else if (scope.searchValue.length < 3){
                            performServerCall = false;
                            }

                             // Checking if the data should be loaded from the cache
                         if ((scope.model.cached === true)
                         & (scope.model.cachedAirports.length > 0)){
                         scope.menuOpen = true;
                            performServerCall = false
                            scope.menuOptions = scope.model.cachedAirports;
                              prevSelected = scope.model.selected.map(item => item.airportText);
                            } // Don't need to load the data if the user inputs the query
                        else if ((prevSearch === scope.searchValue) &&
                                 (prevDistance === scope.model.distance)){
                                performServerCall = false;
                        }

                        if(performServerCall){
                            prevSearch = scope.searchValue;
                            isAllSelected =false;


                            prevSelected = scope.model.selected.map(item => item.airportText);
                            scope.model.selected=[];
                            scope.menuOptions = [];
                            prevDistance = scope.model.distance;

                            if (scope.resolveType === "byAirport"){
                                return ajaxService.resolveAirPorts(scope.searchValue, scope.model.distance)
                                                    .then(data =>{
                                                    scope.model.cachedAirports = scope.menuOptions = data

                                                    });
                            }
                            else if (scope.resolveType === "byAddress"){
                                 return ajaxService.resolveAirPortsByAddress(scope.searchValue)
                                                        .then(data => scope.model.cachedAirports = scope.menuOptions = data);
                            }

                        }
                    }

                    // Select all button logic
                    scope.selectAll = () => {
                        if (scope.selectAllModel===true){
                             scope.model.selected = scope.menuOptions;
                        }
                        else{
                             scope.model.selected =[];
                        }
                    }
                    function isItemChecked(item){
                            if (scope.model.selected.filter(e => e.airportText == item.airportText).length > 0) {
                                    return false;
                            }
                            else {
                                    return true;
                            }
                    }

                    // Called when option clicked in select menu
                    scope.optionClicked = value =>{

                        let difference = 0;
                        if(isItemChecked(value)){
                            difference = 1;
                         }

                        if (value === "all"){
                             isAllSelected = !isAllSelected;

                             if (isAllSelected) {
                                     scope.model.selected = scope.menuOptions;
                                     selectedCount = scope.menuOptions.length;
                                }
                                else{
                                     scope.model.selected =["all"];
                                     selectedCount = 0;
                                 }
                         }
                            // Every thing was checked manually so the select all option
                            // should be updated
                          else if (scope.model.selected.length === (scope.menuOptions.length - difference - isAllSelected)){
                                      scope.model.selected.push("all");
                                      isAllSelected = true;
                          }
                          else if (scope.model.selected.length === (scope.menuOptions.length + isAllSelected)){
                                 let index = scope.model.selected.indexOf("all");
                                 isAllSelected =false;
                                 scope.model.selected.splice(index, 1);
                          }

                        }


                    let setText  = text=> {
                        scope.selectedText = text;
                        if (id != null){

                        let selector = `#${selectId} #${id} > span`;
                        document.querySelector(selector).innerHTML = text;
                        }
                    }


                    scope.$watch("model.selected", ()=> {
                    // Update the select count label

                       let selectedLength = scope.model.getSelectedAirports().length;

                        if (selectedLength > 0){

                                     setText( selectedLength + " selected");


                        }
                        else{
                            setText(scope.selectedText = "NEARBY");
                        }



                    },true);

                    // Reset logic when the query has change
                    scope.$watch("[searchValue, model.distance]", (newVal, oldVal)=> {
                          //Destructing array ES6 syntax
                            let [oldSearchVal, oldDistance] = oldVal;
                            let [newSearchVal, newDistance] = newVal;
                            if (scope.userInput){
                            if ((oldSearchVal != newSearchVal)
                            || (oldDistance != newDistance)){
                                     scope.selectedText = "NEARBY";
                                    //scope.model.selected = [];
                                    scope.model.cached= false;
                                }
                                };
                    },true);
                },
             scope: {
                        searchValue:'=',
                        model:'=',
                        resolveType:'@',
                        userInput:'='
                    }
             }
 });