app.filter('mainFilter', function() {
    let count = 0;
    return function(flightArray,
        priceRange,
        departureLegsHrRange,
        arrivalLegsHrRange,
        flightDurationRange,
        flightDriveDurationRange,
        flightDriveTrafficDurationRange,
        legsDurations,
        legsStopsArrays,
        airlinesArray,
        departureAirportsArrays,
        arrivalAirportsArrays,
        connectionAirportsArrays,
        departureDatesArrays,
        arrivalDatesArrays) {
        count++;
        console.log("minFlightPrice filer is called " + count);
        var filteredData = [];

        // price filter
        for (let currentFlight of flightArray) {
            let push = true;
            push = push && isInRange(priceRange.minPrice, currentFlight.price, priceRange.maxPrice)
            push = push && isInlegsHrRange(currentFlight, departureLegsHrRange, arrivalLegsHrRange, legsDurations);
            push = push && isInRange(flightDurationRange.minDuration, currentFlight.calculations.total_flights_time, flightDurationRange.maxDuration);
            push = push && isInRange(flightDriveDurationRange.minDuration, currentFlight.calculations.total_drives_and_flights_time, flightDriveDurationRange.maxDuration);
            push = push && isInRange(flightDriveTrafficDurationRange.minDuration, currentFlight.calculations.total_drives_and_flights_time_with_traffic, flightDriveTrafficDurationRange.maxDuration);

            push = push && applyStops(currentFlight,
                legsStopsArrays,
                airlinesArray,
                departureAirportsArrays,
                arrivalAirportsArrays,
                connectionAirportsArrays,
                departureDatesArrays,
                arrivalDatesArrays);


            if (push) {
                filteredData.push(currentFlight);
            }
        }

        return filteredData;
    }

    function applyStops(currentFlight,
        legsStopsArrays,
        airlinesArray,
        departureAirportsArrays,
        arrivalAirportsArrays,
        connectionAirportsArrays,
        departureDatesArrays,
        arrivalDatesArrays) {
        let legIndex = 0;
        let out = true;
        let returnVal = true;


        let index = 0;
        let selectedAirlines = airlinesArray.filter(airLine => airLine.Selected === true).map(airLine => airLine.name);

        for (let currentCalc of currentFlight.calculations_per_legs) {
            let currentLeg = currentFlight.legs[index];
            let currentAirLines = currentLeg.airlines;
            let originAirPort = currentLeg.origin.departure_airport;
            let arrivalAirport = currentLeg.destination.airport;
            let flightStops = currentCalc.total_stops;
            let connectionsAirports =
                currentLeg.direct_flights.map(flight => flight.arrival_airport).slice(0, -1);
            let departureDate = currentLeg.direct_flights[0].departure_date;
            let lastFlightIndex = currentLeg.direct_flights.length - 1;
            let arrivalDate = currentLeg.direct_flights[lastFlightIndex].arrival_date;



            index++;
            let stops = legsStopsArrays[index].filter(stop => stop.isChecked === true).map(stop => stop.num)
            let departureAirports = departureAirportsArrays[index].filter(airport => airport.isChecked === true).map(airport => airport.airportName);
            let arrivalAirports = arrivalAirportsArrays[index].filter(airport => airport.isChecked === true).map(airport => airport.airportName);
            let selectedConnectionAirports = connectionAirportsArrays[index].filter(airport => airport.isChecked === true).map(airport => airport.airportName);
            let departureDates = departureDatesArrays[index].filter(date => date.Selected === true).map(date => date.date);
            let arrivalDates = arrivalDatesArrays[index].filter(date => date.Selected === true).map(date => date.date);


            if ((!stops.includes(flightStops)) ||
                (currentAirLines.every(item => selectedAirlines.indexOf(item) === -1)) ||
                (!departureAirports.includes(originAirPort)) ||
                (!arrivalAirports.includes(arrivalAirport)) ||
                ((connectionsAirports.length != 0) && (connectionsAirports.every(item => selectedConnectionAirports.indexOf(item) === -1))) ||
                (!departureDates.includes(departureDate)) ||
                (!arrivalDates.includes(arrivalDate))) {
                return false;
            }


        }
        return out;


    }


    function checkHrRange(time, range) {
        let minTime = range.minTime;
        let maxTime = range.maxTime;


        return isInRange(minTime, time, maxTime);
    }


    function isInlegsHrRange(flight, departureHrRanges, arrivalHrRanges, legsDurations) {
        ans = true;
        let index = 0;
        for (let leg of flight.legs) {



            var firstDirectFlightOfLeg = leg.direct_flights[0];
            var lastDirectFlightOfLeg = leg.direct_flights[leg.direct_flights.length - 1];



            var departureTime = firstDirectFlightOfLeg.departure_time;
            var arrivalTime = lastDirectFlightOfLeg.arrival_time;

            index++;
            ans = ans && checkHrRange(departureTime, departureHrRanges[index]) &&
                checkHrRange(arrivalTime, arrivalHrRanges[index]) &&
                isInRange(legsDurations[index].minDuration, leg.duration, legsDurations[index].maxDuration);




        }
        return ans;

    }

    function isInRange(min, val, max) {

        if ((min < val) && (max > val)) {
            return true;
        }
        return false;
    }


});




