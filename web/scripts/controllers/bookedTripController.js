'use strict';

app.controller('bookedTripController', function ($scope, localStorageService,$http) {

    $scope.chosenHotels = [];
    $scope.chosenFlight = [];
    $scope.chosenHotelAllInformation = {};
    var bookingId = 1;

    $scope.init = function () {
        //$http({
        //    method: 'GET',
        //    url: '/finalOrder'+bookingId,
        //    data: {flights:JSON.stringify($scope.chosenFlight),hotels:JSON.stringify($scope.chosenHotelAllInformation)}
        //}).then(function successCallback(response) {
        //    console.log(response);
        //}, function errorCallback(response) {
        //
        //});
        $http({
            method : "GET",
            url : "/DisplayBooking/"+bookingId
        }).then(function success(response) {
            $scope.data = response.data;
            console.log(response)
        }, function myError(response) {
            console.log(response.statusText);
        });
        if (localStorageService.get('chosenHotels') !== null && localStorageService.get('chosenFlight') !== null) {
            $scope.chosenHotelAllInformation = localStorageService.get('allHotelsAllInformation');
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.chosenFlight = localStorageService.get('chosenFlight');
        }
        else if(localStorageService.get('chosenHotels') !== null)
        {
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.chosenHotelAllInformation = localStorageService.get('allHotelsAllInformation');
        }
        else if(localStorageService.get('chosenFlight') !== null)
        {
            $scope.chosenFlight = localStorageService.get('chosenFlight');

        }
    }

    $scope.getDurationHours = function (duration) {
        var durationInt = parseInt(duration);
        return Math.floor(durationInt / 60);
    }

    $scope.getDurationMinits = function (duration) {
        var durationInt = parseInt(duration);
        return durationInt % 60;
    }

    $scope.sendFinalOrder = function()
    {
        $http({
            method: 'POST',
            url: '/finalOrder',
            data: {flights:JSON.stringify($scope.chosenFlight),hotels:JSON.stringify($scope.chosenHotelAllInformation)}
        }).then(function successCallback(response) {
            console.log(response);
        }, function errorCallback(response) {

        });
    }

    $scope.cleanFinalOrder = function () {
        localStorageService.remove('chosenFlight','allHotelsAllInformation','chosenHotels');
        $scope.chosenHotels = [];
        $scope.chosenFlight = [];
        $scope.chosenHotelAllInformation = {};
    }

    $scope.init();
});

