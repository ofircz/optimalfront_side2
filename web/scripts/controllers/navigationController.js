app.controller('navigationController', function ($scope, localStorageService, ajaxService, $location) {

    $scope.user = null;

    $scope.logOut = function() {
        ajaxService.logout().then(function (dataFromServer) {
            localStorageService.remove('user');
            $location.path('/login');
            $scope.user = null;
            alert(dataFromServer);
        });
    };

    $scope.hideThisMenu = function () {

    };

    $scope.init = function() {
        $scope.user = localStorageService.get('user');
    };

    $scope.init();
});
