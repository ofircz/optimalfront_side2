'use strict';


app
    .controller('adminRegisterController', function ($scope, $http) {


        //$scope.type = {
        //    availableOptions: [
        //        {id: '1', name: 'Adult'},
        //        {id: '2', name: 'child'}
        //    ],
        //    selectedOption: {id: '1', name: 'Adult'} //This sets the default value of the select in the ui
        //};
        $scope.sendForm = function () {

            $scope.title=  $( "#title option:selected" ).text();
            $scope.ptc=  $( "#ptc option:selected" ).text();
            $scope.type=  $( "#type option:selected" ).text();

            var formObj = {

                company: $scope.company,
                //type: $scope.type.selectedOption.name,
                type: $scope.type,
                username: $scope.username,
                email: $scope.email,
                password: $scope.password,
                name: $scope.name,
                lastName: $scope.lastName,
                birthDate: $scope.birthDate,
                passport: $scope.passport,
                title: $scope.title,
                ptc: $scope.ptc,


            };

            $http({

                method: 'POST',
                url: '/adminsignup_angular',
                data: JSON.stringify(formObj)
            }).then(function successCallback(response) {
                console.log(response);
            }, function errorCallback(response) {

            });
        }
    });
