'use strict';


app
    .controller('userRegisterController', function ($scope, httpService, orderService, $http) {
        $scope.sendForm = function () {

            $scope.title=  $( "#title option:selected" ).text();
            $scope.ptc=  $( "#ptc option:selected" ).text();
            $scope.type=  $( "#type option:selected" ).text();
            $scope.admin=  $( "#admin option:selected" ).text();

            var formObj = {

                //type: $scope.type.selectedOption.name,
                type: $scope.type.toLowerCase(),
                username: $scope.username,
                email: $scope.email,
                password: $scope.password,
                name: $scope.name,
                lastName: $scope.lastName,
                birthDate: $scope.birthDate,
                passport: $scope.passport,
                title: $scope.title.toLowerCase(),
                ptc: $scope.ptc,
                admin: $scope.admin,

            };

            $http({

                method: 'POST',
                url: '/signup_angular',
                data: JSON.stringify(formObj)
            }).then(function successCallback(response) {
                console.log(response);
            }, function errorCallback(response) {

            });
        }


    });

