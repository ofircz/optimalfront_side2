'use strict';

var filterCount = 0;
var count = 0;
app.controller('Hotels2Controller', function($scope, httpService, NgMap, localStorageService, $http, $timeout, $mdSidenav, $mdDialog) {

    $scope.pageTitle = "מלונות";
    $scope.allhotels = [];
    $scope.obj = {};
    $scope.hotelslength = 0;
    $scope.chosenHotels = [];
    $scope.chosenHotelAllInformation = [];
    $scope.selectedTab = 0;
    $scope.hotelsNameFilter = {};
    $scope.filterByStars = {
        '3': true,
        '4': true,
        '5': true
    };
    $scope.displayLimitSelect = 10;
    $scope.isAirlinesDropdown = false;
    $scope.isHotelsCheckedAll = true;
    $scope.selectedHotel = {
        hotel: null
    }
    $scope.roomsFilterOptions = {};

    $scope.staticRoomsFilterOptions = {
        "Refundable": true,
        "Not refundable": true,
        "Include Breakfast": true,
        "Do not Include Breakfast": true
    };
    $scope.priceRange = {
        min: 0,
        max: 120,
        step: 10,
        userMinOne: 10,
        userMaxOne: 100
    };

    var vm = $scope;

    $scope.selectedItem = 1;

    var count = 0;

    function resizeAndReboundMap() {
        NgMap.getMap({
            id: 'mainMap'
        }).then(function(map) {

            google.maps.event.trigger(map, 'resize');
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);


            count++;
            console.log('map resized ' + count);


        })
    }


    // Start rooms dialog functionality
    $scope.showRoomsDialog = function(ev, selectedHotel) {
        $scope.selectedHotel.hotel = selectedHotel;
        $mdDialog.show({
                controller: DialogController,
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                scope: $scope,
                preserveScope: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
    }

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }



    // End rooms dialog functionality
    $scope.$watch("selectedTab", function(newValue) {
        localStorageService.set('selectedTab', $scope.selectedTab);
    });


    $scope.getPreviousHotel = function() {
        $scope.selectedTab--;
    }

    $scope.getNextHotel = function() {
        $scope.selectedTab++;
    }


    var addHotelToArray = function(chosenHotel, area) {
        var hotel = {};

        //        hotel.area = area.area_details.area;
        //        hotel.startDate = area.order_details.start_date;
        //        hotel.endDate = area.order_details.end_date;
        //        hotel.name = chosenHotel.name_name;
        //        hotel.price = chosenHotel.price;
        //        hotel.currency = chosenHotel.currency;
        //        hotel.room = chosenHotel.room_name;

        $scope.chosenHotels.push(hotel);
        $scope.chosenHotelAllInformation.push(chosenHotel);

        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
        localStorageService.set('selectedTab', $scope.selectedTab);
    }

    var removeHotelFromArray = function(area) {
        var result = true;
        for (var i = 0; i < $scope.chosenHotels.length && result; i++) {
            if ($scope.chosenHotels[i].area === area) {
                $scope.chosenHotels.splice(i, 1);
                $scope.chosenHotelAllInformation.splice(i, 1);
                result = false;
            }
        }

        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
    }

    $scope.filterHotels = function(currentHotel) {


        var returnVal = ($scope.hotelsNameFilter[currentHotel.hotel_name] &&
            $scope.filterByStars[currentHotel.stars.toString()] &&
            (currentHotel.price <= $scope.priceRange.userMaxOne) &&
            ($scope.priceRange.userMinOne <= currentHotel.price)
        );
        return returnVal;
    }

    $scope.filterRoom = function(selectedItem) {
        return function(room) {
            if (room == null)
                return true;


            return (!(room.name == hotel.selectedItem.name) && (room.price == hotel.selectedItem.price));

        }

    }

    $scope.selectRoomChosen = function(item, model, hotel) {

        if (item.id == 0) {
            hotel.selectLabel = hotel.rooms[1].name;
        } else {
            hotel.selectLabel = hotel.rooms[0].name;
        }

    }


    // Showing all the hotels name
    $scope.checkAllHotels = function(val) {
        $scope.isHotelsCheckedAll = !$scope.isHotelsCheckedAll;
        for (var currHotelIndex in $scope.hotelsNameFilter) {
            $scope.hotelsNameFilter[currHotelIndex] = $scope.isHotelsCheckedAll;
        }


    }
    $scope.toggleLeft = function() {

        $mdSidenav('left').toggle();

    }

    $scope.clickedIsAirlinesDropdown = function() {
        if ($scope.isAirlinesDropdown) {
            $timeout(function() {
                $scope.isAirlinesDropdown = !$scope.isAirlinesDropdown;
            }, 250);
            jQueryUse(document).ready(function() {
                jQueryUse("div[id=dropDownDiv]").css("cssText", "opacity: 0;");
            });
        } else {
            $scope.isAirlinesDropdown = !$scope.isAirlinesDropdown;
            jQueryUse(document).ready(function() {
                jQueryUse("div[id=dropDownDiv]").css({
                    "opacity": "1",
                    "position": "relative"
                });



            });
        }
    };
    $scope.chooseHotelInArea = function(index, hotels, zone) {
        if (zone.isHotelChoosed === true) {
            for (var i = 0; i < hotels.length; i++) {
                hotels[i].isNotChoosed = false;
            }
            zone.isHotelChoosed = false;
            removeHotelFromArray(zone.area_details.area);
        } else {
            for (var i = 0; i < hotels.length; i++) {

                if (i === index) {
                    hotels[i].isNotChoosed = false;
                    addHotelToArray(hotels[i], zone);
                } else {
                    hotels[i].isNotChoosed = true;
                }
            }
            zone.isHotelChoosed = true;
        }
        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
        localStorageService.set('selectedTab', $scope.selectedTab);
    }

    var toggleFunction = function(itemToToggle) {
        itemToToggle = itemToToggle === false ? true : false;
    }


    $scope.range = function(n) {
        if (n !== undefined) {
            return new Array(parseInt(n));
        } else {
            return 0;
        }
    };

    $scope.getchoosedHotels = function() {
        for (var i = 0; i < $scope.obj.hotels[0].length; i++) {
            console.log($scope.obj.hotels[0][i].choosedHotel);
        }
    }


    $scope.init = function() {
        if (localStorageService.get('allHotels') === null || localStorageService.get('chosenHotels') === null) {
            $scope.getHotels();
        } else {
            $scope.allhotels = localStorageService.get('allHotels');
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.selectedTab = localStorageService.get('selectedTab');
        }



    }

    $scope.test = function() {

        console.log('in test');
    }
    var bounds;

    function initMapData() {
        bounds = new google.maps.LatLngBounds();

        for (let i = 0; i < $scope.allhotels[0].highest_ranked.length; i++) {
            let currHotel = $scope.allhotels[0].highest_ranked[i];
            var latlng = new google.maps.LatLng(currHotel.latitude, currHotel.logitude);
            bounds.extend(latlng); //used for centering map view around bounds
        }
        resizeAndReboundMap();
        // $timeout(resizeAndReboundMap, 1000);
    }
    $scope.getHotels = function() {

        var promise = httpService.getObj();

        promise.then(
            function(res) {
                console.log(res);

                $scope.obj = res.data;

                $scope.hotelslength = $scope.obj.hotels.length;
                //console.log($scope.hotelslength);


                $scope.obj.hotels.forEach(function(zone, index) {
                    $scope.allhotels.push(zone);

                    // Creating unique list of hotels name for the filter
                    // Calculating the min and max price for the price filter
                    $scope.priceRange.min = zone.highest_ranked[0].price;
                    $scope.priceRange.max = zone.highest_ranked[0].price;
                    for (var hotel in zone.highest_ranked) { // Setting the minimum
                        if (zone.highest_ranked[hotel].price < $scope.priceRange.min) {
                            $scope.priceRange.min = zone.highest_ranked[hotel].price;
                            $scope.priceRange.userMinOne = zone.highest_ranked[hotel].price;
                        }

                        if (zone.highest_ranked[hotel].price > $scope.priceRange.max) {
                            $scope.priceRange.max = zone.highest_ranked[hotel].price;
                            $scope.priceRange.userMaxOne = zone.highest_ranked[hotel].price;
                        }

                        var hotelName = zone.highest_ranked[hotel].hotel_name;
                        if (Object.keys($scope.hotelsNameFilter).indexOf(hotelName) === -1) {
                            $scope.hotelsNameFilter[hotelName] = true;
                        }




                    }

                });

                $scope.allhotels.forEach(function(zone, index) {

                    zone.isHotelChoosed = false;

                    //Setting the first selected room for the rooms combobox
                    for (let hotel of zone.highest_ranked) {

                        hotel.selectedItem = hotel.rooms[0];
                        hotel.selectLabel = hotel.rooms[0].name;

                        hotel.filteredRooms = hotel.rooms;
                        hotel.rooms.sort(function(a, b) {
                            if (a.price < b.price) {
                                return -1;
                            }

                            if (a.price > b.price) {
                                return 1;
                            }
                            return 0;
                        });
                        // Going through all of the rooms to sort them and create filters
                        hotel.rooms.forEach(function(room, index) {

                            var searchMask = "\\s?\\(?non-refundable\\)?";
                            var regEx = new RegExp(searchMask, "ig");
                            var replaceMask = "";
                            var breakFastMask = "breakfast";
                            var breakFastRegex = new RegExp(breakFastMask, "ig");
                            // Checking if the room is refundable
                            room.isRefundable = (!regEx.test(room.full_name) || !regEx.test(room.info));


                            // Checking if includes breakfast
                            room.includesBreakFast = (breakFastRegex.test(room.info) || breakFastRegex.test(room.board));

                            // Fixing the show name
                            //   room.name = room.name.replace(regEx,replaceMask);

                            //  Adding to filter the show name
                            if (Object.keys($scope.roomsFilterOptions).findIndex(item =>
                                    room.name.toLowerCase() === item.toLowerCase()) == -1) {
                                $scope.roomsFilterOptions[room.name] = true;

                            }


                            room.id = index;
                            if (index == 0) {
                                room.show = false;
                            } else {
                                room.show = true;
                            }
                        })
                    }

                })


                initMapData();
                var ordered = {};
                Object.keys($scope.roomsFilterOptions).sort().forEach(function(key) {
                    ordered[key] = $scope.roomsFilterOptions[key];
                });

                $scope.roomsFilterOptions = ordered;
                console.log($scope.roomsFilterOptions);

            },
            function(err) {
                console.log(err);
            }
        );
    }

    $scope.checkIfHotelChoosedInMap = function(hotelName, zone) {
        var result = false;
        if (zone.isHotelChoosed == false) {
            result = true;
        } else {

            var hotels = zone.highest_ranked;
            for (var i = 0; i < hotels.length && !result; i++) {
                if (hotels[i].name === hotelName && hotels[i].isNotChoosed == false) {
                    result = true;
                }
            }
        }
        return result;
    }




    $scope.filterByRoomName = function(currentRoom) {
        filterCount++;
        jQueryUse("#filterCount").html(filterCount)
        let show = false;
        let showRefundable = Object.values($scope.staticRoomsFilterOptions)[0];
        let showNonRefundable = Object.values($scope.staticRoomsFilterOptions)[1];
        let showIncludeBreakFast = Object.values($scope.staticRoomsFilterOptions)[2];
        let showDontIncludeBreakFast = Object.values($scope.staticRoomsFilterOptions)[3];

        if (((currentRoom.includesBreakFast === showIncludeBreakFast) && showIncludeBreakFast) ||
            ((currentRoom.includesBreakFast !== showDontIncludeBreakFast)) && showDontIncludeBreakFast) {
            show = true;

        }

        if (show && (((currentRoom.isRefundable === showRefundable) && showRefundable) ||
                ((currentRoom.isRefundable !== showNonRefundable)) && showNonRefundable)) {
            show = true;

        } else {
            show = false;
        }

        var returnVal = ($scope.roomsFilterOptions.getProp(currentRoom.name));


        return (returnVal && show);




    }

    //    function getCategories() {
    //    return (zone.highest_ranked || []).
    //      map(function (hotel) { return hotel.hotel_name; }).
    //      filter(function (cat, idx, arr) { return arr.indexOf(cat) === idx; });
    //  }


    $scope.init();
});


app.directive('chooseHotel', function() {
    return {
        restrict: 'A',
        template: '<figure><img width="500px"/><figcaption/></figure>',
        replace: true,
        link: function(scope, element, attrs) {
            attrs.$observe('caption', function(value) {
                element.find('figcaption').text(value)
            })
            attrs.$observe('photoSrc', function(value) {
                element.find('img').attr('src', value)
            })
        }
    }
})

app.directive('checkBoxFilter', function() {

    return {
        templateUrl: 'views/directiveTemplates/checkBoxFilter.html',
        replace: true,
        link: function(scope, elements, attr) {
            scope.isCheckedAll = true;
            scope.isExpanded = false;
            scope.checkAll = function() {
                for (var currentFilter in scope.filterOptions) {
                    scope.filterOptions[currentFilter] = scope.isCheckedAll;
                }
            };




        },
        scope: {
            filterOptions: '=',
            id: '@',
            title: '@'
        }
    }
});

Object.defineProperty(Object.prototype, "getProp", {
    value: function(prop) {
        var key, self = this;
        for (key in self) {
            if (key.toLowerCase() == prop.toLowerCase()) {
                return self[key];
            }
        }
    },
    //this keeps jquery happy
    enumerable: false
});