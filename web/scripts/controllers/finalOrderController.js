'use strict';

app.controller('finalOrderController', function ($scope, httpService, $location, localStorageService, $http, $timeout) {
    $scope.chosenHotels = [];
    $scope.chosenFlights = [];
    $scope.chosenHotelAllInformation = {};
    $scope.isWhite = true;
    $scope.loading = false;

    $scope.convertMinutesToTimeString =  minutes => {
                return commonHelpers.convertMinutesToHoursString(minutes);
            };
    $scope.init = function () {
        if (localStorageService.get('chosenHotels') !== null && localStorageService.get('chosenFlight') !== null) {
            $scope.chosenHotelAllInformation = localStorageService.get('allHotelsAllInformation');
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.chosenFlights = localStorageService.get('chosenFlight');
        }
        else if(localStorageService.get('chosenHotels') !== null)
        {
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.chosenHotelAllInformation = localStorageService.get('allHotelsAllInformation');
        }
        else if(localStorageService.get('chosenFlight') !== null)
        {
            $scope.chosenFlights = localStorageService.get('chosenFlight');

        }
    };

    function redirect(Code) {
        $http({
            method: 'GET',
            url: '/DisplayBookings/' + Code
        }).then(function successCallback(response) {
            localStorageService.set('bookingDetails', response);
            $location.path('/displayBookingCode/' + Code);
        }, function errorCallback(response) {
            $scope.loading = false;
            console.log(response);
        });

    }

    $scope.sendFinalOrder = function() {
        $scope.loading = true;
        $http({
            method: 'POST',
            url: '/finalOrder',
            data: {flights: JSON.stringify($scope.chosenFlights), hotels: JSON.stringify($scope.chosenHotelAllInformation)}
        }).then(function successCallback(response) {
            redirect(response.data);
        }, function errorCallback(response) {
            $scope.loading = false;
            console.log("Fail");
            console.log(response);
        });

        console.log($scope.chosenFlights);
    };

    $scope.cleanFinalOrder = function () {
        localStorageService.remove('chosenFlight','allHotelsAllInformation','chosenHotels');
        $scope.chosenHotels = [];
        $scope.chosenFlight = [];
        $scope.chosenHotelAllInformation = {};
    };

    $scope.init();

    $scope.getDurationHours = function (duration) {
        var durationInt = parseInt(duration);
            return Math.floor(durationInt / 60);
    };


        $scope.getClassOfFlightsString = flights =>{
       let classStr = Array.prototype.map.call(flights, flight=> {return flight.details.class}).join(', ')
       return classStr;
    };

    $scope.getDurationMinits = function (duration) {
        var minits = duration % 60;
        if (minits < 10) minits = '0' + minits;
            return minits;
    };

    jQuery(document).ready(function() {
        jQueryUse("div.row.orderedFlights").css("cssText", "opacity: 1;");
        jQueryUse('#blueButton').on('click', function() {
            jQueryUse("div.row.orderedFlights").css("cssText", "opacity: 0;");
            $timeout(function(){
                $scope.isWhite = false;
                $scope.$apply();
                jQuery(document).ready(function(){
                    $timeout(function() {
                        jQueryUse("div.row.orderedFlightsBlue").css("cssText", "opacity: 1;");
                    }, 450);
                });
            }, 350);
        });

        jQueryUse('#whiteButton').on('click', function() {
            jQueryUse("div.row.orderedFlightsBlue").css("cssText", "opacity: 0;");
            $timeout(function(){
                $scope.isWhite = true;
                $scope.$apply();
                jQuery(document).ready(function(){
                    $timeout(function() {
                        jQueryUse("div.row.orderedFlights").css("cssText", "opacity: 1;");
                    }, 250);
                });
            }, 350);
        });
    });
});

