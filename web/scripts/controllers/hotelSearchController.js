'use strict';

app
    .controller('hotelSearchController', function ($scope, $http, $location, $timeout, localStorageService) {

        var listOfCompanyUsers = [];
        var form;
        var currentlyLoggedIn = localStorageService.get('user');
        var legIndex;

        $scope.loading = false;
        $scope.allCompanyPassengers = [];
        $scope.Hotels = [];
        $scope.getNumber = function(num) { return new Array(num);}
        $scope.extra1 = false;
        $scope.extra2 = false;
        $scope.extra3 = false;
        $scope.extra4 = false;
        $scope.extra5 = false;
        $scope.extra6 = false;
        $scope.passengers = [];
        $scope.passengersAsObjects = [];

        $scope.$watch('allCompanyPassengers', function(newVal, oldVal) {
            if(!(newVal.length == 0)) {
                for(var i = 0; i<$scope.allCompanyPassengers.length; i++) {
                    if($scope.allCompanyPassengers[i].id != currentlyLoggedIn.user_id) {
                        $scope.passengersAsObjects
                            .push({
                                admin: $scope.allCompanyPassengers[i].admin,
                                id: $scope.allCompanyPassengers[i].id,
                                username: $scope.allCompanyPassengers[i].username,
                                last_name: $scope.allCompanyPassengers[i].last_name,
                                name: $scope.allCompanyPassengers[i].name,
                                checked: false
                            });
                    }
                    else {
                        $scope.passengersAsObjects
                            .push({
                                admin: $scope.allCompanyPassengers[i].admin,
                                id: $scope.allCompanyPassengers[i].id,
                                username: $scope.allCompanyPassengers[i].username,
                                last_name: $scope.allCompanyPassengers[i].last_name,
                                name: $scope.allCompanyPassengers[i].name,
                                checked: true
                            });
                    }
                }
            }
        });

        $http({
            method: 'GET',
            url: '/ListCompanyUsers'
        }).then(function successCallback(response) {
                $scope.allCompanyPassengers = response.data;
            },
            function errorCallback(response) {
                alert(response.status);
            });

        $scope.add = function () {
            var legToAdd = {
            };
            $scope.Hotels.push(legToAdd);

            legIndex = $scope.Hotels.length - 1;

            jQueryUse(document).ready(function() {
                jQueryUse(("a[name=aTag" + legIndex + "]")).on('click', function (event) {
                    jQueryUse(this).parent().toggleClass("open");
                });

                jQueryUse(("a[name=aTags" + legIndex + "]")).on('click', function (event) {
                    jQueryUse(this).parent().toggleClass("open");
                });

                jQueryUse("form[name=leg" + legIndex + "]").keydown(function(e){
                    if(e.keyCode == 13) {
                        return false;
                    }
                });

                jQueryUse("button[id=buttonAdditionalAirportsOrigin" + legIndex + "]").on('click', function() {
                    jQueryUse('li.dropdown.mega-dropdown.open').each(function() {
                        var theDropdown = jQueryUse(this);
                        jQueryUse(document).ready(function() {
                            theDropdown.removeClass("open");
                        });
                    });
                });

                jQueryUse("button[id=buttonAdditionalAddressesOrigin" + legIndex + "]").on('click', function() {
                    jQueryUse('li.dropdown.mega-dropdown.open').each(function() {
                        var theDropdown = jQueryUse(this);
                        jQueryUse(document).ready(function() {
                            theDropdown.removeClass("open");
                        });
                    });
                });

                jQueryUse("button[id=buttonAdditionalAirportsDestination" + legIndex + "]").on('click', function() {
                    jQueryUse('li.dropdown.mega-dropdown.open').each(function() {
                        var theDropdown = jQueryUse(this);
                        jQueryUse(document).ready(function() {
                            theDropdown.removeClass("open");
                        });
                    });
                });

                jQueryUse("button[id=buttonAdditionalAddressesDestination" + legIndex + "]").on('click', function() {
                    jQueryUse('li.dropdown.mega-dropdown.open').each(function() {
                        var theDropdown = jQueryUse(this);
                        jQueryUse(document).ready(function() {
                            theDropdown.removeClass("open");
                        });
                    });
                });

                jQueryUse("button[id=buttonMakeWidthSmallerOne" + legIndex + "]")
                    .css("cssText",
                        "background-color: #198b00 !important;" +
                        "line-height: 32px;" +
                        "min-height: 32px;" +
                        "line-width: 80px;" +
                        "min-width: 95%;")
                    .children().on('click', function(e) {
                        var liElement = jQueryUse(e.target).parent().parent().parent();
                        liElement.removeClass('open');
                    });

                jQueryUse("button[id=buttonMakeWidthSmallerTwo" + legIndex + "]")
                    .css("cssText",
                        "background-color: #198b00 !important;" +
                        "line-height: 32px;" +
                        "min-height: 32px;" +
                        "line-width: 80px;" +
                        "min-width: 95%;")
                    .children().on('click', function(e) {
                    var liElement = jQueryUse(e.target).parent().parent().parent();
                    liElement.removeClass('open');
                });

                jQueryUse("button[id=buttonMakeWidthSmallerThree" + legIndex + "]")
                    .css("cssText",
                        "background-color: #198b00 !important;" +
                        "line-height: 32px;" +
                        "min-height: 32px;" +
                        "line-width: 80px;" +
                        "min-width: 95%;")
                    .children().on('click', function(e) {
                        var liElement = jQueryUse(e.target).parent().parent().parent();
                        liElement.removeClass('open');
                    });

                jQueryUse("button[id=buttonMakeWidthSmallerFour" + legIndex + "]")
                    .css("cssText",
                        "background-color: #198b00 !important;" +
                        "line-height: 32px;" +
                        "min-height: 32px;" +
                        "line-width: 80px;" +
                        "min-width: 95%;")
                    .children().on('click', function(e) {
                    var liElement = jQueryUse(e.target).parent().parent().parent();
                    liElement.removeClass('open');
                });
            });

        };

        function redirect(success) {
            if (success) $location.path('/hotels2');
        }

        $scope.sendForm = function() {
            $scope.passengers = [];
            for(var i=0; i<$scope.passengersAsObjects.length; i++) {
                if($scope.passengersAsObjects[i].checked) {
                    $scope.passengers.push($scope.passengersAsObjects[i].id);
                }
            }

            if($scope.passengers.length>9) {
                alert("ERROR, more than 9 passengers have been chosen!");
            }
            else {
                if($scope.passengers.length == 0) {
                    alert("Please choose at least one traveler");
                } else {
                    $scope.loading = true;
                    form = {hotels: $scope.Hotels, passengers: $scope.passengers};

                    $http({
                        method: 'POST',
                        url: '/HotelSearch',
                        data: JSON.stringify(form)
                    }).then(function successCallback(response) {

                            var postSuccess = response.data.success;
                            $scope.loading = false;
                            // Removing the last search results from the browser cache
                            localStorageService.set('allHotels', null);
                            localStorageService.set('allHotelsAllInformation', null);
                            redirect(postSuccess);
                        },
                        function errorCallback(response) {
                            $scope.loading = false;
                            alert(response.status);
                        });
                }
            }
        };
        $scope.add();

        jQueryUse(document).ready(function() {
            jQueryUse("input").keydown(function(e){
                if(e.keyCode == 13) {
                    return false;
                }
            });

            jQueryUse('body').on('click', function (e) {
                if (!jQueryUse('li.dropdown.mega-dropdown').is(e.target)
                    && jQueryUse('li.dropdown.mega-dropdown').has(e.target).length === 0
                    && jQueryUse('.open').has(e.target).length === 0
                    && jQueryUse('li.dropdown.mega-dropdown').hasClass("open")) {
                        jQueryUse('li.dropdown.mega-dropdown.open').removeClass('open');
                }
                else {
                    if(!angular.isUndefined(jQueryUse(e.target).attr("id")) && jQueryUse(e.target).attr("id").length >= 22 && jQueryUse(e.target).attr("id").substring(0, 22) == "buttonMakeWidthSmaller") {
                        var successOrCancel = jQueryUse(e.target).attr("id").substring(22,25);
                        if(successOrCancel == "One" || successOrCancel == "Thr") {
                            var liElement = jQueryUse(e.target).parent().parent();
                            liElement.removeClass('open');
                        } else {
                            var liElement2 = jQueryUse(e.target).parent().parent();
                            liElement2.removeClass('open');
                        }
                    }
                }
            });
        });
    });
