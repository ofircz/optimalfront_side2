'use strict';


app
    .controller('AllFlightsController', function($scope, $window, $location, httpService, orderService, localStorageService, $http, $timeout, $route, $filter) {

        var amountOfLegs = 1;
        $scope.pageTitle = "טיסות";
        $scope.loading = false;
        $scope.showCheapest = false;
        $scope.showBest = true;
        $scope.selectedAll = true;
        $scope.selectedAll2 = true;
        $scope.selectedFlight = false;
        $scope.showShortest = false;
        $scope.chosenFlights = localStorageService.get('chosenFlight');
        $scope.flightadded = [];
        $scope.flightdisabled = [];
        $scope.cheapestFlights = [];
        $scope.bestFlights = [];
        $scope.flightsFilteredByMinPrice = [];
        $scope.shortestFlights = [];
        $scope.currentModalFlight = {};
        $scope.allFlights = [];
        localStorageService.set('chosenFlight', []);
        $scope.showChosenFlights = false;
        $scope.chosenFile = "from_server";
        $scope.files = ["file1", "from_server", "TBS_local"];
        $scope.filePath = ['/resources/newData5.json', '/ShowTBSResults', '/resources/ShowTBSResults.json'];
        $scope.legsStopsArrays = {};
        $scope.legsArrays = {};
        $scope.legsDurations = {};
        $scope.allFilteredFlights = {};
        $scope.legDurationsSlider = {};
        $scope.arrivalLegsTimeRange = {};
        $scope.departureAirportsArrays = {};
        $scope.arrivalAirportsArrays = {};
        $scope.airlinesArray = [];
        $scope.departureLegsTimeRange = {};
        $scope.departureLegsTimeRangeSlider = {};
        $scope.arrivalLegsTimeRangeSlider = {};
        $scope.departureLegsDriveTimeSlider = {};
        $scope.departureLegsDriveTrafficTimeSlider = {};
        $scope.arrivalLegsDriveTimeSlider = {};
        $scope.arrivalLegsDriveTrafficTimeSlider = {};
        $scope.arrivalLegsHrRange = {};
        $scope.minPriceMouseDown = false;
        $scope.maxPriceMouseDown = false;
        $scope.minPriceMouseup = 0;
        $scope.maxPriceMouseup = 0;
        $scope.serverLimit = 200;

        $scope.legsStopsFilter = {
            disabled: false,
            values: null
        };
        $scope.filterStyleValue = "relative";
        $window.onscroll = () => {

            // using angular jquery in order to save $scope.$apply -- Alex Lavriv 02/10/2017
            let filterBtnElement = angular.element(".filterButton");
            if ($window.pageYOffset > 265) {
                filterBtnElement.css("position", "fixed")
                filterBtnElement.css("top", "5px")


            } else {

                filterBtnElement.css("position", "relative")
                filterBtnElement.css("top", "0px")
            }

        };
        $scope.legsAirLinesFilter = {
            disabled: false,
            values: null
        };
        $scope.departureAirportsFilter = {
            disabled: false,
            values: null
        }

        $scope.ArrivalAirportsFilters = {
            disabled: false,
            values: null
        }



        $scope.departureDatesFilter = {
            disabled: false,
            values: null
        }
        $scope.ArrivalDatesFilter = {
            disabled: false,
            values: null
        }

        $scope.connectionAirportsFilter = {
            disabled: false,
            values: null
        }
        $scope.flightDurationSlider = {
            minDuration: 0,
            maxDuration: 10000,
            maxMouseDown: false,
            minMouseDown: false
        };
        $scope.flightDriveDurationSlider = {
            minDuration: 0,
            maxDuration: 10000,
            minMouseDown: false,
            maxMouseDown: false
        };
        $scope.flightDriveTrafficDurationSlider = {
            minDuration: 0,
            maxDuration: 10000,
            minMouseDown: false,
            maxMouseDown: false
        };
        $scope.flightDurationRange = {
            range: {
                min: 0,
                max: 1500
            },
            minDuration: 0,
            maxDuration: 1500,
            disabled: false
        };
        $scope.flightDriveDurationRange = {
            range: {
                min: 0,
                max: 3000
            },
            minDuration: 0,
            maxDuration: 3000,
            disabled: false
        };
        $scope.flightDriveTrafficDurationRange = {
            range: {
                min: 0,
                max: 3000
            },
            minDuration: 0,
            maxDuration: 3000,
            disabled: false
        };
        $scope.priceRange = {
            range: {
                min: 0,
                max: 0
            },
            minPrice: 0,
            maxPrice: 0,
            disabled: false

        };
        $scope.isAirlinesDropdown = false;
        $scope.isConnectionsDropdown = false;
        $scope.displayLimitSelect = "50";
        $scope.sortingDataSelect = "price";
        $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
            alert('done');
        });
        // $scope.serverFilters explained
        /*
         Why would one want to declare a dummy scope variable if it's received from the server anyway?
         Webstorm autocompletes from the same file, if dummy variables inside $scope.serverFilters aren't
         declared webstorm will show low level warnings and won't autocomplete.

         The values in this variable are dummy, and are properly loaded later on
         */
        $scope.clientFilters = {}
        $scope.serverFilters = {
            airport_arrival: 1,
            airport_connection: 1,
            airport_departure: 1,
            date_arrival: 1,
            date_departure: 1,
            max_drive_time: 1,
            max_drive_time_leg: 1,
            max_flight_drive_duration: 1,
            max_flight_drive_duration_leg: 1,
            max_flight_drive_traffic_duration: 1,
            max_flight_drive_traffic_duration_leg: 1,
            max_flight_duration: 1,
            max_flight_duration_leg: 1,
            max_price: 1,
            max_time_arrival: 1,
            max_time_departure: 1,
            min_drive_time: 1,
            min_drive_time_leg: 1,
            min_flight_drive_duration: 1,
            min_flight_drive_duration_leg: 1,
            min_flight_drive_traffic_duration: 1,
            min_flight_drive_traffic_duration_leg: 1,
            min_flight_duration: 1,
            min_flight_duration_leg: 1,
            min_price: 1,
            min_time_arrival: 1,
            min_time_departure: 1,
            pos_airlines: 1,
            stops_range: 1
        };

        function isRangeIncreased(range) {
            let out = false;

            let min = range.minPrice;
            let max = range.maxPrice;
            max = max == null ? range.maxTime : max;
            min = min == null ? range.minTime : min;
            max = max == null ? range.maxDuration : max;
            min = min == null ? range.minDuration : min;

            if (range.lastMin == null){
                range.lastMin = range.range.min;
            }

             if (range.lastMax == null){
                range.lastMax = range.range.max;
            }

            if ((range.lastMin > min) || range.lastMax < max){
                out = true;
            }

            range.lastMax = max;
            range.lastMin = min;

            return out;

        }

        $scope.filterCheckBoxChanged = function(newVal){
             let rangeIncreased = newVal;

            let filtered = $scope.filterClientData();
           let gotDataFromServer = getDataFromServer(rangeIncreased, filtered);

            return filtered;

        }
        $scope.filterClientData = function() {
            let filtered = $scope.bestFlights;

            filtered = $filter('mainFilter')(filtered,
                $scope.priceRange,
                $scope.departureLegsHrRange,
                $scope.arrivalLegsHrRange,
                $scope.flightDurationRange,
                $scope.flightDriveDurationRange,
                $scope.flightDriveTrafficDurationRange,
                $scope.legsDurations,
                $scope.legsStopsArrays,
                $scope.airlinesArray,
                $scope.departureAirportsArrays,
                $scope.arrivalAirportsArrays,
                $scope.connectionAirportsArrays,
                $scope.departureDatesArrays,
                $scope.arrivalDatesArrays);
            $scope.filteredData = filtered;
            return filtered;

        }
        $scope.applyClientFilters = function(range) {
            let rangeIncreased = isRangeIncreased(range);

            let filtered = $scope.filterClientData();
           let gotDataFromServer = getDataFromServer(rangeIncreased, filtered);

           if (gotDataFromServer == false) {
                $scope.$apply()
                };
            return filtered;
        }

        let lastChunk = false;
        function getDataFromServer(rangeIncreased, filtered){
            let shouldCallServer = false;

            if ($scope.bestFlights.length < $scope.serverLimit) {
                lastChunk = true;
            } else {
                lastChunk = false;
            }


            // The range decreased;
            if (rangeIncreased == false){
                    if ((filtered.length < $scope.displayLimitSelect) &&
                        (lastChunk == false)){
                        console.log("Calling server decreased");
                        $scope.filterTheValues();
                        shouldCallServer = true;
                    }
            }
            else{
                // The range increased

                if (filtered.length < $scope.serverLimit){
                        console.log("Calling server increased");
                        $scope.filterTheValues();
                        shouldCallServer = true;
                    }

            }



            return shouldCallServer;
        }
        // Watch Rangers, This function will determine if the range has been increased - Alex Lavriv 18/10/2017

        $scope.rangeIncreased = false;
        $scope.$watchGroup([''], function(newValues, oldValues) {
            applyFilters();

            let [newMin, newMax] = newValues;
            let [oldMin, oldMax] = oldValues;

            if ((newMin < oldMin) || (oldMax > oldMax)) {
                $scope.rangeIncreased = true;
                console.log("range increased: " + $scope.rangeIncreased )

            }


        });




        $scope.filterTheValues = function() {
            $scope.loading = true;
            var array_min_time_arrival = [];
            var array_max_time_arrival = [];
            var array_min_time_departure = [];
            var array_max_time_departure = [];
            var array_date_arrival = [];
            var array_date_departure = [];
            var array_stops_range = [];
            var array_airport_arrival = [];
            var array_airport_departure = [];
            var array_airport_connection = [];
            var checked_airlines = [];
            var array_min_flight_duration_leg = [];
            var array_max_flight_duration_leg = [];
            var array_min_drive_time_leg = [];
            var array_max_drive_time_leg = [];
            var max_drive_time_to_send = 1;
            var min_drive_time_to_send = 2;
            var sortingDataValue = $scope.sortingDataSelect;
            var displayLimitValue = $scope.serverLimit; //parseInt($scope.displayLimitSelect);

            for (var j = 1; j <= amountOfLegs; j++) {
                var someArray = [];
                $scope.connectionAirportsArrays[j].forEach(function(airportObject) {
                    if (airportObject.isChecked) {
                        someArray.push(airportObject.airportName);
                    }
                });
                array_airport_connection.push({
                    values: someArray,
                    leg_index: j
                });
            }

            for (var keyCheckedAirline in $scope.airlinesArray) {
                if ($scope.airlinesArray.hasOwnProperty(keyCheckedAirline)) {
                    if ($scope.airlinesArray[keyCheckedAirline].Selected) {
                        checked_airlines.push($scope.airlinesArray[keyCheckedAirline].name);
                    }
                }
            }

            for (var arrivalMinKey in $scope.arrivalLegsTimeRange) {
                if ($scope.arrivalLegsTimeRange.hasOwnProperty(arrivalMinKey)) {
                    array_min_time_arrival.push({
                        leg_index: arrivalMinKey,
                        value: $scope.arrivalLegsTimeRange[arrivalMinKey].minTime
                    });
                }
            }

            for (var arrivalMaxKey in $scope.arrivalLegsTimeRange) {
                if ($scope.arrivalLegsTimeRange.hasOwnProperty(arrivalMaxKey)) {
                    array_max_time_arrival.push({
                        leg_index: arrivalMaxKey,
                        value: $scope.arrivalLegsTimeRange[arrivalMaxKey].maxTime
                    });
                }
            }

            for (var departureMinKey in $scope.departureLegsTimeRange) {
                if ($scope.departureLegsTimeRange.hasOwnProperty(departureMinKey)) {
                    array_min_time_departure.push({
                        leg_index: departureMinKey,
                        value: $scope.departureLegsTimeRange[departureMinKey].minTime
                    });
                }
            }

            for (var departureMaxKey in $scope.departureLegsTimeRange) {
                if ($scope.departureLegsTimeRange.hasOwnProperty(departureMaxKey)) {
                    array_max_time_departure.push({
                        leg_index: departureMaxKey,
                        value: $scope.departureLegsTimeRange[departureMaxKey].maxTime
                    });
                }
            }

            for (var arrivalDateKey in $scope.arrivalDatesArrays) {
                if ($scope.arrivalDatesArrays.hasOwnProperty(arrivalDateKey)) {
                    var arrayForArrayDateArrival = [];
                    for (var keyForArrivalDateKey in $scope.arrivalDatesArrays[arrivalDateKey]) {
                        if ($scope.arrivalDatesArrays[arrivalDateKey].hasOwnProperty(keyForArrivalDateKey)) {
                            if ($scope.arrivalDatesArrays[arrivalDateKey][keyForArrivalDateKey].Selected) {
                                arrayForArrayDateArrival.push($scope.arrivalDatesArrays[arrivalDateKey][keyForArrivalDateKey].date);
                            }
                        }
                    }
                    array_date_arrival.push({
                        leg_index: arrivalDateKey,
                        values: arrayForArrayDateArrival
                    });
                }
            }

            for (var departureDateKey in $scope.departureDatesArrays) {
                if ($scope.departureDatesArrays.hasOwnProperty(departureDateKey)) {
                    var arrayForArrayDateDeparture = [];
                    for (var keyForDepartureDateKey in $scope.departureDatesArrays[departureDateKey]) {
                        if ($scope.departureDatesArrays[departureDateKey].hasOwnProperty(keyForDepartureDateKey)) {
                            if ($scope.departureDatesArrays[departureDateKey][keyForDepartureDateKey].Selected) {
                                arrayForArrayDateDeparture.push($scope.departureDatesArrays[departureDateKey][keyForDepartureDateKey].date);
                            }
                        }
                    }
                    array_date_departure.push({
                        leg_index: departureDateKey,
                        values: arrayForArrayDateDeparture
                    });
                }
            }

            for (var maxStopsKey in $scope.legsStopsArrays) {
                if ($scope.legsStopsArrays.hasOwnProperty(maxStopsKey)) {
                    var stopsForCurrentLeg = [];
                    for (var keyForStopsKey in $scope.legsStopsArrays[maxStopsKey]) {
                        if ($scope.legsStopsArrays[maxStopsKey].hasOwnProperty(keyForStopsKey)) {
                            if ($scope.legsStopsArrays[maxStopsKey][keyForStopsKey].isChecked) {
                                stopsForCurrentLeg.push($scope.legsStopsArrays[maxStopsKey][keyForStopsKey].num)
                            }
                        }
                    }
                    array_stops_range.push({
                        leg_index: maxStopsKey,
                        values: stopsForCurrentLeg
                    });
                }
            }

            for (var airportArrivalKey in $scope.arrivalAirportsArrays) {
                if ($scope.arrivalAirportsArrays.hasOwnProperty(airportArrivalKey)) {
                    var arrayOfAirportsArrival = [];
                    for (var keyForairportArrivalKey in $scope.arrivalAirportsArrays[airportArrivalKey]) {
                        if ($scope.arrivalAirportsArrays[airportArrivalKey].hasOwnProperty(keyForairportArrivalKey)) {
                            if ($scope.arrivalAirportsArrays[airportArrivalKey][keyForairportArrivalKey].isChecked) {
                                arrayOfAirportsArrival.push($scope.arrivalAirportsArrays[airportArrivalKey][keyForairportArrivalKey].airportName);
                            }
                        }
                    }
                    array_airport_arrival.push({
                        leg_index: airportArrivalKey,
                        values: arrayOfAirportsArrival
                    });
                }
            }

            for (var airportDepartureKey in $scope.departureAirportsArrays) {
                if ($scope.departureAirportsArrays.hasOwnProperty(airportDepartureKey)) {
                    var arrayOfAirportsDeparture = [];
                    for (var keyForairportDepartureKey in $scope.departureAirportsArrays[airportDepartureKey]) {
                        if ($scope.departureAirportsArrays[airportDepartureKey].hasOwnProperty(keyForairportDepartureKey)) {
                            if ($scope.departureAirportsArrays[airportDepartureKey][keyForairportDepartureKey].isChecked) {
                                arrayOfAirportsDeparture.push($scope.departureAirportsArrays[airportDepartureKey][keyForairportDepartureKey].airportName);
                            }
                        }
                    }

                    array_airport_departure.push({
                        leg_index: airportDepartureKey,
                        values: arrayOfAirportsDeparture
                    });
                }
            }

            for (var legDurationKey in $scope.legsDurations) {
                if ($scope.legsDurations.hasOwnProperty(legDurationKey)) {
                    array_min_flight_duration_leg.push({
                        value: $scope.legsDurations[legDurationKey].minDuration,
                        leg_index: legDurationKey
                    });
                    array_max_flight_duration_leg.push({
                        value: $scope.legsDurations[legDurationKey].maxDuration,
                        leg_index: legDurationKey
                    });
                }
            }

            for (var h = 1; h <= amountOfLegs; h++) {
                array_min_drive_time_leg.push({
                    value: 0,
                    leg_index: h
                });
                array_max_drive_time_leg.push({
                    value: 9999,
                    leg_index: h
                });
            }
            // missing filters
            /*

             // such filters don't currently exist on client side
             min_flight_drive_traffic_duration_leg
             max_flight_drive_traffic_duration_leg
             min_flight_drive_duration_leg
             max_flight_drive_duration_leg

             // weirdly calculated both origin and destination
             min_drive_time_leg - const
             max_drive_time_leg - const
             min_drive_time - const
             max_drive_time - const
              */

            var filterValuesJson = {
                filteres_data: {
                    airport_arrival: {
                        value: array_airport_arrival
                    },
                    airport_connection: {
                        value: array_airport_connection
                    },
                    airport_departure: {
                        value: array_airport_departure
                    },
                    date_arrival: {
                        value: array_date_arrival
                    },
                    date_departure: {
                        value: array_date_departure
                    },
                    //max_drive_time: {value: 9999},
                    //max_drive_time_leg: {value: array_max_drive_time_leg},
                    max_flight_drive_traffic_duration: {
                        value: $scope.flightDriveTrafficDurationRange.maxDuration
                    },
                    max_flight_drive_duration: {
                        value: $scope.flightDriveDurationRange.maxDuration
                    },
                    max_flight_duration: {
                        value: $scope.flightDurationRange.maxDuration
                    },
                    max_price: {
                        value: $scope.priceRange.maxPrice
                    },
                    max_time_arrival: {
                        value: array_max_time_arrival
                    },
                    max_time_departure: {
                        value: array_max_time_departure
                    },
                    max_flight_duration_leg: {
                        value: array_max_flight_duration_leg
                    },
                    //min_drive_time: {value: 0},
                    //min_drive_time_leg: {value: array_min_drive_time_leg},
                    min_flight_drive_duration: {
                        value: $scope.flightDriveDurationRange.minDuration
                    },
                    min_flight_drive_traffic_duration: {
                        value: $scope.flightDriveTrafficDurationRange.minDuration
                    },
                    min_flight_duration: {
                        value: $scope.flightDurationRange.minDuration
                    },
                    min_price: {
                        value: $scope.priceRange.minPrice
                    },
                    min_time_arrival: {
                        value: array_min_time_arrival
                    },
                    min_time_departure: {
                        value: array_min_time_departure
                    },
                    min_flight_duration_leg: {
                        value: array_min_flight_duration_leg
                    },
                    pos_airlines: {
                        values: checked_airlines
                    },
                    stops_range: {
                        value: array_stops_range
                    }
                },
                sorting_data: {
                    name: sortingDataValue,
                    reverse: 0
                },
                display_limit: $scope.serverLimit
            };

            console.log("filterValuesJson");
            console.log(filterValuesJson);
            $http({
                method: 'POST',
                url: '/UpdateFiltersData',
                data: JSON.stringify(filterValuesJson)
            }).then(function successCallback(response) {
                    console.log(response);
                    $scope.getTBSResults(false);

                },
                function errorCallback(response) {
                    console.log(response.status);
                });


            // $route.reload();
        };

        function setArrivalLegsTimeRange(flights) {
            for (var i = 1; i <= amountOfLegs; i++) {
                $scope.serverFilters.min_time_arrival.value[i - 1].value = $scope.serverFilters.min_time_arrival.value[i - 1].value;
                $scope.serverFilters.max_time_arrival.value[i - 1].value = $scope.serverFilters.max_time_arrival.value[i - 1].value;
            }

            $scope.arrivalLegsTimeRange = {};
            $scope.arrivalLegsTimeRangeSlider = {};

            for (var j = 1; j <= amountOfLegs; j++) {
                $scope.arrivalLegsTimeRange[j] = {
                    range: {
                        min: 0,
                        max: 1500
                    },
                    minTime: 0,
                    maxTime: 1500,
                    disabled: false
                };

                $scope.arrivalLegsTimeRangeSlider[j] = {
                    minTime: 0,
                    maxTime: 10000,
                    minMouseDown: false,
                    maxMouseDown: false
                };

                $scope.arrivalLegsTimeRange[j].minTime = $scope.serverFilters.min_time_arrival.value[j - 1].value;
                $scope.arrivalLegsTimeRange[j].range.min = $scope.arrivalLegsTimeRange[j].minTime;
                $scope.arrivalLegsTimeRangeSlider[j].minTime = $scope.arrivalLegsTimeRange[j].minTime;

                $scope.arrivalLegsTimeRange[j].maxTime = $scope.serverFilters.max_time_arrival.value[j - 1].value;
                $scope.arrivalLegsTimeRange[j].range.max = $scope.arrivalLegsTimeRange[j].maxTime;
                $scope.arrivalLegsTimeRangeSlider[j].maxTime = $scope.arrivalLegsTimeRange[j].maxTime;
            }


        }
        $scope.goBackToMultipleFlights = function() {
            localStorageService.set("reloadUserInput", "true");
            $location.path("flight/multipleFlights");
        }
        $scope.seeChosenFlightsButton = function() {
            var theButton = jQueryUse("#seeChosenFlightsMdButton");
            if (theButton.text() === "See Chosen Flights") {
                theButton.text("See All Flights");
            } else {
                theButton.text("See Chosen Flights");
            }
            $scope.showChosenFlights = !$scope.showChosenFlights;
        };

        function setArrivalLegsHrRange(flights) {
            $scope.legsArrivalHrArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsArrivalHrArrays[index + 1]) $scope.legsArrivalHrArrays[index + 1] = [];
                    var lastDirectFlightOfLeg = leg.direct_flights[leg.direct_flights.length - 1];
                    var hr = '2016-08-21 ' + lastDirectFlightOfLeg.arrival_time;
                    $scope.legsArrivalHrArrays[index + 1].push(Date.parse(hr));
                });
            });
            $scope.arrivalLegsHrRange = {};
            for (var key in $scope.legsArrivalHrArrays) {
                if ($scope.legsArrivalHrArrays.hasOwnProperty(key)) {
                    $scope.arrivalLegsHrRange[key] = {
                        range: {
                            min: 0,
                            max: 1500
                        },
                        minTime: 0,
                        maxTime: 1500
                    };

                    $scope.legsArrivalHrArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.arrivalLegsHrRange[key].maxTime = 1440;
                    $scope.arrivalLegsHrRange[key].minTime = 0;
                    $scope.arrivalLegsHrRange[key].range.max = 1440;
                    $scope.arrivalLegsHrRange[key].range.min = 0;
                    $scope.arrivalLegsHrRange[key].disabled = false;
                }
            }
        }

        function setDepartureLegsHrRange(flights) {
            $scope.legsDepartureHrArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsDepartureHrArrays[index + 1]) $scope.legsDepartureHrArrays[index + 1] = [];
                    var firstDirectFlightOfLeg = leg.direct_flights[0];
                    var hr = '2016-08-21 ' + firstDirectFlightOfLeg.departure_time;
                    $scope.legsDepartureHrArrays[index + 1].push(Date.parse(hr));
                });
            });
            $scope.departureLegsHrRange = {};

            for (var key in $scope.legsDepartureHrArrays) {
                if ($scope.legsDepartureHrArrays.hasOwnProperty(key)) {
                    $scope.departureLegsHrRange[key] = {
                        range: {
                            min: 0,
                            max: 1500
                        },
                        minTime: 0,
                        maxTime: 1500
                    };

                    $scope.legsDepartureHrArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.departureLegsHrRange[key].maxTime = 1440;
                    $scope.departureLegsHrRange[key].minTime = 0;
                    $scope.departureLegsHrRange[key].range.max = 1440;
                    $scope.departureLegsHrRange[key].range.min = 0;
                    $scope.departureLegsHrRange[key].disabled = false;
                }
            }
        }

        function setLegsStopsArray(flights) {
            $scope.legsStopsArrays = {};

            $scope.serverFilters.stops_range.value.forEach(function(theValue) {
                $scope.legsStopsArrays[theValue.leg_index] = [];
                //   $scope.legsStopsArrays["disabled" + theValue.leg_index] = true;
                for (var i = 0; i < theValue.value.length; i++) {
                    var stopsNum = {
                        num: theValue.value[i],
                        isChecked: true
                    };
                    $scope.legsStopsArrays[theValue.leg_index].push(stopsNum);
                }
            });

            // OWV #11
        }

        function setDepartureLegsTimeRange(flights) {
            for (var i = 1; i <= amountOfLegs; i++) {
                $scope.serverFilters.min_time_departure.value[i - 1].value = $scope.serverFilters.min_time_departure.value[i - 1].value;
                $scope.serverFilters.max_time_departure.value[i - 1].value = $scope.serverFilters.max_time_departure.value[i - 1].value;
            }

            $scope.departureLegsTimeRange = {};
            $scope.departureLegsTimeRangeSlider = {};

            for (var j = 1; j <= amountOfLegs; j++) {
                $scope.departureLegsTimeRange[j] = {
                    range: {
                        min: 0,
                        max: 1500
                    },
                    minTime: 0,
                    maxTime: 1500,
                    disabled: false
                };

                $scope.departureLegsTimeRangeSlider[j] = {
                    minTime: 0,
                    maxTime: 10000,
                    minMouseDown: false,
                    maxMouseDown: false
                };

                $scope.departureLegsTimeRange[j].minTime = $scope.serverFilters.min_time_departure.value[j - 1].value;
                $scope.departureLegsTimeRange[j].range.min = $scope.departureLegsTimeRange[j].minTime;
                $scope.departureLegsTimeRangeSlider[j].minTime = $scope.departureLegsTimeRange[j].minTime;

                $scope.departureLegsTimeRange[j].maxTime = $scope.serverFilters.max_time_departure.value[j - 1].value;
                $scope.departureLegsTimeRange[j].range.max = $scope.departureLegsTimeRange[j].maxTime;
                $scope.departureLegsTimeRangeSlider[j].maxTime = $scope.departureLegsTimeRange[j].maxTime;
            }


        }

        function setLegsDuration(flights) {
            $scope.legsDurations = {};
            $scope.legDurationsSlider = {};
            for (var k = 1; k <= amountOfLegs; k++) {
                $scope.legsDurations[k] = {
                    range: {
                        min: 0,
                        max: 3000
                    },
                    minDuration: 0,
                    maxDuration: 3000,
                    disabled: false
                };

                $scope.legDurationsSlider[k] = {
                    minDuration: 0,
                    maxDuration: 10000,
                    minMouseDown: false,
                    maxMouseDown: false
                };

                $scope.legsDurations[k].minDuration = $scope.serverFilters.min_flight_drive_duration_leg.value[k - 1].value - 1;
                $scope.legsDurations[k].range.min = $scope.legsDurations[k].minDuration;
                $scope.legDurationsSlider[k].minDuration = $scope.legsDurations[k].minDuration;
                $scope.legsDurations[k].maxDuration = $scope.serverFilters.max_flight_drive_duration_leg.value[k - 1].value + 1;
                $scope.legDurationsSlider[k].maxDuration = $scope.legsDurations[k].maxDuration;
                $scope.legsDurations[k].range.max = $scope.legsDurations[k].maxDuration;
            }


        }

        function setLegsDepartureDriveDuration(flights) {
            /*
            $scope.legsDepartureDriveDurations = {};
            for(var j = 1; j <= amountOfLegs; j++) {
                $scope.legsDepartureDriveDurations[j] = {
                    range: {
                        min: 0,
                        max: 3000
                    },
                    minDuration: 0,
                    maxDuration: 3000,
                    disabled:    false
                };

                $scope.legsDepartureDriveArrays[j].sort(function (a, b) {
                    return a - b
                });
                $scope.legsDepartureDriveDurations[j].minDuration = $scope.legsDepartureDriveArrays[j][0] - 1;
                $scope.legsDepartureDriveDurations[j].range.min = $scope.legsDepartureDriveArrays[j][0] - 1;
                $scope.legsDepartureDriveDurations[j].maxDuration = $scope.legsDepartureDriveArrays[j][$scope.legsDepartureDriveArrays[j].length - 1] + 1;

                $scope.legsDepartureDriveDurations[j].range.max = $scope.legsDepartureDriveArrays[j][$scope.legsDepartureDriveArrays[j].length - 1] + 1;
            }
            */

            $scope.legsDepartureDriveArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsDepartureDriveArrays[index + 1]) $scope.legsDepartureDriveArrays[index + 1] = [];
                    $scope.legsDepartureDriveArrays[index + 1].push(leg.origin.drive_time);
                });
            });

            $scope.legsDepartureDriveDurations = {};
            $scope.departureLegsDriveTimeSlider = {};
            for (var key in $scope.legsDepartureDriveArrays) {
                if ($scope.legsDepartureDriveArrays.hasOwnProperty(key)) {
                    $scope.legsDepartureDriveDurations[key] = {
                        range: {
                            min: 0,
                            max: 3000
                        },
                        minDuration: 0,
                        maxDuration: 3000,
                        disabled: false
                    };

                    $scope.departureLegsDriveTimeSlider[key] = {
                        minDuration: 0,
                        maxDuration: 10000,
                        minMouseDown: false,
                        maxMouseDown: false
                    };

                    $scope.legsDepartureDriveArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.legsDepartureDriveDurations[key].minDuration = $scope.legsDepartureDriveArrays[key][0] - 1;
                    $scope.legsDepartureDriveDurations[key].range.min = $scope.legsDepartureDriveDurations[key].minDuration;
                    $scope.departureLegsDriveTimeSlider[key].minDuration = $scope.legsDepartureDriveDurations[key].minDuration;


                    $scope.legsDepartureDriveDurations[key].maxDuration = $scope.legsDepartureDriveArrays[key][$scope.legsDepartureDriveArrays[key].length - 1] + 1;
                    $scope.legsDepartureDriveDurations[key].range.max = $scope.legsDepartureDriveDurations[key].maxDuration;
                    $scope.departureLegsDriveTimeSlider[key].maxDuration = $scope.legsDepartureDriveDurations[key].maxDuration;
                }
            }


        }

        function setLegsDepartureDriveDurationWithTraffic(flights) {
            $scope.legsDepartureDriveTrafficArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsDepartureDriveTrafficArrays[index + 1]) $scope.legsDepartureDriveTrafficArrays[index + 1] = [];
                    $scope.legsDepartureDriveTrafficArrays[index + 1].push(leg.origin.drive_time_traf);
                });
            });

            $scope.legsDepartureDriveTrafficDurations = {};
            $scope.departureLegsDriveTrafficTimeSlider = {};
            for (var key in $scope.legsDepartureDriveTrafficArrays) {
                if ($scope.legsDepartureDriveTrafficArrays.hasOwnProperty(key)) {
                    $scope.legsDepartureDriveTrafficDurations[key] = {
                        range: {
                            min: 0,
                            max: 3000
                        },
                        minDuration: 0,
                        maxDuration: 3000,
                        disabled: false
                    };

                    $scope.departureLegsDriveTrafficTimeSlider[key] = {
                        minDuration: 0,
                        maxDuration: 10000,
                        minMouseDown: false,
                        maxMouseDown: false
                    };

                    $scope.legsDepartureDriveTrafficArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.legsDepartureDriveTrafficDurations[key].minDuration = $scope.legsDepartureDriveTrafficArrays[key][0] - 1;
                    $scope.legsDepartureDriveTrafficDurations[key].range.min = $scope.legsDepartureDriveTrafficDurations[key].minDuration;
                    $scope.departureLegsDriveTrafficTimeSlider[key].minDuration = $scope.legsDepartureDriveTrafficDurations[key].minDuration;


                    $scope.legsDepartureDriveTrafficDurations[key].maxDuration = $scope.legsDepartureDriveTrafficArrays[key][$scope.legsDepartureDriveTrafficArrays[key].length - 1] + 1;
                    $scope.legsDepartureDriveTrafficDurations[key].range.max = $scope.legsDepartureDriveTrafficDurations[key].maxDuration;
                    $scope.departureLegsDriveTrafficTimeSlider[key].maxDuration = $scope.legsDepartureDriveTrafficDurations[key].maxDuration;
                }
            }


        }

        function setLegsArrivalDriveDuration(flights) {
            $scope.legsArrivalDriveArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsArrivalDriveArrays[index + 1]) $scope.legsArrivalDriveArrays[index + 1] = [];
                    $scope.legsArrivalDriveArrays[index + 1].push(leg.destination.drive_time);
                });
            });

            $scope.legsArrivalDriveDurations = {};
            $scope.arrivalLegsDriveTimeSlider = {};
            for (var key in $scope.legsArrivalDriveArrays) {
                if ($scope.legsArrivalDriveArrays.hasOwnProperty(key)) {
                    $scope.legsArrivalDriveDurations[key] = {
                        range: {
                            min: 0,
                            max: 3000
                        },
                        minDuration: 0,
                        maxDuration: 3000,
                        disabled: false
                    };

                    $scope.arrivalLegsDriveTimeSlider[key] = {
                        minDuration: 0,
                        maxDuration: 10000,
                        minMouseDown: false,
                        maxMouseDown: false
                    };

                    $scope.legsArrivalDriveArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.legsArrivalDriveDurations[key].minDuration = $scope.legsArrivalDriveArrays[key][0] - 1;
                    $scope.legsArrivalDriveDurations[key].range.min = $scope.legsArrivalDriveDurations[key].minDuration;
                    $scope.arrivalLegsDriveTimeSlider[key].minDuration = $scope.legsArrivalDriveDurations[key].minDuration;


                    $scope.legsArrivalDriveDurations[key].maxDuration = $scope.legsArrivalDriveArrays[key][$scope.legsArrivalDriveArrays[key].length - 1] + 1;
                    $scope.legsArrivalDriveDurations[key].range.max = $scope.legsArrivalDriveDurations[key].maxDuration;
                    $scope.arrivalLegsDriveTimeSlider[key].maxDuration = $scope.legsArrivalDriveDurations[key].maxDuration;
                }
            }



        }

        function setLegsArrivalDriveTrafficDuration(flights) {
            $scope.legsArrivalDriveTrafficArrays = {};
            flights.forEach(function(flight) {
                flight.legs.forEach(function(leg, index) {
                    if (!$scope.legsArrivalDriveTrafficArrays[index + 1]) $scope.legsArrivalDriveTrafficArrays[index + 1] = [];
                    $scope.legsArrivalDriveTrafficArrays[index + 1].push(leg.destination.drive_time_traf);
                });
            });

            $scope.legsArrivalDriveTrafficDurations = {};
            $scope.arrivalLegsDriveTrafficTimeSlider = {};
            for (var key in $scope.legsArrivalDriveTrafficArrays) {
                if ($scope.legsArrivalDriveTrafficArrays.hasOwnProperty(key)) {
                    $scope.legsArrivalDriveTrafficDurations[key] = {
                        range: {
                            min: 0,
                            max: 3000
                        },
                        minDuration: 0,
                        maxDuration: 3000,
                        disabled: false
                    };

                    $scope.arrivalLegsDriveTrafficTimeSlider[key] = {
                        minDuration: 0,
                        maxDuration: 10000,
                        minMouseDown: false,
                        maxMouseDown: false
                    };

                    $scope.legsArrivalDriveTrafficArrays[key].sort(function(a, b) {
                        return a - b
                    });

                    $scope.legsArrivalDriveTrafficDurations[key].minDuration = $scope.legsArrivalDriveTrafficArrays[key][0] - 1;
                    $scope.legsArrivalDriveTrafficDurations[key].range.min = $scope.legsArrivalDriveTrafficDurations[key].minDuration;
                    $scope.arrivalLegsDriveTrafficTimeSlider[key].minDuration = $scope.legsArrivalDriveTrafficDurations[key].minDuration;


                    $scope.legsArrivalDriveTrafficDurations[key].maxDuration = $scope.legsArrivalDriveTrafficArrays[key][$scope.legsArrivalDriveTrafficArrays[key].length - 1] + 1;
                    $scope.legsArrivalDriveTrafficDurations[key].range.max = $scope.legsArrivalDriveTrafficDurations[key].maxDuration;
                    $scope.arrivalLegsDriveTrafficTimeSlider[key].maxDuration = $scope.legsArrivalDriveTrafficDurations[key].maxDuration;
                }
            }

        }

        // set object contains legs numbers arrays that contains objects with name of airport and if checked or not /tal
        function setDepartureAirports(flights) {
            $scope.departureAirportsArrays = {};

            $scope.serverFilters.airport_departure.value.forEach(function(theValue) {

                let codeDict = getAirportCodeDict(flights);
                $scope.departureAirportsArrays[theValue.leg_index] = [];
                theValue.value.forEach(function(airportNameValue) {
                    $scope.departureAirportsArrays[theValue.leg_index].push({
                        airportName: airportNameValue,
                        isChecked: true,
                        airportCode: codeDict.get(airportNameValue)
                    });
                });
            });

            // OWV #8
        }

        function setDepartureDates(flights) {
            $scope.departureDatesArrays = {};
            $scope.serverFilters.date_departure.value.forEach(function(theValue) {
                $scope.departureDatesArrays[theValue.leg_index] = [];
                theValue.value.reverse();
                theValue.value.forEach(function(dateToAdd) {
                    $scope.departureDatesArrays[theValue.leg_index].push({
                        date: dateToAdd,
                        Selected: true
                    });

                    $scope.departureDatesArrays[theValue.leg_index].sort(function(a, b) {
                        var AYear = parseInt(a.date.substring(0, 4));
                        var BYear = parseInt(b.date.substring(0, 4));
                        var AMonth = parseInt(a.date.substring(5, 7));
                        var BMonth = parseInt(b.date.substring(5, 7));
                        var ADay = parseInt(a.date.substring(8, 10));
                        var BDay = parseInt(b.date.substring(8, 10));
                        if (AYear < BYear) {
                            return -1;
                        } else {
                            if (BYear < AYear) {
                                return 1;
                            } else {
                                if (AMonth < BMonth) {
                                    return -1;
                                } else {
                                    if (AMonth > BMonth) {
                                        return 1;
                                    } else {
                                        if (ADay < BDay) {
                                            return -1;
                                        } else {
                                            return 1;
                                        }
                                    }
                                }
                            }
                        }
                    });

                });
            });

            // OWV #7

        }

        function setArrivalDates(flights) {
            $scope.arrivalDatesArrays = {};
            $scope.serverFilters.date_arrival.value.forEach(function(theValue) {
                $scope.arrivalDatesArrays[theValue.leg_index] = [];
                theValue.value.reverse();
                theValue.value.forEach(function(dateToAdd) {
                    $scope.arrivalDatesArrays[theValue.leg_index].push({
                        date: dateToAdd,
                        Selected: true
                    });
                    $scope.arrivalDatesArrays[theValue.leg_index].sort(function(a, b) {
                        var AYear = parseInt(a.date.substring(0, 4));
                        var BYear = parseInt(b.date.substring(0, 4));
                        var AMonth = parseInt(a.date.substring(5, 7));
                        var BMonth = parseInt(b.date.substring(5, 7));
                        var ADay = parseInt(a.date.substring(8, 10));
                        var BDay = parseInt(b.date.substring(8, 10));
                        if (AYear < BYear) {
                            return -1;
                        } else {
                            if (BYear < AYear) {
                                return 1;
                            } else {
                                if (AMonth < BMonth) {
                                    return -1;
                                } else {
                                    if (AMonth > BMonth) {
                                        return 1;
                                    } else {
                                        if (ADay < BDay) {
                                            return -1;
                                        } else {
                                            return 1;
                                        }
                                    }
                                }
                            }
                        }
                    });
                });
            });

            // OWV #6
        }

        function setArrivalAirports(flights) {
            $scope.arrivalAirportsArrays = {};
            let codeDict = getAirportCodeDict(flights);
            $scope.serverFilters.airport_arrival.value.forEach(function(theValue) {
                $scope.arrivalAirportsArrays[theValue.leg_index] = [];

                for (let airportNameValue of theValue.value) {


                    $scope.arrivalAirportsArrays[theValue.leg_index]
                        .push({
                            airportName: airportNameValue,
                            isChecked: true,
                            airportCode: codeDict.get(airportNameValue)
                        })
                }




            });
        }

        // OWV #5


        function setConnectionAirports(flights) {

            // Alex : Building dictionary(Map object) airport code to airport name
            let codeDict = getAirportCodeDict(flights);

            $scope.connectionAirportsArrays = {};
            $scope.serverFilters.airport_connection.value.forEach(function(theValue) {
                $scope.connectionAirportsArrays[theValue.leg_index] = [];
                theValue.value.forEach(function(airportNameValue) {
                    $scope.connectionAirportsArrays[theValue.leg_index].push({
                        airportName: airportNameValue,
                        isChecked: true,
                        airportCode: codeDict.get(airportNameValue)
                    });
                });
            });
        }

        let codeDictionary;

        function getAirportCodeDict(flights) {
            if (codeDictionary == null) {
                codeDictionary = new Map();
            } else {
                return codeDictionary;
            }

            for (let flight of flights) {
                for (let leg of flight.legs) {
                    for (let directFlight of leg.direct_flights) {
                        codeDictionary.set(directFlight.arrival_airport, directFlight.arrival_airport_code);
                        codeDictionary.set(directFlight.departure_airport, directFlight.departure_airport_code);
                    }
                }
            }
            return codeDictionary;
        }

        function setAirlinesLegsArrays(flights) {
            $scope.airlinesArray = [];
            $scope.serverFilters.pos_airlines.value.forEach(function(theValue) {
                $scope.airlinesArray.push({
                    name: theValue,
                    Selected: true
                });
            });

            // OWV #4
        }

        $scope.getMinMax = function(flights) {
            setDepartureLegsHrRange(flights);
            setArrivalLegsHrRange(flights);
            setArrivalDates(flights);
            setDepartureDates(flights);
            setLegsDepartureDriveDuration(flights);
            setLegsArrivalDriveDuration(flights);
            setLegsStopsArray(flights);
            setAirlinesLegsArrays(flights);
            setArrivalAirports(flights);
            setDepartureAirports(flights);
            setConnectionAirports(flights);
            setArrivalLegsTimeRange(flights);
            setDepartureLegsTimeRange(flights);
            setLegsDuration(flights);
            setLegsDepartureDriveDurationWithTraffic(flights);
            setLegsArrivalDriveTrafficDuration(flights);
            /*
              flights.sort(compareDepartureTime);
              $scope.departureTimeRange.range.max = departureToTimestamp(flights[flights.length - 1]) + 1;
              $scope.departureTimeRange.maxTime = departureToTimestamp(flights[flights.length - 1]) + 1;
              $scope.departureTimeRange.range.min = departureToTimestamp(flights[0]) - 1;
              $scope.departureTimeRange.minTime = departureToTimestamp(flights[0]) - 1;
              flights.sort(compareArrivalTime);
              $scope.arrivalTimeRange.range.max = arrivalToTimestamp(flights[flights.length-1])+1;
              $scope.arrivalTimeRange.maxTime = arrivalToTimestamp(flights[flights.length-1])+1;
              $scope.arrivalTimeRange.range.min = arrivalToTimestamp(flights[0])-1;
              $scope.arrivalTimeRange.minTime = arrivalToTimestamp(flights[0])-1;
            */

            $scope.flightDriveDurationRange.range.min = $scope.serverFilters.min_flight_drive_duration.value - 1;
            $scope.flightDriveDurationRange.range.max = $scope.serverFilters.max_flight_drive_duration.value + 1;
            $scope.flightDriveDurationRange.minDuration = $scope.flightDriveDurationRange.range.min;
            $scope.flightDriveDurationRange.maxDuration = $scope.flightDriveDurationRange.range.max;
            // OWV #3

            $scope.flightDriveTrafficDurationRange.range.min = $scope.serverFilters.min_flight_drive_traffic_duration.value - 1;
            $scope.flightDriveTrafficDurationRange.range.max = $scope.serverFilters.max_flight_drive_traffic_duration.value + 1;
            $scope.flightDriveTrafficDurationRange.minDuration = $scope.flightDriveTrafficDurationRange.range.min;
            $scope.flightDriveTrafficDurationRange.maxDuration = $scope.flightDriveTrafficDurationRange.range.max;

            $scope.flightDurationRange.range.min = $scope.serverFilters.min_flight_duration.value - 1;
            $scope.flightDurationRange.range.max = $scope.serverFilters.max_flight_duration.value + 1;
            $scope.flightDurationRange.minDuration = $scope.flightDurationRange.range.min;
            $scope.flightDurationRange.maxDuration = $scope.flightDurationRange.range.max;
            // OWV #2

            $scope.priceRange.range.min = parseInt($scope.serverFilters.min_price.value) - 1;
            $scope.priceRange.range.max = parseInt($scope.serverFilters.max_price.value) + 1;
            $scope.priceRange.minPrice = $scope.priceRange.range.min;
            $scope.priceRange.maxPrice = $scope.priceRange.range.max;
            // OWV - #1

            $scope.minPriceMouseup = $scope.priceRange.range.min;
            $scope.maxPriceMouseup = $scope.priceRange.range.max;
            $scope.flightDurationSlider.minDuration = $scope.flightDurationRange.minDuration;
            $scope.flightDurationSlider.maxDuration = $scope.flightDurationRange.maxDuration;
            $scope.flightDriveDurationSlider.minDuration = $scope.flightDriveDurationRange.minDuration;
            $scope.flightDriveDurationSlider.maxDuration = $scope.flightDriveDurationRange.maxDuration;
            $scope.flightDriveTrafficDurationSlider.minDuration = $scope.flightDriveTrafficDurationRange.minDuration;
            $scope.flightDriveTrafficDurationSlider.maxDuration = $scope.flightDriveTrafficDurationRange.maxDuration;
        };



        $scope.convertMinutesToTimeString = minutes => {
            return commonHelpers.convertMinutesToHoursString(minutes);
        };



        $scope.showOnlyBest = function() {
            $scope.getMinMax($scope.bestFlights);
            $scope.showBest = true;
            $scope.showCheapest = false;
            $scope.showShortest = false;
        };

        $scope.showOnlyShortest = function() {
            $scope.getMinMax($scope.shortestFlights);
            $scope.showShortest = true;
            $scope.showBest = false;
            $scope.showCheapest = false;
        };

        $scope.showOnlyCheap = function() {
            $scope.getMinMax($scope.cheapestFlights);
            $scope.showCheapest = true;
            $scope.showBest = false;
            $scope.showShortest = false;
        };

        $scope.$watch('chosenFile', function() {
            $scope.getTBSResults(true)
        });


        $scope.getTBSResults = function(resetFilters) {
            $scope.loading = true;
            var index;
            if ($scope.chosenFile === "file1") {
                index = 0;
            } else {
                if ($scope.chosenFile === "from_server") {
                    index = 1;
                } else {
                    index = 2;
                }
            }

            $http({
                method: 'GET',
                url: $scope.filePath[index],


            }).then(function successCallback(response) {

                //                var enc = new TextEncoder("utf-8");
                //                 let  charData = enc.encode(response.data);
                //
                //                // Turn number array into byte-array
                //                var binData     = new Uint8Array(charData);

                // Pako magic
                //let data = lmzDecompress(response.data);
                //response.data = JSON.parse(data);
                // Convert gunzipped byteArray back to ascii string:
                //  var strData     = String.fromCharCode.apply(null, new Uint16Array(data));
                $scope.allFlights = response.data;
                $scope.filteredData = response.data.all;
                $scope.total_results = response.data.filters.total_results;
                $scope.total_filtered_results = response.data.filters.total_filtered_results;
                $scope.serverFilters = response.data.filters.server_all;
                $scope.clientFilters = response.data.filters.client_all;
                //$scope.displayLimitSelect = String (response.data.filters.limit);
                console.log("serverFilters!");
                console.log($scope.serverFilters);
                amountOfLegs = $scope.serverFilters.min_time_arrival.value.length;

                $scope.cheapestFlights = response.data.cheapest;
                $scope.bestFlights = response.data.all;
                //  $scope.allFilteredFlights = $scope.bestFlights;
                console.log("bestFlights");
                console.log($scope.bestFlights);
                $scope.flightsFilteredByMinPrice = response.data.all;
                $scope.shortestFlights = response.data.shortest;
                /*	for(var i = 0 ;  i < $scope.cheapestLength ; i++){
                 $scope.cheapestFlights.push( $scope.obj.cheapest[i]);
                 }

                 for( var i = 0 ;  i < $scope.bestLength ; i++){
                 $scope.bestFlights.push( $scope.obj.best[i]);
                 }

                 for( var i = 0 ;  i < $scope.shortestLength ; i++){
                 $scope.shortestFlights.push( $scope.obj.shortest[i]);
                 }*/
                if (resetFilters) {
                    if ($scope.showCheapest) {
                        $scope.getMinMax($scope.cheapestFlights);
                    } else {
                        if ($scope.showBest) {
                            $scope.getMinMax($scope.bestFlights);
                        } else {
                            if ($scope.showShortest) {
                                $scope.getMinMax($scope.shortestFlights);
                            }
                        }
                    }
                }
                $scope.loading = false;
                //$scope.updateFilterByClientValues();
            }, function errorCallback(response) {
                console.log(response);
            });
        }

        function lmzDecompress(compressed) {
            "use strict";
            // Build the dictionary.
            var i,
                dictionary = [],
                w,
                result,
                k,
                entry = "",
                dictSize = 256;
            for (i = 0; i < 256; i += 1) {
                dictionary[i] = String.fromCharCode(i);
            }

            w = String.fromCharCode(compressed[0]);
            result = w;
            for (i = 1; i < compressed.length; i += 1) {
                k = compressed[i];
                if (dictionary[k]) {
                    entry = dictionary[k];
                } else {
                    if (k === dictSize) {
                        entry = w + w.charAt(0);
                    } else {
                        return null;
                    }
                }

                result += entry;

                // Add w+entry[0] to the dictionary.
                dictionary[dictSize++] = w + entry.charAt(0);

                w = entry;
            }
            return result;
        }


        $scope.updateAirLanesViewByClientValues = function() {

            for (let currValIndex in $scope.airlinesArray) {
                let currVal = $scope.airlinesArray[currValIndex];
                if ($scope.clientFilters.pos_airlines.value.includes(currVal.name)) {
                    currVal.Selected = true;
                } else {
                    currVal.Selected = false;
                }
            }
        }



        $scope.updatePriceViewByClientValues = function() {

            $scope.priceRange.minPrice = $scope.clientFilters.min_price.value;
            $scope.priceRange.maxPrice = $scope.clientFilters.max_price.value;


        }
        $scope.updateFilterByValues = function(viewData, data) {
            for (let currentLegIndex in viewData) {
                let clientAirports = $scope.getLinqValueFromLeg(currentLegIndex, data)


                for (let currValIndex in viewData[currentLegIndex]) {

                    let currVal = viewData[currentLegIndex][currValIndex];


                    if (clientAirports.includes(currVal.airportName)) {
                        currVal.isChecked = true;
                    } else {
                        currVal.isChecked = false;
                    }
                }
            }

        }




        $scope.updateFilterByClientValues = function() {



            $scope.updateAirLanesViewByClientValues();
            $scope.updatePriceViewByClientValues();

            $scope.updateFilterByValues($scope.departureAirportsArrays,
                $scope.clientFilters.airport_departure.value);
            $scope.updateFilterByValues($scope.connectionAirportsArrays,
                $scope.clientFilters.airport_connection.value);

            $scope.updateFilterByValues($scope.arrivalAirportsArrays,
                $scope.clientFilters.airport_arrival.value);

            $scope.updateFilterByValues($scope.departureDatesArrays,
                $scope.clientFilters.date_departure.value);

            $scope.updateFilterByValues($scope.arrivalDatesArrays,
                $scope.clientFilters.date_arrival.value);

            $scope.setSlidersValue($scope.flightDurationRange,
                $scope.clientFilters.min_flight_duration.value,
                $scope.clientFilters.max_flight_duration.value);

            $scope.setSlidersValue($scope.flightDriveDurationRange,
                $scope.clientFilters.min_flight_drive_duration.value,
                $scope.clientFilters.max_flight_drive_duration.value);

            $scope.setSlidersValue($scope.flightDriveTrafficDurationRange,
                $scope.clientFilters.min_flight_drive_traffic_duration.value,
                $scope.clientFilters.max_flight_drive_traffic_duration.value);

            $scope.updateLegsDuration();
            $scope.updateRangeDurations($scope.arrivalLegsTimeRange, $scope.clientFilters.min_time_arrival,
                $scope.clientFilters.max_time_arrival)

            $scope.updateRangeDurations($scope.departureLegsTimeRange, $scope.clientFilters.min_time_departure,
                $scope.clientFilters.max_time_departure);


        };
        $scope.updateRangeDurations = function(durationData, minObj, maxObj) {

            for (let currentLegIndex in durationData) {
                let currentDuration = durationData[currentLegIndex];
                let min = $scope.getLinqValueFromLeg(currentLegIndex, minObj.value);
                let max = $scope.getLinqValueFromLeg(currentLegIndex, maxObj.value);
                currentDuration.minTime = min * 1000;
                currentDuration.maxTime = max * 1000;
            }

        }
        $scope.getLinqValueFromLeg = function(legIndex, data) {
            legIndex = Number(legIndex);

            let value = data.filter(function(el) {
                return el.leg_index === legIndex
            })[0].value

            return value;


        }
        $scope.updateLegsDuration = function() {




            $scope.clientFilters.min_flight_duration_leg


            for (let currentLegIndex in $scope.legsDurations) {
                let currentLegDuration = $scope.legsDurations[currentLegIndex];
                let min = $scope.getLinqValueFromLeg(currentLegIndex, $scope.clientFilters.min_flight_duration_leg.value);
                let max = $scope.getLinqValueFromLeg(currentLegIndex, $scope.clientFilters.max_flight_duration_leg.value);
                currentLegDuration.minDuration = min;
                currentLegDuration.maxDuration = max;
            }

        }
        $scope.setSlidersValue = function(viewData, min, max) {

            viewData.minDuration = min;
            viewData.maxDuration = max;



        }
        $scope.updateHotelsByPrice = function() {
            $scope.updatedMinPrice = $scope.priceRange.minPrice;
            $scope.updatedMaxPrice = $scope.priceRange.maxPrice;
        };
        // ******************* START: Filter Enable / Disable functions  **********************

        $scope.enablePriceFilter = function() {

            if ($scope.priceRange.disabled) {
                $scope.minPriceMouseup = $scope.priceRange.range.min;
                $scope.maxPriceMouseup = $scope.priceRange.range.max;


                $scope.$apply();


            } else {

                $scope.minPriceMouseup = $scope.priceRange.minPrice;
                $scope.maxPriceMouseup = $scope.priceRange.maxPrice;


                $scope.$apply();
            }
        };
        $scope.enableFlightAndDriveDurationFilter = function() {
            if ($scope.flightDriveDurationRange.disabled) {
                $scope.flightDriveDurationSlider.minDuration = $scope.flightDriveDurationRange.range.min;
                $scope.flightDriveDurationSlider.maxDuration = $scope.flightDriveDurationRange.range.max;


                $scope.$apply();


            } else {
                // bind variable-------- control property
                $scope.flightDriveDurationSlider.minDuration = $scope.flightDriveDurationRange.minDuration;
                $scope.flightDriveDurationSlider.maxDuration = $scope.flightDriveDurationRange.maxDuration;


                $scope.$apply();
            }

        };

        $scope.enableDurationFilter = function() {

            if ($scope.flightDurationRange.disabled) {
                $scope.flightDurationSlider.minDuration = $scope.flightDurationRange.range.min;
                $scope.flightDurationSlider.maxDuration = $scope.flightDurationRange.range.max;


                $scope.$apply();


            } else {
                // bind variable-------- control property
                $scope.flightDurationSlider.minDuration = $scope.flightDurationRange.minDuration;
                $scope.flightDurationSlider.maxDuration = $scope.flightDurationRange.maxDuration;


                $scope.$apply();
            }
        };

        $scope.enableFlightDriveTrafficDurationRangeFilter = function() {

            if ($scope.flightDriveTrafficDurationRange.disabled) {
                $scope.flightDriveTrafficDurationSlider.minDuration = $scope.flightDriveTrafficDurationRange.range.min;
                $scope.flightDriveTrafficDurationSlider.maxDuration = $scope.flightDriveTrafficDurationRange.range.max;


                $scope.$apply();


            } else {
                // bind variable-------- control property
                $scope.flightDriveTrafficDurationSlider.minDuration = $scope.flightDriveTrafficDurationRange.minDuration;
                $scope.flightDriveTrafficDurationSlider.maxDuration = $scope.flightDriveTrafficDurationRange.maxDuration;


                $scope.$apply();
            }

        };

        $scope.enableFilter = function(slider, key, value, time) {
            var minKey = 'minDuration';
            var maxKey = 'maxDuration';
            if (time == true) {
                minKey = 'minTime';
                maxKey = 'maxTime';
            }
            if (value.disabled) {
                $scope[slider][key].prevMin = $scope[slider][key][minKey];
                $scope[slider][key].PrevMax = $scope[slider][key][maxKey];
                $scope[slider][key][minKey] = value.range.min;
                $scope[slider][key][maxKey] = value.range.max;

            } else {



                $scope[slider][key][minKey] = $scope[slider][key].prevMin
                $scope[slider][key][maxKey] = $scope[slider][key].PrevMax


            }


        };



        $scope.enableAirlinesFilters = function() {
            if ($scope.legsAirLinesFilter.disabled) {
                // Save the last state
                $scope.legsAirLinesFilter.values = JSON.parse(JSON.stringify($scope.airlinesArray));
                $scope.selectedAll = true;
                $scope.checkAll();



            } else {
                $scope.airlinesArray = $scope.legsAirLinesFilter.values;
            }
        };

        $scope.enableConnectionAirportsFilter = function() {
            if ($scope.connectionAirportsFilter.disabled) {
                // Save the last state
                $scope.connectionAirportsFilter.values = JSON.parse(JSON.stringify($scope.connectionAirportsArrays));
                $scope.selectedAll2 = true;
                $scope.checkAll2();



            } else {
                $scope.connectionAirportsArrays = $scope.connectionAirportsFilter.values;
            }
        };


        $scope.enableCheckBoxFilters = function(arrayName, filterStatus) {
            if (filterStatus.disabled) {
                filterStatus.values = JSON.parse(JSON.stringify($scope[arrayName]));

                for (var legNum in $scope[arrayName]) {
                    for (var stopNum in $scope[arrayName][legNum]) {
                        $scope[arrayName][legNum][stopNum].isChecked = true;
                        $scope[arrayName][legNum][stopNum].Selected = true;
                    }
                }
            } else {
                $scope[arrayName] = filterStatus.values;
            }

        }
        // *******************  END: Filter Enable / Disable functions  **********************

        $scope.markChosenFlights = function() {
            if ($scope.chosenFlights !== null) {
                $scope.chosenFlights.forEach(function(chosenFlight) {
                    $scope.allFlights.all.forEach(function(flight) {
                        if (flight.id === chosenFlight.id) {
                            flight.selected = true;
                        }
                    })
                })
            }
        };

        function comparePrice(a, b) {
            if (parseInt(a.price) < parseInt(b.price)) return -1;
            else if (parseInt(a.price) > parseInt(b.price)) return 1;
            else return 0;
        }

        function compareFlightDriveDuration(a, b) {
            if ((a.calculations.total_drives_and_flights_time) < (b.calculations.total_drives_and_flights_time)) return -1;
            else if ((a.calculations.total_drives_and_flights_time) > (b.calculations.total_drives_and_flights_time)) return 1;
            else return 0;
        }

        function compareFlightDuration(a, b) {
            if ((a.calculations.total_flights_time) < (b.calculations.total_flights_time)) return -1;
            else if ((a.calculations.total_flights_time) > (b.calculations.total_flights_time)) return 1;
            else return 0;
        }

        $scope.checkAll = function() {
            //$scope.selectedAll = !$scope.selectedAll;
            $scope.airlinesArray.forEach(function(airline) {
                airline.Selected = $scope.selectedAll;
            });
        };

        $scope.checkAll2 = function() {
            // $scope.selectedAll2 = !$scope.selectedAll2;
            for (var i = 1; i <= amountOfLegs; i++) {
                $scope.connectionAirportsArrays[i].forEach(function(airportObj) {
                    airportObj.isChecked = $scope.selectedAll2;
                });
            }
        };

        // jQuery code below explained
        /*
        jQuery - Sliders
         var sliderLeft/Right - load the slider handles both left and right into variables.
         .mousedown(..) - setting mouse down to true for slider's handle
         .mouseup(..) - checks which handle was mousedown'd and filters accordingly
         */

    });



let clickedIsAirlinesDropDown = function(id) {
    let dropElement = jQueryUse("#" + id);
    let display = dropElement.css("display");

    if (display == 'none') {
        display = 'block';
    } else {
        display = 'none';
    }
    // ^ means xor, if opacity is 1  then 1^1 = 0, if opacity is 0 then 0^1 =1

    dropElement.css("display", display);


};