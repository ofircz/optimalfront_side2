'use strict';


app
.controller('MainController' , function( $scope,  ){

	// ----------------scope variables---------------------
	$scope.title = "Optimal Trip ";
    $scope.active = 0;
	$scope.myInterval = 4000;
	var slides = $scope.slides = [];
    var currIndex = 0;
	for (var i = 1; i < 4; i++) {
		slides.push({
			image: '/images/' + i + '.jpg',
			 id: currIndex++
		});
	}

	$scope.slide_headers = ['Plan your trip to perfection' , 'Stay at the best hotels' , 'Customize your flight experience'];
	$scope.slide_text = ['with our business plan you can do many great thing such as...' ,
	 'find the best suited hotels for your travel needs...' ,
	  'choose flights to meet your traveling needs...'];

	  slides.forEach(function(element, index){
	  	element.header = $scope.slide_headers[index];
	  	element.text = $scope.slide_text[index];	
	  });

	$scope.slides = slides;
	
});









































