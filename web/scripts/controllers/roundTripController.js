'use strict';


app
    .controller('roundTripController', function ($scope, httpService, localStorageService, $http) {
        //watch the input and copy it to the second form


        $scope.sendForm = function () {
            var form = {

                first_leg: {
                    origin: $scope.firstLeg.origin,
                    transport_origin: $scope.firstLeg.transportOrg,
                    destination: $scope.firstLeg.destination,
                    transport_destination: $scope.firstLeg.transportDes,
                    date: $scope.firstLeg.date,
                    arrive_depart: $scope.firstLeg.arriveDepart,
                    search_from: $scope.firstLeg.searchFrom,
                    search_to: $scope.firstLeg.searchTo,
                    preffered_from: $scope.firstLeg.prefferedFrom,
                    preffered_to: $scope.firstLeg.prefferedTo,
                    number_stops: $scope.firstLeg.numStop,
                    min_stops: $scope.firstLeg.MinStop,
                    max_stop: $scope.firstLeg.MaxStop,
                    header1: $scope.firstLeg.header1,
                    header2: $scope.firstLeg.header2,
                    header3: $scope.firstLeg.header3,
                    header4: $scope.firstLeg.header4,
                    header5: $scope.firstLeg.header5

                },

                second_leg: {
                    origin: $scope.secondLeg.origin,
                    transport_origin: $scope.secondLeg.transportOrg,
                    destination: $scope.secondLeg.destination,
                    transport_destination: $scope.secondLeg.transportDes,
                    date: $scope.secondLeg.date,
                    arrive_depart: $scope.secondLeg.arriveDepart,
                    search_from: $scope.secondLeg.searchFrom,
                    search_to: $scope.secondLeg.searchTo,
                    preffered_from: $scope.secondLeg.prefferedFrom,
                    preffered_to: $scope.secondLeg.prefferedTo,
                    number_stops: $scope.secondLeg.numStop,
                    min_stops: $scope.secondLeg.MinStop,
                    max_stop: $scope.secondLeg.MaxStop,
                    header1: $scope.secondLeg.header1,
                    header2: $scope.secondLeg.header2,
                    header3: $scope.secondLeg.header3,
                    header4: $scope.secondLeg.header4,
                    header5: $scope.secondLeg.header5
                },

                general: {
                    adults: $scope.roundTrip.adults,
                    kids: $scope.roundTrip.kids,
                    seniors: $scope.roundTrip.seniors,
                    class: $scope.roundTrip.class
                },

            }

            $http({
                method: 'POST',
                url: '/roundTripFlightForm',
                data: {form: JSON.stringify(form)}
            }).then(function successCallback(response) {
                console.log(response);
            }, function errorCallback(response) {
            });
        }


        $("#oneWay").css("background-color", "grey");
        $("#multiple").css("background-color", "grey");
        $("#roundTripFlight").css("background-color", "#207ce5");

        $scope.extra = false;
        $scope.extra1 = false;
        $scope.extra2 = false;

        $scope.showExtra = function () {

            $scope.extra = true;
        }
        $scope.showExtra1 = function () {

            $scope.extra1 = true;
        }
        $scope.showExtra2 = function () {

            $scope.extra2 = true;
        }
        $scope.hideExtra = function () {
            $scope.extra = false;
        }
        $scope.hideExtra1 = function () {
            $scope.extra1 = false;
        }
        $scope.hideExtra2 = function () {
            $scope.extra2 = false;
        }
        $scope.flights = 5;


    });









