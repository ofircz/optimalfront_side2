'use strict';


app
.controller('UserController' , function( $scope , orderService , $cookies ){


	$scope.show_ready = false;

	$scope.show_view = false;
	
	$scope.show_ready_menu = function(){
		$scope.show_ready = !$scope.show_ready;
	}

	$scope.show_view_plan = function(){
		$scope.show_view = !$scope.show_view;
	}

	$scope.print_cookie = function(){

		$scope.orderCookie = $cookies.getObject('order');

		console.log($scope.orderCookie);

	}

	$scope.order = orderService.order;



});