/**
 * Created by lironil on 12/11/2016.
 */

'use strict';

app.controller('displayBookingController', function ($scope, localStorageService, TBSService, $routeParams) {
    $scope.chosenHotels = [];
    $scope.chosenFlights = [];
    $scope.chosenHotelAllInformation = {};
    $scope.bookingDetails = "No booking details found";
    $scope.checkInURL = "No URL found";
    $scope.timeLimit = "No time limit found";
    $scope.timeStamp = "No timestamp found";
    $scope.bookingStatus = "No booking status found";
    $scope.confirmationNum = "No confirmation number found";
    $scope.resID = "No reservation ID found";
    $scope.passengerDisplayDetails = [
        'Title',
        'FirstName',
        'LastName',
        'Type',
        'BirthDate',
        'Email',
        'PaxID',
        'document_type',
        'document_number',
        'document_expiry_date',
        'document_issue_country',
        'document_nationality'
    ];

    $scope.passengerDisplayDetailsFixed = [
        'Title',
        'First Name',
        'Last Name',
        'Type',
        'Birth Date',
        'Email',
        'PaxID',
        'Doc Type',
        'Doc Number',
        'Expiry Date',
        'Issue Country',
        'Doc Nationality'
    ];

    $scope.init = function () {
        if(localStorageService.get('chosenHotels') !== null) {
            $scope.chosenHotelAllInformation = localStorageService.get('allHotelsAllInformation');
            $scope.chosenHotels = localStorageService.get('chosenHotels');
        }

        if (localStorageService.get('chosenFlight') !== null) {
            $scope.chosenFlights = localStorageService.get('chosenFlight');
        }


       if  (localStorageService.get('bookingDetails') === null){
           let bookingID = $routeParams.path;
           TBSService.displayBookings(bookingID, $scope.displayBookingsCallback);


       }
       else
       {
            $scope.displayBookingsCallback();
       }







    };
    $scope.displayBookingsCallback = function(data)
    {
     $scope.bookingDetails = localStorageService.get('bookingDetails') || data;
         if($scope.bookingDetails.data.bookings_data.length != 0) {
                $scope.checkInURL = $scope.bookingDetails.data
                    .bookings_data[0]['ns1:Reservation']['ns1:Services']['ns1:AirService']['ns1:Segments']['ns1:OnlineCheckInUrl'];
                $scope.timeLimit = $scope.bookingDetails.data
                    .bookings_data[0]['ns1:Reservation']['ns1:Services']['ns1:AirService']['ns1:Segments']['@TimeLimit'];
                if($scope.bookingDetails.data.bookings_response.length != 0) {
                    $scope.timeStamp = $scope.bookingDetails.data.bookings_response[0]['@TimeStamp'];
                    $scope.bookingStatus = $scope.bookingDetails.data
                        .bookings_response[0]['ns1:Reservation']['ns1:AirService']['@Status'];
                    $scope.confirmationNum = $scope.bookingDetails.data
                        .bookings_response[0]['ns1:Reservation']['ns1:AirService']['ConfirmationNo'];
                    $scope.resID = $scope.bookingDetails.data
                        .bookings_response[0]['ns1:Reservation']['@ReservationID'];
                }
            }
    }
    $scope.getDurationHours = function (duration) {
        var durationInt = parseInt(duration);
        return Math.floor(durationInt / 60);
    };

    $scope.getDurationMinits = function (duration) {
        var minits = duration % 60;
        if (minits < 10) minits = '0' + minits;
        return minits;
    };



    $scope.cancelBooking = function(bookingId){
        localStorageService.set('bookingDetails',null);
        TBSService.cancelBooking(bookingId, $scope.init);
    }

    $scope.init();
});