'use strict';


app
    .controller('FormController', ['$scope', 'ModelsService','localStorageService','orderService' ,function ($scope, ModelsService,localStorageService,orderService) {

        $scope.formObject = {};
        $scope.formStagesArr = [true, false, false];


        $scope.getFlightsModel = function () {

            var promise = ModelsService.getFlightModel();

            promise.then(
                function (res) {

                    $scope.inputArr = res.inputs_array;

                    console.log($scope.inputArr);
                },
                function (err) {
                    console.log(err);
                }
            );

        }




        $scope.$watch("formObject", function (newValue) {
            localStorageService.set('formObject', $scope.formObject);

        },true);

        $scope.recoverData = function () {
            if(localStorageService.get('formObject') !== null) {
                $scope.formObject = localStorageService.get('formObject');
                $scope.formStagesArr = localStorageService.get('formStagesArr');
            }
        };


        $scope.step_next = function (val) {

            $scope.formStagesArr[val] = false;
            $scope.formStagesArr[val + 1] = true;
            localStorageService.set('formStagesArr', $scope.formStagesArr);
        }

        $scope.step_back = function (val) {

            $scope.formStagesArr[val] = false;
            $scope.formStagesArr[val - 1] = true;
            localStorageService.set('formStagesArr', $scope.formStagesArr);
        }


        $scope.submit_order = function (object) {

            orderService.submitOrder(object);
            
            // console.log("object bla");
            // $location.path("/user_admin");
        }

        $scope.cleanForm = function(){
            $scope.formStagesArr = [true, false, false];
            localStorageService.set('formStagesArr', $scope.formStagesArr);
            $scope.formObject = {};
        };

        $scope.getFlightsModel();
        $scope.recoverData();


    }]);


























