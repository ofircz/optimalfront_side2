'use strict';


app.controller('HotelsController', function ($scope, httpService, NgMap,localStorageService) {

    $scope.pageTitle = "מלונות";
    $scope.allhotels = [];
    $scope.obj = {};
    $scope.hotelslength = 0;
    $scope.chosenHotels = [];
    $scope.chosenHotelAllInformation = [];
    $scope.selectedTab = 0;

    var vm = $scope;

    NgMap.getMap().then(function (map) {
        vm.showCustomMarker = function (evt) {
           // map.customMarkers.foo.setVisible(true);
           // map.customMarkers.foo.setPosition(this.getPosition());
        };
        vm.closeCustomMarker = function (evt) {
          //  map.customMarkers.foo.setVisible(false);
        };
    });

    $scope.$watch("selectedTab", function (newValue) {
        localStorageService.set('selectedTab', $scope.selectedTab);
    });


    $scope.getPreviousHotel = function()
    {
            $scope.selectedTab--;
    }

    $scope.getNextHotel = function()
    {
            $scope.selectedTab++;
    }


    var addHotelToArray = function (chosenHotel, area) {
        var hotel = {};

        hotel.area = area.area_details.area;
        hotel.startDate = area.order_details.start_date;
        hotel.endDate = area.order_details.end_date;
        hotel.name = chosenHotel.name_name;
        hotel.price = chosenHotel.price;
        hotel.currency = chosenHotel.currency;
        hotel.room = chosenHotel.room_name;

        $scope.chosenHotels.push(hotel);
        $scope.chosenHotelAllInformation.push(chosenHotel);

        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
        localStorageService.set('selectedTab', $scope.selectedTab);
    }

    var removeHotelFromArray = function (area) {
        var result = true;
        for (var i = 0; i < $scope.chosenHotels.length && result; i++) {
            if ($scope.chosenHotels[i].area === area) {
                $scope.chosenHotels.splice(i,1);
                $scope.chosenHotelAllInformation.splice(i,1);
                result = false;
            }
        }

        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
    }

    $scope.chooseHotelInArea = function (index, hotels, zone) {
        if (zone.isHotelChoosed === true) {
            for (var i = 0; i < hotels.length; i++) {
                hotels[i].isNotChoosed = false;
            }
            zone.isHotelChoosed = false;
            removeHotelFromArray(zone.area_details.area);
        }
        else {
            for (var i = 0; i < hotels.length; i++) {

                if (i === index) {
                    hotels[i].isNotChoosed = false;
                    addHotelToArray(hotels[i], zone);
                }
                else {
                    hotels[i].isNotChoosed = true;
                }
            }
            zone.isHotelChoosed = true;
        }
        localStorageService.set('allHotelsAllInformation', $scope.chosenHotelAllInformation);
        localStorageService.set('allHotels', $scope.allhotels);
        localStorageService.set('chosenHotels', $scope.chosenHotels);
        localStorageService.set('selectedTab', $scope.selectedTab);
    }

    var toggleFunction = function (itemToToggle) {
        itemToToggle = itemToToggle === false ? true : false;
    }


    $scope.range = function (n) {
        if (n !== undefined) {
            return new Array(parseInt(n));
        }
        else {
            return 0;
        }
    };

    $scope.getchoosedHotels = function () {
        for (var i = 0; i < $scope.obj.hotels[0].length; i++) {
            console.log($scope.obj.hotels[0][i].choosedHotel);
        }
    }

    $scope.init = function () {
        if (localStorageService.get('allHotels') === null || localStorageService.get('chosenHotels') === null) {
            $scope.getHotels();
        }
        else {
            $scope.allhotels = localStorageService.get('allHotels');
            $scope.chosenHotels = localStorageService.get('chosenHotels');
            $scope.selectedTab = localStorageService.get('selectedTab');
        }
    }

    $scope.test = function () {

        console.log('in test');
    }


    $scope.getHotels = function () {

        var promise = httpService.getObj();

        promise.then(
            function (res) {
                //console.log(res);

                $scope.obj = res;

                $scope.hotelslength = $scope.obj.hotels.length;
                //console.log($scope.hotelslength);


                $scope.obj.hotels.forEach(function (zone, index) {
                    $scope.allhotels.push(zone);
                });

                $scope.allhotels.forEach(function (zone, index) {
                    zone.isHotelChoosed = false;
                })

                //console.log($scope.allhotels);

                console.log($scope.allhotels);

                // $scope.set_markers();

            },
            function (err) {
                console.log(err);
            }
        );
    }

    $scope.checkIfHotelChoosedInMap = function(hotelName,zone)
    {
        var result = false;
        if(zone.isHotelChoosed == false)
        {
            result =  true;
        }
        else
        {

            var hotels = zone.highest_ranked;
            for (var i = 0; i < hotels.length && !result; i++) {
                if(hotels[i].name === hotelName && hotels[i].isNotChoosed == false)
                {
                    result = true;
                }
            }
        }
        return result;
    }

//    function getCategories() {
//    return (zone.highest_ranked || []).
//      map(function (hotel) { return hotel.hotel_name; }).
//      filter(function (cat, idx, arr) { return arr.indexOf(cat) === idx; });
//  }


    $scope.init();
});


app.directive('chooseHotel', function () {
    return {
        restrict: 'A',
        template: '<figure><img width="500px"/><figcaption/></figure>',
        replace: true,
        link: function (scope, element, attrs) {
            attrs.$observe('caption', function (value) {
                element.find('figcaption').text(value)
            })
            attrs.$observe('photoSrc', function (value) {
                element.find('img').attr('src', value)
            })
        }
    }
})








 

































