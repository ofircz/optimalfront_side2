'use strict';

app
    .controller('multipleFlightsController', function($scope, $http, $interval, $location, $timeout, localStorageService) {

        var listOfCompanyUsers = [];
        var form;
        var currentlyLoggedIn = localStorageService.get('user');
        var legIndex;




        $scope.userInput = false;
        $scope.loading = false;
        $scope.response = ["Running queries..."]
        $scope.allCompanyPassengers = [];
        $scope.currentDate = new Date();
        $scope.listLegs = [];
        $scope.extra1 = false;
        $scope.extra2 = false;
        $scope.extra3 = false;
        $scope.extra4 = false;
        $scope.extra5 = false;
        $scope.extra6 = false;
        $scope.legCommonDetails = {
            cabin: "economy",
            max_price: "usd99999",
            infant_in_lap: 0,
            refundable: true,
            same_airport: true,
            senior: 0,
            adult: 1,
            child: 0,
            saleCountry: "IL",
            infant_in_seat: 0,
            solutions: 500,
            alliance: "",
            corporate: false
        };
        $scope.passengers = [];
        $scope.passengersAsObjects = [];
        $scope.stop_run = function() {
            $http.get('/stop_progress');
        };


        $scope.$watch('allCompanyPassengers', function(newVal, oldVal) {
            if (!(newVal.length == 0)) {
                for (var i = 0; i < $scope.allCompanyPassengers.length; i++) {
                    if ($scope.allCompanyPassengers[i].id != currentlyLoggedIn.user_id) {
                        $scope.passengersAsObjects
                            .push({
                                admin: $scope.allCompanyPassengers[i].admin,
                                id: $scope.allCompanyPassengers[i].id,
                                username: $scope.allCompanyPassengers[i].username,
                                last_name: $scope.allCompanyPassengers[i].last_name,
                                name: $scope.allCompanyPassengers[i].name,
                                checked: false
                            });
                    } else {
                        $scope.passengersAsObjects
                            .push({
                                admin: $scope.allCompanyPassengers[i].admin,
                                id: $scope.allCompanyPassengers[i].id,
                                username: $scope.allCompanyPassengers[i].username,
                                last_name: $scope.allCompanyPassengers[i].last_name,
                                name: $scope.allCompanyPassengers[i].name,
                                checked: true
                            });
                    }
                }
            }
        });

        $http({
            method: 'GET',
            url: '/ListCompanyUsers'
        }).then(function successCallback(response) {
                $scope.allCompanyPassengers = response.data;
            },
            function errorCallback(response) {
                alert(response.status);
            });

        $scope.add = function() {
            var legToAdd = {
                dates_combination_method: "0",
                dates_options: [],
                destination: [],
                origin_input: '',
                days_after_dates: "0",
                destination_input: '',
                origin: [],



                airportsNearbyOrigion: new selectedAirPorts(),

                airportsNearbyDestination: new selectedAirPorts(),
                addressesNearbyOrigion: new selectedAirPorts(),
                addressesNearbyDestination: new selectedAirPorts(),

                nearbyOriginDistance: "150",
                nearbyDestinationDistance: "150",
                nearbyOriginDistanceAddress: "150",
                nearbyDestinationDistanceAddress: "150",
                stay_duration: [],
                days_before_dates: "0",
                destination_change_airport_possibility: 1,
                date: [],
                origin_change_airport_possibility: 1,
                origin_taxi: true,
                origin_private: true,
                origin_rental: true,
                origin_public: false,
                destination_taxi: true,
                destination_private: true,
                destination_rental: true,
                destination_public: false,
                arrive_depart: 'depart',
                preferred_earliest_time: "00:00",
                preferred_latest_time: "23:59",
                max_connection_duration: 1000,
                location_mix: "No",
                cabin: null,
                earliest_time: "00:00",
                latest_time: "23:59",
                max_stop: "5",
                min_stop: "0"
            };
            // pushing the date from the last leg
            legIndex = $scope.listLegs.length - 1;
            if (legIndex >= 0) {
                legToAdd.origin_input = $scope.listLegs[legIndex].destination_input;
                legToAdd.dates_options.push($scope.listLegs[legIndex].dates_options[0]);
                legToAdd.origin[0] = $scope.listLegs[legIndex].destination[0];
                legToAdd.airportsNearbyOrigion = $scope.listLegs[legIndex].airportsNearbyDestination.clone();
                legToAdd.addressesNearbyOrigion = $scope.listLegs[legIndex].addressesNearbyDestination.clone();

            }
            $scope.listLegs.push(legToAdd);

            legIndex++;



        };


        // END: ADD NEW LEG FUNCTION -----------------------------------------------------------



        function redirect(success) {
            if (success) $location.path('/allFlights');
        }


        $scope.removeLegClicked = function(legIndex) {
            //Removing the selected leg from the form
            $scope.listLegs.splice(legIndex, 1);
        }
        $scope.sendForm = function() {
            //$scope.validateDates();
            // Saving the legs input data for the back button feature
            for (let leg of $scope.listLegs) {
                leg.airportsNearbyDestination.cached = true;
                leg.addressesNearbyDestination.cached = true;
                leg.airportsNearbyOrigion.cached = true;
                leg.addressesNearbyOrigion.cached = true;
            }
            localStorageService.set("listLegs", JSON.stringify($scope.listLegs));

            $scope.passengers = [];
            for (var i = 0; i < $scope.passengersAsObjects.length; i++) {
                if ($scope.passengersAsObjects[i].checked) {
                    $scope.passengers.push($scope.passengersAsObjects[i].id);
                }
            }

            if ($scope.passengers.length > 9) {
                alert("ERROR, more than 9 passengers have been chosen!");

            } else {
                if ($scope.passengers.length == 0) {
                    alert("Please choose at least one traveler");
                } else {

                    $scope.loading = true;

                    for (let eachLeg of $scope.listLegs) {

                        let originAirportsNearby = eachLeg.airportsNearbyOrigion.getUniqueSelectedAirPortsCodesArray();
                        let originAirportsAddresses = eachLeg.addressesNearbyOrigion.getUniqueSelectedAirPortsCodesArray();
                        let destinationNearbyAirports = eachLeg.airportsNearbyDestination.getUniqueSelectedAirPortsCodesArray();
                        let destinationNearbyAddresses = eachLeg.addressesNearbyDestination.getUniqueSelectedAirPortsCodesArray();

                        eachLeg.origin[0] = [eachLeg.origin[0], ...originAirportsNearby, ...originAirportsAddresses].unique().join(", ")
                        eachLeg.destination[0] = [eachLeg.destination[0], ...destinationNearbyAirports, ...destinationNearbyAddresses].unique().join(", ");

                    }

                    form = {
                        list_legs: $scope.listLegs,
                        legs_common_details: $scope.legCommonDetails,
                        passengers: $scope.passengers
                    };

                    $http({
                        method: 'POST',
                        url: '/roundTripFlightForm',
                        data: JSON.stringify(form)
                    })

                    // Setup a function to make a call every 5 seconds

                    $scope.currentStatus = {
                        currentQuery: 0,
                        totalQueries: 0,
                        percentage: 5,
                        pBarMode: "determinate"
                    };
                    var querying = $interval(function() {

                        $http.get('/progress') // query for progress
                            .then(function(response) {
                                if (!angular.isDefined(response.data.data))
                                    return;

                                // extract digits with regex

                                let lastIndex = response.data.data.length - 1;
                                $scope.numofQueries = response.data.data[0]

                                let rx = /\d+/g;
                                let result = response.data.data[lastIndex].match(rx);

                                $scope.currentStatus.percentage = Math.round(lastIndex / (Number(result[1])) * 75) + 5;
                                $scope.currentStatus.currentQuery = result[0];
                                $scope.currentStatus.totalQueries = result[1];


                                // check if its time to cancel the interval
                                if (response.data.stop == true) {
                                    if (angular.isDefined(querying)) {
                                        $interval.cancel(querying); // stop make the server call
                                        querying = undefined;
                                        $scope.loading = false;
                                        redirect(response.data);
                                    }
                                }
                            }); // end $http.then
                    }, 5000); // end $interval                            var postSuccess = response.data.success;
                    //                    $http({
                    //                        method: 'POST',
                    //                        url: '/roundTripFlightForm',
                    //                        data: JSON.stringify(form)
                    //                    }).then(function successCallback(response) {
                    //                            var postSuccess = response.data.success;
                    //                            $scope.loading = false;
                    //                            redirect(postSuccess);
                    //                        },
                    //                        function errorCallback(response) {
                    //                            $scope.loading = false;
                    //                            alert(response.status);
                    //                        });
                }
            }
        };
        $scope.add();

        let setSearchFields = legs => {
            for (let currLeg of legs) {
                commonHelpers.normalizeLeg(currLeg);

            }
            $scope.listLegs = legs;
            //$scope.$apply();
        }

        $scope.setLegsFromHistory = data => {

            $scope.userInput = false;
            setSearchFields(data.list_legs);

        }
        $scope.init = function() {
            //Setting the input data when back button was pressed on the allflights view
            if (localStorageService.get("reloadUserInput") == "true") {
                var legs = JSON.parse(localStorageService.get("listLegs"));

                setSearchFields(legs);
                $scope.listLegs = legs;
                localStorageService.set("reloadUserInput", "false");
            }
        }

    });