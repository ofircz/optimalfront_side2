'use strict';


app
    .controller('userLoginController', function ($scope, httpService, orderService, $http, ajaxService, $location, localStorageService, $window) {
        $scope.userData = {};
        $scope.user = null;

        $scope.sendForm = function () {
        $scope.errormsg = ''
            ajaxService.login($scope.userData).then(function (dataFromServer) {
                if(dataFromServer.success === true) {
                    $scope.user = dataFromServer.user_info;
                    $location.path('/');
                    $window.location.reload();
                }
                else {
                $scope.errormsg = 'Username or Password is incorect!'
                }
            });
        };

        $scope.logOut = function() {
            ajaxService.logout().then(function (dataFromServer) {
                localStorageService.remove('user');
                $location.path('/login');
                $scope.user = null;
            });
        };


        $scope.init = function() {
            $scope.user = localStorageService.get('user');
        };

        $scope.init();
    });

