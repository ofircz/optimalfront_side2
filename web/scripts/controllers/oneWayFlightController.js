'use strict';


app
    .controller('oneWayFlightController', function ($scope, httpService, localStorageService, $http) {
        $scope.sendForm = function () {
            var form = {
                origin: $scope.oneWay.origin,
                transport_origin: $scope.oneWay.transportOrg,
                destination: $scope.oneWay.destination,
                transport_destination: $scope.oneWay.transportDes,
                date: $scope.oneWay.date,
                arrive_depart: $scope.oneWay.arriveDepart,
                search_from: $scope.oneWay.searchFrom,
                search_to: $scope.oneWay.searchTo,
                preffered_from: $scope.oneWay.prefferedFrom,
                preffered_to: $scope.oneWay.prefferedTo,
                number_stops: $scope.oneWay.numStop,
                min_stops: $scope.oneWay.MinStop,
                max_stop: $scope.oneWay.MaxStop,
                header1: $scope.oneWay.header1,
                header2: $scope.oneWay.header2,
                header3: $scope.oneWay.header3,
                header4: $scope.oneWay.header4,
                header5: $scope.oneWay.header5,
                adults: $scope.oneWay.adults,
                kids: $scope.oneWay.kids,
                seniors: $scope.oneWay.seniors,
                class: $scope.oneWay.class
            }
            $http({
                method: 'POST',
                url: '/roundTripFlightForm',
                data: {form: JSON.stringify(form)}
            }).then(function successCallback(response) {
                console.log(response);
            }, function errorCallback(response) {

            });
            //console.log(form);
        }


        $("#oneWay").css("background-color", "#207ce5");
        $("#multiple").css("background-color", "grey");
        $("#roundTripFlight").css("background-color", "grey");
        $scope.extra = false;
        $scope.extra1 = false;

        $scope.showExtra = function () {

            $scope.extra = true;
        }
        $scope.hideExtra = function () {
            $scope.extra = false;
        }

        $scope.showExtra1 = function () {

            $scope.extra1 = true;
        }
        $scope.hideExtra1 = function () {
            $scope.extra1 = false;
        }


        $scope.remove = function () {
            alert('remove');
        }
    });