'use strict';

app
    .factory('authService', function ($q, $timeout, $http) {


        // create user variable
        var user = null;

        function isLoggedIn() {
            if (user) {
                return true;
            } else {
                return false;
            }
        }

        function login(email, password) {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a post request to the server
            $http.post('/api/login', {email: email, password: password})
                // handle success
                .then(function (data, status) {
                    if (status === 200 && data.result) {
                        user = true;
                        deferred.resolve();
                    } else {
                        user = false;
                        deferred.reject();
                    }
                }
                // handle error
                ,error=> {
                    user = false;
                    deferred.reject(error);
                });

            // return promise object
            return deferred.promise;
        }

        function logout() {

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a get request to the server
            $http.get('/api/logout')
                // handle success
                .then(function (data) {
                    user = false;
                    deferred.resolve();
                }
                // handle error
                ,data => {
                    user = false;
                    deferred.reject();
                });

            // return promise object
            return deferred.promise;
        }

        function register(email, password) {
            //// create a new instance of deferred
            var deferred = $q.defer();
            $http.defaults.headers.post["Content-Type"] = "application/json";
            //// send a post request to the server
            //$http.post('/registerAtt', {email: email, password: password})
            //    // handle success
            //    .success(function (data, status) {
            //        if (status === 200 && data.result) {
            //            deferred.resolve();
            //        } else {
            //            alert('first');
            //            deferred.reject();
            //        }
            //    })
            //    // handle error
            //    .error(function (data) {
            //        deferred.reject();
            //    });
            //
            //// return promise object

            $http({
                url: '/registerAtt',
                method: "POST",
                data: {email: email, password: password}
            })
                .then(function (response) {
                    // success
                    alert('success');
                },
                function (response) { // optional
                    // failed
                    alert('failed');
                });

            return deferred.promise;
        }


        // return available functions for use in controllers
        return ({
            isLoggedIn: isLoggedIn,
            login: login,
            logout: logout,
            register: register
        });
    });
