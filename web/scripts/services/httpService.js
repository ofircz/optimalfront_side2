'use strict';

app
.service('httpService' , function($http , $q , $rootScope ){

	//create obj instance
	var httpService = this;
	// container obj for http response
	httpService.obj = {};

	
	httpService.getObj = function(){

		var deffered = $q.defer();

		var req = { headers: {'Content-Type': 'application/json; charset=utf-8'} , 
					//withCredentials: false, 
					method: 'GET',
					/*  url: '/resources/newData.json'  */
					 url: '/HotelSearch'
					  /*url: '/summery/' 
						params: {id: user_id}
					  */
				  };

		$http(req)
		.then(function(res){

			httpService.obj = res;

			deffered.resolve(res);

		}
		,err =>{

			deffered.reject(err);
		})

		return deffered.promise;

	}


	
return httpService;	
});














