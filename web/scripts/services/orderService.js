'use strict';

app
    .service('orderService', function ($http, $q, $cookies) {


        var orderService = {};

        orderService.submitOrder = function (object) {

            var deffered = $q.defer();

             $http({
                url: '/flightFormAtt',
                method: "POST",
                data: {obj: object}
            })
            // $http({
            //     url: '/repeat',
            //     method: "POST",
            //     data: {obj: object}
            // })
                .then(function (response) {
                    // success
                     alert("response data:" + response.data);
                    deferred.resolve(response);
                },
                function (response) { // optional
                    // failed
                    alert('failed orderService');
                });


            return deffered.promise;

        }


        return orderService;

    });