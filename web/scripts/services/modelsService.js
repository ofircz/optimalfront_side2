'use strict';

app
.service( 'ModelsService' , function($http , $q){
 	
	var ModelsService = this;
	
	ModelsService.getFlightModel = function(){

		var deffered = $q.defer();

		var req = { headers: {'Content-Type': 'application/json; charset=utf-8'} , 
					//withCredentials: false, 
					method: 'GET',
					 url: '/resources/flightModel.json' 
					  /*url: '/summery/' 
						params: {id: user_id}
					  */
				  };

		$http(req)
		.then(function(res){

			deffered.resolve(res);

		},
		err =>{

			deffered.reject(err);
		})

		return deffered.promise;
	}

	return ModelsService;

});
 