﻿app.factory('ajaxService', function($http, $q, $timeout) {

    var ajaxFunctions = {};
    ajaxFunctions.login = function(userData) {
        var defer = $q.defer();
        $http({
            url: '/login_angular',
            method: 'POST',
            data: JSON.stringify(userData),
            headers: {
                'content-type': 'application/json'
            }
        }).then(response => {
            defer.resolve(response.data);
        },error => {
            alert('Error!');
            defer.reject(error);
        });
        return defer.promise;
    }

    ajaxFunctions.checkIfUserLoggedIn = function() {
        var defer = $q.defer();
        $http({
            url: '/IsLoggedIn',
            method: 'GET'
        }).then(response => {
            defer.resolve(response.data);
        },error => {
            alert('Error!');
            defer.reject(error);
        });
        return defer.promise;
    }

    ajaxFunctions.logout = function() {
        var defer = $q.defer();
        $http({
            url: '/logout',
            method: 'GET'
        }).then(function(response) {
            defer.resolve(response.data);
        },error => {
            alert('Error!');
            defer.reject(error);
        });
        return defer.promise;
    }

    ajaxFunctions.resolveAirPorts = function(airPort, distance) {
        // Remove to test the loading progress
        //return  $timeout(function(){}, 5000).then(function(){
        // Should not resolve the airport names if the length is less then 3
        if (airPort.length < 3)
            defer.resolve([]);
        let jsonData = JSON.stringify({
            ap_code: airPort,
            max_distance: Number(distance)
        });
        var defer = $q.defer();
        $http({
            url: '/ApNearbyOpt',
            method: 'POST',
            data: jsonData,
            headers: {
                'content-type': 'application/json'
            }
        }).then(response => {
            let airPorts = [];
            let data = response.data;

             airPorts = createArrayOfAirports(data);

            defer.resolve(airPorts);
        }, error => {
            alert('Error!');
            defer.reject("error");
            console.log(error);
        });

        return defer.promise;

        // Remove to test the loading progress
        //   });
    }


    ajaxFunctions.resolveAirPortsByAddress = autoCompleteAddress => {

        let jsonData = JSON.stringify({
            address: autoCompleteAddress
        })
        var defer = $q.defer();
        $http({
            url: "/NearbyByAddress",
            method: "POST",
            data: jsonData,
            headers: {
                'content-type': 'application/json'
            }
        }).then(response => {

            let addressNearby = [];

            addressNearby = createArrayOfAirports(response.data);

            defer.resolve(addressNearby);
        }, error => {
            alert('Error!');
            defer.reject("error");
            console.log(error);
        });
        return defer.promise;

    }

    function createArrayOfAirports(data){

                    let airPorts= [];

                    for (let airportData of data) {

                   // regex for airportCode
                    let rx = /\[.*\]/g;
                    // regex for airport name
                    let nameRx = /.+?(?=\s\{)/g;
                    // regex for cordinate object
                    let coordRx = /\{.*\}/g;

                    let dataValue = String(airportData);

                 let airportName;
                 let airportCord
                nameRxResult = dataValue.match(nameRx);
                if (nameRxResult){
                   airportName = nameRxResult[0];
                    let airportCordStr = dataValue.match(coordRx)[0].replace(/'/g, '"');
                     airportCord =  JSON.parse(airportCordStr);
                }
                else{
                    airportName = dataValue
                    airportCord = null;
                }


                    let airportCode = dataValue.match(rx)[0];


                    airPorts.push({

                        name: airportCode.slice(1, -1),
                        airportText: airportName,
                       airportCord: airportCord



                    });

            }

            return airPorts;

    }




     ajaxFunctions.getSearchHistory = () => {


         return new Promise((resolve, reject) => {
                $http({
                url: "/ShowSearchHistory",
                method: "GET",

                headers: {
                    'content-type': 'application/json'
                }
            }).then(response => {
                let data = response.data;

                for (let currentData of data){
                    for (let leg of currentData.list_legs){
                        for (let currentDateIndex in leg.dates_options){
                                let currentDate = new Date(leg.dates_options[currentDateIndex])
                                leg.dates_options[currentDateIndex] = currentDate;
                        }

                       commonHelpers.normalizeLeg(leg);
                    }
                }
                resolve(data);
            }, error => {

                reject("error");
                console.log(error);
            });
        });



    }

    return ajaxFunctions;
});