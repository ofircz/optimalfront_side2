'use strict';

var jQueryUse = jQuery.noConflict();

var app = angular
    .module('OptimalApp',
        [
            'ngRoute',
            'ui.bootstrap',
            'ngAnimate',

            'ngMap',
            'ngMaterial',
            'ngMessages',
            'ngCookies',
            'LocalStorageModule',

        
            'ngSanitize',
            'ui.select',
            'ui.router',
            'ui-rangeSlider'


        ]
    )

    .config(function ($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('blue');
    })

    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainController',
                controllerAs: 'mainCtrl'
            })
            .when('/user_admin', {
                templateUrl: 'views/user_admin.html',
                controller: 'UserController',
                controllerAs: 'userCtrl'

            })
            .when('/form', {
                templateUrl: 'views/form.html',
                controller: 'FormController',
                controllerAs: 'formCtrl'

            })
            .when('/flights', {
                templateUrl: 'views/flights.html',
                controller: 'FlightsController',
                controllerAs: 'flightsCtrl'

            })
            .when('/allFlights', {
                templateUrl: 'views/allFlights.html',
                controller: 'AllFlightsController',
                controllerAs: 'allFlightsCtrl',
                reloadOnSearch: true

            })
            .when('/hotels', {
                templateUrl: 'views/hotels.html',
                controller: 'HotelsController',
                controllerAs: 'hotelsCtrl'

            })
            .when('/hotels2', {
                templateUrl: 'views/hotels2.html',
                controller: 'Hotels2Controller',
                controllerAs: 'hotels2Ctrl'

            })
            .when('/register', {
                templateUrl: 'views/register.html',
                controller: 'RegisterController',
                controllerAs: 'regCtrl'

            })
            .when('/login', {
                templateUrl: 'views/signin.html',
                controller: 'userLoginController',
                controllerAs: 'signCtrl'
            })

            .when('/flight/multipleFlights', {
                templateUrl: 'views/flightsForms/multipleFlightForm.html',
                controller: 'multipleFlightsController',
                controllerAs: 'multipleFlightsCtrl'
            })

            .when('/hotelSearch', {
                templateUrl: 'views/hotelsForm.html',
                controller: 'hotelSearchController',
                controllerAs: 'HotelSearchCtrl'
            })

            .when('/finalOrder', {
                templateUrl: 'views/finalOrder.html',
                controller: 'finalOrderController',
                controllerAs: 'finalOrderCtrl'
            })

            .when('/userLogin', {
                templateUrl: 'views/signin.html',
                controller: 'userLoginController',
                controllerAs: 'signCtrl'

            })
            .when('/userRegister', {
                templateUrl: "views/userRegister.html",
                controller: 'userRegisterController',
                controllerAs: 'userRegisterCtrl'

            })
            .when('/adminRegister', {
                templateUrl: "views/adminRegister.html",
                controller: 'adminRegisterController',
                controllerAs: 'adminRegisterCtrl'

            })
            .when('/bookedTrip', {
                templateUrl: "views/bookedTrip.html",
                controller: 'bookedTripController',
                controllerAs: 'bookedTripCtrl'
            })

            .when('/displayBookingCode/:path?', {
                templateUrl: "views/displayBookingCode.html",
                controller: 'displayBookingController',
                controllerAs: 'displayBookingCtrl'
            })

            .when('/dc', {

                templateUrl: "views/directiveTemplates/dependencyCheck.html",
                controller: 'dependency',
                controllerAs: 'dependency'
            })
            
            .when('/forgotPassword', {
                templateUrl: "views/forgotPassword.html",
                controller: 'userLoginController',
                controllerAs: 'signCtrl'
            })

            .otherwise({
                redirectTo: '/'
            });
    });



app.constant('_', window._);

app.run(['$rootScope', '$location', 'ajaxService', 'localStorageService','$window', function ($rootScope, $location, ajaxService, localStorageService, $window) {
    $rootScope._ = window._;
    //Checking if the app is online
     $rootScope.online = navigator.onLine;
      $window.addEventListener("offline", function () {
        $rootScope.$apply(function() {
          $rootScope.online = false;
        });
      }, false);
      $window.addEventListener("online", function () {
        $rootScope.$apply(function() {
          $rootScope.online = true;
        });
      }, false);






    $rootScope.$on('$routeChangeStart', function (event) {

        ajaxService.checkIfUserLoggedIn().then(function (dataFromServer) {

            localStorageService.set('user', dataFromServer);

            if ((dataFromServer === null) && ($location.path()!== "/") && ($location.path()!== "") && ($location.path()!== "/userLogin")
            && ($location.path()!== "/userRegister") && ($location.path()!== "/register") && ($location.path()!== "/login") && ($location.path()!== "/adminRegister") && ($location.path()!== "/forgotPassword")  ) {

                $location.path('/userLogin');
            }
        });
    });
}]);

app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('optimalTrip');
});

app.config(function($mdAriaProvider) {
   // Globally disables all ARIA warnings.
   $mdAriaProvider.disableWarnings();
});

app.config(function($stateProvider, $locationProvider) {

      // remove ! hash prefix
      $locationProvider.hashPrefix('');

     // ui-router code ...
    });