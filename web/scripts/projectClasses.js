// Alex Lavriv 02/09/2017
// This file should contain JS classes and object extensions

class selectedAirPorts{

  selectedToCommaString() {

        return this.selected.map(elem => elem.name).unique().join(", ");
    };

    getUniqueSelectedAirPortsCodesArray(){
                let arr = this.getSelectedAirports();

                let airportsCodes = arr.map(elem => elem.name);
          return  airportsCodes.unique();
    }

    clone(){
        let clone = new selectedAirPorts();

      clone.cached =           this.cached        ;
      clone.selected =         this.selected    ;
      clone.cachedAirports =   this.cachedAirports;
      clone.distance =         this.distance ;

        return clone;


    }

    getSelectedAirports(){
         let arr = this.selected.slice();
         let index = arr.indexOf("all");

         // -1 will delete the last object in the array
         if (index !=-1){
             arr.splice(index,1);
         }
         return arr;

    }

    constructor(source){
        if (source instanceof  selectedAirPorts){
            return source;
        }
        if (source != null){
     this.cached        = source.cached;
     this.selected      = source.selected;
     this.cachedAirports = source.cachedAirports;
     this.distance      = source.distance;
      this.searchString =source.searchString;
     }
     else{
         this.cached = false;
         this.selected = [];
         this.cachedAirports = [];
         this.distance =  150;
         this.searchString ='';
     }

    }

}

class commonHelpers {
    // Converts Minutes to 00:00 time format - Alex Lavriv
    static convertMinutesToHoursString(minutes){
        if (!Number.isInteger(minutes)) {
                return minutes;
            }
      let h = Math.floor(minutes / 60);
      let m = minutes % 60;
      h = h < 10 ? '0' + h : h;
      m = m < 10 ? '0' + m : m;
      return h + ':' + m;
    }

    static normalizeLeg (currLeg) {
        for (let currDateOption in currLeg.dates_options)
                     {
                        if ((currLeg.dates_options[currDateOption] instanceof Date) === false ){

                         currLeg.dates_options[currDateOption]  = new Date(currLeg.dates_options[currDateOption]);
                         }


                     }
                     currLeg.airportsNearbyOrigion = new selectedAirPorts(currLeg.airportsNearbyOrigion);
                    currLeg.addressesNearbyOrigion =  new selectedAirPorts(currLeg.addressesNearbyOrigion);
                     currLeg.airportsNearbyDestination =  new selectedAirPorts(currLeg.airportsNearbyDestination);
                    currLeg.addressesNearbyDestination =  new selectedAirPorts(currLeg.addressesNearbyDestination);


    }



}

// Prototype modify
// This function return unique values of the array - Alex Lavriv
// usage example:
//var a = ['a', 1, 'a', 2, '1'];
//var unique = a.unique(); // returns ['a', 1, 2, '1']

Array.prototype.unique = function() {
    let cleanArray = this.clean();
    uniqueArray = cleanArray.filter( (value,index,self) => {
         if (typeof value === "string") {
             return self.findIndex(item => value.toLowerCase() === item.toLowerCase()) === index;
        }
        return false;
    })
   return uniqueArray;
}

Array.prototype.clean = function() {
      var newArray = new Array();
      for (let current of this) {
        if (current) {
          newArray.push(current);
        }
      }
  return newArray;
};