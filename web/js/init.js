(function($) {
    $(function() {
    }); // end of document ready
})(jQuery); // end of jQuery name space
json = {};

//when SUbmit pressed
function submitToServer() {
    var list = getCheckedCheckboxesFor('select');
    var arr = [];
    for (var i = 0; i < list.length; i++) {
        if (list[i].value == 'on')
            arr.push(list[i].id);
    }
    saveFlightData(arr);
    list = getCheckedCheckboxesFor('input');
    for(var i =0;i<list.length;i++)
    {

    }

}
//set number of hotels
function setHotelsInputs(loc,num)
{
    var div = document.getElementById("hotelsLocation"+num);
    while(div.hasChildNodes()){
        div.removeChild(div.lastChild);
    }
    for(i=1;i<=loc;i++)
    {
        var row = document.createElement("div");
        row.setAttribute("class","row");
        var input =  document.createElement("input");
        input.setAttribute("name","locationsName"+i);
        input.setAttribute("id","locationsNum"+i);
        input.setAttribute("type","text");
        var d =  document.createElement("div");
        d.setAttribute("class","input-field col l3"); 
        d.appendChild(input);
        var name =  document.createElement("label");
        name.setAttribute("for","locationsNum"+i);
        name.innerHTML = "Hotel - "+i;

        d.appendChild(name);
        row.appendChild(d);
        div.appendChild(row);
    }
}
function getCheckedCheckboxesFor(name) {
    var checkboxes = document.querySelectorAll('input[name="' + name + '"]:checked'),
        values = [];
    Array.prototype.forEach.call(checkboxes, function(el) {
        values.push(el);
    });
    return values;
}

function saveFlightData(arr) {
    var toJson = {};
    var count = 0;
    for (i in arr) {
        var price = document.getElementById("price" + arr[i]);
        var title = document.getElementById("from_to" + arr[i]).children[0];
        var date = document.getElementById("date" + arr[i]);
        var fl = document.querySelectorAll('*[id^="fl' + arr[i] + '"]');
        var flights = {};
        Array.prototype.forEach.call(fl, function(el) {
            var temp = el.id.split('.');
            var tempName = temp[1];
            if (!flights[temp[temp.length - 1]])
                flights[temp[temp.length - 1]] = {}
            if (tempName == "layover")
            {
                if (!flights[temp[temp.length - 1]][tempName])
                flights[temp[temp.length - 1]][tempName] = {}
                flights[temp[temp.length - 1]][tempName][temp[2]] = el.innerHTML;    
            } 
            else 
            flights[temp[temp.length - 1]][tempName] = el.innerHTML;

        });
        var fromTo = title.innerHTML.split("to");
        var cutTitle = fromTo[0].split("(");
        var from = cutTitle[0];
        cutTitle = fromTo[1].split("(");
        var to = cutTitle[0];
        toJson[count] = {
            'from': from,
            'to': to,
            'price': price.value,
            'title': title.innerHTML,
            'date': date.innerHTML,
            'flights': flights
        };
        count++;
    }
    $.ajax({
        type: 'POST',
        url: '/updateflight',
        data: JSON.stringify(toJson),
        contentType: 'application/json',
        success: function(data, textStatus, jqXHR) {
            console.log('POST response: ');
            console.log(data);
        }
    });
}

function setDeal(num, name){
       var li = document.getElementById("dealSelect"+num).querySelectorAll('li');
       for (var i =0;i<li.length;i++){
        li[i].removeAttribute("class");
       }
       document.getElementById("li"+name+num).setAttribute("class", "active");
       document.getElementById("dealSelectBtn"+num).innerHTML = name;
}
