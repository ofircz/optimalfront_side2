import json

class HotelsForClient()
    @staticmethod
    def set_hotels():
        from api.trip_advisor import tripAdvisor
        t = tripAdvisor()
        main_hotels = [] # added row - tal
        params = {
        'lat': '42.33141',
        'long': '-71.099396'
        }
        hotels = t.get_hotel_list(params)
        json = {
                  "coords": [
              {
                "latitude": "32.830593",
                "longitude": "-79.825432",
                "id": ""
              },
              {
                "latitude": "32.863488",
                "longitude": "-80.023833",
                "id": ""
              },
              {
                "latitude": "32.853438",
                "longitude": "-80.056785",
                "id": ""
              }
            ],
              'area_details': {'area': 'Lake Forest', 'State': 'California',
                'continent': 'America'},
              'order_details': {'start_date':'14.8','end_date':'18.8', 'duration':'4',
                                'adults': '1',
                                'children': '0',
                                'senior:':'0',
                                'breakfast': 'included',
                                'refundable': 'yes'},
                                'link_to_hotels_in_the_area': ['?http://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799??',
                                                       '?http://www.booking.com/searchresults.he.html?city?? ??=20013943&aid=825774??'],
              'highest_ranked': [{
              'name': '??Staybridge?? ??Suites?? ??Irvine?? ??East?? ??-?? ??Lake?? ??Forest??' ,
              'price': '946',
              'tax_to_pay_in_hotel':'10%',
              'rank': ranks('name'),
              'address':'2 Orchard, Lake Forest, California, United States',
              'link': 'https://www.hotelscombined.co.il/Hotel/Staybridge_Suites_Irvine_East_Lake_Forest.htm?a_aid=139799',
              'pension': 'breakfast',
              'reviews':reviews(),
              'hotel_details':hotel_details(hotels[0]),
              'added_device':hotel_addons(),
              'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg',
              },{
              'name': '??Holiday?? ??Inn?? ??Irvine?? ??Spectrum??' ,
              'price': ranks('name'),
              'tax_to_pay_in_hotel':'10%',
              'rank': '8.9' ,
              'address':'23131 Lake Center Drive, Lake Forest, California, United States',
              'link': 'https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Irvine_Spectrum.htm?a_aid=139799',
              'pension': 'breakfast',
                        'reviews':reviews(),
              'hotel_details':hotel_details(hotels[1]),
              'added_device':hotel_addons(),
              'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg',
              },{
              'name': '??Prominence?? ??Hotel?? ?&? ??Suites??' ,
              'price': '465' ,
              'tax_to_pay_in_hotel':'10%',
              'rank': ranks('name'),
              'address':'20768 Lake Forest Drive, Lake Forest, California, United States',
              'link': 'https://www.hotelscombined.co.il/Hotel/Prominence_Hotel_Suites.htm?a_aid=139799',
              'pension': 'breakfast',
                        'reviews':reviews(),
              'hotel_details':hotel_details(hotels[2]),
              'added_device':hotel_addons(),
                  'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg',
              }]
              },{
              'area':'North Carolina',
              'order_details': {'dates':'19-24.8', 'adults':'1', 'children': '0', 'breakfast': 'included', 'refundable':'yes'},
              'hotels_in_the_area':['????http://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799??'],
              'highest_ranked': [{
              'name': '????Hawthorne?? ??Inn?? ??Winston?? ??Salem??' ,
              'price': '601',
              'tax_to_pay_in_hotel':'0',
              'rank': ranks('name'),
              'link': 'https://www.hotelscombined.co.il/Hotel/Hawthorne_Inn_Winston_Salem.htm?a_aid=139799',
              'address':'460 North Cherry Street, Winston-Salem, North Carolina, United States',
              'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg',

                        'reviews':reviews(),
              'hotel_details':hotel_details(hotels[3]),
              'added_device':hotel_addons()
              },{
              'name': '????Embassy?? ??Suites?? ??Hotel?? ??Winston?? ??-?? ??Salem??' ,
              'price': '834' ,
              'tax_to_pay_in_hotel':'0',
              'rank': ranks('name'),
              'address':'460 North Cherry Street, Winston-Salem, North Carolina, United States',
              'link': 'https://www.hotelscombined.co.il/Hotel/Embassy_Suites_Hotel_Winston_Salem.htm?a_aid=139799',
                   'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg'
              },{
              'name': '????Holiday?? ??Inn?? ??Express?? ??Winston?? ??-?? ??Salem??' ,
              'price': '685' ,
              'tax_to_pay_in_hotel':'0',
              'rank': ranks('name'),
              'address':'2520 Peters Creek Pkwy, Winston-Salem, North Carolina, United States',
              'link': 'https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Express_Winston_Salem.htm?a_aid=139799',
                   'pictures':[
              'https://media.datahc.com/HI104237475.jpg',
              'https://media.datahc.com/HI104237501.jpg',
              'https://media.datahc.com/HI104237478.jpg',
              'https://media.datahc.com/HI104237497.jpg'
              ],
              'header':'https://media.datahc.com/HI104237497.jpg',

                        'reviews':reviews(),
              'hotel_details':hotel_details(hotels[5]),
              'added_device':hotel_addons()
              }
              ]}
        main_hotels.append(json) # added row - tal
        return main_hotels #added row - tal
        #return json

    def reviews(hotel = None):
        reviews = {
        'source0':'this is a review',
        'source1': 'this is another review',
        'source2': 'third review'
        }
        return reviews


    def hotel_addons(hotel = None):
        devices = [
        'first device',
        'second device',
        'third device'
        ]
        return devices


    def hotel_details(hotel = None):
        hotel_dict = {
        'check_in': '12:00',
        'check_out': '14:00',
        'reception': {'from':'7:00', 'to':'23:00'},
        'availeable_rooms': '250',
        'laggage_keeping':'yes',
        'parking':'yes',
        'stars':hotel['rating']['rate'],
        'rooms':[{
        'type':'single',
        'price_per_night':'60'
        }],
        'lat': hotel['lat'],
        'long': hotel['long'],
        'distances':{
        'from_address':{
        'drive':'10',
        'aerial': '10',
        'driving_time':'25'
        },
        'from_airport':{
        'drive':'10',
        'aerial': '10',
        'driving_time':'25'
        },
        }
        }
        return hotel_dict

    def ranks(hotel):
        ranks = {
            'tripadvisor':{
            'rank':'9',
            'ranks_num':'1000'
            },
            'site1':{
            'rank':'9',
            'ranks_num':'1000'
            },
            'site2':{
            'rank':'9',
            'ranks_num':'1000'
            },
            'average_rank':'9',
            'ranks_sum':'3000'
        }
        return ranks

    # def json_create(change):
    #     global Flights
    #     jsond = {}
    #     jsond['price'] = '862'
    #     jsond['num_of_legs'] = '2'
    #     jsond['travelers_details'] = [{"type": 'adult', 'ammount': '3'}]
    #     jsond['legs'] = [{
    #     'airlines': [],
    #     'direct_flights':[
    #     {
    #     "departure_time":'14:00',
    #     "departure_date":'14.8',
    #     'landing_date':'14.8',
    #     'flight_length':'2:00',
    #     "departure_airport_code":'LAX',
    #     "arrival_airport_code":'NDA',
    #     "departure_airport_city":'Los Angeles',
    #     "arrival_airport_city":'California',
    #     'stop_length':'1:00',
    #     'class':'business',
    #     'plane_type':'boing',
    #     'arrival_time':'16:00',
    #     'airline':'LA airlines',
    #     'flight_num':'2252'
    #     },
    #         {
    #     "departure_time": '17:00',
    #     "departure_date": '14.8',
    #     'landing_date': '15.8',
    #     'flight_length': '11:00',
    #     "departure_airport_code": 'NDA',
    #     "arrival_airport_code": 'TLV',
    #     "departure_airport_city": 'California',
    #     "arrival_airport_city": 'Tel Aviv',
    #     'stop_length': '0:00',
    #     'class': 'business',
    #     'plane_type': 'boing',
    #     'arrival_time': '4:00',
    #     'airline': 'Arkia',
    #     'flight_num': '4855'
    #     }
    #     ],
    #     'origin': {
    #     'airport': 'Ben Gurion Airport 7015001',
    #     'date': '13.6', 'time': '',
    #     'address': 'Jerusalem, Yaffo 11',
    #     'airport_code': 'sss',
    #     'distance': '50', 'drive_time': '75',
    #     'drive_distance': '60'
    #     },
    #     'destination': {
    #       'airport': '1 World Way, Los Angeles, CA 90045, United States',
    #         'date': '13.6', 'time': '',
    #         'address': 'Crescent Bay Dr, Lake Forest, CA 92630 20481',
    #         'airport_code': 'ssssss',
    #         'distance': '', 'drive_time': '',
    #         'drive_distance': ''
    #         }
    #      },
    #      {
    #     'direct_flights': [{}],
    #     'origin': {
    #     'airport': '1 World Way, Los Angeles, CA 90045, United States',
    #     'date': '18.6', 'time': '',
    #     'address': 'Crescent Bay Dr, Lake Forest, CA 92630 20481',
    #     'airport_code': 'sss',
    #     'distance': '', 'drive_time': '',
    #     'drive_distance': ''
    #     },
    #     'destination': {
    #     'airport': '3801 N Liberty St #204, Winston-Salem, NC 27105, United States',
    #     'date': '18.6', 'time': '',
    #     'address': 'Crestwood Drive Southwest, Winston-Salem, NC 27101 1109',
    #         'airport_code': 'sss',
    #     'distance': '', 'drive_time': '',
    #     'drive_distance': ''}
    #     }, {
    #     'direct_flights': [{}],
    #     'origin': {
    #     'airport': '3801 N Liberty St #204, Winston-Salem, NC 27105, United States',
    #     'date': '24.6', 'time': '',
    #     'address': 'Crestwood Drive Southwest, Winston-Salem, NC 27101 1109',
    #         'airport_code': 'sss',
    #     'distance': '', 'drive_time': '',
    #     'drive_distance': ''},
    #     'destination': {
    #     'airport': 'Ben Gurion Airport 7015001',
    #     'date': '24.6', 'time': '',
    #     'address': "Jerusalem, Yaffo 11",
    #     'airport_code': 'sss',
    #     'distance': '50', 'drive_time': '75',
    #     'drive_distance': '60'
    #     }}]
    #     return jsond