import json
from operator import itemgetter
import views as vs

__author__ = 'tal'

# Here im reading all the data from the json file.
#json_data = json.loads(open('C:\\Users\\tal\\Desktop\\jd.txt').read())
json_data = None
my_data = {"plan_id": "156189181818181", "cheapest": None, "else": None, "shortest": None, "best": None, "hotels": None}
flight_id = {"id": 1000}


class clientJson():

    #Method that gives us the json data variable.
    @staticmethod
    def get_json_data():
        return json_data

    # Method that calculate all the cheapest flights in the data and return their indexes
    def sort_by_price(self):
        temp_cheapest_flights = []

        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for spesific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    flight_price_and_id = []
                    price = float(json_data[flight]['trips']['tripOption'][spesific_flight].get('saleTotalQpx').split('USD')[1])
                    flight_price_and_id.append(price)
                    flight_price_and_id.append(str(flight))
                    flight_price_and_id.append(str(spesific_flight))
                    temp_cheapest_flights.append(flight_price_and_id)

        cheapest_flights = sorted(temp_cheapest_flights, key=itemgetter(0))
        return cheapest_flights

    # Method that calculate all the shortest flights in the data and return their indexes
    def sort_by_shortest(self):
        temp_shortest_flights = []

        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for spesific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_flight_and_drive_time = []
                    flight_drive_time = self.total_flight_time_and_drive_time(json_data[flight]['trips']['tripOption'][spesific_flight])
                    temp_flight_and_drive_time.append(flight_drive_time)
                    temp_flight_and_drive_time.append(str(flight))
                    temp_flight_and_drive_time.append(str(spesific_flight))
                    temp_shortest_flights.append(temp_flight_and_drive_time)

        shortest_flights = sorted(temp_shortest_flights, key=itemgetter(0))

        return shortest_flights

    # Method that calculate total flight and drive time per flight.
    def total_flight_time_and_drive_time(self, current_flight):
        number_of_legs = int(current_flight.get('number_of_legs'))
        total_flight_with_drive_time = 0
        for leg in range(0, number_of_legs):
            temp_time_and_drive = int(current_flight['legs'][leg].get('durationWithDrive'))
            total_flight_with_drive_time += temp_time_and_drive

        return total_flight_with_drive_time

    #Method that will build the details about the travelers.
    def travelers_details_build(self, person_type, amount):
        travelers_details = {'type': None, 'amount': None}
        travelers_details['type'] = person_type
        travelers_details['amount'] = amount
        return travelers_details

    # Method that build the cheapest data section (include puling data) in the json file that i create.
    def cheapest_data(self, flight_id):
        cheap_flights = []
        flights_sorted_by_price = self.sort_by_price()
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, 5):
                    cheap_data = {"price": None, "legs": None, "travelers_details": None, "num_of_legs": None, "calculations": None}
                    travelers_details = []
                    first_index = int(flights_sorted_by_price[specific_flight][1])
                    sec_index = int(flights_sorted_by_price[specific_flight][2])
                    calculations = {}
                    person_amount = 0
                    total_flights_time = 0
                    total_stops_time = 0
                    total_mileage = 0
                    airlines = {}
                    list_of_airlines = []
                    airlines_counter = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'adultCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("adult", person_amount))

                    person_amount = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'seniorCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("senior", person_amount))

                    print json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails']
                    cheap_data['flight_id'] = flight_id["id"]
                    cheap_data['price'] = json_data[first_index]['trips']['tripOption'][sec_index].get('saleTotalQpx')
                    flight_id["id"] += 1

                    legs_list = []
                    for leg in range(0, int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))):
                        direct_flights = []
                        legs_data = {}
                        total_flights_time += self.calculate_total_flights_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_stops_time += self.calculate_total_stops_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_mileage += self.calculate_total_mileage(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        legs_data['duration'] = json_data[first_index]['trips']['tripOption'][sec_index]['legs'][leg].get('duration')
                        legs_origin = self.origin_build(first_index, leg, sec_index)
                        legs_data['origin'] = legs_origin
                        legs_destination = self.destination_build(first_index, leg, sec_index)
                        legs_data['destination'] = legs_destination
                        self.airline_build(first_index, airlines, airlines_counter, list_of_airlines, sec_index)
                        legs_data['airlines'] = list_of_airlines
                        legs_data['stops'] = self.stops_build(first_index, leg, sec_index)
                        self.direct_flights_build(first_index, leg, direct_flights, sec_index)
                        legs_data['direct_flights'] = direct_flights

                        legs_list.append(legs_data)
                    calculations['total_flights_time'] = total_flights_time
                    calculations['total_drives_and_flights_time'] = self.total_flight_time_and_drive_time(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops'] = self.calculate_total_amount_of_stops(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops_time'] = total_stops_time
                    calculations['total_mileage'] = total_mileage
                    cheap_data['calculations'] = calculations
                    cheap_data['legs'] = legs_list
                    cheap_data['num_of_legs'] = int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))
                    cheap_data['travelers_details'] = travelers_details
                    cheap_flights.append(cheap_data)
        my_data['cheapest'] = cheap_flights

    # Method that build the else data section (include puling data) in the json file that i create.
    def else_data(self, flight_id):
        else_flights = []
        flights_sorted_by_else = self.sort_by_price() # need to change this sort to amiel result of sort's
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                #for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                for specific_flight in range(5, 13):
                    else_data = {"price": None, "legs": None, "travelers_details": None, "num_of_legs": None, "calculations": None}
                    travelers_details = []
                    first_index = int(flights_sorted_by_else[specific_flight][1])
                    sec_index = int(flights_sorted_by_else[specific_flight][2])
                    calculations = {}
                    person_amount = 0
                    total_flights_time = 0
                    total_stops_time = 0
                    total_mileage = 0
                    airlines = {}
                    list_of_airlines = []
                    airlines_counter = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'adultCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("adult", person_amount))

                    person_amount = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'seniorCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("senior", person_amount))
                    else_data['flight_id'] = flight_id["id"]
                    else_data['price'] = json_data[first_index]['trips']['tripOption'][sec_index].get('saleTotalQpx')
                    flight_id["id"] += 1

                    legs_list = []
                    for leg in range(0, int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))):
                        direct_flights = []
                        legs_data = {}
                        total_flights_time += self.calculate_total_flights_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_stops_time += self.calculate_total_stops_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_mileage += self.calculate_total_mileage(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        legs_data['duration'] = json_data[first_index]['trips']['tripOption'][sec_index]['legs'][leg].get('duration')
                        legs_origin = self.origin_build(first_index, leg, sec_index)
                        legs_data['origin'] = legs_origin
                        legs_destination = self.destination_build(first_index, leg, sec_index)
                        legs_data['destination'] = legs_destination
                        self.airline_build(first_index, airlines, airlines_counter, list_of_airlines, sec_index)
                        legs_data['airlines'] = list_of_airlines
                        legs_data['stops'] = self.stops_build(first_index, leg, sec_index)
                        self.direct_flights_build(first_index, leg, direct_flights, sec_index)
                        legs_data['direct_flights'] = direct_flights

                        legs_list.append(legs_data)
                    calculations['total_flights_time'] = total_flights_time
                    calculations['total_drives_and_flights_time'] = self.total_flight_time_and_drive_time(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops'] = self.calculate_total_amount_of_stops(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops_time'] = total_stops_time
                    calculations['total_mileage'] = total_mileage
                    else_data['calculations'] = calculations
                    else_data['legs'] = legs_list
                    else_data['num_of_legs'] = int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))
                    else_data['travelers_details'] = travelers_details
                    else_flights.append(else_data)

        my_data['else'] = else_flights

    # Method that build the best data section (include puling data) in the json file that i create.
    def best_data(self, flight_id):
        best_flights = []
        flight_sorted_by_best = self.sort_by_shortest()
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                #for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                for specific_flight in range(5, 13):
                    best_data = {"price": None, "legs": None, "travelers_details": None, "num_of_legs": None, "calculations": None}
                    travelers_details = []
                    first_index = int(flight_sorted_by_best[specific_flight][1])
                    sec_index = int(flight_sorted_by_best[specific_flight][2])
                    calculations = {}
                    person_amount = 0
                    total_flights_time = 0
                    total_stops_time = 0
                    total_mileage = 0
                    airlines = {}
                    list_of_airlines = []
                    airlines_counter = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'adultCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("adult", person_amount))

                    person_amount = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'seniorCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("senior", person_amount))
                    best_data['flight_id'] = flight_id["id"]
                    best_data['price'] = json_data[first_index]['trips']['tripOption'][sec_index].get('saleTotalQpx')
                    flight_id["id"] += 1

                    legs_list = []
                    for leg in range(0, int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))):
                        direct_flights = []
                        legs_data = {}
                        total_flights_time += self.calculate_total_flights_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_stops_time += self.calculate_total_stops_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_mileage += self.calculate_total_mileage(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        legs_data['duration'] = json_data[first_index]['trips']['tripOption'][sec_index]['legs'][leg].get('duration')
                        legs_origin = self.origin_build(first_index, leg, sec_index)
                        legs_data['origin'] = legs_origin
                        legs_destination = self.destination_build(first_index, leg, sec_index)
                        legs_data['destination'] = legs_destination
                        self.airline_build(first_index, airlines, airlines_counter, list_of_airlines, sec_index)
                        legs_data['airlines'] = list_of_airlines
                        legs_data['stops'] = self.stops_build(first_index, leg, sec_index)
                        self.direct_flights_build(first_index, leg, direct_flights, sec_index)
                        legs_data['direct_flights'] = direct_flights

                        legs_list.append(legs_data)
                    calculations['total_flights_time'] = total_flights_time
                    calculations['total_drives_and_flights_time'] = self.total_flight_time_and_drive_time(json_data[flight]['trips']['tripOption'][sec_index])
                    calculations['total_stops'] = self.calculate_total_amount_of_stops(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops_time'] = total_stops_time
                    calculations['total_mileage'] = total_mileage
                    best_data['calculations'] = calculations
                    best_data['legs'] = legs_list
                    best_data['num_of_legs'] = int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))
                    best_data['travelers_details'] = travelers_details
                    best_flights.append(best_data)

        my_data['best'] = best_flights

    # Method that build the shortest data section (include puling data) in the json file that i create.
    def shortest_data(self, flight_id):
        shortest_flights = []
        flights_sorted_by_shortest = self.sort_by_shortest()
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                #for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                for specific_flight in range(0, 5):
                    shortest_data = {"price": None, "legs": None, "travelers_details": None, "num_of_legs": None, "calculations": None}
                    travelers_details = []
                    first_index = int(flights_sorted_by_shortest[specific_flight][1])
                    sec_index = int(flights_sorted_by_shortest[specific_flight][2])
                    calculations = {}
                    person_amount = 0
                    total_flights_time = 0
                    total_stops_time = 0
                    total_mileage = 0
                    airlines = {}
                    list_of_airlines = []
                    airlines_counter = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'adultCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("adult", person_amount))

                    person_amount = 0
                    person_amount = json_data[first_index]['trips']['tripOption'][sec_index]['passengersDetails'].get(
                        'seniorCount')
                    if(person_amount != 0):
                        travelers_details.append(self.travelers_details_build("senior", person_amount))
                    shortest_data['flight_id'] = flight_id["id"]
                    shortest_data['price'] = json_data[first_index]['trips']['tripOption'][sec_index].get('saleTotalQpx')
                    flight_id["id"] += 1

                    legs_list = []
                    for leg in range(0, int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))):
                        direct_flights = []
                        legs_data = {}
                        total_flights_time += self.calculate_total_flights_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_stops_time += self.calculate_total_stops_time(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        total_mileage += self.calculate_total_mileage(leg, json_data[first_index]['trips']['tripOption'][sec_index])
                        legs_data['duration'] = json_data[first_index]['trips']['tripOption'][sec_index]['legs'][leg].get('duration')
                        legs_origin = self.origin_build(first_index, leg, sec_index)
                        legs_data['origin'] = legs_origin
                        legs_destination = self.destination_build(first_index, leg, sec_index)
                        legs_data['destination'] = legs_destination
                        self.airline_build(first_index, airlines, airlines_counter, list_of_airlines, sec_index)
                        legs_data['airlines'] = list_of_airlines
                        legs_data['stops'] = self.stops_build(first_index, leg, sec_index)
                        self.direct_flights_build(first_index, leg, direct_flights, sec_index)
                        legs_data['direct_flights'] = direct_flights

                        legs_list.append(legs_data)
                    calculations['total_flights_time'] = total_flights_time
                    calculations['total_drives_and_flights_time'] = self.total_flight_time_and_drive_time(json_data[flight]['trips']['tripOption'][sec_index])
                    calculations['total_stops'] = self.calculate_total_amount_of_stops(json_data[first_index]['trips']['tripOption'][sec_index])
                    calculations['total_stops_time'] = total_stops_time
                    calculations['total_mileage'] = total_mileage
                    shortest_data['calculations'] = calculations
                    shortest_data['legs'] = legs_list
                    shortest_data['num_of_legs'] = int(json_data[first_index]['trips']['tripOption'][sec_index].get('number_of_legs'))
                    shortest_data['travelers_details'] = travelers_details
                    shortest_flights.append(shortest_data)

        my_data['shortest'] = shortest_flights

    def calculate_total_mileage(self, leg, current_flight):
        total_mileage = 0

        for part in range(0, len(current_flight['legs'][leg]['part'])):
            total_mileage += current_flight['legs'][leg]['part'][part]['leg_details'][0].get('mileage')

        return total_mileage

    #Method that calculate the total stops time for each full flight.
    def calculate_total_stops_time(self, leg, current_flight):
        total_stops_time = 0

        for part in range(0, len(current_flight['legs'][leg]['part'])-1):
            total_stops_time += current_flight['legs'][leg]['part'][part].get('connectionDuration')

        return total_stops_time

    #Method that caclulate the total amount of stops in each full flight.
    def calculate_total_amount_of_stops(self, current_flight):
        total_stops = 0

        for leg in range(0, len(current_flight['legs'])):
            total_stops += current_flight['legs'][leg].get('stops')

        return total_stops

    #Method that calculate the total flights time of each full flight.
    def calculate_total_flights_time(self, leg, current_flight):
        total_flights_time = 0

        for part in range(0, len(current_flight['legs'][leg]['part'])):
            total_flights_time += current_flight['legs'][leg]['part'][part].get('duration')

        return total_flights_time

    # Method that build the stops section for each flight that in the json file.
    def stops_build(self, flight, leg, specific_flight):
        stops = []
        #stops_counter = 0
        number_of_stops = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('stops')
        for stop in range(0, number_of_stops):
            stop_length = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][stop].get('connectionDuration')
            str_stop_length = str(stop_length)
            stop_place = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][stop]['leg_details'][0].get(
                'destination_airport')
            final_stop = stop_place + " " + str_stop_length
            stops.append(final_stop)
            #stops_counter += 1
        return stops

    # Method that build the direct flights section for each flight that in the json file.
    def direct_flights_build(self, flight, leg, direct_flights, specific_flight):
        part_length = len(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'])
        for dire_flight in range(0, part_length):
            direct_flight = {}
            details = {}
            direct_flight['arrival_airport'] = \
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get(
                'destination_airport')
            direct_flight['departure_airport'] = \
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get(
                'origin_airport')
            dep_date = self.get_time_and_date(
                json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get(
                    'departureTime'))
            arr_date = self.get_time_and_date(
                json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get(
                    'arrivalTime'))
            direct_flight['departure_date'] = dep_date[0]
            direct_flight['departure_time'] = dep_date[1].split("+")[0]
            direct_flight['arrival_date'] = arr_date[0]
            direct_flight['arrival_time'] = arr_date[1].split("+")[0]
            direct_flight['departure_airport_code'] = \
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get('origin')
            direct_flight['arrival_airport_code'] = \
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get(
                'destination')
            direct_flight['flight_num'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight][
                'flight'].get('number')
            direct_flight['flight_duration'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][
                dire_flight].get('duration')
            direct_flight['plane_type'] = None
            direct_flight['departure_airport_city'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get('origin_airport')
            direct_flight['arrival_airport_city'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][0].get('destination_airport')
            direct_flight['stop_length'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][
                dire_flight].get('connectionDuration')
            direct_flight['airline'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight][
                'flight'].get('name')
            details['meal'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight]['leg_details'][
                0].get('meal')
            details['class'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][dire_flight].get('cabin')
            direct_flight['details'] = details
            direct_flights.append(direct_flight)

    # Method that build the airlines section for each flight that in the json file.
    def airline_build(self, flight, airlines, airlines_counter, list_of_airlines, specific_flight):
        for leg in range(0, int(json_data[flight]['trips']['tripOption'][specific_flight].get('number_of_legs'))):
            part_iterator = len(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'])
            for carrier in range(0, part_iterator):
                airline = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][carrier]['flight'].get('name')
                if airline not in airlines.values():
                    str_counter = str(airlines_counter)
                    airlines[str_counter] = airline
                    list_of_airlines.append(airline)
                    airlines_counter += 1

    # Method that build the destination section for each flight that in the json file.
    def destination_build(self, flight, leg, specific_flight):
        destination = {}
        length = len(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'])
        state = self.get_state(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('destinationAddress')
                          , json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][length - 1]['leg_details'][
                              0].get('destination'))

        destination['state'] = state
        destination['distance'] = 0
        last_leg = len(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part']) - 1
        destination['airport'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][last_leg]['leg_details'][
            0].get('destination_airport')
        destination['airport_code'] = state.split(" ")[1]
        destination['drive_time'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('timeFromAirport')
        date_and_time = self.get_time_and_date(
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get('arrivalTime'))
        if (date_and_time[1].find("+") == -1):
            destination['time'] = date_and_time[1].split('-')[0]
        else:
            destination['time'] = date_and_time[1].split('+')[0]
        destination['date'] = date_and_time[0]
        destination['address'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('destinationAddress')
        destination['drive_distance'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('distanceFromAirport')

        return destination

    # Method that build the origin section for each flight that in the json file.
    def origin_build(self, flight, leg, specific_flight):
        origin = {}
        state = self.get_state(json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('originAddress')
                          , json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
                'origin'))

        origin['state'] = state
        origin['distance'] = 0
        origin['airport_code'] = state.split(' ')[1]
        origin['airport'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get(
            'origin_airport')
        origin['drive_time'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('timeToAirport')
        date_and_time = self.get_time_and_date(
            json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg]['part'][0]['leg_details'][0].get('departureTime'))
        if (date_and_time[1].find("+") == -1):
            origin['time'] = date_and_time[1].split('-')[0]
        else:
            origin['time'] = date_and_time[1].split('+')[0]
        origin['date'] = date_and_time[0]
        origin['address'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('originAddress')
        origin['drive_distance'] = json_data[flight]['trips']['tripOption'][specific_flight]['legs'][leg].get('distanceToAirport')

        return origin

    # Method that puling the state data from the old json file.
    def get_state(self, state_data, city_data):
        comma_counter = state_data.count(",")
        state = state_data.split(", ")[comma_counter] + " " + city_data
        return state

    # Method that get the flight time and date (for full flight or leg).
    def get_time_and_date(self, time_data):
        time_and_date = time_data.split('T')
        return time_and_date

    # # Here im writing to the new json file all the data that i build in this program.
    # def writing_to_the_file(self):
    #     with open('C:\\Users\\tal\\Desktop\\client_json.json', 'w') as json_file:
    #         self.cheapest_data(flight_id)
    #         #self.else_data(flight_id)
    #         #self.best_data(flight_id)
    #         #self.shortest_data(flight_id)
    #         json.dump(my_data, json_file, indent=4, sort_keys=True)
    #
    #     json_file.close()


def main():
    import Creating_json_file_for_client_side
    import views as vs
    client_json = Creating_json_file_for_client_side.clientJson()
    client_json.cheapest_data(flight_id)
    client_json.else_data(flight_id)
    client_json.best_data(flight_id)
    client_json.shortest_data(flight_id)
    my_data['hotels'] = vs.set_hotels()

if __name__ == "__main__":
    main()
