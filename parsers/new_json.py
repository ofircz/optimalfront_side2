import json
from operator import itemgetter

json_data = json.loads(open("c:\\vtoc\\w\\jd.txt").read())
list_flight = []


class clientJson():
    group_reference = {'best_price': None, 'best_flight_time': None, 'best_travel_time': None,'best_flight_and_travel_time': None,
                       'best_duration_stop': None, 'best_amount_stop': None, 'best_differnce_favorite_time': None,
                       'highest_miles': None}
    group_steps = {'price_step_percent': None, 'price_step_amount': None,
                   'flight_time_step_percent': None, 'flight_time_step_amount': None,
                   'travel_time_step_percent': None, 'travel_time_step_amount': None,
                   'flight_and_travel_time_step_percent': None, 'flight_and_travel_time_step_amount': None,
                   'duration_stop_step_percent': None, 'duration_stop_step_amount': None,
                   'amount_stop_step_percent': None,'amount_stop_step_amount': None,
                   'difference_favorite_time_step_percent': None, 'differnce_favorite_time_step_amount': None,
                   'mileage_step_percent': None, 'mileage_step_amount': None}

    def assignment_group_steps(self):
        self.group_steps['price_step_percent'] = 5
        self.group_steps['price_step_amount'] = 30
        self.group_steps['flight_time_step_percent'] = 5
        self.group_steps['flight_time_step_amount'] = 30
        self.group_steps['travel_time_step_percent'] = 5
        self.group_steps['travel_time_step_amount'] = 30
        self.group_steps['flight_and_travel_time_step_percent'] = 5
        self.group_steps['flight_and_travel_time_step_amount'] = 30
        self.group_steps['duration_stop_step_percent'] = 5
        self.group_steps['duration_stop_step_amount'] = 30
        self.group_steps['amount_stop_step_percent'] = 50
        self.group_steps['amount_stop_step_amount'] = 1
        self.group_steps['difference_favorite_time_step_percent'] = 5
        self.group_steps['difference_favorite_time_step_amount'] = 20
        self.group_steps['mileage_step_percent'] = 5
        self.group_steps['mileage_step_amount'] = 30

    def assigment_cheapest_weights(self):
        const = 10
        list_weights = []
        cheapest_weights1 = {}
        cheapest_weights2 = {}
        cheapest_weights3 = {}
        cheapest_weights4 = {}

        #cheapest combination 1-4
        cheapest_weights1['alg_type'] = 'cheapest'
        cheapest_weights1['alg_name'] = 'cheapest1'
        cheapest_weights1['num_of_flights'] = 3
        cheapest_weights1['weight_price'] = 60
        cheapest_weights1['weight_duration_flight_and_travel_time'] = 10
        cheapest_weights1['weight_duration_travel_time'] = 5
        cheapest_weights1['weight_amount_stop'] = 10
        cheapest_weights1['weight_duration_stop_time'] = 5
        cheapest_weights1['gr_price_max_num'] = const
        cheapest_weights1['gr_duration_flight_time_and_travel_time_max_num'] = const
        cheapest_weights1['gr_duration_travel_time_max_num'] = const
        cheapest_weights1['gr_duration_flight_time_max_num'] = const
        cheapest_weights1['gr_mileage_max_num'] = const
        cheapest_weights1['gr_amount_stop_max_num'] = const
        cheapest_weights1['gr_duration_stop_time_max_num'] = const
        list_weights.append(cheapest_weights1)

        cheapest_weights2['alg_type'] = 'cheapest'
        cheapest_weights2['alg_name'] = 'cheapest2'
        cheapest_weights2['num_of_flights'] = 3
        cheapest_weights2['weight_price'] = 90
        cheapest_weights2['weight_duration_flight_and_travel_time'] = 10
        cheapest_weights2['weight_duration_travel_time'] = 0
        cheapest_weights2['weight_amount_stop'] = 0
        cheapest_weights2['weight_duration_stop_time'] = 0
        cheapest_weights2['gr_price_max_num'] = const
        cheapest_weights2['gr_duration_flight_time_and_travel_time_max_num'] = const
        cheapest_weights2['gr_duration_travel_time_max_num'] = const
        cheapest_weights2['gr_duration_flight_time_max_num'] = const
        cheapest_weights2['gr_mileage_max_num'] = const
        cheapest_weights2['gr_amount_stop_max_num'] = const
        cheapest_weights2['gr_duration_stop_time_max_num'] = const
        list_weights.append(cheapest_weights2)

        cheapest_weights3['alg_type'] = 'cheapest'
        cheapest_weights3['alg_name'] = 'cheapest3'
        cheapest_weights3['num_of_flights'] = 3
        cheapest_weights3['weight_price'] = 70
        cheapest_weights3['weight_duration_flight_and_travel_time'] = 20
        cheapest_weights3['weight_duration_travel_time'] = 0
        cheapest_weights3['weight_amount_stop'] = 10
        cheapest_weights3['weight_duration_stop_time'] = 0
        cheapest_weights3['gr_price_max_num'] = const
        cheapest_weights3['gr_duration_flight_time_and_travel_time_max_num'] = const
        cheapest_weights3['gr_duration_travel_time_max_num'] = const
        cheapest_weights3['gr_duration_flight_time_max_num'] = const
        cheapest_weights3['gr_mileage_max_num'] = const
        cheapest_weights3['gr_amount_stop_max_num'] = const
        cheapest_weights3['gr_duration_stop_time_max_num'] = const
        list_weights.append(cheapest_weights3)

        cheapest_weights4['alg_type'] = 'cheapest'
        cheapest_weights4['alg_name'] = 'cheapest4'
        cheapest_weights4['num_of_flights'] = 3
        cheapest_weights4['weight_price'] = 60
        cheapest_weights4['weight_duration_flight_and_travel_time'] = 0
        cheapest_weights4['weight_duration_travel_time'] = 0
        cheapest_weights4['weight_amount_stop'] = 20
        cheapest_weights4['weight_duration_stop_time'] = 0
        cheapest_weights4['gr_price_max_num'] = const
        cheapest_weights4['gr_duration_flight_time_and_travel_time_max_num'] = const
        cheapest_weights4['gr_duration_travel_time_max_num'] = const
        cheapest_weights4['gr_duration_flight_time_max_num'] = const
        cheapest_weights4['gr_mileage_max_num'] = const
        cheapest_weights4['gr_amount_stop_max_num'] = const
        cheapest_weights4['gr_duration_stop_time_max_num'] = const
        list_weights.append(cheapest_weights4)

        return list_weights

    def assigment_best_weights(self):
        const = 100
        list_weights = []
        best_weights1 = {}
        best_weights2 = {}
        best_weights3 = {}
        best_weights4 = {}

        #best combination 1-4
        best_weights1['alg_type'] = 'best'
        best_weights1['alg_name'] = 'best1'
        best_weights1['num_of_flights'] = 3
        best_weights1['weight_price'] = 40
        best_weights1['weight_duration_flight_and_travel_time'] = 40
        best_weights1['weight_duration_travel_time'] = 5
        best_weights1['weight_amount_stop'] = 10
        best_weights1['weight_duration_stop_time'] = 5
        best_weights1['gr_price_max_num'] = const
        best_weights1['gr_duration_flight_time_and_travel_time_max_num'] = const
        best_weights1['gr_duration_travel_time_max_num'] = const
        best_weights1['gr_duration_flight_time_max_num'] = const
        best_weights1['gr_mileage_max_num'] = const
        best_weights1['gr_amount_stop_max_num'] = const
        best_weights1['gr_duration_stop_time_max_num'] = const
        list_weights.append(best_weights1)

        best_weights2['alg_type'] = 'best'
        best_weights2['alg_name'] = 'best2'
        best_weights2['num_of_flights'] = 3
        best_weights2['weight_price'] = 50
        best_weights2['weight_duration_flight_and_travel_time'] = 30
        best_weights2['weight_duration_travel_time'] = 0
        best_weights2['weight_amount_stop'] = 10
        best_weights2['weight_duration_stop_time'] = 10
        best_weights2['gr_price_max_num'] = const
        best_weights2['gr_duration_flight_time_and_travel_time_max_num'] = const
        best_weights2['gr_duration_travel_time_max_num'] = const
        best_weights2['gr_duration_flight_time_max_num'] = const
        best_weights2['gr_mileage_max_num'] = const
        best_weights2['gr_amount_stop_max_num'] = const
        best_weights2['gr_duration_stop_time_max_num'] = const
        list_weights.append(best_weights2)

        best_weights3['alg_type'] = 'best'
        best_weights3['alg_name'] = 'best3'
        best_weights3['num_of_flights'] = 3
        best_weights3['weight_price'] = 60
        best_weights3['weight_duration_flight_and_travel_time'] = 10
        best_weights3['weight_duration_travel_time'] = 0
        best_weights3['weight_amount_stop'] = 10
        best_weights3['weight_duration_stop_time'] = 20
        best_weights3['gr_price_max_num'] = const
        best_weights3['gr_duration_flight_time_and_travel_time_max_num'] = const
        best_weights3['gr_duration_travel_time_max_num'] = const
        best_weights3['gr_duration_flight_time_max_num'] = const
        best_weights3['gr_mileage_max_num'] = const
        best_weights3['gr_amount_stop_max_num'] = const
        best_weights3['gr_duration_stop_time_max_num'] = const
        list_weights.append(best_weights3)

        best_weights4['alg_type'] = 'best'
        best_weights4['alg_name'] = 'best4'
        best_weights4['num_of_flights'] = 3
        best_weights4['weight_price'] = 30
        best_weights4['weight_duration_flight_and_travel_time'] = 30
        best_weights4['weight_duration_travel_time'] = 0
        best_weights4['weight_amount_stop'] = 10
        best_weights4['weight_duration_stop_time'] = 30
        best_weights4['gr_price_max_num'] = const
        best_weights4['gr_duration_flight_time_and_travel_time_max_num'] = const
        best_weights4['gr_duration_travel_time_max_num'] = const
        best_weights4['gr_duration_flight_time_max_num'] = const
        best_weights4['gr_mileage_max_num'] = const
        best_weights4['gr_amount_stop_max_num'] = const
        best_weights4['gr_duration_stop_time_max_num'] = const
        list_weights.append(best_weights4)

        return list_weights

    def assigment_shrtest_weights(self):
        const = 100
        list_weights = []
        shortest_weights1 = {}
        shortest_weights2 = {}
        shortest_weights3 = {}
        shortest_weights4 = {}

        shortest_weights1['alg_type'] = 'shortest'
        shortest_weights1['alg_name'] = 'shortest1'
        shortest_weights1['num_of_flights'] = 3
        shortest_weights1['weight_price'] = 60
        shortest_weights1['weight_duration_flight_and_travel_time'] = 10
        shortest_weights1['weight_duration_travel_time'] = 5
        shortest_weights1['weight_amount_stop'] = 10
        shortest_weights1['weight_duration_stop_time'] = 5
        shortest_weights1['gr_price_max_num'] = const
        shortest_weights1['gr_duration_flight_time_and_travel_time_max_num'] = const
        shortest_weights1['gr_duration_travel_time_max_num'] = const
        shortest_weights1['gr_duration_flight_time_max_num'] = const
        shortest_weights1['gr_mileage_max_num'] = const
        shortest_weights1['gr_amount_stop_max_num'] = const
        shortest_weights1['gr_duration_stop_time_max_num'] = const
        list_weights.append(shortest_weights1)

        shortest_weights2['alg_type'] = 'shortest'
        shortest_weights2['alg_name'] = 'shortest2'
        shortest_weights2['num_of_flights'] = 3
        shortest_weights2['weight_price'] = 90
        shortest_weights2['weight_duration_flight_and_travel_time'] = 10
        shortest_weights2['weight_duration_travel_time'] = 0
        shortest_weights2['weight_amount_stop'] = 0
        shortest_weights2['weight_duration_stop_time'] = 0
        shortest_weights2['gr_price_max_num'] = const
        shortest_weights2['gr_duration_flight_time_and_travel_time_max_num'] = const
        shortest_weights2['gr_duration_travel_time_max_num'] = const
        shortest_weights2['gr_duration_flight_time_max_num'] = const
        shortest_weights2['gr_mileage_max_num'] = const
        shortest_weights2['gr_amount_stop_max_num'] = const
        shortest_weights2['gr_duration_stop_time_max_num'] = const
        list_weights.append(shortest_weights2)

        shortest_weights3['alg_type'] = 'shortest'
        shortest_weights3['alg_name'] = 'shortest3'
        shortest_weights3['num_of_flights'] = 3
        shortest_weights3['weight_price'] = 70
        shortest_weights3['weight_duration_flight_and_travel_time'] = 20
        shortest_weights3['weight_duration_travel_time'] = 0
        shortest_weights3['weight_amount_stop'] = 10
        shortest_weights3['weight_duration_stop_time'] = 0
        shortest_weights3['gr_price_max_num'] = const
        shortest_weights3['gr_duration_flight_time_and_travel_time_max_num'] = const
        shortest_weights3['gr_duration_travel_time_max_num'] = const
        shortest_weights3['gr_duration_flight_time_max_num'] = const
        shortest_weights3['gr_mileage_max_num'] = const
        shortest_weights3['gr_amount_stop_max_num'] = const
        shortest_weights3['gr_duration_stop_time_max_num'] = const
        list_weights.append(shortest_weights3)

        shortest_weights4['alg_type'] = 'shortest'
        shortest_weights4['alg_name'] = 'shortest4'
        shortest_weights4['num_of_flights'] = 3
        shortest_weights4['weight_price'] = 60
        shortest_weights4['weight_duration_flight_and_travel_time'] = 0
        shortest_weights4['weight_duration_travel_time'] = 0
        shortest_weights4['weight_amount_stop'] = 20
        shortest_weights4['weight_duration_stop_time'] = 0
        shortest_weights4['gr_price_max_num'] = const
        shortest_weights4['gr_duration_flight_time_and_travel_time_max_num'] = const
        shortest_weights4['gr_duration_travel_time_max_num'] = const
        shortest_weights4['gr_duration_flight_time_max_num'] = const
        shortest_weights4['gr_mileage_max_num'] = const
        shortest_weights4['gr_amount_stop_max_num'] = const
        shortest_weights4['gr_duration_stop_time_max_num'] = const
        list_weights.append(shortest_weights4)

        return list_weights

    # Method that calculate total flight and drive time per flight.
    def total_flight_time_and_drive_time(self, current_flight):
        number_of_legs = int(current_flight.get("number_of_legs"))
        total_flight_with_drive_time = 0
        for leg in range(0,number_of_legs):
            total_flight_with_drive_time += current_flight["legs"][leg].get("durationWithDrive")
        return total_flight_with_drive_time

    # Method that calculate all the shortest flights in the data and return their indexes
    def sort_by_shortest(self):
        temp_shortest_flights = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) !=0:
                for specific_flight in range(0, len(json_data[flight]["trips"]["tripOption"])):
                    temp_flight_and_drive_time = []
                    flight_drive_time = self.total_flight_time_and_drive_time(json_data[flight]["trips"]["tripOption"][specific_flight])
                    temp_flight_and_drive_time.append(flight_drive_time)
                    temp_flight_and_drive_time.append(str(flight))
                    temp_flight_and_drive_time.append(str(specific_flight))
                    temp_shortest_flights.append(temp_flight_and_drive_time)
        shortest_flights = sorted(temp_shortest_flights, key = itemgetter(0))
        return shortest_flights

    # Method that calculate the total flights time of each full flight.
    def calculate_total_flights_time(self, leg, current_flight):
        total_flights_time = 0
        for part in range(0,len(current_flight["legs"][leg]["part"])):
            total_flights_time+= current_flight["legs"][leg]["part"][part].get("duration")
        return total_flights_time

    # Method that calculate the total stops time for each full flight.
    def calculate_total_stops_time(self, leg, current_flight):
        total_stops_time = 0
        for part in range(0, len(current_flight["legs"][leg]["part"])-1):
            total_stops_time += current_flight["legs"][leg]["part"][part].get("connectionDuration")
        return total_stops_time

    # Method that caclulate the total amount of stops in each full flight.
    def calculate_total_amount_of_stops(self, current_flight):
        total_stops = 0
        for leg in range(0, len(current_flight["legs"])):
            total_stops += current_flight["legs"][leg].get("stops")
        return total_stops

    def calculate_total_mileage(self, leg, current_flight):
        total_mileage = 0
        for part in range(0, len(current_flight["legs"][leg]["part"])):
            total_mileage += current_flight["legs"][leg]["part"][part]["leg_details"][0].get("mileage")
        return total_mileage

    # Method that puling the state data from the old json file.
    def get_state(self, state_data, city_data):
        comma_counter = state_data.count(",")
        state = state_data.split(", ")[comma_counter] + " " + city_data
        return state

    # Method that get the flight time and date (for full flight or leg).
    def get_time_and_date(self, time_data):
        time_and_date = time_data.split("T")
        return time_and_date

    # Method that build the origin section for each flight that in the json file.
    def origin_build(self, flight, leg, specific_flight):
        origin = {}
        state = self.get_state(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("originAddress"),
                                json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][0]["leg_details"][0].get("origin"))
        origin["state"] = state
        origin["distance"] = 0
        origin["airport_code"] = state.split(" ")[1]
        origin["airport"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][0]["leg_details"][0].get("origin_airport")
        origin["drive_time"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("timeToAirport")
        date_and_time = self.get_time_and_date(
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][0]["leg_details"][0].get("departureTime"))
        if (date_and_time[1].find("+") == -1):
            origin["time"] = date_and_time[1].split("-")[0]
        else:
            origin["time"] = date_and_time[1].split("+")[0]
        origin["date"] = date_and_time[0]
        origin["address"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("originAddress")
        origin["drive_distance"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("distanceToAirport")
        return origin

    # Method that build the destination section for each flight that in the json file.
    def destination_build(self, flight, leg, specific_flight):
        destination = {}
        length = len(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"])
        state = self.get_state(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("destinationAddress")
                          , json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][length - 1]["leg_details"][
                              0].get("destination"))

        destination["state"] = state
        destination["distance"] = 0
        last_leg = len(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"]) - 1
        destination["airport"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][last_leg]["leg_details"][
            0].get("destination_airport")
        destination["airport_code"] = state.split(" ")[1]
        destination["drive_time"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("timeFromAirport")
        date_and_time = self.get_time_and_date(
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][0]["leg_details"][0].get("arrivalTime"))
        if (date_and_time[1].find("+") == -1):
            destination["time"] = date_and_time[1].split("-")[0]
        else:
            destination["time"] = date_and_time[1].split("+")[0]
        destination["date"] = date_and_time[0]
        destination["address"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("destinationAddress")
        destination["drive_distance"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("distanceFromAirport")

        return destination

    # Method that build the airlines section for each flight that in the json file.
    def airline_build(self, flight, airlines, airlines_counter, list_of_airlines, specific_flight, leg):
        part_iterator = len(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"])
        for carrier in range(0, part_iterator):
            airline = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][carrier]["flight"].get("name")
            if airline not in airlines.values():
                str_counter = str(airlines_counter)
                airlines[str_counter] = airline
                list_of_airlines.append(airline)
                airlines_counter += 1

    # Method that build the direct flights section for each flight that in the json file.
    def direct_flights_build(self, flight, leg, direct_flights, specific_flight):
        part_length = len(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"])
        for dire_flight in range(0, part_length):
            direct_flight = {}
            details = {}
            direct_flight["arrival_airport"] = \
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get(
                "destination_airport")
            direct_flight["departure_airport"] = \
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get(
                "origin_airport")
            dep_date = self.get_time_and_date(
                json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get(
                    "departureTime"))
            arr_date = self.get_time_and_date(
                json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get(
                    "arrivalTime"))
            direct_flight["departure_date"] = dep_date[0]
            direct_flight["departure_time"] = dep_date[1].split("+")[0]
            direct_flight["arrival_date"] = arr_date[0]
            direct_flight["arrival_time"] = arr_date[1].split("+")[0]
            direct_flight["departure_airport_code"] = \
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get("origin")
            direct_flight["arrival_airport_code"] = \
            json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get(
                "destination")
            direct_flight["flight_num"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight][
                "flight"].get("number")
            direct_flight["flight_duration"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][
                dire_flight].get("duration")
            direct_flight["plane_type"] = None
            direct_flight["departure_airport_city"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get("origin_airport")
            direct_flight["arrival_airport_city"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][0].get("destination_airport")
            direct_flight["stop_length"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][
                dire_flight].get("connectionDuration")
            direct_flight["airline"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight][
                "flight"].get("name")
            details["meal"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight]["leg_details"][
                0].get("meal")
            details["class"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg]["part"][dire_flight].get("cabin")
            direct_flight["details"] = details
            direct_flights.append(direct_flight)

    # Method that return a duration flight time and travel time specific
    def total_duration_flight_time_and_travel_time(self, current_flight):
        number_of_legs = int(current_flight.get('number_of_legs'))
        total_flight_with_drive_time = 0
        for leg in range(0, number_of_legs):
            temp_time_and_drive = int(current_flight['legs'][leg].get('durationWithDrive'))
            total_flight_with_drive_time += temp_time_and_drive

        return total_flight_with_drive_time

    # Method that return a duration of time travel specific
    def total_duration_travel_time(self, current_flight):
        number_of_legs = int(current_flight.get('number_of_legs'))
        total_drive_time = 0
        for leg in range(0, number_of_legs):
            temp_drive_time = int(current_flight['legs'][leg].get('durationWithDrive'))- int(current_flight['legs'][leg].get('duration'))
            total_drive_time += temp_drive_time
        return total_drive_time

    # Method that sorts all flights and returns an array of price from low to high,ID external and ID internal.
    def sort_by_price(self):
        temp_cheapest_flights = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    flight_price_and_id = []
                    price = float(json_data[flight]['trips']['tripOption'][specific_flight].get('saleTotalQpx').split('USD')[1])
                    flight_price_and_id.append(price)
                    flight_price_and_id.append(str(flight))
                    flight_price_and_id.append(str(specific_flight))
                    temp_cheapest_flights.append(flight_price_and_id)
        cheapest_flights = sorted(temp_cheapest_flights, key=itemgetter(0))
        self.group_reference['best_price']= cheapest_flights[0][0]
        return cheapest_flights

    # Method that sorts all flights and returns an array of duration_flight time and travel time from low to high,ID external and ID internal.
    def sort_by_duration_flight_time_and_travel_time(self):
        temp_shortest_flights = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_flight_and_drive_time = []
                    flight_drive_time = self.total_duration_flight_time_and_travel_time(json_data[flight]['trips']['tripOption'][specific_flight])
                    temp_flight_and_drive_time.append(flight_drive_time)
                    temp_flight_and_drive_time.append(str(flight))
                    temp_flight_and_drive_time.append(str(specific_flight))
                    temp_shortest_flights.append(temp_flight_and_drive_time)

        shortest_flights = sorted(temp_shortest_flights, key=itemgetter(0))
        self.group_reference['best_flight_and_travel_time']= shortest_flights[0][0]
        return shortest_flights

    # Method that sorts all flights and returns an array of duration travel time from low to high,ID external and ID internal.
    def sort_by_duration_travel_time(self):
        temp_shortest_flights = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_drive_time = []
                    drive_time = self.total_duration_travel_time(json_data[flight]['trips']['tripOption'][specific_flight])
                    temp_drive_time.append(drive_time)
                    temp_drive_time.append(str(flight))
                    temp_drive_time.append(str(specific_flight))
                    temp_shortest_flights.append(temp_drive_time)
        shortest_drivess = sorted(temp_shortest_flights, key=itemgetter(0))
        self.group_reference['best_travel_time']= shortest_drivess[0][0]
        return shortest_drivess

    # Method that sorts all flights and returns an array of flight_time_duration from low to high,ID external and ID internal.
    def sort_by_duration_flight_time(self):
        temp_shortest_flights = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_flight_time = []
                    flight_time = 0
                    for leg in range(0,int(json_data[flight]['trips']['tripOption'][specific_flight].get('number_of_legs'))):
                        flight_time += self.calculate_total_flights_time(leg, json_data[flight]['trips']['tripOption'][specific_flight])

                    temp_flight_time.append(flight_time)
                    temp_flight_time.append(str(flight))
                    temp_flight_time.append(str(specific_flight))
                    temp_shortest_flights.append(temp_flight_time)
        shortest_flights = sorted(temp_shortest_flights, key=itemgetter(0))
        self.group_reference['best_flight_time'] = shortest_flights[0][0]
        return shortest_flights

    # Method returns miles per all legs.
    def miles_all_legs(self, current_flight):
        number_of_legs = int(current_flight.get('number_of_legs'))
        total_miles_in_all_legs = 0
        for leg in range(0, number_of_legs):
            number_of_parts = len(current_flight['legs'][leg]['part'])
            for part in range(0, number_of_parts):
                temp_miles_in_all_legs = int(current_flight['legs'][leg]['part'][part]['leg_details'][0].get('mileage'))
                total_miles_in_all_legs += temp_miles_in_all_legs

        return total_miles_in_all_legs

    # Method that sorts all flights and returns an array of duration travel time from low to high,ID external and ID internal.
    def sort_by_miles(self):
        miles = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_miles = []
                    miles_value = 0
                    miles_value += self.miles_all_legs(json_data[flight]['trips']['tripOption'][specific_flight])
                    temp_miles.append(miles_value)
                    temp_miles.append(str(flight))
                    temp_miles.append(str(specific_flight))
                    miles.append(temp_miles)
        total_miles = sorted(miles, key=itemgetter(0),reverse=True)
        self.group_reference['highest_miles']= total_miles[0][0]
        return total_miles

    # Method that sorts all flights and returns an array of amount stop from low to high,ID external and ID internal.
    def sort_by_amount_stop(self):
        amount_stop = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_amount_stop = []
                    amount_stop_value = self.calculate_total_amount_of_stops(json_data[flight]['trips']['tripOption'][specific_flight])
                    temp_amount_stop.append(amount_stop_value)
                    temp_amount_stop.append(str(flight))
                    temp_amount_stop.append(str(specific_flight))
                    amount_stop.append(temp_amount_stop)
        total_amount_stop = sorted(amount_stop, key=itemgetter(0))
        self.group_reference['best_amount_stop'] = total_amount_stop[0][0]
        return  total_amount_stop

    # Method that sorts all flights and returns an array of duration stop time from low to high,ID external and ID internal.
    def sort_by_duration_stop(self):
        duration_stop = []
        for flight in range(0, len(json_data)):
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]['trips']['tripOption'])):
                    temp_duration_stop_time = []
                    duration_stop_time_value = 0
                    for leg in range(0,int(json_data[flight]['trips']['tripOption'][specific_flight].get('number_of_legs'))):
                        duration_stop_time_value += self.calculate_total_stops_time(leg,json_data[flight]['trips']['tripOption'][specific_flight])
                    temp_duration_stop_time.append(duration_stop_time_value)
                    temp_duration_stop_time.append(str(flight))
                    temp_duration_stop_time.append(str(specific_flight))
                    duration_stop.append(temp_duration_stop_time)
        total_duration_stop = sorted(duration_stop, key=itemgetter(0))
        self.group_reference['best_duration_stop']= total_duration_stop[0][0]
        return total_duration_stop

    def finish_grade_after_crop(self, combining_weights, list_flights):
        flight_for_client = []
        flights_numbers = []
        for weights in range(0, len(combining_weights)):
            combining_flights_and_weights = []
            combining_best_flights_and_weights = []
            for flight in range(0, len(list_flights)):
                temp_weight = list_flights[flight]['groups'].get('group_by_price') * \
                                  combining_weights[weights]['weight_price'] +\
                                  list_flights[flight]['groups'].get('group_by_duration_flight_time_and_travel_time') *\
                                  combining_weights[weights]['weight_duration_flight_and_travel_time'] + \
                                  list_flights[flight]['groups'].get('group_by_duration_travel_time') *\
                                  combining_weights[weights]['weight_duration_travel_time'] + \
                                  list_flights[flight]['groups'].get('group_by_amount_stop') *\
                                  combining_weights[weights]['weight_amount_stop'] + \
                                  list_flights[flight]['groups'].get('group_by_duration_stop_time') * \
                                  combining_weights[weights]['weight_duration_stop_time']

                temp_grade = []
                temp_grade.append(list_flights[flight].get('flight_id'))
                temp_grade.append(temp_weight)
                temp_grade.append(combining_weights[weights].get('alg_name'))
                combining_flights_and_weights.append(temp_grade)
                if (list_flights[flight]['groups'].get('group_by_price') <= combining_weights[weights]['gr_price_max_num'] and
                    list_flights[flight]['groups'].get('group_by_duration_flight_time_and_travel_time') <= combining_weights[weights]['gr_duration_flight_time_and_travel_time_max_num'] and
                    list_flights[flight]['groups'].get('group_by_duration_travel_time') <= combining_weights[weights]['gr_duration_travel_time_max_num'] and
                    list_flights[flight]['groups'].get('group_by_duration_flight_time') <= combining_weights[weights]['gr_duration_flight_time_max_num'] and
                    list_flights[flight]['groups'].get('group_by_mileage') <= combining_weights[weights]['gr_mileage_max_num'] and
                    list_flights[flight]['groups'].get('group_by_amount_stop') <= combining_weights[weights]['gr_amount_stop_max_num'] and
                    list_flights[flight]['groups'].get('group_by_duration_stop_time') <= combining_weights[weights]['gr_duration_stop_time_max_num']):
                    temp_best_grade = []
                    temp_best_grade.append(list_flights[flight].get('flight_id'))
                    temp_best_grade.append(temp_weight)
                    temp_best_grade.append(combining_weights[weights].get('alg_name'))
                    combining_best_flights_and_weights.append(temp_best_grade)
            if len(combining_best_flights_and_weights) != 0:
                sorted_combining_flights_and_weights = sorted(combining_best_flights_and_weights, key=itemgetter(1))
            else:
                sorted_combining_flights_and_weights = sorted(combining_flights_and_weights, key=itemgetter(1))
            cropped_combining_flights_and_weights = self.crop_by_absolut(sorted_combining_flights_and_weights, flights_numbers, combining_weights[weights].get('num_of_flights'))
            flight_for_client.append(cropped_combining_flights_and_weights)

        return flight_for_client

    def concat_flights(self):
        concatenation = []
        for flight in range(0, len(list_flight)):
            for specific_flight in range(0, len(list_flight[flight])):
                concatenation.append(list_flight[flight][specific_flight])
        return concatenation

    def genery_sort(self, list_flights, causes_maine, number_return_flights, calaulations=False, reverse=False):
        temp_flights = []
        for flight in range(0, len(list_flights)):
            flight_data = []
            if calaulations is True:

                data = list_flights[flight]['calculations'].get(causes_maine)
            else:
                data = list_flights[flight].get(causes_maine)

            flight_data.append(list_flights[flight])
            flight_data.append(data)
            temp_flights.append(flight_data)
        sorted_flights = sorted(temp_flights, key=itemgetter(1), reverse=reverse)
        return_flights = []
        for i in range(0, number_return_flights):
            return_flights.append(sorted_flights[i][0])
        return return_flights

    def sort_and_crop(self):
        shortest_flights = self.genery_sort(self.concat_flights(), 'total_drives_and_flights_time', 150, calaulations=True)
        mileage_flights = self.genery_sort(shortest_flights, 'total_mileage', 100, calaulations=True)
        cheapest_flights = self.genery_sort(mileage_flights, 'price', 50)

        selected_flights = {"cheapest": None, "best": None, "shortest": None}

        combining_weights = self.assigment_cheapest_weights()
        cheapest_flight_for_client = self.finish_grade_after_crop(combining_weights, cheapest_flights)
        selected_flights["cheapest"] = self.find_flights(cheapest_flight_for_client)

        combining_weights = self.assigment_best_weights()
        best_flight_for_client = self.finish_grade_after_crop(combining_weights, cheapest_flights)
        selected_flights["best"] = self.find_flights(best_flight_for_client)

        combining_weights = self.assigment_shrtest_weights()
        shortest_flight_for_client = self.finish_grade_after_crop(combining_weights, cheapest_flights)
        selected_flights["shortest"] = self.find_flights(shortest_flight_for_client)

        dump = json.dumps(selected_flights)

        opened_file = open('c:\\vtoc\\w\\print_crop_flights.txt', 'w')
        opened_file.write(str(dump))
        opened_file.close()

    # Method that calculate scores by jumping amount or percent for every flight
    def calculate_groups(self, list_flights):
        self.assignment_group_steps()

        sorted_by_price = self.sort_by_price()
        best_cheap = sorted_by_price[0][0]
        temp_percent_add = self.group_steps.get('price_step_percent')
        temp_amount_add = self.group_steps.get('price_step_amount')
        percent_calc = (sorted_by_price[0][0] * temp_percent_add )/100
        if percent_calc != 0 and percent_calc < temp_amount_add:
            price_add = percent_calc
        else:
            price_add = temp_percent_add

        sorted_by_flight_and_drive = self.sort_by_duration_flight_time_and_travel_time()
        best_drive_short = sorted_by_flight_and_drive[0][0]
        temp_percent_add = self.group_steps.get('flight_and_travel_time_step_percent')
        temp_amount_add = self.group_steps.get('flight_and_travel_time_step_amount')
        percent_calc = float((sorted_by_flight_and_drive[0][0] * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            flight_and_travel_time_add = percent_calc
        else:
            flight_and_travel_time_add = temp_amount_add

        sorted_by_drive = self.sort_by_duration_travel_time()
        best_travel_short = sorted_by_drive[0][0]
        temp_percent_add = self.group_steps.get('travel_time_step_percent')
        temp_amount_add = self.group_steps.get('travel_time_step_amount')
        percent_calc = float((sorted_by_drive[0][0] * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            travel_time_add = percent_calc
        else:
            travel_time_add = temp_amount_add

        sorted_by_flight = self.sort_by_duration_flight_time()
        best_flight_short = sorted_by_flight[0][0]
        temp_percent_add = self.group_steps.get('flight_time_step_percent')
        temp_amount_add = self.group_steps.get('flight_time_step_amount')
        percent_calc = float((sorted_by_flight[0][0] * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            flight_time_add = percent_calc
        else:
            flight_time_add = temp_amount_add

        sorted_by_mileage = self.sort_by_miles()
        best_mileage = sorted_by_mileage[0][0]
        temp_percent_add = self.group_steps.get('mileage_step_percent')
        temp_amount_add = self.group_steps.get('mileage_step_amount')
        percent_calc = float((sorted_by_mileage[0][0] * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            mileage_add = percent_calc
        else:
            mileage_add = temp_amount_add

        sorted_by_amount_stop = self.sort_by_amount_stop()
        best_amount_stop = sorted_by_amount_stop[0][0]
        temp_percent_add = self.group_steps.get('amount_stop_step_percent')
        temp_amount_add = self.group_steps.get('amount_stop_step_amount')
        percent_calc = int((best_amount_stop * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            amount_stop_add = percent_calc
        else:
            amount_stop_add = temp_amount_add

        sorted_by_duration_stop_time = self.sort_by_duration_stop()
        best_duration_stop_time = sorted_by_duration_stop_time[0][0]
        temp_percent_add = self.group_steps.get('duration_stop_step_percent')
        temp_amount_add = self.group_steps.get('duration_stop_step_amount')
        percent_calc = int((best_duration_stop_time * temp_percent_add) / 100)
        if percent_calc != 0 and percent_calc < temp_amount_add:
            duration_stop_add = percent_calc
        else:
            duration_stop_add = temp_amount_add

        for flight in range(0, len(sorted_by_price)):
            list_flights[int(sorted_by_price[flight][1])][int(sorted_by_price[flight][2])]["groups"]["group_by_price"] =\
                int((sorted_by_price[flight][0] - best_cheap)/price_add) + 1
            list_flights[int(sorted_by_flight_and_drive[flight][1])][int(sorted_by_flight_and_drive[flight][2])]["groups"]["group_by_duration_flight_time_and_travel_time"]=\
                int((sorted_by_flight_and_drive[flight][0] - best_drive_short) / flight_and_travel_time_add) + 1
            list_flights[int(sorted_by_drive[flight][1])][int(sorted_by_drive[flight][2])]["groups"]["group_by_duration_travel_time"]=\
                int((sorted_by_drive[flight][0] - best_travel_short) / travel_time_add) + 1
            list_flights[int(sorted_by_flight[flight][1])][int(sorted_by_flight[flight][2])]["groups"]["group_by_duration_flight_time"]=\
                int((sorted_by_flight[flight][0] - best_flight_short) / flight_time_add) + 1
            list_flights[int(sorted_by_mileage[flight][1])][int(sorted_by_mileage[flight][2])]["groups"]["group_by_mileage"]=\
                int((best_mileage - sorted_by_mileage[flight][0]) / mileage_add) + 1
            list_flights[int(sorted_by_amount_stop[flight][1])][int(sorted_by_amount_stop[flight][2])]["groups"]["group_by_amount_stop"]=\
                int((sorted_by_amount_stop[flight][0] - best_amount_stop) / amount_stop_add) + 1
            list_flights[int(sorted_by_duration_stop_time[flight][1])][int(sorted_by_duration_stop_time[flight][2])]["groups"]["group_by_duration_stop_time"]=\
                int((sorted_by_duration_stop_time[flight][0] - best_duration_stop_time) / duration_stop_add) + 1

    def build_json(self):
        for flight in range(0, len(json_data)):
            best_flights = []
            if len(json_data[flight]) != 0:
                for specific_flight in range(0, len(json_data[flight]["trips"]["tripOption"])):
                    best_data = {"price": None, "legs": None, "travelers_details": None, "num_of_legs": None, "calculations": None}
                    travelers_details = {"senior_amount": None, "adult_amount": None, "kids_amount": None, "baby_amount": None}
                    groups = {"group_by_price": None, "group_by_duration_flight_time_and_travel_time": None,
                              "group_by_duration_travel_time": None, "group_by_duration_flight_time": None,
                              "group_by_mileage": None, "group_by_amount_stop": None, "group_by_duration_stop_time": None,
                              "group_jump_by_difference_favorite_time_leg1": None}
                    algorithms_scores = {"cheapest1": None, "cheapest2": None, "cheapest3": None, "cheapest4": None,
                                         "best1": None, "best2": None, "best3": None, "best4": None,
                                         "shortest1": None, "shortest2": None, "shortest3": None, "shortest4": None, }
                    calculations = {}
                    total_flights_time = 0
                    total_stops_time = 0
                    total_mileage = 0
                    total_flight_with_drive_time = 0
                    total_stops = 0
                    airlines_per_flight = {}
                    list_airlines_per_flight = []
                    airlines_per_flight_counter = 0
                    travelers_details["adult_amount"] = json_data[flight]["trips"]["tripOption"][specific_flight]["passengersDetails"].get(
                    "adultCount")
                    best_data["flight_id"] = str(flight) + '-' + str(specific_flight)
                    best_data["external_id"] = flight
                    best_data["internal_id"] = specific_flight
                    best_data["price"] = json_data[flight]["trips"]["tripOption"][specific_flight].get("saleTotalQpx")

                    legs_list = []
                    calculations_legs_list = []
                    for leg in range(0, int(json_data[flight]["trips"]["tripOption"][specific_flight].get("number_of_legs"))):
                        direct_flights = []
                        legs_data = {}
                        calculations_legs_data = {}
                        list_airlines_per_leg = []
                        airlines_per_leg = {}
                        airlines_per_leg_counter = 0
                        flights_time = self.calculate_total_flights_time(leg,json_data[flight]["trips"]["tripOption"][specific_flight])
                        total_flights_time += flights_time
                        calculations_legs_data["total_flights_time"] = flights_time
                        stops_time = self.calculate_total_stops_time(leg, json_data[flight]["trips"]["tripOption"][specific_flight])
                        total_stops_time += stops_time
                        calculations_legs_data["total_stops_time"] = stops_time
                        mileage = self.calculate_total_mileage(leg, json_data[flight]["trips"]["tripOption"][specific_flight])
                        total_mileage += mileage
                        calculations_legs_data["total_mileage"] = mileage
                        duration_with_drive = int(json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("durationWithDrive"))
                        total_flight_with_drive_time += duration_with_drive
                        calculations_legs_data["total_drives_and_flights_time"] = duration_with_drive
                        stops = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("stops")
                        total_stops += stops
                        calculations_legs_data["total_stops"] = stops
                        calculations_legs_data["total_drives_time"] = duration_with_drive - flights_time
                        legs_data["duration"] = json_data[flight]["trips"]["tripOption"][specific_flight]["legs"][leg].get("duration")
                        legs_data["origin"] = self.origin_build(flight, leg, specific_flight)
                        legs_data["destination"] = self.destination_build(flight, leg, specific_flight)

                        self.airline_build(flight, airlines_per_leg, airlines_per_leg_counter, list_airlines_per_leg, specific_flight, leg)
                        legs_data["airlines"] = list_airlines_per_leg
                        self.airline_build(flight, airlines_per_flight, airlines_per_flight_counter, list_airlines_per_flight, specific_flight, leg)

                        self.direct_flights_build(flight, leg, direct_flights, specific_flight)
                        legs_data["direct_flights"] = direct_flights

                        legs_list.append(legs_data)
                        calculations_legs_list.append(calculations_legs_data)

                    calculations["total_flights_time"] = total_flights_time
                    calculations["total_stops_time"] = total_stops_time
                    calculations["total_mileage"] = total_mileage
                    calculations["total_drives_and_flights_time"] = total_flight_with_drive_time
                    calculations["total_stops"] = total_stops
                    calculations["total_drives_time"] = total_flight_with_drive_time - total_flights_time
                    best_data["calculations"] = calculations
                    best_data["calculations_per_legs"] = calculations_legs_list
                    best_data["legs"] = legs_list
                    best_data["num_of_legs"] = int(json_data[flight]["trips"]["tripOption"][specific_flight].get("number_of_legs"))
                    best_data["travelers_details"] = travelers_details
                    best_data["airlines"] = list_airlines_per_flight
                    best_data["groups"] = groups
                    best_data["algorithms_scores"] = algorithms_scores
                    best_flights.append(best_data)
            list_flight.append(best_flights)
        self.calculate_groups(list_flight)
        self.calculate_weights()
        self.sort_and_crop()
        bb = json.dumps(list_flight)

        opened_file = open('c:\\vtoc\\w\\print_new_json.txt', 'w')
        opened_file.write(str(bb))
        opened_file.close()

    def finish_grade(self, combining_weights):
        flight_for_client = []
        flights_numbers = []
        for weights in range(0, len(combining_weights)):
            for flight in range(0, len(list_flight)):
                if len(list_flight[flight]) != 0:
                    combining_flights_and_weights = []
                    combining_best_flights_and_weights = []
                    for specific_flight in range(0, len(list_flight[flight])):
                        temp_weight = list_flight[flight][specific_flight]['groups'].get('group_by_price') * \
                                          combining_weights[weights]['weight_price'] +\
                                          list_flight[flight][specific_flight]['groups'].get('group_by_duration_flight_time_and_travel_time') *\
                                          combining_weights[weights]['weight_duration_flight_and_travel_time'] + \
                                          list_flight[flight][specific_flight]['groups'].get('group_by_duration_travel_time') *\
                                          combining_weights[weights]['weight_duration_travel_time'] + \
                                          list_flight[flight][specific_flight]['groups'].get('group_by_amount_stop') *\
                                          combining_weights[weights]['weight_amount_stop'] + \
                                          list_flight[flight][specific_flight]['groups'].get('group_by_duration_stop_time') * \
                                          combining_weights[weights]['weight_duration_stop_time']
                        list_flight[flight][specific_flight]['algorithms_scores'][combining_weights[weights].get('alg_name')] = temp_weight
                        temp_grade = []
                        temp_grade.append(list_flight[flight][specific_flight].get('flight_id'))
                        temp_grade.append(temp_weight)
                        temp_grade.append(combining_weights[weights].get('alg_name'))
                        combining_flights_and_weights.append(temp_grade)
                        if (list_flight[flight][specific_flight]['groups'].get('group_by_price') <= combining_weights[weights]['gr_price_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_duration_flight_time_and_travel_time') <= combining_weights[weights]['gr_duration_flight_time_and_travel_time_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_duration_travel_time') <= combining_weights[weights]['gr_duration_travel_time_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_duration_flight_time') <= combining_weights[weights]['gr_duration_flight_time_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_mileage') <= combining_weights[weights]['gr_mileage_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_amount_stop') <= combining_weights[weights]['gr_amount_stop_max_num'] and
                            list_flight[flight][specific_flight]['groups'].get('group_by_duration_stop_time') <= combining_weights[weights]['gr_duration_stop_time_max_num']):
                            temp_best_grade = []
                            temp_best_grade.append(list_flight[flight][specific_flight].get('flight_id'))
                            temp_best_grade.append(temp_weight)
                            temp_best_grade.append(combining_weights[weights].get('alg_name'))
                            combining_best_flights_and_weights.append(temp_best_grade)
                    if len(combining_best_flights_and_weights) != 0:
                        sorted_combining_flights_and_weights = sorted(combining_best_flights_and_weights, key=itemgetter(1))
                    else:
                        sorted_combining_flights_and_weights = sorted(combining_flights_and_weights, key=itemgetter(1))
                    cropped_combining_flights_and_weights = self.crop_by_absolut(sorted_combining_flights_and_weights, flights_numbers, combining_weights[weights].get('num_of_flights'))
                    flight_for_client.append(cropped_combining_flights_and_weights)

        return flight_for_client

    def find_flights(self, flight_for_client):
        flight_numbers = []
        selected_flights = []
        for item in range(0, len(flight_for_client)):
            for j in range(0, len(flight_for_client[item])):
                flight_numbers.append(flight_for_client[item][j][0])
        for flight in range(0, len(list_flight)):
                if len(list_flight[flight]) != 0:
                    for specific_flight in range(0, len(list_flight[flight])):
                        if list_flight[flight][specific_flight].get('flight_id') in flight_numbers:
                            selected_flights.append(list_flight[flight][specific_flight])
        return selected_flights

    def calculate_weights(self):
        selected_flights = {"cheapest": None, "best": None, "shortest": None}

        combining_weights = self.assigment_cheapest_weights()
        cheapest_flight_for_client = self.finish_grade(combining_weights)
        selected_flights["cheapest"] = self.find_flights(cheapest_flight_for_client)

        combining_weights = self.assigment_best_weights()
        best_flight_for_client = self.finish_grade(combining_weights)
        selected_flights["best"] = self.find_flights(best_flight_for_client)

        combining_weights = self.assigment_shrtest_weights()
        shortest_flight_for_client = self.finish_grade(combining_weights)
        selected_flights["shortest"] = self.find_flights(shortest_flight_for_client)
        bb = json.dumps(selected_flights)

        opened_file = open('c:\\vtoc\\w\\print_selected_flights.txt', 'w')
        opened_file.write(str(bb))
        opened_file.close()

    def crop_by_percentage(self, list_flight_sorted, percentage):
        list_croped = []
        percent = (int(len(list_flight_sorted)) * percentage) / 100
        for crop in range(0, percent):
            list_croped.append(list_flight_sorted[crop])

        return list_croped

    def crop_by_absolut(self, list_flight_sorted, flights_numbers, absolute):
        list_cropped = []
        if absolute >= len(list_flight_sorted):
            return list_flight_sorted
        else:
            # for crop in range(0, absolute):
            crop = 0
            while crop < absolute:
                if not list_flight_sorted[crop][0] in flights_numbers:
                    list_cropped.append(list_flight_sorted[crop])
                    flights_numbers.append(list_flight_sorted[crop][0])
                elif absolute < len(list_flight_sorted):
                    absolute = absolute + 1
                crop = crop + 1

        return list_cropped

def main():
    import new_json
    cj = new_json.clientJson()
    cj.build_json()



if __name__ == "__main__":
    main()