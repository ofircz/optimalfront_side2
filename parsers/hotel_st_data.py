import json

__author__ = 'Ofir'

class hotel_static_data():

    @staticmethod
    def set_hotels():
        hotel_st_data = [

            {
                "order_details": {
                    "senior:": "0",
                    "duration": "4",
                    "end_date": "18.8",
                    "start_date": "14.8",
                    "children": "0",
                    "breakfast": "included",
                    "adults": "1",
                    "refundable": "yes"
                },
                "area_details": {
                    "State": "California",
                    "area": "Lake Forest",
                    "continent": "America"
                },
                "highest_ranked": [
                    {
                        "price": "946",
                        "reviews": {
                            "source1": "this is another review",
                            "source0": "this is a review",
                            "source2": "third review"
                        },
                        "rank": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "address": "2 Orchard, Lake Forest, California, United States",
                        "pension": "breakfast",
                        "added_device": [
                            "first device",
                            "second device",
                            "third device"
                        ],
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "link": "https://www.hotelscombined.co.il/Hotel/Staybridge_Suites_Irvine_East_Lake_Forest.htm?a_aid=139799",
                        "hotel_details": {
                            "reception": {
                                "to": "23:00",
                                "from": "7:00"
                            },
                            "laggage_keeping": "yes",
                            "lat": "42.32928",
                            "check_in": "12:00",
                            "parking": "yes",
                            "stars": "4",
                            "rooms": [
                                {
                                    "price_per_night": "60",
                                    "type": "single"
                                }
                            ],
                            "check_out": "14:00",
                            "availeable_rooms": "250",
                            "distances": {
                                "from_address": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                },
                                "from_airport": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                }
                            },
                            "long": "-71.09487"
                        },
                        "name": "\u202b\u202aStaybridge\u202c\u202c \u202b\u202aSuites\u202c\u202c \u202b\u202aIrvine\u202c\u202c \u202b\u202aEast\u202c\u202c \u202b\u202a-\u202c\u202c \u202b\u202aLake\u202c\u202c \u202b\u202aForest\u202c\u202c",
                        "tax_to_pay_in_hotel": "10%"
                    },
                    {
                        "price": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "reviews": {
                            "source1": "this is another review",
                            "source0": "this is a review",
                            "source2": "third review"
                        },
                        "rank": "8.9",
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "address": "23131 Lake Center Drive, Lake Forest, California, United States",
                        "pension": "breakfast",
                        "added_device": [
                            "first device",
                            "second device",
                            "third device"
                        ],
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "link": "https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Irvine_Spectrum.htm?a_aid=139799",
                        "hotel_details": {
                            "reception": {
                                "to": "23:00",
                                "from": "7:00"
                            },
                            "laggage_keeping": "yes",
                            "lat": "42.33",
                            "check_in": "12:00",
                            "parking": "yes",
                            "stars": "4",
                            "rooms": [
                                {
                                    "price_per_night": "60",
                                    "type": "single"
                                }
                            ],
                            "check_out": "14:00",
                            "availeable_rooms": "250",
                            "distances": {
                                "from_address": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                },
                                "from_airport": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                }
                            },
                            "long": "-71.111336"
                        },
                        "name": "\u202b\u202aHoliday\u202c\u202c \u202b\u202aInn\u202c\u202c \u202b\u202aIrvine\u202c\u202c \u202b\u202aSpectrum\u202c\u202c",
                        "tax_to_pay_in_hotel": "10%"
                    },
                    {
                        "price": "465",
                        "reviews": {
                            "source1": "this is another review",
                            "source0": "this is a review",
                            "source2": "third review"
                        },
                        "rank": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "address": "20768 Lake Forest Drive, Lake Forest, California, United States",
                        "pension": "breakfast",
                        "added_device": [
                            "first device",
                            "second device",
                            "third device"
                        ],
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "link": "https://www.hotelscombined.co.il/Hotel/Prominence_Hotel_Suites.htm?a_aid=139799",
                        "hotel_details": {
                            "reception": {
                                "to": "23:00",
                                "from": "7:00"
                            },
                            "laggage_keeping": "yes",
                            "lat": "42.33871",
                            "check_in": "12:00",
                            "parking": "yes",
                            "stars": "4.0",
                            "rooms": [
                                {
                                    "price_per_night": "60",
                                    "type": "single"
                                }
                            ],
                            "check_out": "14:00",
                            "availeable_rooms": "250",
                            "distances": {
                                "from_address": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                },
                                "from_airport": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                }
                            },
                            "long": "-71.10686"
                        },
                        "name": "\u202b\u202aProminence\u202c\u202c \u202b\u202aHotel\u202c\u202c \u202b&\u202c \u202b\u202aSuites\u202c\u202c",
                        "tax_to_pay_in_hotel": "10%"
                    }
                ],
                "link_to_hotels_in_the_area": [
                    "\u202bhttp://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799\u202c\u202c",
                    "\u202ahttp://www.booking.com/searchresults.he.html?city\u202c\u202c \u202b\u202a=20013943&aid=825774\u202c\u202c"
                ],
                "coords": [
                    {
                        "latitude": "32.830593",
                        "longitude": "-79.825432",
                        "id": ""
                    },
                    {
                        "latitude": "32.863488",
                        "longitude": "-80.023833",
                        "id": ""
                    },
                    {
                        "latitude": "32.853438",
                        "longitude": "-80.056785",
                        "id": ""
                    }
                ]
            },
            {
                "order_details": {
                    "dates": "19-24.8",
                    "refundable": "yes",
                    "children": "0",
                    "adults": "1",
                    "breakfast": "included"
                },
                "coords": [
                    {
                        "latitude": "32.830593",
                        "longitude": "-79.825432",
                        "id": ""
                    },
                    {
                        "latitude": "32.863488",
                        "longitude": "-80.023833",
                        "id": ""
                    },
                    {
                        "latitude": "32.853438",
                        "longitude": "-80.056785",
                        "id": ""
                    }
                ],
                "hotels_in_the_area": [
                    "\u202b\u202a\u202b\u202ahttp://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799\u202c\u202c"
                ],
                "highest_ranked": [
                    {
                        "price": "601",
                        "reviews": {
                            "source1": "this is another review",
                            "source0": "this is a review",
                            "source2": "third review"
                        },
                        "rank": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "address": "460 North Cherry Street, Winston-Salem, North Carolina, United States",
                        "name": "\u202b\u202a\u202b\u202aHawthorne\u202c\u202c \u202b\u202aInn\u202c\u202c \u202b\u202aWinston\u202c\u202c \u202b\u202aSalem\u202c\u202c",
                        "added_device": [
                            "first device",
                            "second device",
                            "third device"
                        ],
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "link": "https://www.hotelscombined.co.il/Hotel/Hawthorne_Inn_Winston_Salem.htm?a_aid=139799",
                        "hotel_details": {
                            "reception": {
                                "to": "23:00",
                                "from": "7:00"
                            },
                            "laggage_keeping": "yes",
                            "lat": "42.34346",
                            "check_in": "12:00",
                            "parking": "yes",
                            "stars": 5,
                            "rooms": [
                                {
                                    "price_per_night": "60",
                                    "type": "single"
                                }
                            ],
                            "check_out": "14:00",
                            "availeable_rooms": "250",
                            "distances": {
                                "from_address": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                },
                                "from_airport": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                }
                            },
                            "long": "-71.09825"
                        },
                        "tax_to_pay_in_hotel": "0"
                    },
                    {
                        "price": "834",
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "address": "460 North Cherry Street, Winston-Salem, North Carolina, United States",
                        "rank": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "link": "https://www.hotelscombined.co.il/Hotel/Embassy_Suites_Hotel_Winston_Salem.htm?a_aid=139799",
                        "tax_to_pay_in_hotel": "0",
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "name": "\u202b\u202a\u202b\u202aEmbassy\u202c\u202c \u202b\u202aSuites\u202c\u202c \u202b\u202aHotel\u202c\u202c \u202b\u202aWinston\u202c\u202c \u202b\u202a-\u202c\u202c \u202b\u202aSalem\u202c\u202c"
                    },
                    {
                        "price": "685",
                        "reviews": {
                            "source1": "this is another review",
                            "source0": "this is a review",
                            "source2": "third review"
                        },
                        "rank": {
                            "site2": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "site1": {
                                "ranks_num": "1000",
                                "rank": "9"
                            },
                            "average_rank": "9",
                            "ranks_sum": "3000",
                            "tripadvisor": {
                                "ranks_num": "1000",
                                "rank": "9"
                            }
                        },
                        "header": "https://media.datahc.com/HI104237497.jpg",
                        "address": "2520 Peters Creek Pkwy, Winston-Salem, North Carolina, United States",
                        "name": "\u202b\u202a\u202b\u202aHoliday\u202c\u202c \u202b\u202aInn\u202c\u202c \u202b\u202aExpress\u202c\u202c \u202b\u202aWinston\u202c\u202c \u202b\u202a-\u202c\u202c \u202b\u202aSalem\u202c\u202c",
                        "added_device": [
                            "first device",
                            "second device",
                            "third device"
                        ],
                        "pictures": [
                            "https://media.datahc.com/HI104237475.jpg",
                            "https://media.datahc.com/HI104237501.jpg",
                            "https://media.datahc.com/HI104237478.jpg",
                            "https://media.datahc.com/HI104237497.jpg"
                        ],
                        "link": "https://www.hotelscombined.co.il/Hotel/Holiday_Inn_Express_Winston_Salem.htm?a_aid=139799",
                        "hotel_details": {
                            "reception": {
                                "to": "23:00",
                                "from": "7:00"
                            },
                            "laggage_keeping": "yes",
                            "lat": "42.34482",
                            "check_in": "12:00",
                            "parking": "yes",
                            "stars": "4",
                            "rooms": [
                                {
                                    "price_per_night": "60",
                                    "type": "single"
                                }
                            ],
                            "check_out": "14:00",
                            "availeable_rooms": "250",
                            "distances": {
                                "from_address": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                },
                                "from_airport": {
                                    "driving_time": "25",
                                    "drive": "10",
                                    "aerial": "10"
                                }
                            },
                            "long": "-71.09677"
                        },
                        "tax_to_pay_in_hotel": "0"
                    }
                ],
                "area_details": {
                    "State": "North Carolaina",
                    "area": "Winston Salem",
                    "continent": "America"
                }
            }

        ]

        return hotel_st_data

