# -*- coding: utf-8 -*-
import os
import platform
import re
from lxml import html
from lxml.etree import tostring
from google.appengine.ext import blobstore
import logging


class Parser():

    def getTripDetails(self, row, title):
        dict = {}
        dict['name'] = self.getName(title)
        tup = self.splitTitle(title)
        dict['date'] = tup[0].__str__()
        details = row.xpath('.//div[@class="gwt-Label"]/text()')
        dict['airline'] = details[0].__str__()
        dict['departure'] = details[1][5:].__str__()
        dict['arrival'] = details[2][5:].__str__()
        dict['time'] = details[3].__str__()
        dict['plane'] = details[4].__str__()
        dict['class'] = details[5].__str__()
        return dict

    def getName(self, title):
        arr = title.split('-')
        return arr[0]

    def splitTitle(self, title):
        arr = title.split()
        return ' '.join(arr[-3:]), ' '.join(arr[:-4])

    def main(self, blob_key):
        blob_reader = blobstore.BlobReader(blob_key)
        data = blob_reader.read()
        ans = re.findall('<table.*?<\/table>', data)
        tables = []
        dup = []
        for x in ans:
            tree = html.fromstring(x)
            title = tree.xpath('//td[@colspan=5]/div/text()')
            if not title:
                continue
            rows = tree.xpath('//table//tr')
            dict = {}
            dict['flights'] = []
            dict['layovers'] = []
            price = tree.xpath('//button/span/text()')
            if not price and len(dup) > 0:
                tables.append(dup)

            else:
                dup = []
                f_price = price[0].split('$')
                dict['price'] = f_price[1]
                # price[0].__str__()
            for i, row in enumerate(rows):
                elem = row.xpath('./td[@colspan=5]/div/text()')
                if elem:
                    tup = self.splitTitle(elem[0])
                    dict['title'] = tup[1].__str__()
                    dict['date'] = tup[0].__str__()
                    continue
                elem = row.xpath('./td[@colspan=7]/div/text()')
                if elem:
                    dict['flights'].append(
                        self.getTripDetails(rows[i + 1], elem[0]))
                    continue
                elem = row.xpath('.//div[@class="gwt-Label"]/text()')
                if elem:
                    continue
                if "Layover" in tostring(row):
                    layover = {}
                    cells = row.xpath('./td')
                    layover['in'] = cells[1].xpath('./div/text()')[0].__str__()
                    layover['time'] = cells[3].xpath(
                        './div/text()')[0].__str__()
                    dict['layovers'].append(layover)
            dup.append(dict)
        return tables
