import time

from google.appengine.ext import ndb
from webapp2_extras import auth, security
from webapp2_extras.appengine.auth.models import Unique, UserToken

class Booked_Itineraries(ndb.Model):
    """ Queries needed:
            All bookings with status X or Y
             All bookings for passenger ABC
    """

    booking_date = ndb.DateTimeProperty(auto_now_add=True)
    bookings_data = ndb.TextProperty()
    bookings_response = ndb.TextProperty()
    passengers = ndb.TextProperty()
    fare_rules = ndb.TextProperty()
    flights = ndb.TextProperty()
    active = ndb.BooleanProperty()
    company = ndb.StringProperty()

    @classmethod
    def query_data(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).order(cls.index)
    @classmethod
    def get_bookings_by_company(cls,company):
        return cls.query(cls.company == company).fetch()
    @classmethod
    def get_active_bookings_by_company(cls,company):
        return cls.query(cls.active == True, cls.company == company).fetch()
    @classmethod
    def get_active_bookings(cls):
        return cls.query(cls.active == True).fetch()

class Itineraries(ndb.Model):
    index = ndb.IntegerProperty()
    itinerary = ndb.TextProperty()

    @classmethod
    def query_data(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).order(cls.index)

class PersistentData(ndb.Model):
    data = ndb.TextProperty()

    @classmethod
    def query_data(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key)

class EnteredData(ndb.Model):
    date = ndb.DateTimeProperty(auto_now_add=True)
    enteredData = ndb.TextProperty()

    @classmethod
    def query_data(cls, ancestor_key):
        return cls.query(ancestor=ancestor_key).order(-cls.date).fetch(20)

class User(ndb.Expando):

    unique_model = Unique
    unique_properties = ('auth_id', 'email')
    token_model = UserToken

    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    # ID for third party authentication. UNIQUE. Do we need it?
    auth_ids = ndb.StringProperty(repeated=True)
    password = ndb.StringProperty()
    admin = ndb.BooleanProperty()
    company = ndb.StringProperty()
    # Details for booking
    title = ndb.StringProperty()
    birth_date = ndb.StringProperty()
    type = ndb.StringProperty()
    PTC = ndb.StringProperty()
    passport = ndb.StringProperty()
    document_type = ndb.StringProperty()
    document_number = ndb.StringProperty()
    document_expiry_date = ndb.StringProperty()
    document_issue_country = ndb.StringProperty()
    document_nationality = ndb.StringProperty()
    seat_preference = ndb.StringProperty()

    frequent_traveller_cards = ndb.StringProperty() # json of a list of {'airline_code': 123, 'number': 456 }

    def get_id(self):
        return self._key.id()

    @classmethod
    def get_by_auth_id(cls, auth_id):
        return cls.query(cls.auth_ids == auth_id).get()

    @classmethod
    def get_users_by_company(cls,company):
        return cls.query(cls.company == company).fetch()

    @classmethod
    def get_by_auth_password(cls, auth_id, password):
        user = cls.get_by_auth_id(auth_id)
        if not user:
            raise auth.InvalidAuthIdError()
        if not security.check_password_hash(password, user.password):
            raise auth.InvalidPasswordError()
        return user

    @classmethod
    def get_by_auth_token(cls, user_id, token, subject='auth'):
        token_key = cls.token_model.get_key(user_id, subject, token)
        user_key = ndb.Key(cls, user_id)
        # Use get_multi() to save a RPC call.
        valid_token, user = ndb.get_multi([token_key, user_key])
        if valid_token and user:
            timestamp = int(time.mktime(valid_token.created.timetuple()))
            return user, timestamp
        return None, None

    @classmethod
    def create_auth_token(cls, user_id, subject='auth'):
        return cls.token_model.create(user_id, subject).token

    @classmethod
    def delete_auth_token(cls, user_id, token, subject='auth'):
        cls.token_model.get_key(user_id, subject, token).delete()

    @classmethod
    def create_user(cls, auth_id, **user_values):
        assert user_values.get('password') is None, \
            'Use password_raw instead of password to create new users.'

        assert not isinstance(auth_id, list), \
            'Creating a user with multiple auth_ids is not allowed, ' \
            'please provide a single auth_id.'

        if 'password_raw' in user_values:
            user_values['password'] = security.generate_password_hash(
                user_values.pop('password_raw'), length=12)

        user_values['auth_ids'] = [auth_id]
        user = cls(**user_values)

        # Set up unique properties
        uniques = []
        user_values['auth_id'] = auth_id
        for name in cls.unique_properties:
            key = '%s.%s:%s' % (cls.__name__, name, user_values[name])
            uniques.append((key, name))

        ok, existing = cls.unique_model.create_multi(k for k, v in uniques)
        if ok:
            user.put()
            return True, user
        else:
            properties = [v for k, v in uniques if k in existing]
            return False, properties
