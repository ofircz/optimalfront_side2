import requests
import urllib3
import xmltodict
import pickle


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def get_countries():
    request = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
       <soapenv:Header>
          <ns:AuthHeader>
             <ns:ResellerCode>V2CC</ns:ResellerCode>
             <ns:Username>tomer</ns:Username>
             <ns:Password>tomer1</ns:Password>
             <!--Optional:-->
          </ns:AuthHeader>
       </soapenv:Header>
       <soapenv:Body>
          <ns:GetCountriesRQ>
             <!--Optional:-->
          </ns:GetCountriesRQ>
       </soapenv:Body>
    </soapenv:Envelope>"""

    encoded_request = request.encode('utf-8')
    url = "http://www.talmatravel.co.il/tbs/reseller/ws/"
    headers = {"Host": "www.talmatravel.co.il",
               "Content-Type": "text/xml;charset=UTF-8",
               "Content-Length": str(len(encoded_request)),
               "SOAPAction": "http://tbs.dcsplus.net/ws/StaticData_GetCountries",
               "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
               "Connection": "Keep-Alive"}


    query_results = requests.post(url,data=encoded_request,headers=headers,verify=False).text
    return xmltodict.parse(query_results)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:GetCountriesRS']['ns1:Countries']['ns1:Country']



def get_cities(Countries):

    request = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
       <soapenv:Header>
          <ns:AuthHeader>
             <ns:ResellerCode>V2CC</ns:ResellerCode>
             <ns:Username>tomer</ns:Username>
             <ns:Password>tomer1</ns:Password>
             <!--Optional:-->
          </ns:AuthHeader>
       </soapenv:Header>
       <soapenv:Body>
          <ns:GetCitiesRQ>
             <ns:CountryID>%s</ns:CountryID>
          </ns:GetCitiesRQ>
       </soapenv:Body>
    </soapenv:Envelope>"""

    encoded_request = request.encode('utf-8')
    url = "http://www.talmatravel.co.il/tbs/reseller/ws/"
    headers = {"Host": "www.talmatravel.co.il",
               "Content-Type": "text/xml;charset=UTF-8",
               "Content-Length": str(len(encoded_request)),
               "SOAPAction": "http://tbs.dcsplus.net/ws/StaticData_GetCities",
               "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
               "Connection": "Keep-Alive"}

    Cities = []
    for country in Countries:
        query_results = requests.post(url, data=encoded_request % country['@ID'], headers=headers, verify=False).text
        ans = xmltodict.parse(query_results)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:GetCitiesRS']['ns1:Cities']
        if ans:
            if type(ans['ns1:City'])==list:
                Cities = Cities + ans['ns1:City']
            else:
                Cities.append(ans['ns1:City'])
    return Cities


def get_hotels(cities):
    request = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
       <soapenv:Header>
          <ns:AuthHeader>
             <ns:ResellerCode>V2CC</ns:ResellerCode>
             <ns:Username>tomer</ns:Username>
             <ns:Password>tomer1</ns:Password>
          </ns:AuthHeader>
       </soapenv:Header>
       <soapenv:Body>
          <ns:GetHotelsRQ>
             <ns:CityID>%s</ns:CityID>
          </ns:GetHotelsRQ>
       </soapenv:Body>
    </soapenv:Envelope>"""

    encoded_request = request.encode('utf-8')
    url = "http://www.talmatravel.co.il/tbs/reseller/ws/"
    headers = {"Host": "www.talmatravel.co.il",
               "Content-Type": "text/xml;charset=UTF-8",
               "Content-Length": str(len(encoded_request)),
               "SOAPAction": "http://tbs.dcsplus.net/ws/StaticData_GetHotels",
               "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
               "Connection": "Keep-Alive"}


    Hotels = []
    for city in cities:
        query_results = requests.post(url, data=encoded_request % city['@ID'], headers=headers, verify=False).text
        ans = xmltodict.parse(query_results)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:GetHotelsRS']['ns1:Hotels']
        print ans
        if ans:
            if type(ans['ns1:Hotel']) == list:
                Hotels = Hotels + ans['ns1:Hotel']
            else:
                Hotels = Hotels + [ans['ns1:Hotel']]
    return Hotels

def get_hotels_details(all_hotels):
    request = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
           <soapenv:Header>
              <ns:AuthHeader>
                 <ns:ResellerCode>V2CC</ns:ResellerCode>
                 <ns:Username>tomer</ns:Username>
                 <ns:Password>tomer1</ns:Password>
              </ns:AuthHeader>
           </soapenv:Header>
       <soapenv:Body>
          <ns:GetHotelDetailsRQ>
             <!--1 to 30 repetitions:-->
             %s
          </ns:GetHotelDetailsRQ>
       </soapenv:Body>
        </soapenv:Envelope>"""

    hotels_details = []
    for hotels in chunks(all_hotels,30):

        hotel_list = '\n'.join(["<ns:HotelID>%s</ns:HotelID>" %  hotel['@ID'] for hotel in hotels])
        cur_request = request % hotel_list
        print cur_request
        encoded_request = cur_request.encode('utf-8')
        url = "http://www.talmatravel.co.il/tbs/reseller/ws/"
        headers = {"Host": "www.talmatravel.co.il",
                   "Content-Type": "text/xml;charset=UTF-8",
                   "Content-Length": str(len(encoded_request)),
                   "SOAPAction": "http://tbs.dcsplus.net/ws/StaticData_GetHotels",
                   "User-Agent": "Apache - HttpClient / 4.1.1(java 1.5)",
                   "Connection": "Keep-Alive"}


        query_results = requests.post(url, data=encoded_request, headers=headers, verify=False).text
        hotels_details = hotels_details + xmltodict.parse(query_results)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:GetHotelDetailsRS']['ns1:HotelDetails']
    return hotels_details



Countries = get_countries()
print "got countries"
Cities = get_cities(Countries[:5])
print "got cities"
Hotels = get_hotels(Cities[:50])
print "got hotels"
Hotel_Details =  get_hotels_details(Hotels[:50])
print Hotel_Details

Static_Data = {'countries': Countries, 'cities': Cities, 'hotels':Hotels, 'hotel_details':Hotel_Details}
pickle.dump(Static_Data, open( "static_data.pickle", "wb"))
print 'Done.'
