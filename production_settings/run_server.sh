#!/bin/bash
if [ `ps aux|grep -v grep | grep -c dev_appserver.py` -eq 0 ]; then
	cd /home/optimal/PycharmProjects
	log=logs/server_`date +"%d_%m_%y_%H:%M"`.log
	echo "Running server. Check $log for logs."
	./google_appengine/dev_appserver.py --host=10.0.0.6 --log_level info --storage_path database.db optimallinux 2>$log &
fi
