import webapp2
import json
import math
from views_classes.Users import BaseHandler
import logging
import datetime
from google.appengine.ext import ndb
#import suds

from ap_db_loc_wo_doubles import AirportDbLocationsWoDoubles
from ap_nearby_400km import AirportDbNearby400KM
from models import Itineraries, User, EnteredData, Booked_Itineraries, PersistentData
from api.FlightsArea import Itinerary_Keys, Itinerary_Filters
import xml.etree.cElementTree as ET
import xmltodict

class IsLoggedIn(BaseHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(self.user_info))

class APcodeAutocomplete(BaseHandler):
    def post(self):
        from airports_codes_and_amadeus_db import AirportCodesDB
        ap_code_list = AirportCodesDB.AirportcodebasicDB()
        input_term = json.loads(self.request.body) #send part of the airport code
        logging.info(input_term)
        matched_aps = []
        len_input = len(input_term)
        for ap in ap_code_list:
            if str(input_term).lower() == ap[:len_input].lower():
                matched_aps.append(ap) # todo : add more airport details when possible

        self.response.write(json.dumps(matched_aps, indent=2))

class APcodeAutocompleteDet(BaseHandler):
    def post(self):
        from airports_codes_and_amadeus_db import AirportCodesDB
        ap_code_list_details = AirportCodesDB.AirportAmadeusData()
        data = json.loads(self.request.body)
        input_term = data['request_detail']  # send part of the airport code
        matched_aps = []
        len_input = len(input_term)
        for ap in ap_code_list_details:
            # logging.info("ap: %s" % ap)
            if str(input_term).lower() == ap['ap_code'][:len_input].lower():
                if ap.get('ap_details'):
                    if len(ap['ap_details']) == 1:
                        ap_det = ap['ap_code'] + "-" + ap['ap_details'][0]['name'] + "," + ap['ap_details'][0][
                            'city_name'] + " " + ap['ap_details'][0]['country']
                    else: #more than 1 airport
                        ap_det = ap['ap_code'] + ": " + str(len(ap['ap_details'])) + " airports" + "," + \
                                 ap['ap_details'][0]['city_name'] + " " + ap['ap_details'][0]['country']
                else:
                    ap_det = ap['ap_code']
                matched_aps.append(ap_det)

        self.response.write(json.dumps(matched_aps, indent=2))

class NearbyByAddress(BaseHandler):
    def post(self):
        from api.google_maps import GoogleMaps
        google_maps = GoogleMaps

        apDB_locations_and_det = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()
        data = json.loads(self.request.body)
        input_address = data['address']
        addr_geo = google_maps.get_geo_string_result(input_address)
        logging.info("++++++++++++++++++++++++++++++++++++++++++++++++'")
        logging.info("addr_geo %s" % addr_geo)
        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
            lat2 = float(addr_geo.split(',')[0])
            lng2 = float(addr_geo.split(',')[1])
            print 'lat, lng :' , lat2,' ', lng2
            max_distance = 400  # km

            airports_in_distance = []
            for ap_det in apDB_locations_and_det:  #

                if len(ap_det['ap_details']) == 1:  # there is '0'
                    lat1 = ap_det['ap_details'][0]['location']['latitude']
                    lng1 = ap_det['ap_details'][0]['location']['longitude']
                else:
                    lat1 = ap_det['ap_details']['location']['latitude']
                    lng1 = ap_det['ap_details']['location']['longitude']

                ap_code_1 = ap_det['ap_code']
                ap_parent_code_1 = ap_det['ap_parent_code']

                # calculate air distance: http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-lnggitude-points-haversine-formula
                p = 0.017453292519943295  # PI / 180
                a = 0.5 - math.cos((lat2 - lat1) * p) / 2 + math.cos(lat1 * p) * math.cos(lat2 * p) * (1 - math.cos((lng2 - lng1) * p)) / 2
                distance = 12742 * math.asin(math.sqrt(a))  # 2 * R; R = 6371 km (radius of earth)
                if distance < max_distance:

                    if ap_det['ap_code'] == ap_det[
                        'ap_parent_code']:  # no different parent (nyc is the parent of jfk and ewr for example)
                        #print "ap_det ", ap_det
                        if len(ap_det['ap_details']) == 1:  # there is '0'
                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                        ap_det['ap_details'][0]['country'] + ' [' + ap_det[
                                            'ap_code'] + '] - ' + str(int(distance)) + ' KM ' + str(ap_det['ap_details'][0]['location'])
                        else:
                            ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                        ap_det['ap_details']['country'] + ' [' + ap_det[
                                            'ap_code'] + '] - ' + str(int(distance)) + ' KM ' + str(ap_det['ap_details']['location'])
                    else:
                        if len(ap_det['ap_details']) == 1:  # there is '0'
                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                        ap_det['ap_details'][0]['country'] + ' ' + ap_det[
                                            'ap_code'] + ' [' + ap_det[
                                            'ap_parent_code'] + '] - ' + str(int(distance)) + ' KM ' + str(ap_det['ap_details'][0]['location'])
                        else:
                            ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                        ap_det['ap_details']['country'] + ' ' + ap_det[
                                            'ap_code'] + ' [' + ap_det[
                                            'ap_parent_code'] + '] - ' + str(int(distance)) + ' KM ' + str(ap_det['ap_details']['location'])

                    # airports_in_distance.append(ap_to_add)
                    airports_in_distance.append([int(distance), ap_to_add])

            all_nearby_ap_sorted1 = sorted(airports_in_distance, key=lambda k: k[0])
            all_nearby_ap_sorted2 = []
            for ap in all_nearby_ap_sorted1:
                all_nearby_ap_sorted2.append(ap[1])
            self.response.write(json.dumps(all_nearby_ap_sorted2, indent=2))

        else:
            self.response.write("Error -No geocode [ERR]")





class ApNearbyOptions(BaseHandler):

    def post(self):
        ap_db_nearby = AirportDbNearby400KM.ApDbNearby400KM()
        #element: {'ap_parent_code': 'YUY', 'lat': 48.20611, 'APDist400km': [{'CodeDestPar': 'YSB', 'movements': 25221, 'CodeDest': 'YSB', 'CodeOrig': 'YUY', 'dist': 229}, {'CodeDestPar': 'YKQ', 'movements': 33948, 'CodeDest': 'YKQ', 'CodeOrig': 'YUY', 'dist': 363}, {'CodeDestPar': 'YVO', 'movements': 21219, 'CodeDest': 'YVO', 'CodeOrig': 'YUY', 'dist': 79}, {'CodeDestPar': 'YTS', 'movements': 45399, 'CodeDest': 'YTS', 'CodeOrig': 'YUY', 'dist': 191}, {'CodeDestPar': 'YTM', 'movements': 6246, 'CodeDest': 'YTM', 'CodeOrig': 'YUY', 'dist': 365}, {'CodeDestPar': 'YY', 'movements': 7775, 'CodeDest': 'YY', 'CodeOrig': 'YUY', 'dist': 297}, {'CodeDestPar': 'YYB', 'movements': 16123, 'CodeDest': 'YYB', 'CodeOrig': 'YUY', 'dist': 209}, {'CodeDestPar': 'YMT', 'movements': 14341, 'CodeDest': 'YMT', 'CodeOrig': 'YUY', 'dist': 359}, {'CodeDestPar': 'YMO', 'movements': 29551, 'CodeDest': 'YMO', 'CodeOrig': 'YUY', 'dist': 365}], 'ap_code': 'YUY', 'lng': -78.83556}
        ap_db_details = AirportDbLocationsWoDoubles.ApDbWITHLocationsWoDoubles()#todo: add the needed values from ap_db_details to the nearby db
        #element: {'ap_parent_code': 'YUY', 'ap_details': [{'location': {'longitude': -78.83556, 'latitude': 48.20611}, 'city_code': 'YUY', 'aircraft_movements': 12986, 'name': 'Rouyn-Noranda Airport', 'code': 'YUY', 'city_name': 'Rouyn-Noranda', 'timezone': 'America/Toronto', 'state': '', 'country': 'CA'}], 'ap_code': 'YUY'}
        data = json.loads(self.request.body)
        ap_input = data['ap_code']
        max_distance = data.get('max_distance')
        if not max_distance:
            max_distance = 150
        matched_aps = []
        len_input = len(ap_input)
        if len_input !=3:
            self.response.write("Error: please insert airport code with 3 letters")# todo: this check should be in the client
        else:
            for ap in ap_db_nearby:
                if ap_input.lower() == ap['ap_code'].lower():
                    all_nearby_ap_sorted = sorted(ap['APDist400km'],key=lambda k: k['dist'])
                    # todo: add count of amount of aps and using the 'movment'
                    # todo: need to change the way 'parent' ap shows - with few aps
                    for ap_nearby in all_nearby_ap_sorted:
                        if ap_nearby['dist'] < max_distance:# if in the required distance. todo: can add tests how many aps in the range and add / delete aps by movements
                            for ap_det in ap_db_details:
                                if ap_det['ap_code'] == ap_nearby['CodeDest']: #if the same airport
                                    if ap_nearby['CodeDest'] == ap_nearby['CodeDestPar']:# no different parent
                                        if len(ap_det['ap_details']) == 1:  # there is '0'
                                            ap_to_add = ap_det['ap_details'][0]['city_name']+ " " + ap_det['ap_details'][0]['country'] + ' [' + ap_det['ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details'][0]['location'])
                                        else:
                                            ap_to_add = ap_det['ap_details']['city_name']+ " " + ap_det['ap_details']['country'] + ' [' + ap_det['ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details']['location'])
                                    else:
                                        if len(ap_det['ap_details']) == 1:  # there is '0'
                                            ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + ap_det['ap_details'][0]['country'] +' ' + ap_det['ap_code'] +' [' + ap_det['ap_parent_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details'][0]['location'])
                                        else:
                                            ap_to_add = ap_det['ap_details']['city_name'] + " " + ap_det['ap_details']['country'] +' ' + ap_det['ap_code'] +' [' + ap_det['ap_parent_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details']['location'])
                                    matched_aps.append(ap_to_add)
                                    break
                        else:
                            break # sorted by distance, the next aps will have bigger distance

            ##############################################################
            # workaround for multy airports cities like LON,NYC(and TLV) #
            # if ap wasn't found, use the first airport that has the     #
            # ap_parent_code (not so accurate way)                       #
            ##############################################################

            if len(matched_aps)==0:
                First_match =1
                for ap in ap_db_nearby:
                    if ap_input.lower() == ap['ap_parent_code'].lower() and First_match:
                        First_match = 0
                        all_nearby_ap_sorted = sorted(ap['APDist400km'], key=lambda k: k['dist'])
                        # todo: add count of amount of aps and using the 'movment'
                        # todo: need to change the way 'parent' ap shows - with few aps

                        ############
                        # only in the workaround - add the current ap to the results todo: make it in the same way, right now less details
                        ############
                        for ap_det in ap_db_details:
                            if ap_det['ap_code'] == ap['ap_code']:  # if the same airport
                                ap_to_add = ap['ap_code'] + ' [' + ap['ap_parent_code'] + '] - ' +  '0 KM ' + str(ap_det['ap_details']['location'])
                                matched_aps.append(ap_to_add)


                        for ap_nearby in all_nearby_ap_sorted:
                            if ap_nearby[
                                'dist'] < max_distance:  # if in the required distance. todo: can add tests how many aps in the range and add / delete aps by movements
                                for ap_det in ap_db_details:
                                    if ap_det['ap_code'] == ap_nearby['CodeDest']:  # if the same airport
                                        if ap_nearby['CodeDest'] == ap_nearby['CodeDestPar']:  # no different parent
                                            if len(ap_det['ap_details']) == 1:  # there is '0'
                                                ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                                            ap_det['ap_details'][0]['country'] + ' [' + ap_det[
                                                                'ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details'][0]['location'])
                                            else:
                                                ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                                            ap_det['ap_details']['country'] + ' [' + ap_det[
                                                                'ap_code'] + '] - ' + str(ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details']['location'])
                                        else:
                                            if len(ap_det['ap_details']) == 1:  # there is '0'
                                                ap_to_add = ap_det['ap_details'][0]['city_name'] + " " + \
                                                            ap_det['ap_details'][0]['country'] + ' ' + ap_det[
                                                                'ap_code'] + ' [' + ap_det[
                                                                'ap_parent_code'] + '] - ' + str(
                                                    ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details'][0]['location'])
                                            else:
                                                ap_to_add = ap_det['ap_details']['city_name'] + " " + \
                                                            ap_det['ap_details']['country'] + ' ' + ap_det[
                                                                'ap_code'] + ' [' + ap_det[
                                                                'ap_parent_code'] + '] - ' + str(
                                                    ap_nearby['dist']) + 'KM ' + str(ap_det['ap_details']['location'])
                                        matched_aps.append(ap_to_add)
                                        break
                            else:
                                break  # sorted by distance, the next aps will have bigger distance

            logging.info(matched_aps)
            self.response.write(json.dumps(matched_aps, indent=2))


#   Handle flights form
class FlightFormAttHandler(webapp2.RequestHandler):
    def post(self):

        data = json.loads(self.request.body)
        # read form data:
        obj = data['obj']

        from api.QpxExpress import QpxExpress
        qpx = QpxExpress

        qpx_request = qpx.create_qpx_json_from_request(obj)


        dict_details = qpx_request['dict_details']
        list_legs = qpx_request['list_legs']

        wrap_qpx = qpx.qpx_wrapping(list_legs, dict_details)

        self.response.write("qpx response:")
        self.response.write(json.dumps(wrap_qpx, indent=4))

        return self.response


class FinalOrder(BaseHandler):

    def post(self):
        return # disable booking for production server
        from api.FlightsArea import TBS_Functions
        data = json.loads(self.request.body)
        api_results = self.app.config.get('api_results')
        api_results.get_itineraries(self.user_info['token_ts']) # just to load data from db if not it memory
        passengers_ids = api_results.get_passengers(self.user_info['token_ts'])
        passengers = []
        PTC_list = []
        for id in passengers_ids:
            user = User.get_by_id(int(id))
            logging.info("LIRON PRINT NOTICE")
            logging.info("user = User.get_by_id(int(id))")
            logging.info(user)
            PTC_list.append(user.PTC)
            passengers.append({'Title': user.title,
                             'FirstName': user.name,
                             'LastName': user.last_name,
                             'BirthDate': user.birth_date.split('T')[0],
                             'Email': user.email,
                             'Type': user.type,
                             'PTC': user.PTC,
                             'PaxRef': user.document_number, # was "1".
                             'document_type': user.document_type,
                             'document_number': user.document_number,
                             'document_expiry_date': ('2020-01-01' if user.document_expiry_date is None else user.document_expiry_date.split('T')[0]),
                             'document_issue_country': user.document_issue_country,
                             'document_nationality': user.document_nationality,
                             'seat_preference': user.seat_preference,

            })
        itineraries = json.loads(data['flights'])
        details = []
        booking_results = []
        flight =[]
        fare_rules = []
        for itinerary in itineraries:
            flight.append(itinerary)
            logging.info("processing itinerary...")
            # create booking
            itinerary_id = itinerary['id']
            query_resultCode = itinerary['query_resultCode']
            xml = TBS_Functions.Create_Booking_XML(query_resultCode,itinerary_id,  passengers, 1)
            logging.info("XMl booking: %s" % xml)
            output = TBS_Functions.Send_TBS_Query(xml, "http://tbs.dcsplus.net/ws/Air_MakeReservation")
            booking_entry = xmltodict.parse(output)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:Air_MakeReservationRS']
            logging.info(json.dumps(booking_entry))

            # booking_entry = {'ReservationID': 92949,
            #                  'ConfirmationNo': query_resultCode,
            #                  'Status': 'sucess',
            #                  'Price': '123',
            #                  'Currency': 'USD'}
            # logging.info("entry mockup: %s " % booking_entry)
            booking_results.append(booking_entry)

            if "ns1:Errors" in booking_entry.keys():
                logging.error("Error while booking: %s" % booking_entry['ns1:Errors']['ns1:Error']['ns1:Message'])
            else:
                # # after booking, get booking information. First, prepare the xml query
                # query_booking_msg = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
                #    <soapenv:Header>
                #       <ns:AuthHeader>
                #          <ns:ResellerCode>V2CC</ns:ResellerCode>
                #          <ns:Username>tomer</ns:Username>
                #          <ns:Password>tomer1</ns:Password>
                #          <!--Optional:-->
                #          <ns:ApplicationId>?</ns:ApplicationId>
                #       </ns:AuthHeader>
                #    </soapenv:Header>
                #    <soapenv:Body>
                #       <ns:ReservationGetDetailsRQ>
                #          <ns:ReservationID>{ReservationID}</ns:ReservationID>
                #       </ns:ReservationGetDetailsRQ>
                #    </soapenv:Body>
                # </soapenv:Envelope>"""
                # msg = query_booking_msg.format(ReservationID=booking_entry["ns1:Reservation"]["@ReservationID"])
                # output = TBS_Functions.Send_TBS_Query(msg, "http://tbs.dcsplus.net/ws/Reservation_GetDetails")
                # results = xmltodict.parse(output)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:ReservationGetDetailsRS']
                #
                # #output = client.service.Reservation_GetDetails(__inject={'msg': query_booking_msg.format(ReservationID=booking_entry['ReservationID'])})
                # logging.info("Reservation details: %s" % json.dumps(results))
                # details.append(results)

                # get flight fare for each booked flight
                query_flight_fare = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://tbs.dcsplus.net/ws/1.0/">
                   <soapenv:Header>
                      <ns:AuthHeader>
                         <ns:ResellerCode>V2CC</ns:ResellerCode>
                         <ns:Username>tomer</ns:Username>
                         <ns:Password>tomer1</ns:Password>
                         <!--Optional:-->
                         <ns:ApplicationId>?</ns:ApplicationId>
                      </ns:AuthHeader>
                   </soapenv:Header>
                   <soapenv:Body>
                      <ns:Air_FareRulesRQ>
                         <ns:ResultCode>{ResultCode}</ns:ResultCode>
                         <ns:ItineraryCode>{ItineraryCode}</ns:ItineraryCode>
                         <ns:PTC>{PTC}</ns:PTC>
                      </ns:Air_FareRulesRQ>
                   </soapenv:Body>
                </soapenv:Envelope>"""
                rules = []
                for PTC in set(PTC_list): # fare rules changes per PTC
                    msg = query_flight_fare.format(ResultCode=query_resultCode,ItineraryCode=itinerary_id,PTC=PTC)
                    output = TBS_Functions.Send_TBS_Query(msg, "http://tbs.dcsplus.net/ws/Air_FareRules")
                    print output
                    rules.append(xmltodict.parse(output)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:Air_FareRulesRS'])
                fare_rules.append(rules)

        ans = Booked_Itineraries(
                                 passengers=json.dumps(passengers),
                                 bookings_response=json.dumps(booking_results),
                                 bookings_data=None,
                                 active=None,
                                 company=self.user_info['company'],
                                 fare_rules=json.dumps(fare_rules),
                                 flights=json.dumps(flight)).put()
        key_id = ans._Key__pairs[0][1] # there is probably a better way to get the id...
        logging.info('key id: %s' % key_id)
        self.response.headers['Content-Type'] = 'application/json'
        self.response.write(json.dumps(key_id))
        # Booked_Itineraries_Status(
        #         passengers=[x["Email"] for x in passengers],
        #         booking_id=[x["ns1:Reservation"]["@ReservationID"] for x in details],
        #         status=[x["ns1:Reservation"]["ns1:Services"]["ns1:AirService"]["@Status"] for x in details],
        #         bookings_data=[json.dumps(x) for x in details]).put()


        # email to all admins
        admins = filter(lambda x: x.admin == True, User.get_users_by_company(self.user_info['company']))
        emails = ",".join(map(lambda x: x.email, admins))
        logging.info('Sending emails to: %s' % emails)

        # sender_address = (
        #     'OptimalTrip.com Support <{}@appspot.gserviceaccount.com>'.format(
        #         app_identity.get_application_id()))
        # subject = 'Reservation Notification'
        # body = """Reservation Status..."""
        # mail.send_mail(sender_address, emails, subject, body)



        # read data from client
        # generate email
        # send email

        #self.response.write(json.dumps(data, indent=2))
        #return self.response



        # Cancel the booking
#         msg = TBS_Functions.Cancle_Booked_Itinerary(booking_entry['ReservationID'])
#         result = client.service.Air_MakeReservation(__inject={'msg': msg})
#         self.response.write('<xmp>%s</xmp><br>' % result)

#         ans = Booked_Itineraries(passengers=json.dumps(passengers), itineraries=json.dumps([itinerary])).put()
#         logging.info(ans)
#         self.response.write('<h2>Booking response</h2><br>')
#         self.response.write('<p>%s</p>' % ans)



class UpdateFilteredData(BaseHandler):
    def post(self):
        username = self.user_info['token_ts']
        api_results = self.app.config.get('api_results')
        logging.info("Received from client: %s" % self.request.body)
        data = json.loads(self.request.body)
        api_results.put_filters_data(username,data['filteres_data'])
        api_results.put_sorting_data(username, data['sorting_data'])
        api_results.put_display_limit(username, data['display_limit'])
        logging.info("data['filteres_data': %s" % data['filteres_data'])
        logging.info("data['sorting_data']: %s" % data['sorting_data'])
        logging.info("data['display_limit']: %s" % data['display_limit'])

        #
        # persistent_data = api_results.data[self.user_info['token_ts']]
        # persistent_data['itineraries'] = None
        # persistent_data['filters'] = data['filteres_data']
        # persistent_data['sorting_data'] = data['sorting_data']
        # persistent_data['display_limit'] = data['display_limit']
        #
        # logging.info("Saving  persistent data to database... ")
        # logging.debug("Saving  persistent data to database: %s " % json.dumps(persistent_data))
        #
        # ancestor_key = ndb.Key("PersistentData", self.user_info['token_ts'])
        # keys = PersistentData.query().fetch(keys_only=True)
        # for key in keys:
        #     if key.pairs()[0] == ancestor_key.pairs()[0]:
        #         key.delete()
        # PersistentData(parent=ancestor_key, data=json.dumps(persistent_data)).put()

        answer = {'success': True,
                  'message': ''}
        self.response.write(json.dumps(answer))

    def get(self):
        filter_data = {
            'min_price': {'value': 3000},
            'max_price': {'value': 20000},
            'stops_range': {'values': [1]},
                       }
        sorting_data  = {'name': 'duration', 'reverse': False}
        display_limit = 10
        username = self.user_info['token_ts']
        api_results = self.app.config.get('api_results')
        logging.info("Received from client: %s" % json.dumps({'filters_data': filter_data,
                                                              'sorting_data': sorting_data,
                                                              'display_limit': display_limit}))
        api_results.put_filters_data(username,filter_data)
        api_results.put_sorting_data(username, sorting_data)
        api_results.put_display_limit(username, display_limit)


class roundTripFlightForm(BaseHandler):

    def post(self):
        """ Get a search query from the client. Create queries for TBS, send them in parallel,
            and save the aggregated results in global var in memory of specific user, to allow
            serving to client in later time and/or different methods. At the moment, only later
            and as a block of data. """
        logging.info("roundTripFlightForm: Entering")
        username = self.user_info['token_ts']
        from api.FlightsArea import FlightsMain,TBS_Functions
        api_results = self.app.config.get('api_results')

        logging.info("Received from client: %s" % self.request.body)
        data = json.loads(self.request.body)
        username_for_history = self.user_info['auth_ids'][0]
        api_results.put_user_input(username,data)



        # clean filters/sort/limit data
        api_results.put_filters_data(username,None)
        api_results.put_sorting_data(username, None)
        api_results.put_display_limit(username, None)

        api_results.put_server_filters_limits(username, None)

        # allow multiple coma separated airport list
        for leg in data['list_legs']:
            ap_list=[]
            split_ap = [airport.strip() for airport in leg['destination'][0].split(',')]
            for ap in split_ap:
                if len(ap) == 3:
                    ap_list.append(ap)            
            leg['destination'] = list(set(ap_list))

            ap_list=[]
            split_ap = [airport.strip() for airport in leg['origin'][0].split(',')]
            for ap in split_ap:
                if len(ap) == 3:
                    ap_list.append(ap)            
            leg['origin'] = list(set(ap_list))
            
        # for date1 in leg['dates_options']
        #     logging.info("leg['dates_options'] :%s" % leg['dates_options'])

            if leg['date']: leg['date'] = [date1.strip() for date1 in leg['date'][0].split(',')]
            if leg['stay_duration']: leg['stay_duration'] = [duration.strip() for duration in leg['stay_duration'][0].split(',')]
            if leg['dates_options']: leg['dates_options'] = [roundTripFlightForm.fix_earlier_date(self, leg['dates_options'][0])]
            logging.info('leg[dates_options] %s' %leg['dates_options'])

        data_for_api = FlightsMain.GetAllFlightsCombinations(data)
        logging.info("data_for_api %s " % data_for_api)

        filters_data = roundTripFlightForm.create_filters_from_form(data['list_legs'])
        api_results.put_filters_data(username, filters_data)

        logging.info("data['list_legs'] :%s" %data['list_legs'])
        passengers = data['passengers']
        # count all passengers as adults.
        # Todo: get information on each user and count adult/senior.
        data['legs_common_details']['adult'] = len(passengers)

        queries = TBS_Functions.generate_TBS_queries(data_for_api)
        # logging.info("queries %s " % queries)
        raw_results = TBS_Functions.Query_Flights_TBS_Servers(queries, self)

        for raw_result in raw_results:
            root = ET.fromstring(raw_result)
            if len(root[0][0]) == 0: 
                logging.info("Query returned no results")
            elif len(root[0][0][0]) == 0:
                logging.info("Query returned no results 2")
            elif 'Errors' in root[0][0][0].tag:
                for error in root[0][0][0]:
                    logging.info("Query returned an error: %s" % error[0].text)
                    self.response.write(json.dumps(error[0].text))
                raw_results.remove(raw_result)
        num_legs = len(data['list_legs'])
        itineraries = TBS_Functions.Convert_TBS_to_Internal_Format(raw_results, num_legs)
        logging.info("Got %s itinerary results" % len(itineraries))
        # logging.info("itineraries %s " % itineraries )


        # filter itineraries by form inputs
        logging.info('Filters Data: %s' % filters_data)
        filtered_itineraries = filter(lambda x: Itinerary_Filters.combined_filters(filters_data, x),itineraries)
        logging.info('Len of filtered_list: %s' % len(filtered_itineraries)) #todo : TBD- maybe if the len is 0 continue with the non filtered results
        api_results.put_filters_data(username, None) #the form filters are for cutting the filtered flights from the results. there isn't a point for running these fillters again becuse it should allways give true (there isn'r a way new results will arrive for now)


        # add land calculations data to the itineraries
        # itineraries_w_land_calc = TBS_Functions.internal_JSON_land_calc(filtered_itineraries[:20], data)
        itineraries_w_land_calc = TBS_Functions.internal_JSON_land_calc_parallel(filtered_itineraries,data)
        logging.info("itineraries_w_land_calc: %s" % len(itineraries_w_land_calc))
        new_itineraries_w_land_calc = FlightsMain.flights_calculations_per_flight(itineraries_w_land_calc)
        #roundTripFlightForm.save_itineraries_to_disk(self,new_itineraries_w_land_calc)
        logging.info("new_itineraries_w_land_calc: %s" % len(new_itineraries_w_land_calc))

        itineraries_server_limits_and_ranges = Itinerary_Filters.Create_itineraries_limits_and_ranges(data['list_legs'], new_itineraries_w_land_calc)
        api_results.put_server_filters_limits(username, itineraries_server_limits_and_ranges)
        #itineraries_for_client = FlightsMain.Convert_Our_Format_to_Client_Format(new_itineraries_w_land_calc)
        # logging.info("itineraries for_client: %s" % itineraries_for_client)
        # save all query results per user in memory. Delete old data if exist.
        api_results.put_itineraries(username, new_itineraries_w_land_calc)
        #logging.info(new_itineraries_w_land_calc)
        # api_results.put_itineraries(username, itineraries)
        api_results.put_passengers(username, passengers)

# save data to disk
#         itineraries = new_itineraries_w_land_calc[:1500]
#         logging.info("Saving  persistent data and %s itineraries to database..." % len(itineraries))
#         # get all other data of a user, without itineraries which can be very big
#         persistent_data = api_results.data[self.user_info['token_ts']]
#         persistent_data['itineraries'] = None
#
#
#         # create a key and delete all previous results of that user
#         ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
#         keys = Itineraries.query().fetch(keys_only=True)
#         for key in keys:
#             if key.pairs()[0] == ancestor_key.pairs()[0]:
#                 key.delete()
#         for i, element in enumerate(itineraries, 1):
#             Itineraries(parent=ancestor_key, index=i, itinerary=json.dumps(element)).put()
#             logging.info("Saving itinerary %s to database" %i )
#         logging.info("Saving itineraries to database")
#         ancestor_key = ndb.Key("PersistentData", self.user_info['token_ts'])
#         keys = PersistentData.query().fetch(keys_only=True)
#         for key in keys:
#             if key.pairs()[0] == ancestor_key.pairs()[0]:
#                 key.delete()
#         PersistentData(parent=ancestor_key, data=json.dumps(persistent_data)).put()
#         # logging.info("Saving  persistent data to database: %s " % json.dumps(persistent_data))


        answer = {'success': True,
                  'message': ''}
        self.response.write(json.dumps(answer))
        logging.info("roundTripFlightForm: Exiting")
        api_results.stop_progress(username)


        #print ShowHistory(self).getFormById("2016817125742ziv")


    # Gets date in the format: 2016-10-26T21:00:00.000Z
    # returns 2016-10-27T00:00 if the hours after the 'T' are between 12:01 to 23:59
    # if the hours between 00:00 to 12:00 it returns 2016-10-26T00:00
    def fix_earlier_date(self, org_date):
        from datetime import datetime, timedelta

        str_org_date = org_date[0:10]
        str_org_time = org_date[11:19]
        new_str_time = "00:00"
        str_org_h_time = int(str(org_date)[11:13])

        str_org_date_time = str_org_date + ' ' + str_org_time
        format_date_time = datetime.strptime(str_org_date_time, "%Y-%m-%d %H:%M:%S")
        date_time_plus_1_day = format_date_time + timedelta(hours=24)

        if str_org_h_time >= 12 and str_org_h_time < 24:
            format_date_time = date_time_plus_1_day

        # str_fixed_date = str(format_date_time)[0:10] + 'T' + new_str_time
        str_fixed_date = str(format_date_time)[0:10]

        return str_fixed_date


    @staticmethod
    def create_filters_from_form(list_legs):
        # logging.info(list_legs)
        # min_time_arrival = {'value': []}
        # max_time_arrival = {'value': []}
        # min_time_departure = {'value': []}
        # max_time_departure = {'value': []}
        date_arrival = {'value': []}
        for i,leg in enumerate(list_legs,1):
            if leg['arrive_depart'] == "arrive":
                f = {'values': [], 'leg_index': i}
                for single_date in leg['dates_options_arrival']:
                    f['values'].append(single_date.split('T')[0])
                date_arrival['value'].append(f)


        # date_departure = {'value': []}
        max_stops = {'value': []}
        min_stop_time = {'value': []}
        max_stop_time = {'value': []}
        # pos_airlines = {'value': []}
        # neg_airlines = {'value': []}
        min_hour_arrival = {'value': []}
        min_hour_departure = {'value': []}
        max_hour_arrival = {'value': []}
        max_hour_departure = {'value': []}

        for leg_index in range(len(list_legs)):
            max_stops_data = {'leg_index': leg_index + 1,
                         'value': list_legs[leg_index]['max_stop']
                         }
            max_stops['value'].append(max_stops_data)

            max_stop_time_data = {'leg_index': leg_index + 1,
                                  'value': int(list_legs[leg_index]['max_connection_duration'])*60
                                  }
            max_stop_time['value'].append(max_stop_time_data)

            # Fixed bug : changed int casting to float casting.
            min_stop_time_data = { 'leg_index': leg_index+1,
                         'value' : float(list_legs[leg_index]['min_stop'])*60
            }
            min_stop_time['value'].append(min_stop_time_data)

            if list_legs[leg_index]['arrive_depart'] == 'depart':
                # date_departure_data = { 'leg_index': leg_index + 1,
                #                         'values': list_legs[leg_index]['dates_options'][0].split('T')[0]
                # }
                min_hour_departure_data = { 'leg_index': leg_index + 1,
                                            'value': int(
                                                list_legs[leg_index]['earliest_time'].split(':')[0]) * 60 + int(
                                                list_legs[leg_index]['earliest_time'].split(':')[1])
                                            }
                max_hour_departure_data = { 'leg_index': leg_index + 1,
                                            'value': int(list_legs[leg_index]['latest_time'].split(':')[0]) * 60 +int(list_legs[leg_index]['latest_time'].split(':')[1])
                }
                # date_departure['value'].append(date_departure_data)
                min_hour_departure['value'].append(min_hour_departure_data)
                max_hour_departure['value'].append(max_hour_departure_data)

            else:
                # date_arrival_data = { 'leg_index': leg_index + 1,
                #                       'values': list_legs[leg_index]['dates_options'][0].split('T')[0]
                # }
                min_hour_arrival_data = {'leg_index': leg_index + 1,
                                           'value': int(list_legs[leg_index]['earliest_time'].split(':')[0])*60+int(list_legs[leg_index]['earliest_time'].split(':')[1])
                }
                max_hour_arrival_data = {'leg_index': leg_index + 1,
                                           'value': int(list_legs[leg_index]['latest_time'].split(':')[0])*60+ int(list_legs[leg_index]['latest_time'].split(':')[1])
                }
                # date_arrival['value'].append(date_arrival_data)
                min_hour_arrival['value'].append(min_hour_arrival_data)
                max_hour_arrival['value'].append(max_hour_arrival_data)
        filters_data={
            'max_stops': max_stops,
            'max_stop_time': max_stop_time,
            # 'date_arrival':date_arrival,
            'min_hour_arrival':min_hour_arrival,
            'max_hour_arrival':max_hour_arrival,
            # 'date_departure': date_departure,
            'min_hour_departure': min_hour_departure,
            'max_hour_departure': max_hour_departure,
            'min_stop_time':min_stop_time,
            'date_arrival': date_arrival
        }
        logging.info("filters_data: %s" % filters_data)
        return filters_data


class ShowHistory(BaseHandler):
    def showAllForms(self):
        ancestor_key = ndb.Key("Entered Data", self.user_info['token_ts'])
        data = EnteredData.query_data(ancestor_key)
        for element in data:
          print "form id: %s\n" % element.form_id
          list_legs = json.loads(element.enteredData)['list_legs']
          destination = list_legs[0]['destination']
          #print destination
          preferred_earliest_time = list_legs[0]['preferred_earliest_time']
          #print preferred_earliest_time

    def getAllIds(self):
        ancestor_key = ndb.Key("Entered Data", self.user_info['token_ts'])
        data = EnteredData.query_data(ancestor_key)
        all_ids = []
        for element in data:
            all_ids.append(element.form_id)
        return all_ids

    def getFormById(self, fid):
        ancestor_key = ndb.Key("Entered Data", self.user_info['token_ts'])
        data = EnteredData.query_data(ancestor_key)
        for element in data:
            if element.form_id == fid:
                return json.loads(element.enteredData)
