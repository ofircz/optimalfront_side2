# -*- coding: utf-8 -*-
from views_classes.Users import BaseHandler
import json
from google.appengine.ext import ndb
from models import Itineraries, User, Booked_Itineraries, PersistentData
import logging
import Queue
import copy
from api.FlightsArea import FlightsMain, TBS_Functions
import xml.etree.cElementTree as ET
from api.FlightsArea import Itinerary_Keys, Itinerary_Filters
from api.google_maps import GoogleMaps
import xmltodict
import re

# import suds


def user_required(handler):
    """
      Decorator that checks if there's a user associated with the current session.
      Will also fail if there's no session present.
    """

    def check_login(self, *args, **kwargs):
        if not self.user_info:
            return self.redirect(self.uri_for('login'), abort=True)
        return handler(self, *args, **kwargs)

    return check_login

class ShowProgress(BaseHandler):
    def get(self):
        api_results = self.app.config.get('api_results')
        progress = api_results.get_progress(self.user_info['token_ts'])
        return self.response.write(json.dumps(progress, indent=2))

class StopProgress(BaseHandler):
    def get(self):
        api_results = self.app.config.get('api_results')
        api_results.stop_progress(self.user_info['token_ts'])


class HotelSearch(BaseHandler):
    def post(self):
        from api.FlightsArea import FlightsMain, TBS_Functions
        from string import Template

        logging.info("HotelSearch: Entering")
        username = self.user_info['token_ts']
        from api.FlightsArea import FlightsMain,TBS_Functions
        api_results = self.app.config.get('api_results')

        logging.info("Received from client: %s" % self.request.body)
        data = json.loads(self.request.body)
        logging.info(json.dumps(data,indent=2))
        # query_data = data['query']
        # filters_data = data['filters']
        query_data = data

        # # get geo location
        from api.google_maps import GoogleMaps
        google_maps = GoogleMaps
        input_address = query_data['hotels'][0]['location']
        addr_geo = google_maps.get_geo_string_result(input_address)
        logging.info("addr_geo %s" % addr_geo)
        if addr_geo != '0,0':  # '0,0' is the get_geo_string_result's result if there isn't geocode
            lat = float(addr_geo.split(',')[0])
            lng = float(addr_geo.split(',')[1])
        # build query for TBS
        request_json = {'timeout':60,
                        'limit_results':50,
                        'latitude':lat,
                        'longitude':lng,
                        'radius':query_data['hotels'][0]['radius'],
                        'start':query_data['hotels'][0]['arrive_dates_options']['0'].split('T')[0],
                        'end': query_data['hotels'][0]['depart_dates_options']['0'].split('T')[0],
                        'stars': query_data['hotels'][0].get('stars',""),
                        'rooms': query_data['hotels'][0]['room'],
                        'natinality_id':1,
                        }

        # send query and store results
        xml = TBS_Functions.generate_TBS_hotel_query(request_json)
        logging.info(xml)

        query_results = TBS_Functions.Send_TBS_Query(xml, "Hotel_GetAvailabilityXML").replace('\n','')
        # logging.info(query_results)
        raw_results = xmltodict.parse(query_results)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:HotelGetAvailabilityRS']
        raw_results = json.loads(json.dumps(raw_results))
        # logging.info("--------------------------------------------------------")
        # logging.info(json.dumps(raw_results))
        # logging.info("--------------------------------------------------------")

        # parse results to internal format
        room_template = Template("""{
                            "name": "$room_name",
                            "full_name": "$room_full_name",
                            "board": "$board",
                            "info": "$room_info",
                            "adults": "$adults",
                            "price": $price,
                            "currency": "$currency",
                            }""")

        hotel_template = Template("""{
                            "hotel_name": "$hotel_name",
                            "hotel_id": "$hotel_id",
                            "rooms": [$rooms],
                            "type": "$type",
                            "stars": "$stars",
                            "price": $price,
                            "currency": "$currency",
                            "image": "$image",
                            "latitude": "$latitude",
                            "logitude": "$longitude",
                            "address": \"\"\"$address\"\"\",
                            "description": \"\"\"$description\"\"\",
                        }""")

        hotels_template = Template(""" {"hotels": [
                {
                    "area_details": {
                        "State": "California",
                        "area": "California",
                        "continent": "America"
                    },
                    "highest_ranked": [$hotel_list],
                    "link_to_hotels_in_the_area": [
                        "\u202bhttp://www.hotelscombined.co.il/Place/Lake_Forest.htm?a_aid=139799\u202c\u202c",
                        "\u202ahttp://www.booking.com/searchresults.he.html?city\u202c\u202c \u202b\u202a=20013943&aid=825774\u202c\u202c"
                    ],
                    "coords": [
                        {
                            "latitude": "32.853438",
                            "longitude": "-80.056785",
                            "id": ""
                        }
                    ]
                },
                ]}""")
        hotel_list = []
        # check for errors
        if 'ns1:Errors' in raw_results.keys():
            msg = raw_results['ns1:Errors']['ns1:Error']['ns1:Message']
            logging.info(msg)
            self.response.write(json.dumps({'error': msg}))
            results = []
        elif raw_results['ns1:AvailHotels']['@CountHotels'] != "0":
            for hotel in raw_results['ns1:AvailHotels']['ns1:Hotel']:
                hotel_id = hotel['ns1:HotelDetails']['@ID']
                hotel_name = hotel['ns1:HotelDetails']['@Name']
                hotel_type = hotel['ns1:HotelDetails']['@Type']
                hotel_stars = hotel['ns1:HotelDetails']['@Stars']
                if 'ns1:Image' in hotel['ns1:HotelDetails'].keys():
                    image = hotel['ns1:HotelDetails']['ns1:Image']['@URL']
                else:
                    image = ""
                latitude = hotel['ns1:HotelDetails']['ns1:Position']['@Latitude']
                longitude = hotel['ns1:HotelDetails']['ns1:Position']['@Longitude']
                address = hotel['ns1:HotelDetails']['ns1:Address']
                description = hotel['ns1:HotelDetails']['ns1:Descriptions']['ns1:ShortDescription']

                logging.info("Processing hotel %s" % hotel_name)
                if type(hotel['ns1:HotelOffers']['ns1:HotelOffer'])==dict:
                    hotel_offers = [hotel['ns1:HotelOffers']['ns1:HotelOffer']]
                else:
                    hotel_offers = hotel['ns1:HotelOffers']['ns1:HotelOffer']
                room_list = []
                for hotel_offer in hotel_offers:
                    # print "hotel_offer: %s " % hotel_offer
                    if type(hotel_offer['ns1:Packages']['ns1:Package'])!=list:
                        packages = [hotel_offer['ns1:Packages']['ns1:Package']]
                    else:
                        packages = hotel_offer['ns1:Packages']['ns1:Package']

                    if type(hotel_offer['ns1:Rooms']['ns1:Room'])!=list:
                        rooms = [hotel_offer['ns1:Rooms']['ns1:Room']]
                    else:
                        rooms = hotel_offer['ns1:Rooms']['ns1:Room']

                    for package in packages:
                        PackageCode = package['@PackageCode']
                        price = package['ns1:Price']['@Amount']
                        currency = package['ns1:Price']['@Currency']
                        if type(package['ns1:PackageRooms']['ns1:PackageRoom'])==dict:
                            package_rooms = [package['ns1:PackageRooms']['ns1:PackageRoom']]
                        else:
                            package_rooms = package['ns1:PackageRooms']['ns1:PackageRoom']
                        for package_room in package_rooms:
                            room_ref = package_room['ns1:RoomRefs']['ns1:RoomRef']
                            if type(room_ref)==list:
                                room_ref = int(room_ref[0]['@RoomCode'])
                            else:
                                room_ref = int(room_ref['@RoomCode'])
                            adults = package_room['ns1:Occupancy']['@Adults']
                            room_full_name = rooms[room_ref]['ns1:Name']
                            #### normalize hotel name ###
                            room_desc = ['Standard','Delux', 'Executive', ]
                            room_type = ['Room', 'Suite', 'Club', 'Dormitory']
                            for t in room_type:
                                room_type_normalized = room_type[0]
                                if t.lower() in room_full_name.lower():
                                    room_type_normalized = t
                                    break
                            for d in room_desc:
                                room_desc_normalized = room_desc[0]
                                if d.lower() in room_full_name.lower():
                                    room_desc_normalized = d
                                    break
                            room_name = "%s %s" % (room_desc_normalized, room_type_normalized)
                            logging.info("Normalize '%s' to '%s'" % (room_full_name,room_name))

                            ### done normalizing room name ###
                            board = rooms[room_ref].get('ns1:Board',"N/A")
                            info = rooms[room_ref].get('ns1:Info',"N/A")
                            room_list.append(room_template.substitute(board=board, room_full_name=room_full_name, room_name=room_name, room_info=info, adults=adults,
                                                                      price=float(price), currency=currency))
                # logging.info("Adding hotel: %s to the list" % hotel_name)
                # logging.info("room_list: %s" % room_list)
                room_list.sort(key=lambda x: eval(x)['price']) # sort room by price
                hotel_list.append(hotel_template.substitute(hotel_name=hotel_name,
                                                            hotel_id=hotel_id,
                                                            type=hotel_type,
                                                            stars=hotel_stars,
                                                            image=image,
                                                            address=address,
                                                            longitude=longitude,
                                                            latitude=latitude,
                                                            description=description,
                                                            rooms=','.join(room_list),
                                                            price=float(price),
                                                            currency=currency))
            results = eval(hotels_template.substitute(hotel_list=','.join(hotel_list)))
        else:
            results = []

        #filtered_hotels = filter(lambda x: Hotel_Filters.combined_filters(filters_data, x),results['highest_ranked'])
        #api_results.put_hotels(username, filtered_hotels)
        #logging.info(results)
        api_results.put_hotels(username, results)
        answer = {'success': True,
                  'message': ''}
        self.response.write(json.dumps(answer))
        logging.info("HotelSearch: Exiting")



    def get(self):
        from api.FlightsArea import FlightsMain, TBS_Functions
        from string import Template

        logging.info("HotelSearch: Get")
        username = self.user_info['token_ts']
        from api.FlightsArea import FlightsMain,TBS_Functions
        api_results = self.app.config.get('api_results')
        results = api_results.get_hotels(username)
        self.response.write(json.dumps(results))


class DisplayTBSQueries(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        from api.FlightsArea import FlightsMain, TBS_Functions
        import parsers.FlightAlg as FAlg

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        data_for_api = FlightsMain.GetAllFlightsCombinations(server_qpx_inputs)
        qpx_queries = TBS_Functions.generate_TBS_queries(data_for_api)
        self.response.write('<h2>QPX Queries: %s</h2><br>' % len(qpx_queries))
        for i, q in enumerate(qpx_queries, 1):
            self.response.write('<h3>Query %s</h3><br>' % i)
            self.response.write('<xmp>%s</xmp><br>' % q)
            logging.debug(q)


class DisplayTBSResults(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        from api.FlightsArea import FlightsMain, TBS_Functions

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        data_for_api = FlightsMain.GetAllFlightsCombinations(server_qpx_inputs)
        queries = TBS_Functions.generate_TBS_queries(data_for_api)
        ans = FlightsMain.tbs_flights([queries[0]])
        self.response.write('<h2>Number of TBS queries to send: %s</h2><br>' % len(ans))

        for i, [q, a] in enumerate(zip(queries, ans), 1):
            self.response.write('<h3>Sending Query %s</h3><br>' % i)
            self.response.write('<xmp>%s</xmp><br>' % q)
            self.response.write('<h3>Receiving:</h3><br>')
            self.response.write('<xmp>%s</xmp><br>' % a)


# class RestoreTBSResults(BaseHandler):
#     @user_required
#     def get(self):
#         self.render_template('main.html')
#         api_results = self.app.config.get('api_results')
#         itineraries = api_results.get_filtered_sorted_itineraries(self.user_info['token_ts'])
#         if not itineraries:
#             self.response.write('<b>No results in memory</b><br>')
#         else:
#             logging.info("Got %s results" % len(itineraries))
#
#             # ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
#             # data = Itineraries.query_data(ancestor_key)
#             # for i,element in enumerate(data,1):
#             self.response.write(
#                 '<pre>%s</pre><br>' % json.dumps(api_results.get_passengers(self.user_info['token_ts']), indent=2))
#             for i, element in enumerate(itineraries, 1):
#                 self.response.write('<b>Itinerary %s:</b><br>' % i)
#                 self.response.write('<pre>%s</pre><br>' % json.dumps(element, indent=2))


class ShowTBSResults(BaseHandler):
    @user_required
    def get(self):
        from api.FlightsArea import FlightsMain
        api_results = self.app.config.get('api_results')
        itineraries = api_results.get_filtered_sorted_itineraries(self.user_info['token_ts'])
        user_input= api_results.get_user_input(self.user_info['token_ts'])
        list_legs= user_input['list_legs']

        all_server_filters_limit = api_results.get_server_filter_limit(self.user_info['token_ts'])
        limit = api_results.get_display_limit(self.user_info['token_ts'])
        sort_by = api_results.get_sorting_data(self.user_info['token_ts'])
        # logging.info("Client_Format:\n %s " % FlightsMain.Convert_Our_Format_to_Client_Format(itineraries))
        self.response.write(json.dumps(FlightsMain.Convert_Our_Format_to_Client_Format(itineraries, all_server_filters_limit,sort_by, limit,list_legs), indent=2))

        # ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
        # data = Itineraries.query_data(ancestor_key)
        # ans = []
        # self.response.headers['Content-Type'] = 'application/json'
        # for i,element in enumerate(data,1):
        #     ans.append(json.loads(element.itinerary))
        # self.response.write(json.dumps(FlightsMain.Convert_Our_Format_to_Client_Format(ans),indent=2))


# class DebugRestoreTBSResults(BaseHandler):
#     @user_required
#     def get(self):
#         self.render_template('main.html')
#         api_results = self.app.config.get('api_results')
#         itineraries = api_results.get_filtered_sorted_itineraries(self.user_info['token_ts'])
#         # total_stops= Itinerary_Keys.itinerary_stops_num(itineraries)
#         if not itineraries:
#             self.response.write('<b>No results in memory</b><br>')
#         else:
#             logging.info("Got %s results" % len(itineraries))
#
#             # ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
#             # data = Itineraries.query_data(ancestor_key)
#             # for i,element in enumerate(data,1):
#             self.response.write(
#                 '<pre>%s</pre><br>' % json.dumps(api_results.get_passengers(self.user_info['token_ts']), indent=2))
#             for i, element in enumerate(itineraries, 1):
#                 self.response.write('<b>Itinerary %s:</b><br>' % i)
#                 self.response.write('<pre>%s</pre><br>' % json.dumps(element, indent=2))
#                 self.response.write('<pre>total stops %s</pre><br>' % Itinerary_Keys.itinerary_stops_num(element))


class QueryTBS(BaseHandler):
    def get(self):
        """ Get a search query from the client. Create queries for TBS, send them in parallel,
            and save the aggregated results in global var in memory of specific user, to allow
            serving to client in later time and/or different methods. At the moment, only later
            and as a block of data. """
        logging.info("ConvertTBSResults: Entering")
        from api.FlightsArea import FlightsMain, TBS_Functions
        from api.Flight_inputs import QpxInputs
        data = QpxInputs.ServerQpxInputsBasic()
        # logging.info("Received static: %s" % json.dumps(data))
        passengers = data['passengers']
        data_for_api = FlightsMain.GetAllFlightsCombinations(data)
        queries = TBS_Functions.generate_TBS_queries(data_for_api)
        raw_results = TBS_Functions.Query_Flights_TBS_Servers(queries)
        for raw_result in raw_results:
            root = ET.fromstring(raw_result)
            if 'Errors' in root[0][0][0].tag:
                for error in root[0][0][0]:
                    logging.info("Query returned an error: %s" % error[0].text)
                    self.response.write(json.dumps(error[0].text))
                raw_results.remove(raw_result)
        num_legs = len(data['list_legs'])
        itineraries = TBS_Functions.Convert_TBS_to_Internal_Format(raw_results, num_legs)
        # logging.info("QueryTBS Internal_Format itineraries: %s" % itineraries)

        # save all query results per user in memory. Delete old data if exist.
        api_results = self.app.config.get('api_results')
        api_results.put_itineraries(self.user_info['token_ts'], itineraries)
        api_results.put_passengers(self.user_info['token_ts'], passengers)
        logging.info("len(itineraries): %s" % len(api_results.get_itineraries(self.user_info['token_ts'])))

        answer = {'success': True,
                  'message': ''}
        self.render_template('main.html')
        self.response.write(json.dumps(answer))
        logging.info("ConvertTBSResults: Exiting")


class CancelBookings(BaseHandler):
    def get(self, id):
        from api.FlightsArea import TBS_Functions
        xml = TBS_Functions.Cancle_Booked_Itinerary(id)
        logging.info("Cancle booking: %s" % id)
        output = TBS_Functions.Send_TBS_Query(xml, "http://tbs.dcsplus.net/ws/Reservation_Cancel")
        booking_entry = xmltodict.parse(output)['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:ReservationCancelRS']
        logging.info(json.dumps(booking_entry))
        self.response.write('<pre>%s</pre><br>' % json.dumps(booking_entry, indent=2))


class DisplayBookings(BaseHandler):
    def get(self, id):
        booked = Booked_Itineraries.get_by_id(int(id))
        if not booked:
            logging.info('Could not find any booking with id "%s"' % id)
            self.abort(404, 'This link has expired')
        else:
            self.response.headers['Content-Type'] = 'application/json'
            data = {
                'passengers': json.loads(booked.passengers),
                'bookings_response': json.loads(booked.bookings_response),
                'bookings_data': [],
                'company': booked.company,
                'active': False,
                'fare_rules': json.loads(booked.fare_rules),
                'flights': json.loads(booked.flights)
            }
            for itinerary in data['bookings_response']:
                # after booking, get booking information. First, prepare the xml query
                results = TBS_Functions.get_flights_reservation(itinerary["ns1:Reservation"]["@ReservationID"])
                logging.info("Reservation details: %s" % json.dumps(results))
                data['bookings_data'].append(results)
                if results['ns1:Reservation']['ns1:Services']['ns1:AirService']['@Status']=='OK':
                    data['active']=True
            self.response.write(json.dumps(data, indent=2))
            booked.active = data['active']
            booked.put()



class BookItineraries(BaseHandler):
    @user_required
    def post(self):
        """
        Get a list of passengers and itineraries to book.
        Book them, and store in database for later use
        """
        logging.info('running BookItineraries()')
        data = json.loads(self.request.body)
        from api.FlightsArea import TBS_Functions, FlightsMain
        user = User.get_by_id(self.user_info['user_id'])
        logging.debug(user)
        passengers = [{'Title': user.title,
                       'FirstName': user.name,
                       'LastName': user.last_name,
                       'BirthDate': user.birth_date.split('T')[0],
                       'Email': user.email,
                       'Type': user.type,
                       'PTC': user.PTC,
                       'PaxRef': "1",
                       }]

        self.render_template('main.html')
        self.response.write('<h3>Sending Booking Data to Server</h3><br>')
        for itinerary in data['itineraries']:
            msg = TBS_Functions.Book_Itinerary(itinerary['resultCode'], itinerary['id'], passengers, 1)
            logging.debug('XML for booking: %s' % msg)
            self.response.write('<xmp>%s</xmp><br>' % msg)
            self.response.write('<h3>Result from server</h3><br>')
            result = client.service.Air_MakeReservation(__inject={'msg': msg})
            self.response.write('<xmp>%s</xmp><br>' % result)
            # Do the booking
            #         url = 'http://www.talmatravel.co.il/tbs/reseller/ws/?wsdl'
            #         client = Client(url)
            #         result = client.service.Air_MakeReservation(__inject={'msg': msg})
            #         self.response.write('<xmp>%s</xmp><br>' % result)
            #
            #         booking_entry = {'ReservationID': result.Reservation._ReservationID,
            #                          'ConfirmationNo': result.Reservation.AirService._ConfirmationNo,
            #                          'Status': result.Reservation.AirService._Status,
            #                          'Price': result.Reservation.AirService.Price._Amount,
            #                          'Currency': result.Reservation.AirService.Price._Currency}
            #         logging.info(booking_entry)

            # Cancel the booking
            #         msg = TBS_Functions.Cancle_Booked_Itinerary(booking_entry['ReservationID'])
            #         result = client.service.Air_MakeReservation(__inject={'msg': msg})
            #         self.response.write('<xmp>%s</xmp><br>' % result)

            #         ans = Booked_Itineraries(passengers=json.dumps(passengers), itineraries=json.dumps([itinerary])).put()
            #         logging.info(ans)
            #         self.response.write('<h2>Booking response</h2><br>')
            #         self.response.write('<p>%s</p>' % ans)


class ShowBookingXML(BaseHandler):
    @user_required
    def get(self):
        logging.info('running ShowBookingXML()')
        #client = self.app.config.get('wsdl_client')
        from api.FlightsArea import TBS_Functions, FlightsMain
        user = User.get_by_id(self.user_info['user_id'])
        logging.info(user)
        passengers = [{'Title': user.title,
                       'FirstName': user.name,
                       'LastName': user.last_name,
                       'BirthDate': user.birth_date.split('T')[0],
                       'Email': user.email,
                       'Type': user.type,
                       'PTC': user.PTC,
                       'PaxRef': "1",
                       }]

        # get the first itinerary from saved results
        ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
        data = Itineraries.query_data(ancestor_key)
        for element in data:
            itinerary = json.loads(element.itinerary)
            break

        self.render_template('main.html')
        self.response.write('<h3>Sending Booking Data to Server</h3><br>')
        msg = TBS_Functions.Book_Itinerary(itinerary['resultCode'], itinerary['id'], passengers, 1)
        logging.debug('XML for booking: %s' % msg)
        self.response.write('<xmp>%s</xmp><br>' % msg)
        self.response.write('<h3>Result from server</h3><br>')

        # Do the booking
        #         url = 'http://www.talmatravel.co.il/tbs/reseller/ws/?wsdl'
        #         client = Client(url)
        #         result = client.service.Air_MakeReservation(__inject={'msg': msg})
        #         self.response.write('<xmp>%s</xmp><br>' % result)
        #
        #         booking_entry = {'ReservationID': result.Reservation._ReservationID,
        #                          'ConfirmationNo': result.Reservation.AirService._ConfirmationNo,
        #                          'Status': result.Reservation.AirService._Status,
        #                          'Price': result.Reservation.AirService.Price._Amount,
        #                          'Currency': result.Reservation.AirService.Price._Currency}
        #         logging.info(booking_entry)

        # Cancel the booking


# msg = TBS_Functions.Cancle_Booked_Itinerary(booking_entry['ReservationID'])
#         result = client.service.Air_MakeReservation(__inject={'msg': msg})
#         self.response.write('<xmp>%s</xmp><br>' % result)

#         ans = Booked_Itineraries(passengers=json.dumps(passengers), itineraries=json.dumps([itinerary])).put()
#         logging.info(ans)
#         self.response.write('<h2>Booking response</h2><br>')
#         self.response.write('<p>%s</p>' % ans)


class SaveResultsToDisk(BaseHandler):
    def get(self):
        api_results = self.app.config.get('api_results')
        itineraries = api_results.get_itineraries(self.user_info['token_ts'])

        # get all other data of a user, without itineraries which can be very big
        persistent_data = api_results.data[self.user_info['token_ts']]
        persistent_data['itineraries'] = None

        if not itineraries:
            self.render_template('main.html')
            self.response.write('<b>No results in memory</b><br>')
        else:
            self.render_template('main.html')
            # create a key and delete all previous results of that user
            ancestor_key = ndb.Key("TBS_Results", self.user_info['token_ts'])
            keys = Itineraries.query().fetch(keys_only=True)
            for key in keys:
                if key.pairs()[0] == ancestor_key.pairs()[0]:
                    key.delete()
            for i, element in enumerate(itineraries, 1):
                Itineraries(parent=ancestor_key, index=i, itinerary=json.dumps(element)).put()
                # logging.info("Saving itinerary %s to database" % i)
            logging.info("Saving itineraries to database")
            ancestor_key = ndb.Key("PersistentData", self.user_info['token_ts'])
            keys = PersistentData.query().fetch(keys_only=True)
            for key in keys:
                if key.pairs()[0] == ancestor_key.pairs()[0]:
                    key.delete()
            PersistentData(parent=ancestor_key, data=json.dumps(persistent_data)).put()
            logging.info("Saving  persistent data to database: %s " % json.dumps(persistent_data))
            self.response.write('<p>Results saved to database.</p>')


