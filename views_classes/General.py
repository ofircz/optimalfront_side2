from views_classes.Users import BaseHandler
import json
from models import Booked_Itineraries
from api.FlightsArea import TBS_Functions
import logging
import datetime

def user_required(handler):
    """
      Decorator that checks if there's a user associated with the current session.
      Will also fail if there's no session present.
    """
    def check_login(self, *args, **kwargs):
        if not self.user_info:
            return self.redirect(self.uri_for('login'), abort=True)
        return handler(self, *args, **kwargs)
    return check_login


class MainHandler(BaseHandler):
    def get(self):
        return self.render_template('home.html')

class DisplayUserInput(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        #self.response.headers['Content-Type'] = 'application/json'
        self.response.write('<h1> User Input </h1>')
        out = json.dumps(server_qpx_inputs, indent=2)
        self.response.write('<pre> %s </pre>' % out)
        #self.response.write(json.dumps(server_qpx_inputs, indent=2))


class DisplayDataForAPIGenerator(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        from api.FlightsArea import FlightsMain

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        data_for_api = FlightsMain.GetAllFlightsCombinations(server_qpx_inputs)
        for key in data_for_api.keys():
            self.response.write('<h2>%s</h2><br>' % key)
            self.response.write('<pre>%s</pre><br>' % json.dumps(data_for_api[key], indent=2))


# Not in use at the moment
class AddCalculationsOnResults(BaseHandler):
    @user_required
    def get(self):
        from api.FlightsArea import FlightsMain
        self.render_template('main.html')
        ancestor_key = ndb.Key("Data", "QPX_Results")
        data = Json_Data.query_data(ancestor_key)
        ans = FlightsMain.Add_Calculations_on_Results([x.flights for x in data])
        self.response.write('<pre>%s</pre><br>' % json.dumps(ans, indent=2))

class ShowCompanyBookings(BaseHandler):
    @user_required
    def get(self):
        bookings = Booked_Itineraries.get_bookings_by_company(self.user_info['company'])
        results = []
        for b in bookings:
            psg = []
            for p in json.loads(b.passengers):
                psg.append("%s %s" % (p['FirstName'], p['LastName']))
            results.append({'passengers': ','.join(psg),
                            'departure_date': json.loads(b.flights)[0]['legs'][0]['origin']['departure_date'],
                            'url': "/displayBookingCode/%s" % b.key._Key__pairs[0][1]})
        self.response.write('<pre>%s</pre><br>' % json.dumps(results, indent=2))


class ShowCompanyActiveBookings(BaseHandler):
    @user_required
    def get(self):
        bookings = Booked_Itineraries.get_active_bookings_by_company(self.user_info['company'])
        for b in bookings:
            id = b.key._Key__pairs[0][1]
            booked = Booked_Itineraries.get_by_id(int(id))
            b.active = False
            if booked:
                for itinerary in json.loads(booked.bookings_response):
                    logging.info("verifying booking")
                    results = TBS_Functions.get_flights_reservation(itinerary["ns1:Reservation"]["@ReservationID"])
                    if results['ns1:Reservation']['ns1:Services']['ns1:AirService']['@Status'] == 'OK':
                        booked.active = True
                booked.put()

        bookings = Booked_Itineraries.get_active_bookings_by_company(self.user_info['company'])
        results = []
        for b in bookings:
            psg = []
            for p in json.loads(b.passengers):
                psg.append("%s %s" % (p['FirstName'], p['LastName']))
            results.append({'passengers': ','.join(psg),
                            'departure_date': json.loads(b.flights)[0]['legs'][0]['origin']['departure_date'],
                            'url': "/displayBookingCode/%s" % b.key._Key__pairs[0][1]})
        self.response.write('<pre>%s</pre><br>' % json.dumps(results, indent=2))


class ShowAllActiveBookings(BaseHandler):
    @user_required
    def get(self):
        bookings = Booked_Itineraries.get_active_bookings()
        for b in bookings:
            id = b.key._Key__pairs[0][1]
            booked = Booked_Itineraries.get_by_id(int(id))
            b.active = False
            if booked:
                for itinerary in json.loads(booked.bookings_response):
                    logging.info("verifying booking")
                    results = TBS_Functions.get_flights_reservation(itinerary["ns1:Reservation"]["@ReservationID"])
                    if results['ns1:Reservation']['ns1:Services']['ns1:AirService']['@Status'] == 'OK':
                        booked.active = True
                booked.put()

        bookings = Booked_Itineraries.get_active_bookings()
        results = []
        for b in bookings:
            psg = []
            for p in json.loads(b.passengers):
                psg.append("%s %s" % (p['FirstName'], p['LastName']))
            results.append({'passengers': ','.join(psg),
                            'departure_date': json.loads(b.flights)[0]['legs'][0]['origin']['departure_date'],
                            'url': "/displayBookingCode/%s" % b.key._Key__pairs[0][1]})
        self.response.write('<pre>%s</pre><br>' % json.dumps(results, indent=2))

class ClearMemory(BaseHandler):
    def get(self):
        import os
        # clear user data
        api_results = self.app.config.get('api_results')
        active_users = api_results.get_all_users()
        if not active_users:
            self.response.write('<pre>No data in memory</pre><br>')
            return
        for user in active_users:
            delta = datetime.datetime.now() - api_results.get_last_update(user)
            mins_to_wait = 40
            if delta.seconds/60 > mins_to_wait:
                logging.info("User: %s - More then %s minutes from last update. Clearing memory of last search" % (user,mins_to_wait))
                api_results.put_itineraries(user,[])
                api_results.put_hotels(user, [])
                api_results.put_sorting_data(user, [])
                self.response.write('<pre>Clearing memory of user %s</pre><br>' % user)
            else:
                self.response.write('<pre>User: %s - Still fresh. Not clearing memory of user</pre><br>' % user)



class ShowSearchHistory(BaseHandler):
    def get(self):
        # Show User's search history
        api_results = self.app.config.get('api_results')
        username = self.user_info['token_ts']
        username_for_history = self.user_info['auth_ids'][0]
        history = api_results.get_user_input_history(username)
        self.response.write(json.dumps(history, indent=2))

