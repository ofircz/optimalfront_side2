from views_classes.Users import BaseHandler
import json
from google.appengine.ext import ndb
from models import Itineraries


def user_required(handler):
    """
      Decorator that checks if there's a user associated with the current session.
      Will also fail if there's no session present.
    """
    def check_login(self, *args, **kwargs):
        if not self.user_info:
            return self.redirect(self.uri_for('login'), abort=True)
        return handler(self, *args, **kwargs)
    return check_login

class DisplayQPXQueries(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        from api.FlightsArea import FlightsMain, QPX_Functions
        import parsers.FlightAlg as FAlg

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        data_for_api = FlightsMain.GetAllFlightsCombinations(server_qpx_inputs)
        qpx_queries = QPX_Functions.generate_QPX_queries (data_for_api)
        self.response.write('<h2>QPX Queries: %s</h2><br>' % len(qpx_queries))
        for i,q in enumerate(qpx_queries,1):
            self.response.write('<h3>Query %s</h3><br>' % i)
            self.response.write('<pre>%s</pre><br>' % json.dumps(q, indent=2))


class DisplayQPXResults(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        from api.Flight_inputs import QpxInputs
        from api.FlightsArea import FlightsMain, QPX_Functions

        server_qpx_inputs = QpxInputs.ServerQpxInputsBasic()
        data_for_api = FlightsMain.GetAllFlightsCombinations(server_qpx_inputs)
        qpx_queries = QPX_Functions.generate_QPX_queries (data_for_api)
        ans = QPX_Functions.qpx_flights_new(qpx_queries[:2])
        self.response.write('<h2>Number of QPX queries to send: %s</h2><br>' % len(ans))

        ancestor_key = ndb.Key("QPX_Results", self.user_info['name'])
        keys = Itineraries.query().fetch(keys_only=True)
        for key in keys:
            if key.pairs()[0] == ancestor_key.pairs()[0]:
                key.delete()

        for i,[q,a] in enumerate(zip(qpx_queries,ans),1):
            self.response.write('<h3>Sending Query %s</h3><br>' % i)
            self.response.write('<pre>%s</pre><br>' % json.dumps(q, indent=2))
            self.response.write('<h3>Receiving %s options:</h3><br>' % len(a['trips']['tripOption']))
            Itineraries(parent=ancestor_key, index=i, itinerary=json.dumps(a)).put()
            for n,item in enumerate(a['trips']['tripOption']):
                self.response.write('<b>Option %s:</b><br>' % n)
                self.response.write('<pre>%s</pre><br>' % json.dumps(item, indent=2))

class RestoreQPXResults(BaseHandler):
    @user_required
    def get(self):
        self.render_template('main.html')
        ancestor_key = ndb.Key("QPX_Results", self.user_info['name'])
        data = Itineraries.query_data(ancestor_key)
        for i,element in enumerate(data,1):
            self.response.write('<h3>Data for Query %s</h3><br>' % i)
            self.response.write('<b>General Data:</b><br>')
            self.response.write('<pre>%s</pre><br>' % json.dumps(json.loads(element.itinerary)['trips']['data'], indent=2))
            for n,item in enumerate(json.loads(element.flights)['trips']['tripOption'],1):
                self.response.write('<b>Option %s:</b><br>' % n)
                self.response.write('<pre>%s</pre><br>' % json.dumps(item, indent=2))


class ConvertQPXResults(BaseHandler):
    @user_required
    def get(self):
        from api.FlightsArea import QPX_Functions
        self.render_template('main.html')
        ancestor_key = ndb.Key("QPX_Results", self.user_info['name'])
        data = Itineraries.query_data(ancestor_key)
        ans = QPX_Functions.Convert_QPX_to_Our_Format(data)
        self.response.write('<pre>%s</pre><br>' % json.dumps(ans, indent=2))

