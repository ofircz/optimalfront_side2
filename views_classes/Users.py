# encoding=utf8

# libs
import webapp2
from google.appengine.ext.webapp import template
from google.appengine.api import mail
from google.appengine.api import app_identity
import os.path
from webapp2_extras import auth
from webapp2_extras import sessions
from webapp2_extras.auth import InvalidAuthIdError
from webapp2_extras.auth import InvalidPasswordError
from models import User
import logging
import json

def user_required(handler):
    """
      Decorator that checks if there's a user associated with the current session.
      Will also fail if there's no session present.
    """

    def check_login(self, *args, **kwargs):
        if not self.user_info:
            return self.redirect(self.uri_for('login'), abort=True)
        return handler(self, *args, **kwargs)

    return check_login


class BaseHandler(webapp2.RequestHandler):

    @webapp2.cached_property
    def user_info(self):
        """Shortcut to access a subset of the user attributes that are stored
        in the session.

        The list of attributes to store in the session is specified in
          config['webapp2_extras.auth']['user_attributes'].
        :returns
          A dictionary with most user information
        """
        return auth.get_auth().get_user_by_session()

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        params['user'] = self.user_info
        path = os.path.join(os.path.dirname(__file__), '../templates', view_filename)
        return self.response.out.write(template.render(path, params))

    def display_message(self, message):
        """Utility function to display a template with a simple message."""
        return self.render_template('message.html', {'message': message})

    # this is needed for webapp2 sessions to work
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)
        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)



class SignupHandlerAngular(BaseHandler):
    @user_required

    def get(self):
        return self.render_template('signup.html')

    def post(self):
        company = self.user_info['company'] # getting "company" field from current admin user.
        try:
            body = json.loads(self.request.body)
            print body
            username = body['username']
            is_admin = True if body['admin'] == "Yes" else False
            user_data = User.create_user(username,
                                         email=body['email'],
                                         password_raw=body['password'],
                                         name=body['name'],
                                         last_name=body['lastName'],
                                         admin=is_admin,
                                         company=company,
                                         title=body['title'],
                                         birth_date = body['birthDate'],
                                         type = body['type'],
                                         PTC = body['ptc'],
                                         passport = str(body['passport']),
                                         frequent_traveller_cards = "",
                                         verified=False)
            logging.info(user_data)
            if not user_data[0]:  # user_data is a tuple
                logging.warn('Unable to create user for email %s because of duplicate keys %s' % (username, user_data[1]))
                answer = {'success': False, 
                          'message': 'Unable to create user for email %s because of duplicate keys %s' % (username, user_data[1])}
                self.response.write(json.dumps(answer))
    
            user_id = user_data[1].get_id()
            verification_type = 'signup_angular'
            token = User.create_auth_token(user_id, verification_type)
            verification_url = self.uri_for('verification',
                                            verification_type=verification_type,
                                            user_id=user_id, token=token, _full=True)
    
            answer = {'success': True, 
                      'message': 'Email was sent to user in order to verify their address. \
                       You can login by clicking of this link:',
                       'url': verification_url}
            self.response.write(json.dumps(answer))
            
    
        except Exception:
            logging.exception("An error happened while registering user")
            answer = {'success': False, 
                      'message': 'Unable to create user'}
            self.response.write(json.dumps(answer))

class SignupHandler(BaseHandler):
    @user_required

    def get(self):
        return self.render_template('signup.html')

    def post(self):
        company = self.user_info['company'] # getting "company" field from current admin user.
        username = self.request.get('username')
        email = self.request.get('email')
        if 'y' in self.request.get('admin'):
            admin=True
        else:
            admin=False
        user_data = User.create_user(username,
                                     email=self.request.get('email'),
                                     password_raw=self.request.get('password'),
                                     name=self.request.get('name'),
                                     last_name=self.request.get('lastname'),
                                     admin=admin,
                                     company=company,
                                     title=self.request.get('title'),
                                     birth_date=self.request.get('birthdate'),
                                     type=self.request.get('type'),
                                     PTC=self.request.get('PTC'),
                                     passport=self.request.get('passport'),
                                     frequent_traveller_cards="",
                                     verified=False)
        if not user_data[0]:  # user_data is a tuple
            return self.display_message(
                'Unable to create user for email %s because of duplicate keys %s'
                % (username, user_data[1]))

        user_id = user_data[1].get_id()
        verification_type = 'signup'
        token = User.create_auth_token(user_id, verification_type)
        verification_url = self.uri_for('verification',
                                        verification_type=verification_type,
                                        user_id=user_id, token=token, _full=True)

        msg = 'An email is send to user in order to verify their address. \
          They will be able to do so by visiting <a href="{url}">{url}</a>'

        sender_address = (
            'OptimalTrip.com Support <{}@appspot.gserviceaccount.com>'.format(
                app_identity.get_application_id()))
        subject = 'Confirm your registration'
        body = """Thank you for creating an account!
        Please confirm your email address by clicking on the link below:
        {}
        """.format(verification_url)
        mail.send_mail(sender_address, email, subject, body)
        # [END send-confirm-email]

        return self.display_message(msg.format(url=verification_url))




class AdminSignupHandlerAngular(BaseHandler):

    def get(self):
        return self.render_template('adminsignup.html')

    def post(self):
        try:
            self.response.headers['Content-Type'] = 'application/json'
            body = json.loads(self.request.body)
            logging.info("Received body: %s" % body)
            username = body['username']
            user_data = User.create_user(username,
                                         email=body['email'],
                                         password_raw=body['password'],
                                         name=body['name'],
                                         last_name=body['lastName'],
                                         admin=True,
                                         company=body['company'],
                                         title=body['title'],
                                         birth_date = body['birthDate'],
                                         type = body['type'],
                                         PTC = body['ptc'],
                                         passport = str(body['passport']),
                                         frequent_traveller_cards = "",
                                         verified=False)
            logging.info(user_data)
            if not user_data[0]:  # user_data is a tuple
                logging.warn('Unable to create user for email %s because of duplicate keys %s' % (username, user_data[1]))
                answer = {'success': False, 
                          'message': 'Unable to create user for email %s because of duplicate keys %s' % (username, user_data[1])}
                self.response.write(json.dumps(answer))
    
            user_id = user_data[1].get_id()
            verification_type = 'signup_angular'
            token = User.create_auth_token(user_id, verification_type)
            verification_url = self.uri_for('verification',
                                            verification_type=verification_type,
                                            user_id=user_id, token=token, _full=True)
    
            answer = {'success': True, 
                      'message': 'Email was sent to user in order to verify their address. \
                      You can login by clicking of this link:',
                      'url': verification_url}
            self.response.write(json.dumps(answer))
            
    
        except Exception:
            logging.exception("An error happened while registering user")
            answer = {'success': False, 
                      'message': 'Unable to create user'}
            self.response.write(json.dumps(answer))
        



class AdminSignupHandler(BaseHandler):

    def get(self):
        return self.render_template('adminsignup.html')

    def post(self):
        username = self.request.get('username')
        user_data = User.create_user(username,
                                     email=self.request.get('email'),
                                     password_raw=self.request.get('password'),
                                     name=self.request.get('name'),
                                     last_name=self.request.get('lastname'),
                                     admin=True,
                                     company=self.request.get('company'),
                                     title=self.request.get('title'),
                                     birth_date = self.request.get('birthdate'),
                                     type = self.request.get('type'),
                                     PTC = self.request.get('PTC'),
                                     document_type="passport",
                                     document_number = self.request.get('document_number'),
                                     document_expiry_date = self.request.get('document_expiry_date'),
                                     document_issue_country = self.request.get('document_issue_country'),
                                     document_nationality = self.request.get('document_nationality'),
                                     seat_preference = self.request.get('seat_preference'),
                                     frequent_traveller_cards = "",
                                     verified=False)
        print user_data
        if not user_data[0]:  # user_data is a tuple
            return self.display_message(
                'Unable to create user for email %s because of duplicate keys %s'
                % (username, user_data[1]))

        user_id = user_data[1].get_id()
        verification_type = 'signup'
        token = User.create_auth_token(user_id, verification_type)
        verification_url = self.uri_for('verification',
                                        verification_type=verification_type,
                                        user_id=user_id, token=token, _full=True)

        msg = 'Send an email to user in order to verify their address. \
          They will be able to do so by visiting <a href="{url}">{url}</a>'

        return self.display_message(msg.format(url=verification_url))


class ListCompanyUsers(BaseHandler):
    def get(self):
        if self.user_info:
            users = User.get_users_by_company(self.user_info['company'])
            user_list = [{'username':user.auth_ids[0],
                          'name': user.name,
                          'last_name': user.last_name,
                          'id':user.get_id(),
                          'admin': user.admin,
                          } for user in users]
            # for user in users:
            #     print user
            #     u = {'name': user.name, 'last_name': user.last_name, 'id':user.key.id()}
            self.response.write(json.dumps(user_list, indent=2))
            #Edited by Liron - removed '<pre>%s</pre><br> %'  between '(' and 'json.dumps'
        else:
            self.response.write(json.dumps([], indent=2))

class GetUserByID(BaseHandler):
    def get(self,id):
        user = User.get_by_id(int(id))
        user_dict = {'id': int(id),
                     'PTC': user.PTC,
                     'birth_date': user.birth_date,
                     'email': user.email,
                     'frequent_traveller_cards': user.frequent_traveller_cards,
                     'last_name': user.last_name,
                     'name': user.name,
                     'passport': user.passport,
                     'password': user.password,
                     'title': user.title,
                     'type': user.type,
                     'username': user.auth_ids[0],
                     'admin': user.admin}
        self.response.headers['Content-Type'] = 'application/json'
        #self.response.write(json.dumps(user_dict))
        self.response.write(json.dumps(user_dict, indent=2))

class UpdateUser(BaseHandler):
        def post(self):
            # expecting the same json that was send with GET with different values
            body = json.loads(self.request.body)
            user = User.get_by_id(body['id'])
            user.PTC = body['PTC']
            user.birth_date = body['birth_date']
            user.email = body['email']
            user.frequent_traveller_cards = body['frequent_traveller_cards']
            user.last_name = body['last_name']
            user.name = body['name']
            user.passport = body['passport']
            user.password = body['password']
            user.title = body['title']
            user.type = body['type']
            user.auth_ids[0] = body['username']
            user.admin = body['admin']
            user.put()



class ForgotPasswordHandler(BaseHandler):

    def get(self):
        return self._serve_page()

    def post(self):
        username = self.request.get('username')
        user = User.get_by_auth_id(username)
        if not user:
            logging.info('Could not find any user entry for username %s', username)
            return self._serve_page(not_found=True)

        user_id = user.get_id()
        verification_type = 'reset'
        token = User.create_auth_token(user_id, verification_type)
        verification_url = self.uri_for('verification',
                                        verification_type=verification_type,
                                        user_id=user_id, token=token, _full=True)

        msg = 'Send an email to user in order to reset their password. \
          They will be able to do so by visiting <a href="{url}">{url}</a>'

        return self.display_message(msg.format(url=verification_url))

    def _serve_page(self, not_found=False):
        username = self.request.get('username')
        params = {
            'username': username,
            'not_found': not_found
        }
        return self.render_template('forgot.html', params)


class VerificationHandler(BaseHandler):

    def get(self, verification_type, user_id, token):
        user, ts = User.get_by_auth_token(int(user_id), token, verification_type)
        if not user:
            logging.info('Could not find any user with id "%s" token "%s"',
                         user_id, token)
            self.abort(404, 'This link has expired')

        # remove token, we don't want users to come back with an old link
        User.delete_auth_token(user.get_id(), token, verification_type)

        # store user data in the session
        auth_obj = auth.get_auth()
        # invalidate current session (if any) and set a new one
        auth_obj.unset_session()
        auth_obj.set_session(auth_obj.store.user_to_dict(user), remember=True)

        if verification_type == 'signup':
            if not user.verified:
                user.verified = True
                user.put()
            return self.display_message('User email address has been verified.')
        elif verification_type == 'signup_angular':
            if not user.verified:
                user.verified = True
                user.put()
            self.response.headers['Content-Type'] = 'application/json'
            self.response.write(json.dumps({'message': 'User email address has been verified.'}))
        elif verification_type == 'reset':
            return self.render_template('resetpassword.html')

        # return this comment after moving all functionality to client side
        #assert False, verification_type


class ResetPasswordHandler(BaseHandler):

    @user_required
    def post(self):
        password = self.request.get('password')
        if not password or password != self.request.get('confirm_password'):
            return self.display_message('passwords do not match')

        user = User.get_by_id(self.user_info['user_id'])
        user.password = security.generate_password_hash(password, length=12)
        user.put()

        return self.display_message('Password updated')


class LoginHandlerAngular(BaseHandler):
    def _serve_page(self, failed=False):
        try:
            body = json.loads(self.request.body)
            username = body['username']
        except Exception:
            username = self.request.get('username')
        params = {
            'username': username,
            'failed': failed,
        }
        return self.render_template('login.html', params)

    def get(self):
        return self._serve_page()

    def post(self):
        self.response.headers['Content-Type'] = 'application/json'
        logging.info("Got request: %s" % self.request)
        try:
            body = json.loads(self.request.body)
            username = body['username']
            password = body['password']
            auth.get_auth().get_user_by_password(username, password,
                                                 remember=True, save_session=True)
            self.response.write(json.dumps({'success': True, 'user_info': self.user_info}))


        except auth.InvalidAuthIdError:
            logging.error("Cannot login - user not found")
            self.response.write(json.dumps({'success': False, 'message': 'Error reading username or password'}))

        except auth.InvalidPasswordError:
            logging.error("Cannot login - wrong password")
            self.response.write(json.dumps({'success': False, 'message': 'Error reading username or password'}))

        except auth.AuthError as e:
            logging.error('Login failed for user %s because of %s', username, e.message)
            self.response.write(json.dumps({'success': False, 'message': 'Login failed for user %s because of %s' % (username, type(e))}))


class LoginHandler(BaseHandler):

    def get(self):
        return self._serve_page()

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        try:
            auth.get_auth().get_user_by_password(username, password,
                                                 remember=True, save_session=True)
            return self.redirect(self.uri_for('home'))
        except auth.AuthError as e:
            logging.info('Login failed for user %s because of %s', username, type(e))
            return self._serve_page(True)

    def _serve_page(self, failed=False):
        try:
            body = json.loads(self.request.body)
            username = body['username']
        except Exception:
            username = self.request.get('username')
        params = {
            'username': username,
            'failed': failed,
        }
        return self.render_template('login.html', params)


class LogoutHandler(BaseHandler):

    def get(self):
        auth.get_auth().unset_session()
        return self.redirect(self.uri_for('home'))


class AuthenticatedHandler(BaseHandler):

    @user_required
    def get(self):
        return self.render_template('authenticated.html')

