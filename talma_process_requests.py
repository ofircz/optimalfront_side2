#!/usr/bin/python
import json
import xmltodict
xml = open('response.xml').read()

results = xmltodict.parse(xml)
results = results['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:HotelGetAvailabilityRS']
print json.dumps(json.dumps(results))
#
import logging
import xml.etree.cElementTree as ET
# tree = ET.parse('response.xml')
# root = tree.getroot()

#
# bookings = []
# for query_results in results:  # results has answers from multiple queries
#     booking={}
#     Reservation = query_results[0][0][1]
#     ReservationID = Reservation.get("ReservationID")  # this code is required for booking
#     booking['Agency'] = Reservation[0].attrib
#     booking['Passengers'] = Reservation[1].attrib
#     booking['Services'] = {}
#     Service = Reservation[2]
#     booking['Services']['AirService'] = {}
#     AirService = Service[0]
#     booking['Services']['AirService']['Account'] = AirService[0].attrib
#     booking['Services']['AirService']['Price'] = AirService[1].attrib
#     booking['Services']['AirService']['ServicePeriod'] = AirService[2].attrib
#     booking['Services']['AirService']['Documents'] = AirService[3].attrib
#     booking['Services']['AirService']['FareDetails'] = AirService[4].attrib
#     FareDetails = AirService[4]
#     booking['Services']['AirService']['FareDetails']['PaxFare'] = FareDetails[0].attrib
#     PaxFare = FareDetails[0]
#     booking['Services']['AirService']['FareDetails']['PaxFare']['BaggageAllowance'] = []
#     for BaggageAllowance in PaxFare:
#         booking['Services']['AirService']['FareDetails']['PaxFare']['BaggageAllowance'].append(BaggageAllowance.attrib)
#     booking['Services']['AirService']['FareDetails']['TimeLimit'] = FareDetails[1].attrib
#     booking['Services']['AirService']['FareDetails']['ValidatingCarrier'] = FareDetails[2].attrib
#     booking['Services']['AirService']['Segments'] = AirService[5].attrib
#     booking['Services']['AirService']['PNRLines'] = AirService[6].attrib
#     booking['Services']['AirService']['OnlineCheckInUrl'] = AirService[7].text
#     bookings.append(booking)
#
# print  json.dumps(bookings)

results = []
# one-way query, only one leg per itinerary
itineraries = []
for query_results in results:  # results has answers from multiple queries
    PricedItineraries = query_results[0][0][1]
    ResultCode = PricedItineraries.attrib  # this code is required for booking
    for PricedItinerary in PricedItineraries:  # flights are grouped by price
        Price = PricedItinerary[0]
        print "processing priced group %s" % Price.get("Amount")
        FareDetails = PricedItinerary[1]
        AllRoutes = PricedItinerary.findall('{http://tbs.dcsplus.net/ws/1.0/}Routes')
        Itineraries = PricedItinerary.findall('{http://tbs.dcsplus.net/ws/1.0/}Itinerary')

        # get all legs in searchable data structure
        legs = {}
        for Routes in AllRoutes:
            Index = Routes.get("Index")
            legs[Index] = {}
            for Route in Routes:  # Routes
                Ref = Route.get('Ref')
                Duration = Route.get("Duration","N/A")
                leg = {'duration': Duration, 'segments': []}
                legs[Index][Ref] = leg
                for Segment in Route:  # Route
                    Origin = Segment[0]
                    O_Airport = Segment[0][0]
                    Destination = Segment[1]
                    D_Airport = Segment[1][0]
                    Carrier = Segment[2]
                    Flight = Segment[3]
                    Aircraft = Segment[4]

                    segment = {"origin": {
                        "date": str(Origin.get('Date')),
                        "time": str(Origin.get('Time')),
                        "terminal:": Origin.get('Terminal','N/A'),
                        "airport": {"name": O_Airport.text,
                                    "city": O_Airport.get('City'),
                                    "code": O_Airport.get('Code'),
                                    "city_code": O_Airport.get('CityCode'),
                                    "city_id": O_Airport.get('CityId')}
                    },
                    "destination": {
                        "date": str(Destination.get('Date')),
                        "time": str(Destination.get('Time')),
                        "terminal:": Destination.get('Terminal', 'N/A'),
                        "airport": {"name": D_Airport.text,
                                    "city": D_Airport.get('City'),
                                    "code": D_Airport.get('Code'),
                                    "city_code": D_Airport.get('CityCode'),
                                    "city_id": D_Airport.get('CityId')}
                    },
                    "flight": {
                        "duration": Flight.get("Duration","N/A"),
                        "meal": Flight.get("Meal","N/A"),
                        "number": Flight.get("Number"),
                        "class": Flight.get("Class"),
                        "cabin_type": Flight.get("CabinType"),
                        "stop_time": Flight.get("StopTime","00:00"),
                        "number_of_seats": Flight.get("NumberOfSeats"),
                        #"CodeShareInfo": tbs_segment.Flight._CodeShareInfo,
                    },
                    "aircraft": {"name": Aircraft.text,
                                 "code": Aircraft.get('Code')
                     },
                    "carrier": {"marketing": {"name": Carrier[0].text, "code": Carrier[0].get("Code")},
                                "operating": {"name": Carrier[1].text, "code": Carrier[1].get("Code")},
                                        },
            #                 "mileage": "N/A",
                    }
                    leg['segments'].append(segment)
        for Itinerary in Itineraries:
            itinerary = {'api': 'TBS',
                         'id': Itinerary.get("ItineraryCode"),
                         'resultCode': ResultCode,
                         'price': Price.get("Amount"),
                         'currency': Price.get("Currency"),
                         'fare_details': {"IsAutoTicketable": FareDetails.get("IsAutoTicketable"),
                                          "Currency": FareDetails.get("Currency"),
                                          "BaseFare": FareDetails.get("BaseFare"),
                                          "FullFare": FareDetails.get("FullFare"),
                                          "ServiceFee": FareDetails.get("ServiceFee"),
                                          "ValidatingCarrier": FareDetails[2].text if len(FareDetails)==3 else "N/A",
                                          "ValidatingCarrierCode": FareDetails[2].get("Code") if len(FareDetails)==3 else "N/A",
                                          "TimeLimit": FareDetails[1].text,
                                          "PaxFare": [{"count": pax.get("Count"),
                                                       "FullFare": pax.get("FullFare"),
                                                       "BaseFare": pax.get("BaseFare"),
                                                       "ServiceFee": pax.get("ServiceFee"),
                                                       "Currency": pax.get("Currency"),
                                                       "PTC": pax.get("PTC")} for pax in FareDetails[:-2]],
                                          },
                         'legs': [],
            }
            for tbs_leg in Itinerary:
                logging.debug("Leg: Index %s, Ref: %s" % (tbs_leg.get("Index"), tbs_leg.get("Ref")))
                print "Leg: Index %s, Ref: %s" % (tbs_leg.get("Index"), tbs_leg.get("Ref"))

                itinerary['legs'].append(legs[tbs_leg.get("Index")][tbs_leg.get("Ref")])
                itineraries.append(itinerary)
                #Itineraries(parent=ancestor_key, index=0, itinerary=json.dumps(itinerary)).put()
    import json
    print json.dumps(itineraries[0])
